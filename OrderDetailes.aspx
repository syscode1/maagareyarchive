﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="OrderDetailes.aspx.cs" Inherits="MaagareyArchive.OrderDetailes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }
        function btnConfirm_click(evt) {
            var div = document.getElementById('dvErrorMessage');
            if (document.getElementById('<%=ddlEmployee.ClientID%>').value != "0") {
                        div.innerText = "";
                        div.style.display = "none";
                        if (confirm("האם אתה בטוח?") == true) {
                            document.getElementById('<%=btnConfirm.ClientID%>').click();
                }
                else {
                    evt.childNodes["0"].checked = false;
                }

            }
            else {

                evt.childNodes["0"].checked = false;
                div.innerText = "יש לבחור עובד";
                div.style.display = "";
                return false;
            }
        }
        function btnPrint_click() {
            debugger
            var div = document.getElementById('dvErrorMessage');
            if (document.getElementById('<%=ddlEmployee.ClientID%>').value != "0") {
                div.innerText = "";
                div.style.display = "none";
                document.getElementById('<%=hfSelectedEmployee.ClientID%>').value = document.getElementById('<%=ddlEmployee.ClientID%>').value;
                return true;
            }
            else {
                div.innerText = "יש לבחור עובד";
                div.style.display = "";
                return false;
            }
        }

        var checkArrayLeft = [];
        var checkArrayTop = [];
        var checkArrayRight = [];
        var checkArrayBottom = [];
        $(document).ready(function () {
            isStop = false;
            $('input:checkbox').each(function () {

                var thisOffset = $(this).offset();
                var thisWidth = $(this).width();
                var thisHeight = $(this).height();

                checkArrayRight.push(thisOffset.left);
                checkArrayBottom.push(thisOffset.top);
                checkArrayTop.push(thisOffset.top + thisHeight);
                checkArrayLeft.push(thisOffset.left + thisWidth);
            });

        });

        var exEnabled = true;
        var moudow = false;

        $(document).on('mousedown', function () {
            moudow = true;
        });

        $(document).on('mouseup', function () {
            moudow = false;
        });

        var dragging = false;
        var shifting = false;
        var mx = 0;
        var my = 0;

        $(document).keyup(function (e) {

            if (e.keyCode == 16) {
                dragging = false;
                shifting = false;
                defaulted = true;
                $('.highlight-area').remove();
            }

        });

        $(document).on('mousedown', function (e) {

            mx = e.pageX;
            my = e.pageY;
            $('body').prepend('<div class="highlight-area" style="position: absolute; background: rgba(21, 155, 255, 0.2); border: 1px solid rgba(21, 155, 255, 0.8);"></div>');

            if ($(e.target).closest('a#fileStatus').length > 0)
                $(e.target).closest('a#fileStatus')[0].click();
            else if ($(e.target).closest('a#shelfID').length > 0)
                $(e.target).closest('a#shelfID')[0].click();
            else
                startDrag(mx, my);
        });

        $(document).on('mouseup', function () {
            registerChecks();
        });

        $(document).on('mousemove', function (e) {

            if (dragging) {

                var moveX = e.pageX;
                var moveY = e.pageY;
                var xDif = moveX - mx;
                var yDif = moveY - my;

                if (xDif < 0) {
                    $('.highlight-area').offset({
                        left: moveX
                    });
                }

                if (yDif < 0) {
                    $('.highlight-area').offset({
                        top: moveY
                    });
                }

                $('.highlight-area').css({

                    width: Math.abs(xDif),
                    height: Math.abs(yDif)

                });

            }

        });

        function startDrag(x, y) {

            dragging = true;

            $('.highlight-area').offset({
                top: y,
                left: x
            });

        }

        function registerChecks() {

            dragging = false;

            var areaWidth = $('.highlight-area').width();
            var areaHeight = $('.highlight-area').height();
            var hiRangeLeft = $('.highlight-area').offset().left;
            var hiRangeRight = $('.highlight-area').offset().left + areaWidth;
            var hiRangeTop = $('.highlight-area').offset().top;
            var hiRangeBottom = $('.highlight-area').offset().top + areaHeight;

            for (var i = 0; i < checkArrayLeft.length; i++) {

                if (checkArrayRight[i] <= hiRangeRight && checkArrayLeft[i] >= hiRangeLeft && checkArrayBottom[i] <= hiRangeBottom && checkArrayTop[i] >= hiRangeTop) {
                    $('body input[type="checkbox"]').eq(i).click();
                }

            };
            $('.highlight-area').remove();

        }

        function Print() {
            debugger


            var printWin = window.open('', '', 'left=0", ",top=0,width=1000,height=600,status=0');
            var checkboxes = $(".cbCheck:checked");
            var row = checkboxes.closest("tr").clone(true);

            //$("#<%=dgResults.ClientID%> tr.header td.input").remove();
            $("#<%=dgResults.ClientID%> td.input").remove();
            $("td.input", row).remove();
            var table = $("[id*=dgResults]").clone(true);
            table["0"].dir = "rtl";
            $("tr", table).not($("tr:first-child", table)).remove();
            $("td.description", table).width = "100%"
            table.append(row);
            // $("input", table).closest("th").toggle();

            //$("input", table).closest("td").toggle();


            var dv = $("<div />");
            var ddlWorker = document.getElementById('<%=ddlEmployee.ClientID%>');





            dv.append("<span style='float:right' > עובד: " + setSelectedValue(ddlWorker, document.getElementById('<%=hfSelectedEmployee.ClientID%>').value) + "</span>");
            dv.append(table);
            $("input", dv).not($("input", table)).remove();
            printWin.document.write(dv.html());


            var css = '@page { size: landscape; }',
head = printWin.document.head || document.getElementsByTagName('head')[0],
style = printWin.document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(printWin.document.createTextNode(css));
            }

            head.appendChild(style);

            printWin.document.close();
            printWin.focus();
            printWin.print();
            printWin.close();
            document.getElementById('<%=btnReload.ClientID%>').click();

        }

        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].value == valueToSet) {
                    return selectObj.options[i].text;
                    ;
                }
            }
        }

        function popup(mylink, windowname, paramName, ID, w, h) {

            if (!window.focus)
                return true;
            var href;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            if (typeof (mylink) == 'string')
                href = mylink;
            else href = mylink.href;
            window.open(href + "?" + paramName + "=" + ID, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }

        var soundObject = null;
        var isStop;
        var interval = null;

        function blink(timeOut) {
            var el = $('#blinkDiv');
            interval = window.setInterval(function () {
                el.toggleClass('blinking');
            }, 500);
            setTimeout(function () { $('#blinkDiv')[0].style.visibility = "hidden"; }, timeOut);
        }

        function playSound(src) {
            debugger
            setTimeout("stop()", 2000);
            play(src);
            blink(1500);
        }

        function stop() {
            debugger
            if (soundObject != null) {
                document.body.removeChild(soundObject);
                soundObject.removed = true;
                soundObject = null;
                isStop = true;
                clearInterval(interval);
                $('#blinkDiv')[0].style.visibility = "hidden";
            }

        }
        function play(src) {
            debugger
            if (soundObject != null) {
                document.body.removeChild(soundObject);
                soundObject.removed = true;
                soundObject = null;
            }
            if (isStop == false) {
                setTimeout("play('" + src + "')", 0);
            }
            else
                isStop = false;

            soundObject = document.createElement("embed");
            soundObject.setAttribute("src", src);

            soundObject.setAttribute("hidden", true);
            soundObject.setAttribute("autostart", true);
            soundObject.setAttribute("loop", true);
            document.body.appendChild(soundObject);

        }

        function inputFile(evt) {

            var dvError = document.getElementById('<%= dvScanErrorMessage.ClientID%>');
            if (evt.currentTarget.value.length > 13 || evt.currentTarget.value.length < 13) {
                {
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    return false;
                }

            }
            if (evt.currentTarget.value.length == 13) {
                if (evt.currentTarget.value.indexOf("261") != 0 &&
                    evt.currentTarget.value.indexOf("262") != 0) {
                    blink()
                    dvError.style.display = "";
                    dvError.innerText = "מספר לא תקין";
                    evt.currentTarget.value = "";
                    return false;
                }
                else {
                    stop();
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    document.getElementById('<%=btnInputFileBoxID.ClientID %>').click();
                    return true;
                }

            }
        }

        function getShelfHistoryTable(boxID) {
            jQuery.ajax({
                url: 'OrderDetailes.aspx/getShelfHistory',
                type: "POST",
                data: "{boxID : '" + boxID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (data) {
                    debugger
                    if (data.d != null) {
                        buildShelfHistoryTable(data.d);

                    }
                },
                failure: function (msg) { }
            });
        }
        function buildShelfHistoryTable(data) {

            var obj = JSON.parse(data);
            var globalCounter = 0;
            tbody = document.getElementById('tbodyShelfsHistory');
            tbody.innerHTML = "";
            for (var i = 0; i < obj.length; i++) {
                var tr = "<tr>";
                /* Must not forget the $ sign */
                tr += "<td>" + new Date(obj[i].Date).format('MM/dd/yyyy') + "</td><td>" + obj[i].ShelfID + "</td>" + "</tr>";

                /* We add the table row to the table body */
                tbody.innerHTML += tr;
            }

        }
        function updateShelf(a, boxID) {

            var newShelf;
            $('#txtNewShelf')[0].value = "";
            $('#dvMessage').get(0).innerText = "";
            $('#txtNewShelf')[0].focus();
            getShelfHistoryTable(boxID);
            var text = $("#dialog").dialog(
                {
                    buttons: {

                        'OK': function () {
                            debugger
                            var x = confirm('האם אתה בטוח?');
                            if (x == true) {
                                $('#dvMessage').get(0).innerText = '';
                                newShelf = $('#txtNewShelf').get(0).value;
                                $('#txtNewShelf').get(0).focus();
                                jQuery.ajax({
                                    url: 'OrderDetailes.aspx/updateShelfID',
                                    type: "POST",
                                    data: "{boxID : '" + boxID + "',newShelfID: '" + newShelf + "'}",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    beforeSend: function () {
                                    },
                                    success: function (data) {
                                        debugger
                                        if (data.d == "0") {
                                            $('#dvMessage').get(0).innerText = 'שגיאה בשמירת נתונים';
                                        }
                                        else {
                                            $('#spnShelfID', a)[0].innerText = newShelf;
                                            $("#dialog").dialog("destroy");

                                        }
                                    },
                                    failure: function (msg) { }
                                });

                            }


                        },
                        'Cancel': function () {
                            $(this).dialog("destroy");
                            // I'm sorry, I changed my mind                 
                        }
                    }

                });
            $('#txtNewShelf')[0].focus();
        }
    </script>

    <h1 id="header" runat="server">פרטי הזמנה 
    </h1>
    <div id="dialog" title="הכנס מיקום" style="display: none">
        <input id="txtNewShelf" size="25" />
        <div id="dvMessage" style="color: red"></div>
        <div id="dvShelfsHistory">
            <div class="h1_sub" style="width: 216px; font-size: 16px; padding-top: 5px; margin-bottom: 8px;">הסטורית מיקומים</div>
            <table style="width: 239px;" border="1" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>תאריך</th>
                        <th>מדף</th>
                    </tr>
                </thead>
                <tbody id="tbodyShelfsHistory"></tbody>
            </table>
        </div>
    </div>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <div id="dvWorker" style="float: right; direction: rtl; padding-top: 18px; padding-bottom: 18px;">
                            <span>עובד</span>
                            <asp:HiddenField ID="hfSelectedEmployee" runat="server" Value="0" />
                            <asp:DropDownList Width="170px" EnableViewState="false" ID="ddlEmployee" runat="server">
                            </asp:DropDownList>
                        </div>
                    </td>
                    <td>

                        <div id="blinkDiv" name="blinkDiv">
                            <asp:Image ImageUrl="resources/images/light.png" Style="width: 25px; float: right" runat="server" />
                        </div>
                        <asp:HiddenField ID="hfSoundPath" Value="" runat="server" />
                        <div style="height: 71px; border-style: solid; border-width: 4px; border-color: black; width: 300px; float: left">
                            <div class="h1_sub" style="font-weight: bold; font-size: 15px; margin-bottom: 2px; width: 92%">
                                סריקת תיקים שנשלפו
                            </div>
                            <span>סרוק תיק או ארגז</span>

                            <input type="text" id="txtFileBoxID" oninput="return inputFile(event)" maxlength="13" onkeypress="return isNumberKey(event)" runat="server"
                                style="width: 150px" />
                            <asp:Button Style="display: none" ID="btnInputFileBoxID" runat="server" UseSubmitBehavior="true" OnClick="btnInputFileBoxID_Click" />

                            <div id="dvScanErrorMessage" runat="server"></div>
                        </div>
                    </td>
                </tr>
            </table>

            <div>
                <asp:Button Style="display: none" ID="btnReload" OnClick="btnReload_Click" runat="server" />
                <asp:DataGrid ID="dgResults" runat="server" AutoGenerateColumns="false" Width="100%" EnableViewState="true" OnItemDataBound="dgResults_ItemDataBound">

                    <HeaderStyle CssClass="header" />

                    <AlternatingItemStyle CssClass="alt" />
                    <ItemStyle CssClass="row" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="בחר" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="input" />
                            <ItemStyle CssClass="input" />
                            <ItemTemplate>
                                <input id="cbCheck" runat="server" checked="checked" class="cbCheck" type="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="לקוח" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "CustomerID")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="מספר ארגז לקוח" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "CustomerBoxNum")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ארגז" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span style="display: none" id="spnFileID" runat="server"><%#DataBinder.Eval(Container.DataItem, "FileID")%></span>
                                <span style="display: none" id="spnBoxID" runat="server"><%#DataBinder.Eval(Container.DataItem, "BoxID")%></span>
                                <span style="display: none" id="spnCustomerID" runat="server"><%#DataBinder.Eval(Container.DataItem, "CustomerID")%></span>
                                <span style="display: none" id="spnDepartmentID" runat="server"><%#DataBinder.Eval(Container.DataItem, "DepartmentID")%></span>
                                <span style="display: none" id="spnOrderID" runat="server"><%#DataBinder.Eval(Container.DataItem, "OrderID")%></span>
                                <span style="display: none" id="spnNumInOrder" runat="server"><%#DataBinder.Eval(Container.DataItem, "NumInOrder")%></span>
                                <%#DataBinder.Eval(Container.DataItem, "BoxID")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="תיק" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <a id="fileStatus" href="FileStatusHistory.aspx" style="text-decoration: none;" onclick="return popup(this, 'notes','fileID','<%#(DataBinder.Eval(Container.DataItem, "FileID"))%>',500,600)">
                                    <span><%#DataBinder.Eval(Container.DataItem, "FileID")%></span>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="תאור" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="description" Width="100%" />
                            <ItemStyle CssClass="description" Width="100%" />
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description")%></ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="מס תיק לקוח" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle />
                            <ItemStyle />
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "CustomFileNum")%></ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="משנה" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle />
                            <ItemStyle />
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "StartYear")%></ItemTemplate>
                        </asp:TemplateColumn>


                        <asp:TemplateColumn HeaderText="מיקום" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <a id="shelfID" href="#" style="text-decoration: none;" onclick="return updateShelf(this,'<%#(DataBinder.Eval(Container.DataItem, "BoxID"))%>')">
                                    <span id="spnShelfID">
                                        <%#DataBinder.Eval(Container.DataItem, "ShelfID")%>
                                    </span>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="פקס/מייל" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#(Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "OrderFax") )==true?"פקס":"")+
                                             (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "OrderFax") )==true && Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "OrderEmail") )==true?",":"") +
                                             (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "OrderEmail") )==true?"מייל":"")     %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="הערות" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "FaxEmailComment")%></ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ת.ז." ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Identity")%></ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="מטפל" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="input" />
                            <ItemStyle CssClass="input" />
                            <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "EmployName")%></ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="לא אותר" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="input" />
                            <ItemStyle CssClass="input" />
                            <ItemTemplate>
                                <asp:CheckBox ID="cbNotExist" runat="server" onchange="return btnConfirm_click(this);" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="הפק מדבקה" ItemStyle-Width="100px" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="input" />
                            <ItemStyle CssClass="input" />
                            <ItemTemplate>
                                <asp:Button ID="btnPrintlable" runat="server" Height="18px" Width="111px" Style="margin: 5px" OnClick="btnPrintlable_Click"
                                    CommandName="printLable" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "FileID")+","+DataBinder.Eval(Container.DataItem, "BoxID")%>' />

                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="הסר" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="input" />
                            <ItemStyle CssClass="input" />
                            <ItemTemplate>
                                <input id="cbRemove" runat="server" class="cbCheck" type="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>



            </div>
            <div>
                <table style="margin: 0px; border-spacing: 0px; float: left; width: 100%">
                    <tr>
                        <td style="width: 240px; text-align: right;">
                            <input type="submit" class="btn" value="יציאה" onclick="exit()" runat="server" />
                        </td>
                        <td style="width: 508px;">
                            <asp:Button Text="הסר" ID="btnRemove" OnClick="btRemove_Click" Style="float: left;" Width="100px" runat="server" />
                        </td>
                        <td>
                            <asp:Button Text="אשר תיקים שלא אותרו" ID="btnConfirm" OnClick="btnConfirm_Click" Style="float: left; display: none" Width="200px" runat="server" />
                        </td>

                        <td>
                            <asp:Button Text="הפק מדבקות" ID="btnPrintLables" Style="float: left; margin: 10px" Width="105px" runat="server" />

                        </td>
                        <td style="width: 100px;">
                            <asp:Button Text="הדפס" ID="btnPrint" OnClick="btnPrint_Click" OnClientClick="return btnPrint_click();" runat="server" Style="width: 100px;" />
                        </td>
                    </tr>
                </table>
                <div id="dvErrorMessage" style="display: none" class="dvErrorMessage">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
