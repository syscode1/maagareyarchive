﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="OrderFromSupplier.aspx.cs" Inherits="MaagareyArchive.OrderFromSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function printWin() {
            debugger
            // var e = document.createElement('div');
            // e.id = 'dvPrintStyle';
            // e.innerHTML = $('#dvPrintStyle').html();

            $("#dvPrintStyle")[0].style.display = "";
            //// var html=('<!DOCTYPE html>    <html xmlns="http://www.w3.org/1999/xhtml"><head >    <title></title></head><body style="font-family: arial,sans-serif; direction: rtl">    <form id="form1" >        <div style="height: 1000px; width: 750px; border: solid;">            <div style="padding-right: 48px; width: 670px">                <p style="font-size: 37px; margin-bottom: 0px; color: #339966; font-weight: bold;">מאגרי ארכיב וגניזה בע"מ</p>                <asp:Image ImageUrl="~/resources/images/Logo.png" Style="float: left; position: relative; top: -60px; left: 65px" runat="server" />                <p style="font-size: 21px; margin-top: 3px; color: #339966; font-weight: bold;">שרותי ארכיון</p>                                                <p style="font-size: 22px;">טופס הזמנת מוצר</p>                                <div style="width: 550px">                    <div style="float: right">                        <span>הזמנת רכש מס:</span>                        <span>3345577446</span>                                            </div>                    <div style="float: left">                        <span>תאריך:</span>                        <span>13/07/2012</span>                                            </div>                </div>                <div style="height:36px"></div>                <div style="width: 550px;padding-bottom:63px">                    <div style="float: right">                        <span>ספק:</span>                        <span>יוניטד טק.</span>                                            </div>                    <div style="float: left;">                        <span>טלפון:</span>                        <span>02-67123456</span>                        <span>פקס:</span>                        <span>02-67123456</span>                    </div>                                   </div>            </div>            <p style="padding-top:20px">** ספק, נא ציין מועד אספקה</p>            <div>            </div>            <div style="text-align:center;font-weight:bold;width:200px;padding-top:30px;">                <p>תודה מראש,</p> <p>מאגרי ארכיב וגניזה בע"מ</p>            </div>                <div style="text-align: center;width: 100%;position: absolute;right: -291px;bottom: -721px;">                <p>ת.ד. 33 בני עי"ש 608600  . טל : 08-9356037 / 08-9419401  .  פקס : 08-9349491</p>                <p > כתובת דואר אלקטרונית :       archive.ream@gmail.com  אתר אינטרנט : www.m-archive.co.il</p>            </div>        </div>    </form></body></html>');
            // window.frames["print_frame"].document.body.appendChild(e);//.innerHTML =$("#dvPrintStyle").html()+  document.getElementById("dvGridResults").innerHTML;
            // window.frames["print_frame"].window.focus();
            // window.frames["print_frame"].window.print();



            var prtContent = document.getElementById('dvGridResults');
            if ($('tr', prtContent).length > 18)
                for (i = 18; i < $('tr', prtContent).length; i += 18) {
                    $('tr', prtContent)[i].className = "breakMe";
                }
            var contents = $('#dvPrintStyle')[0].outerHTML.replace("insertTableContent", "<div style='width:100%;;margin-right:10mm;' >" + prtContent.outerHTML + "</div>");
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-10px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write('<html><head><title>DIV Contents</title>');
            frameDoc.document.write('</head><body style="border-style:solid;">');
            //Append the external CSS file.
            frameDoc.document.write('<link href="resources/css/master.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');

            var css = ' @page{size: auto;margin-top:2mm;margin-bottom: 5mm;margin-right:10mm;margin-left:10mm;  }' +
            '.breakMe{page-break-before: always;}' +
            '@media print { .grid{page-break-inside:auto}' +
            'thead { display: table-header-group; }' +
            'tfoot { display: table-footer-group; }}  ',
head = frameDoc.document.head || document.getElementsByTagName('head')[0],
style = frameDoc.document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(frameDoc.document.createTextNode(css));
            }
            head.appendChild(style);

            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);
        }





    </script>
    <h1>הזמנה מספק</h1>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
            <table style="padding-right: 20px">
                <tr>

                    <td style="vertical-align: bottom; margin: 0px; padding: 0px; padding-left: 10px;">

                        <table style="border-spacing: 0px;">
                            <tr>
                                <td style="padding-bottom: 15px; width: 120px">
                                    <asp:DropDownList ID="ddlSuppliers" AutoPostBack="true" OnSelectedIndexChanged="ddlSuppliers_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlItems" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td style="vertical-align: bottom; padding-left: 10px;">
                        <input type="text" runat="server" placeholder="כמות" id="txtAmount" onkeypress="return isNumberKey(event)" />
                    </td>
                    <td style="vertical-align: bottom; padding-left: 10px;">
                        <input type="text" runat="server" placeholder="הערה" id="txtRemark" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: bottom; padding-left: 10px;">
                        <input type="button" value="יציאה" style="width: 100%" onclick="exitArchive()" runat="server" />

                    </td>
                    <td></td>
                    <td style="vertical-align: bottom; padding-left: 10px;">
                        <asp:Button Text="הוסף להזמנה" Style="width: 100%" ID="btnAdd" OnClick="btnAdd_Click" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:RequiredFieldValidator ID="re2" class="dvErrorMessage" ControlToValidate="ddlSuppliers" runat="Server" InitialValue="0" ErrorMessage="נא לבחור ספק"></asp:RequiredFieldValidator>
            <br />
            <asp:RequiredFieldValidator ID="re1" class="dvErrorMessage" ControlToValidate="ddlItems" runat="Server" InitialValue="0" ErrorMessage="נא לבחור פריט"></asp:RequiredFieldValidator>

            <br />
            <asp:RequiredFieldValidator ID="re3" class="dvErrorMessage" ControlToValidate="txtAmount" runat="Server" ErrorMessage="יש להזין כמות"></asp:RequiredFieldValidator>



            <table id="tblResults" runat="server" style="display: none">
                <tr>
                    <td>
                        <div id="dvDetailes" style="direction: rtl">
                            <table style="display: inline">
                                <tr>
                                    <td style="padding-left: 310px;">
                                        <label style="font-size: 18px; font-weight: bold;">ספק:</label>
                                        <label style="font-size: 18px;" id="lblSupplier" runat="server"></label>

                                    </td>
                                    <td>
                                        <label style="font-size: 18px; font-weight: bold;">הזמנה מספר:</label>
                                        <label style="font-size: 18px;" id="lblOrderNum" runat="server"></label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>

                    <td>

                        <div id="dvGridResults" style="direction: rtl">

                            <asp:GridView ID="dgResults" runat="server" Width="650px"
                                AutoGenerateColumns="false" CssClass="grid"
                                EnableViewState="true">
                                <%--DataKeyField="FileID"--%>

                                <HeaderStyle CssClass="header" />

                                <AlternatingRowStyle CssClass="alt" />
                                <RowStyle CssClass="row" />
                                <Columns>
                                    <asp:TemplateField HeaderText="מס'" ItemStyle-CssClass="ltr">
                                        <ItemTemplate>
                                            <%# rownum++  %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="פריט" ItemStyle-CssClass="ltr">
                                        <ItemTemplate>
                                            <%#(DataBinder.Eval(Container.DataItem, "ItemName")).ToString()%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="כמות" ItemStyle-CssClass="ltr">
                                        <ItemTemplate>
                                            <%#(DataBinder.Eval(Container.DataItem, "Amount")).ToString()%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="הערות" ItemStyle-CssClass="ltr">
                                        <ItemTemplate>
                                            <%#(DataBinder.Eval(Container.DataItem, "Remark")).ToString()%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" class="dvSucceedMessage">
                            <span id="dvErrorMessage" runat="server" class="dvSucceedMessage"></span>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table style="margin: 0px; border-spacing: 0px; float: left;">
        <tr>
            <td style="width: 620px; text-align: right;"></td>
            <td>
                <asp:Button Text="הדפס" ID="btnPrint" OnClick="btnPrint_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />
                <asp:Button Text="שמור בשם" ID="btnSaveToFile" OnClick="btnSaveToFile_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />
                <asp:Button Text="שלח במייל" ID="btnSendMail" OnClick="btnSendMail_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />
            </td>
        </tr>
    </table>
    <div id="dvPrintStyle" style="display: none; font-family: arial,sans-serif; direction: rtl">
        <asp:Image ID="imgLogo" ImageUrl="~/resources/images/Logo.png" runat="server" />

        <div style="height: 99%; width: 99%; border: solid; border-width: 1px;">

            <div style="padding-right: 48px; width: 770px; float: right; font-family: arial,sans-serif; direction: rtl" id="dvHeader" runat="server">
                 <div style="float: left; position: absolute; margin-top: 20px; top:25px; left: 70px; vertical-align: top" runat='server'>
                    logo
                </div>
                <p style="font-size: 37px; margin-bottom: 0px; color: #339966; font-weight: bold;">מאגרי ארכיב וגניזה בע"מ</p>
               
                <p style="font-size: 21px; margin-top: 3px; color: #339966; font-weight: bold;">שרותי ארכיון</p>


                <p style="font-size: 22px;">טופס הזמנת מוצר</p>

                <div style="width: 550px">
                    <div style="float: right">
                        <span>הזמנת רכש מס':</span>
                        <span>spnOrderNum</span>

                    </div>
                    <div style="float: left">
                        <span>תאריך:</span>
                        <span>spnOrderDate</span>

                    </div>

                </div>
                <div style="height: 36px"></div>
                <div style="width: 550px; padding-bottom: 63px">
                    <div style="float: right">
                        <span>ספק:</span>
                        <span>spnSupplier</span>

                    </div>
                    <div style="float: left;">
                        <span>טלפון:</span>
                        <span>spnPhone</span>
                        <span>פקס:</span>
                        <span>spnFax</span>
                    </div>

                </div>
            </div>



            insertTableContent
     
       <div id="dvFooter" runat="server" style="direction: rtl">
           <p style="padding-top: 20px">** ספק, נא ציין מועד אספקה</p>
           <div>
           </div>
           <div style="text-align: center; font-weight: bold; width: 200px; padding-top: 30px;">
               <p>תודה מראש,</p>
               <p>מאגרי ארכיב וגניזה בע"מ</p>
           </div>

           <div id="dvFooterButtom" runat="server" style="text-align: center; position: absolute; width: 100%; right: 0px; bottom: 0px;">
               <p>
                   ת.ד. 33 בני עי"ש 608600  . טל : 08-9356037 / 08-9419401  .  פקס : 08-9349491
               </p>
               <p>
                   כתובת דואר אלקטרונית :       archive.ream@gmail.com  אתר אינטרנט : www.m-archive.co.il
               </p>
           </div>

       </div>

        </div>
    </div>
    <iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank" style="direction: rtl"></iframe>
    <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
