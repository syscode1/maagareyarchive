﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="InputBoxes.aspx.cs" Inherits="MaagareyArchive.InputBoxes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function inputFile() {
            var dvError = document.getElementById('<%= dvErrorMessage.ClientID%>');
            var boxNum = document.getElementById('<%= txtBoxNum.ClientID%>');
            if (boxNum.value.length > 13 || boxNum.value.length < 13) {
                dvError.style.display = "";
                dvError.innerText = "יש להזין מספר בן 13 ספרות";
                boxNum.value = "";
                boxNum.focus();
                return false;
            }
            if (boxNum.value.length == 13) {
                if (boxNum.value.indexOf("261") != 0) {
                    dvError.style.display = "";
                    dvError.innerText = "מספר ארגז לא תקין";
                    boxNum.value = "";
                    boxNum.focus();
                    return false;
                }
                else {
                    dvError.style.display = "none";
                    dvError.innerText = "";
                }

            }

            if (document.getElementById('<%=ddlCustomers.ClientID%>').value != "0") {
                dvError.innerText = "";
                dvError.style.display = "none";
                return true;
            }
            else {
                dvError.innerText = "יש לבחור עובד";
                dvError.style.display = "";
                return false;
            }
           
            return false;
        }
        function tab(evt) {
            if (evt.currentTarget.value.length == 13)
                document.getElementById("<%=txtCustomerBoxNum.ClientID%>").focus();
        }
        function cbAllWarehouseChecked(event) {
            var allCheck = "input:checkbox[id*='cblWarehouse']";
            $(allCheck).prop("checked", event.checked);
        }
        function cbAllHeightChecked(event) {
            var allCheck = "input:checkbox[id*='cblHeight']";
            $(allCheck).prop("checked", event.checked);
        }
        function ddlPlaceCustomer_change(event) {
            var allCheck = "input:checkbox[id*='cbPlace']";
            $(allCheck).prop("checked", false);
            var customerGroup = "input:checkbox[customer='" + event.value + "']";
            $(customerGroup).prop("checked", true);

        }

               function PrintWindow() {
                   window.frames["print_frame"].document.body.innerHTML = document.getElementById("<%=dvPlaces.ClientID%>").innerHTML;
                       window.frames["print_frame"].window.focus();
                       window.frames["print_frame"].window.print();
               }
        
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }
    </script>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
            <h1>
                קליטת ארגזים
            </h1>
            <div>
                <table style="width:500px">
                    <tr>
                        <td  >
                            <span>לקוח</span>
                            <asp:DropDownList ID="ddlCustomers" AutoPostBack="true" Width="170px" OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged" runat="server">
                            </asp:DropDownList>
                        </td>
       <td  >
                            <span>קוד לקוח</span>
                            <asp:DropDownList ID="ddlCustomersID" AutoPostBack="true" Width="90px" OnSelectedIndexChanged="ddlCustomersID_SelectedIndexChanged" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <span>מחלקה</span>
                            <asp:DropDownList ID="ddlDepartment" runat="server">
                            </asp:DropDownList>
                        </td>
                    
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span>מספר ארגז
                            </span>
                            <input type="text" id="txtBoxNum" name="name" runat="server" oninput="tab(event)" maxlength="13" onkeypress="return isNumberKey(event)" />
                        </td>

                        <td>
                            <span>מספר ארגז לקוח
                            </span>
                            <input type="text" id="txtCustomerBoxNum" name="name" runat="server"  maxlength="9" onkeypress="return isNumberKey(event)"  />

                        </td>
                        </tr>
                    <tr>
                            <td>
                            <span >טקסט לארגז</span>
                            <input type="text" runat="server" id="txtBoxDesc" />
                        </td>
                        <td>
                            <span >שנת ביעור</span>
                            <input type="text" runat="server" id="txtExtermYear" maxlength="4" style="width:90px" onkeypress="return isNumberKey(event)" />
                        </td>
                        <td>
                            <asp:Button Text="שמור" ID="btnSave" OnClientClick="return inputFile();" OnClick="btnSave_Click" Style="margin-top: 17px" runat="server" />
                        </td>
                          <td>
                            <asp:Button Text="עבור לרישום תוכן   ->" ID="btnInputData" OnClientClick="  window.location.href = 'inputData.aspx'"  Style="margin-top: 17px" runat="server" />
                        </td>
                    </tr>

                </table>
                <br />
                <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display: none"></span>
                <span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display: none"></span>

            </div>
            <div>
                <asp:DataGrid ID="dgResults" runat="server" AutoGenerateColumns="false" Width="100%" EnableViewState="false">

                    <HeaderStyle CssClass="header" />

                    <AlternatingItemStyle CssClass="alt" />
                    <ItemStyle CssClass="row" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="תאריך" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "InsertDate")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="לקוח" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "CustomerName")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="מחלקה" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "DepartmentID")!=null && DataBinder.Eval(Container.DataItem, "DepartmentID").ToString()=="0"?"":DataBinder.Eval(Container.DataItem, "DepartmentID")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ארגז" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnBoxID" runat="server"> <%#DataBinder.Eval(Container.DataItem, "BoxID")%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="מספר ארגז לקוח" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "CustomerBoxNum")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="הקלדה" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "InsertData").ToString()=="0"?"ללא הקלדה":(DataBinder.Eval(Container.DataItem, "InsertData").ToString()=="1"?"נדרש הקלדה":"הוקלד")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="טקסט לארגז" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "BoxDescription")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="שנת ביעור" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "ExterminateYear")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="הקצה מיקום" ItemStyle-CssClass="ltr" SortExpression="Description1">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <input type="checkbox" id="cbPlace" runat="server" customer='<%# DataBinder.Eval(Container.DataItem, "CustomerID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>

            </div>
            <table style="margin-top: 24px; width: 982px">
                <tr>
                    <td style="width: 535px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exit()" runat="server" />
                    </td>
                    <td style="float:left">
                        <asp:DropDownList ID="ddlPlaceCustomer" onchange="ddlPlaceCustomer_change(this)" runat="server" Width="170px">
                        </asp:DropDownList>

                    </td>

                </tr>
<%--                <tr>
                    <td></td>
                    <td style="float:left">
                        <table>
                            <tr>
                                <td><span>מחסן:</span></td>
                                <td>
                                    <asp:CheckBox Text="הכל" Checked="true" ID="cbAllWarehouse" onclick="cbAllWarehouseChecked(this)" runat="server" /></td>
                                <td>
                                    <asp:CheckBoxList ID="cblWarehouse" RepeatDirection="Horizontal" runat="server">
                                    </asp:CheckBoxList></td>
                            </tr>
                        </table>



                    </td>

                </tr>
                <tr>
                    <td></td>
                    <td style="float:left">

                        <table>
                            <tr>
                                <td><span>גובה:</span></td>
                                <td>
                                    <asp:CheckBox Text="הכל" Checked="true" ID="cbAllHeight" onclick="cbAllHeightChecked(this)" runat="server" /></td>
                                <td>
                                    <asp:CheckBoxList ID="cblHeight" RepeatDirection="Horizontal" runat="server">
                                    </asp:CheckBoxList></td>
                            </tr>
                        </table>

                    </td>

                </tr>--%>
                <tr>
                    <td></td>
                    <td>
                            <table id="tblPlace" runat="server" cellpadding="0" cellspacing="0" style="float:left;height: 146px;width: 170px;">
        <tr>
            <td style="padding-left:15px">
                <span>ממחסן</span>
                <asp:DropDownList ID="ddlFromWarehouse" runat="server">
                </asp:DropDownList>

            </td>
            <td>
                <span>עד מחסן</span>
                <asp:DropDownList ID="ddlToWarehouse" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="padding-left:15px">
                <span>משורה</span>
              <asp:DropDownList ID="ddlFromRow" runat="server">
                </asp:DropDownList>

            </td>
            <td>
                <span>עד שורה</span>
                              <asp:DropDownList ID="ddlToRow" runat="server">
                </asp:DropDownList>

            </td>
        </tr>
                                </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnUpdate" Text="הקצה מיקום" style="width:170px; margin-top:11px" OnClick="btnUpdate_Click" runat="server" />
                    </td>
                </tr>
            </table>

            <div id="dvPlaces" runat="server" style="display:none">
                <h1 style="direction:rtl">תוצאות הקצאת מיקומים:</h1>
            </div>
            <div id="dvPlaceErrorMessage" runat="server" class="dvErrorMessage"> </div>
            
                <iframe name="print_frame" style="float:right;direction:rtl;text-align:right" width="0" height="0" frameborder="0" src="about:blank"></iframe>
        </ContentTemplate>
    </asp:UpdatePanel>
       <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
