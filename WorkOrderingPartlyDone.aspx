﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" EnableEventValidation="false" CodeBehind="WorkOrderingPartlyDone.aspx.cs" Inherits="MaagareyArchive.WorkOrderingPartlyDone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script>
        function btnUpdate_Click() {
            var div = document.getElementById('dvErrorMessage');
            if (document.getElementById('<%=ddlUser.ClientID%>').value == "0") {

                div.innerText = "יש לבחור שם מאשר";
                div.style.display = "";
                return false;
            }
            return true
        }

        function highlightText(event) {
            var color = ($("[id*='txtComment']", $(event).parents("tr"))[0]).style.backgroundColor;
            if (color == "#FFFF00" || color == "rgb(255, 255, 0)")
                ($("[id*='txtComment']", $(event).parents("tr"))[0]).style.backgroundColor = "";
            else
                ($("[id*='txtComment']", $(event).parents("tr"))[0]).style.backgroundColor = "#FFFF00";

            return false;
        }

        function closePopup() {
            window.opener.$("[id*='" + "cbPartlyDone_" + getParameterByName('rowIndex') + "']")[0].checked = false;
            window.close();
        }

        function getParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <div id="dvPrint" style="direction: rtl; text-align: center;">
                <h1 id="header" runat="server">סידור עבודה לנהג
                </h1>
                <h2 id="hDate" runat="server"></h2>
                <h2 id="hShippingID" runat="server"></h2>
                <h2 id="hDriverID" runat="server"></h2>
                <asp:HiddenField ID="hfIndex" Value="" runat="server" />
                <asp:GridView ID="dgResultsR" runat="server" CssClass="grid"
                    OnRowDataBound="dgResultsR_RowDataBound"
                    EnableViewState="true" Width="1038px"
                    AutoGenerateColumns="false"
                    SelectedRowStyle-BackColor="Red"
                    EnablePersistedSelection="true"
                    DataKeyNames="NumInShipping">
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="alt" />
                    <RowStyle CssClass="row" />
                    <Columns>
                        <asp:TemplateField HeaderText="לקוח" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnCustomerName" runat="server"><%#(Eval("CustomerName"))%> </span>
                                <span id="spnRowID" runat="server" style="display: none"><%#(Eval("rowID"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="מחלקה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnDepartmentName" runat="server"><%#(Eval("DepartmentName"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="עיר" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%# (Eval("CityAddress"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="כתובת" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "FullAddress"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="איש קשר" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "ContactMan"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="טלפון 1" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "Telephone1"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="טלפון 2" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "Telephone2"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="הערות" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <asp:TextBox ID="txtComment" runat="server" Enabled="false" TextMode="MultiLine" onkeypress="return maxCharLength(event,64)" Style="width: 200px; height: auto" Text='<%#(Eval("Comment"))%>' />
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ תיקים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumFiles"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumBoxes"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים ריקים" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnSumNewBoxOrder" runat="server"><%#(Eval( "SumNewBoxOrder"))%> </span>
                                <br />
                                <asp:TextBox ID="txtSumNewBoxOrder" runat="server" onkeypress="return isNumberKey(event,9)" Text='<%#(Eval( "SumNewBoxOrder"))%>'></asp:TextBox>
                                <br />
                                <asp:CheckBox ID="cbSumNewBoxOrder" runat="server" Text="ביטול יתרה" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים לפינוי" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnSumFullBoxes" runat="server"><%#(Eval( "SumFullBoxes"))%> </span>
                                <br />
                                <asp:TextBox ID="txtSumFullBoxes" runat="server" onkeypress="return isNumberKey(event,9)" Text='<%#(Eval( "SumFullBoxes"))%>'></asp:TextBox>
                                <br />
                                <asp:CheckBox ID="cbSumFullBoxes" runat="server" Text="ביטול יתרה" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים להחזרה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnSumRetriveBoxes" runat="server"><%#(Eval( "SumRetriveBoxes"))%> </span>
                                <br />
                                <asp:TextBox ID="txtSumRetriveBoxes" runat="server" onkeypress="return isNumberKey(event,9)" Text='<%#(Eval( "SumRetriveBoxes"))%>'></asp:TextBox>
                                <br />
                                <asp:CheckBox ID="cbSumRetriveBoxes" runat="server" Text="ביטול יתרה" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים לגריסה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnSumExterminateBoxes" runat="server"><%#(Eval( "SumExterminateBoxes"))%> </span>
                                <br />
                                <asp:TextBox ID="txtSumExterminateBoxes" runat="server" onkeypress="return isNumberKey(event,9)" Text='<%#(Eval( "SumExterminateBoxes"))%>'></asp:TextBox>
                                <br />
                                <asp:CheckBox ID="cbSumExterminateBoxes" runat="server" Text="ביטול יתרה" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <FooterStyle />
                </asp:GridView>
            </div>
<div>
    <span>שם מאשר</span>
               <asp:DropDownList ID="ddlUser" runat="server" Width="80px" AutoPostBack="true">
                        </asp:DropDownList>

</div>
            <table style="float: left; width: 100%">
                <tr>
                    <td colspan="2">
                                          <div id="dvErrorMessage" style="display:none" class="dvErrorMessage">
                    </td>
                </tr>
                <tr>
                    <td style="width: 662px; text-align: right;">
                        <input type="button" value="ביטול" onclick="closePopup()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnUpdate" Text="עדכן" runat="server" OnClientClick="return btnUpdate_Click()" OnClick="btnUpdate_Click" Style="margin-bottom: 10px; width: 130px" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
        <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
