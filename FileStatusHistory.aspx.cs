﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class FileStatusHistory : BasePage
    {
        public int rownum = 1;
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.anyUser }; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            string fileID=Request.QueryString["fileID"];
            if(!string.IsNullOrEmpty(fileID))
            {
                header.InnerText = " הסטורית תיק " + fileID;
            }
            if (!Page.IsPostBack)
            {
                dgResults.DataSource = FileStatusArchiveController.GetFileStatusArchive(fileID);
                dgResults.DataBind();
            }
        }
    }
}