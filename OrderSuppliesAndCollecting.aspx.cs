﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class OrderSuppliesAndCollecting : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<OrderSuppliedUI> Data
        {
            get
            {
                if (Session["OrderSuppliesAndCollecting_Data"] == null)
                    return null;
                return Session["OrderSuppliesAndCollecting_Data"] as List<OrderSuppliedUI>;
            }
            set
            {
                Session["OrderSuppliesAndCollecting_Data"] = value;
            }
        }

        internal List<Users> users
        {
            get
            {
                if (Session["users_OrderSuppliesAndCollecting"] == null)
                    Session["users_OrderSuppliesAndCollecting"] =new List<Users>();
                return Session["users_OrderSuppliesAndCollecting"] as List<Users>;
            }
            set
            {
                Session["users_OrderSuppliesAndCollecting"] = value;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {


                if (user.ArchiveWorker == true)
                {
                    dvData.Style["display"] = "none";
                    dvCustomer.Style["display"] ="" ;
                    fillCustomerDdl();
                }
                else {
                    dvData.Style["display"] = "";
                    dvCustomer.Style["display"] = "none"; 
                    Data = new List<OrderSuppliedUI> { new OrderSuppliedUI() { UserID = null, EmptyBoxes = null, ExterminateBoxes = null, FullBoxes = null, RetriveBoxes = null } };
                    dataBind(Data);

                }

                var listCache = new MyCache();
                    ddlCities.DataSource = listCache.Cities;
                    ddlCities.DataBind();
            }
            showHideErrorMessage(false);
        }

        private void dataBind(List<OrderSuppliedUI>  data)
        {
            dgResults.DataSource = Data;
            dgResults.DataBind();
            int customerID = user.ArchiveWorker == true ? Helper.getIntNotNull(ddlCustomer.SelectedValue) : user.CustomerID;
            dgSuppliersHistory.DataSource = OrdersController.getOrderByCust(customerID).Where(x=>string.IsNullOrEmpty( x.FileID) && string.IsNullOrEmpty( x.BoxID)).OrderByDescending(x=>x.Date);
            dgSuppliersHistory.DataBind();
        }
        private void fillCustomerDdl()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
        }
        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlPlaces = (e.Item.FindControl("ddlPlace") as DropDownList);

                List<ListItem> data = new List<ListItem>();
                    List<Department> departments;
                    int customerID =user.ArchiveWorker == true ?Convert.ToInt32(ddlCustomer.SelectedItem.Value):user.CustomerID;
                    if (user.ArchiveWorker == true)
                    {
                        departments = DepartmentController.GetCustomerDepartments(customerID);


                    }
                    else
                        departments = allowedDepartments;
                    users = UserController.getUsersListCustomers(customerID);
                    foreach (var item in users)
                    {
                        string text = String.Format("{0} {1} {2}", item.CityAddress, item.FullAddress, item.DescriptionAddress);
                        if (!string.IsNullOrEmpty(text.Trim()))
                            data.Add(new ListItem { Text = text, Value = item.UserID.ToString() });
                    };
                    Customers cust = CustomersController.getCustomerByID(customerID);
                    data.Add(new ListItem { Text = String.Format("{0} {1} {2}", cust.CityAddress, cust.FullAddress, cust.DescriptionAddress), Value = "-1" });
                    ddlPlaces.DataSource = data;
                ddlPlaces.DataTextField = "Text";
                ddlPlaces.DataValueField = "Value";
                ddlPlaces.DataBind();
                ddlPlaces.Items.Add(new ListItem(Consts.CONST_ELSE_COMBO_ITEM, "0"));
                ddlPlaces.SelectedValue = (e.Item.DataItem as OrderSuppliedUI).UserID == null  ? "-1" : (e.Item.DataItem as OrderSuppliedUI).UserID.ToString();

            }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }



        protected void btnAddRow_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
            bool isEmptyAddress = false;
            Data = new List<OrderSuppliedUI>();
            foreach (DataGridItem item in dgResults.Items)
            {
                if (Convert.ToInt16((item.FindControl("ddlPlace") as DropDownList).SelectedValue) == 0)
                    isEmptyAddress = true;
                Data.Add(new OrderSuppliedUI()
                {
                    UserID = Convert.ToInt16((item.FindControl("ddlPlace") as DropDownList).SelectedValue),
                    EmptyBoxes = !string.IsNullOrEmpty((item.FindControl("txtEmptyBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtEmptyBoxes") as TextBox).Text) : 0,
                    ExterminateBoxes = !string.IsNullOrEmpty((item.FindControl("txtNumExterminateBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtNumExterminateBoxes") as TextBox).Text) : 0,
                    FullBoxes = !string.IsNullOrEmpty((item.FindControl("txtFullBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtFullBoxes") as TextBox).Text) : 0,
                    RetriveBoxes = !string.IsNullOrEmpty((item.FindControl("txtNumRetriveBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtNumRetriveBoxes") as TextBox).Text) : 0
                });
            }
            if (!isEmptyAddress)
                Data.Add(new OrderSuppliedUI() { UserID = null, EmptyBoxes = null, ExterminateBoxes = null, FullBoxes = null, RetriveBoxes = null });
            else
                showHideErrorMessage(true, Consts.CONST_DUPLICATE_EMPTY_ADDRESS_ERROR_MESSAGE);
                dataBind(Data);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        protected void btnOrder_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validatePage())
                    showHideErrorMessage(true, Consts.CONST_REQUIERD_FIELDS_ERROR_MESSAGE);
                else
                {
                    List<Orders> orders = new List<Orders>();
                    int i = 1;
                    string orderID = Helper.getOrderID(user.UserID);
                    int numNewBoxOrder = 0;
                    int numExterminateBoxes = 0;
                    int numFullBoxes = 0;
                    int numRetriveBoxes = 0;
                    Customers cust = CustomersController.getCustomerByID(Helper.getIntNotNull(ddlCustomer.SelectedValue));

                    foreach (DataGridItem item in dgResults.Items)
                    {
                        numNewBoxOrder = !string.IsNullOrEmpty((item.FindControl("txtEmptyBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtEmptyBoxes") as TextBox).Text) : 0;
                        numExterminateBoxes = !string.IsNullOrEmpty((item.FindControl("txtNumExterminateBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtNumExterminateBoxes") as TextBox).Text) : 0;
                        numFullBoxes = !string.IsNullOrEmpty((item.FindControl("txtFullBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtFullBoxes") as TextBox).Text) : 0;
                        numRetriveBoxes = !string.IsNullOrEmpty((item.FindControl("txtNumRetriveBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtNumRetriveBoxes") as TextBox).Text) : 0;
                        if (numNewBoxOrder + numExterminateBoxes + numFullBoxes + numRetriveBoxes > 0)
                        {
                            int userID = Convert.ToInt16((item.FindControl("ddlPlace") as DropDownList).SelectedValue);
                            orders.Add(new Orders()
                            {
                                LoginID = user.UserID,
                                DepartmentID = 0,//userID == 0 || userID == -1 ? 0 : users.Where(d => d.UserID == userID).FirstOrDefault().DepartmentID,
                                CustomerID = user.ArchiveWorker == true ? cust.CustomerID : user.CustomerID,
                                NumNewBoxOrder = !string.IsNullOrEmpty((item.FindControl("txtEmptyBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtEmptyBoxes") as TextBox).Text) : 0,
                                NumExterminateBoxes = !string.IsNullOrEmpty((item.FindControl("txtNumExterminateBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtNumExterminateBoxes") as TextBox).Text) : 0,
                                NumFullBoxes = !string.IsNullOrEmpty((item.FindControl("txtFullBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtFullBoxes") as TextBox).Text) : 0,
                                NumRetriveBoxes = !string.IsNullOrEmpty((item.FindControl("txtNumRetriveBoxes") as TextBox).Text) ? Convert.ToInt16((item.FindControl("txtNumRetriveBoxes") as TextBox).Text) : 0,
                                Urgent = false,
                                Date = DateTime.Now,
                                OrderID = orderID,
                                NumInOrder = i++,
                                CityAddress = userID == 0 ? ddlCities.SelectedItem.Text : (userID == -1 ? cust.CityAddress : users.Where(d => d.UserID == userID).FirstOrDefault().CityAddress),
                                FullAddress = userID == 0 ? txtAddress.Value : (userID == -1 ? cust.FullAddress : users.Where(d => d.UserID == userID).FirstOrDefault().FullAddress),
                                DescriptionAddress = userID == 0 ? txtDescription.Value : (userID == -1 ? cust.DescriptionAddress : users.Where(d => d.UserID == userID).FirstOrDefault().DescriptionAddress),
                                ContactMan = userID == 0 ? txtContactName.Value : "",
                                Telephone1 = userID == 0 ? "" : (userID == -1 ? cust.Telephon1 : users.Where(d => d.UserID == userID).FirstOrDefault().Telephone1),
                                Telephone2 = userID == 0 ? "" : (userID == -1 ? cust.Telephon2 : users.Where(d => d.UserID == userID).FirstOrDefault().Telephone2),
                                Email = userID == 0 ? "" : (userID == -1 ? cust.Email : users.Where(d => d.UserID == userID).FirstOrDefault().Email),
                                Comment = userID == 0 ? txtComment.Value : "",
                            });
                        }
                    }
                    if (orders.Count > 0)
                    {
                        if (OrdersController.Insert(orders))
                            Response.Redirect("OrderConfirmation.aspx?orderID=" + orderID);
                        else
                        {
                            showHideErrorMessage(true, Consts.CONST_SAVE_ERROR_MESSAGE);
                        }
                    }
                }
            }
                       
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                showHideErrorMessage(true, Consts.CONST_SAVE_ERROR_MESSAGE);  
            }
        }



        private bool validatePage()
        {
            foreach (DataGridItem item in dgResults.Items)
            {

                if ((item.FindControl("ddlPlace") as DropDownList).SelectedValue == "0")
                    if (string.IsNullOrEmpty(txtAddress.Value) || string.IsNullOrEmpty(txtContactName.Value)
                        || string.IsNullOrEmpty(txtDescription.Value))//string.IsNullOrEmpty(txtPhone.Value))
                        return false;
            }
            return true;
        }

        private void showHideErrorMessage(bool toShow, string text = "")
        {
            if (toShow)
            {
                dvErrorMessage.InnerText = text;
                dvErrorMessage.Style["display"] = "block";
                dvErrorMessage.Style["visibility"] = "visible";
                dvErrorMessage.Style["background-color"] = "rgba(253, 157, 157, 0.16)";
            }
            else
            {
                dvErrorMessage.Style["display"] = "none";
                dvErrorMessage.Style["visibility"] = "hidden";
                dvErrorMessage.Style["background-color"] = "rgba(255, 255, 255, 0.16)";
            }

        }
       protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCustomer.SelectedValue != null && ddlCustomer.SelectedValue!="0")
            {
                dvData.Style["display"] = "";
                Data = new List<OrderSuppliedUI> { new OrderSuppliedUI() { UserID = null, EmptyBoxes = null, ExterminateBoxes = null, FullBoxes = null, RetriveBoxes = null } };
                dataBind(Data);
                
            }

        }


 
    }
}