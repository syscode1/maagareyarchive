﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerSubjects.aspx.cs" Inherits="MaagareyArchive.CustomerSubjects" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script> 
    </script>

    <h1 id="header" runat="server">תחזוקת נושאים ללקוח</h1>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
            <table style="width:480px">
                <tr>
                    <td>

                        <div style="clear: both">
                       
                            <div id="dvCustomerName" runat="server" style="float: right; margin-top: 3px; margin-right: 20px;">
                                <label>בחר לקוח</label>

                                <asp:DropDownList ID="ddlCustomerName" Width="130px" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                            <div id="dvCustomerID" style="float: right; margin-top: 3px; margin-right: 20px;" runat="server">
                                <label>מספר לקוח</label>
                                <asp:DropDownList ID="ddlCustomerID" Width="130px" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerID_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>


                    <asp:DataGrid ID="dgResults" runat="server" OnItemDataBound="dgResults_ItemDataBound"
                        AutoGenerateColumns="false" CssClass="grid" Width="400px" Style="margin-bottom: 30px">


                        <HeaderStyle CssClass="header" />

                        <AlternatingItemStyle CssClass="alt" />
                        <ItemStyle CssClass="row" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="מס'" ItemStyle-CssClass="ltr">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate><%#(DataBinder.Eval(Container.DataItem, "SubjectID"))==null || (DataBinder.Eval(Container.DataItem, "SubjectID")).ToString()=="0"?"":(DataBinder.Eval(Container.DataItem, "SubjectID")).ToString()%></ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="נושא" ItemStyle-CssClass="ltr">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <asp:TextBox runat="server"  ID="subjectName" runat="server" MaxLength="256"  Text='<%#(DataBinder.Eval(Container.DataItem, "SubjectName")).ToString()%>' /> 
                                    <span id="subjectID" style="display: none; visibility: hidden" runat="server"><%#(DataBinder.Eval(Container.DataItem, "SubjectID"))==null?"":(DataBinder.Eval(Container.DataItem, "SubjectID")).ToString()%></span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                    
                        </Columns>
                    </asp:DataGrid>
                    </td>
                    <td style="height:100%;vertical-align:bottom;padding-bottom: 30px;">
                        
                        <asp:ImageButton ID="btnAddRow" style="display:none;" OnClick="btnAddRow_Click" ImageUrl="resources/images/add.png" Height="35px" runat="server" />
                     <%--   <asp:ImageButton ImageUrl="resources/images/remove.png" Height="35px" runat="server" />--%>
                    </td>
                </tr>

            </table>
             <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 462px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnSave" Text="שמור"  OnClick="btnSave_Click" runat="server" />
                    </td>

                </tr>
                  <caption>
                       <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display:none"></span><span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display:none"></span>
                            <asp:RequiredFieldValidator ID="re2" class="dvErrorMessage" ControlToValidate="ddlCustomerName" runat="Server" InitialValue="0" ErrorMessage="נא לבחור לקוח"></asp:RequiredFieldValidator>

                       </caption>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

           <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
