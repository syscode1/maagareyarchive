﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ExportOrders : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (user.ArchiveWorker == true)
                {

                    trCustomers.Visible = true;
                    fillCustomerDdl();
                    string customerID = Request.QueryString["custID"];
                    if (!string.IsNullOrEmpty(customerID))
                    {
                        ddlCustomer.SelectedValue = customerID;
                        btnBackCustomer.Style["display"] = "";
                        btnShow_Click(null, null);
                    }
                }
                else
                {
                    trCustomers.Visible = false;
                }



                
            }
        }

        private void fillCustomerDdl()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
            int? department = null; //user.ArchiveWorker == true ? null: (int?)user.DepartmentID;
            DateTime? fromShippingDate = Helper.getDate(txtFromDate.Value);
            DateTime? toShgippingDate = Helper.getDate(txtToDate.Value);
            string orderNumber = txtOrderNumber.Text;
            dgResults.DataSource = OrdersController.exportGroupedOrders(customerID, department, fromShippingDate, toShgippingDate, orderNumber);
            dgResults.DataBind();
            btnPrint.Visible = true;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
            List<Orders> orders = getData();
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add(ExcelHeaders.ResourceManager.GetString("Customer"), ddlCustomer.SelectedItem != null ? ddlCustomer.SelectedItem.Text : user.CustomerID.ToString());
            param.Add(ExcelHeaders.ResourceManager.GetString("FromOrderDate"), txtFromDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("ToOrderDate"), txtToDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("OrderNum"), txtOrderNumber.Text);
            param.Add(ExcelHeaders.ResourceManager.GetString("NumOfOrders"), orders.Count.ToString());

            exportToExcell(orders, "export_orders_data", param);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }


        }

 

        private List<Orders> getData()
        {
            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID; ;
            int? department = null;//user.ArchiveWorker == true ? null : (int?)user.DepartmentID;
            DateTime? fromShippingDate = Helper.getDate(txtFromDate.Value);
            DateTime? toShgippingDate = Helper.getDate(txtToDate.Value);
            string orderNumber = txtOrderNumber.Text;
            return OrdersController.exportOrders(customerID, department, fromShippingDate, toShgippingDate, orderNumber);

        }

    }
}