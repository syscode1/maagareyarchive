﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DeliveryConfirmationReport.aspx.cs" Inherits="MaagareyArchive.DeliveryConfirmationReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function btnUpdate_Click() {
            debugger

            var div = document.getElementById('<%=dvErrorMessage.ClientID%>');
            var value = document.getElementById('<%=ddlReceivedBy.ClientID%>').value;
            var driver = document.getElementById('<%=spnDriver.ClientID%>').innerText.trim();
            if ($('#<%= hfPassword.ClientID%>')[0].value != "" && $('#<%= hfPassword.ClientID%>')[0].value != undefined) {
                $('#<%= hfPassword.ClientID%>')[0].value = "";
                $("#dialog").dialog("destroy");
                return true;
            }
            if (value == "0" ||
                value == "") {

                div.innerText = "יש לבחור שם מאשר";
                div.style.display = "";
                return false;
            }
            else {
                if (value == driver)
                    return true;
                else {
                    var password;
                    var text = $("#dialog").dialog(
                        {
                            buttons: {
                                'OK': function () {
                                    debugger
                                    $('#dvMessage').get(0).innerText = '';
                                    password = $('#txtPassword').get(0).value;

                                    jQuery.ajax({
                                        url: 'DeliveryConfirmationReport.aspx/checkPassword',
                                        type: "POST",
                                        data: "{userID : '" + value + "',password: '" + password + "'}",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        beforeSend: function () {
                                        },
                                        success: function (data) {
                                            debugger
                                            if (data.d == "0") {
                                                $('#dvMessage').get(0).innerText = 'סיסמא שגויה ,נסה שנית';
                                            }
                                            else {

                                                $('#<%= hfPassword.ClientID%>')[0].value = password;
                                                $('#<%= btnUpdate.ClientID%>').click();

                                            }
                                        },
                                        failure: function (msg) { }
                                    });

                                    return true;
                                },
                                'Cancel': function () {
                                    $(this).dialog("destroy");
                                    // I'm sorry, I changed my mind                 
                                }
                            }
                        });
                        return false;
                    }
                }

            }

        function printTable() {
            debugger
            //var prtContent = document.getElementById('dvGridResults');
            //prtContent.border = 1; //set no border here

            //var WinPrint = window.open('', '', 'left=0,top=100,width=1500,height=900,toolbar=0,scrollbars=1,status=0,resizable=1');
            //WinPrint.document.write("<html><body style='direction:rtl'><link href='resources/css/master.css' rel='stylesheet' type='text/css' />" + $('.dvMasterBackground').html() + prtContent.outerHTML + "</body></html>");
            //WinPrint.document.close();
            //WinPrint.focus();
            //WinPrint.print();
            //WinPrint.close();


var prtContent = document.getElementById('dvGridResults');
var contents = $('.dvMasterBackground')[0].outerHTML + "<div style='width:100%;;margin-right:10mm;' >" + prtContent.outerHTML + "</div>" + $('#dvFooter')[0].outerHTML.replace("position: relative", "position: absolute");
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write('<html><head><title>DIV Contents</title>');
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            frameDoc.document.write('<link href="resources/css/master.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');

            var css = ' @page{size: auto;margin-top:2mm;margin-bottom: 2mm;margin-right:10mm;  }',
head = frameDoc.document.head || document.getElementsByTagName('head')[0],
style = frameDoc.document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(frameDoc.document.createTextNode(css));
            }
            head.appendChild(style);

            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);
        }

    </script>
    <div id="dialog" title="הכנס סיסמא" style="display: none">
        <input id="txtPassword" type="password" size="25" />
        <div id="dvMessage" style="color: red"></div>
    </div>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
            <asp:HiddenField ID="hfPassword" Value="" runat="server" />

            <div id="dvGridResults" >
            <h1 id="header" runat="server">אישור מסירה  </h1>
            <div>
                <table border="1" cellpadding="0" cellspacing="0" style="display: inline-table; width: 486px; margin: 20px; }">
                    <tr>
                        <td class="tbDeliveryreport">
                            <b>לקוח</b>
                        </td>
                        <td class="tbDeliveryreport">
                            <b>מחלקה
                            </b>
                        </td>
                        <td class="tbDeliveryreport">
                            <b>עיר
                            </b>
                        </td>
                        <td class="tbDeliveryreport">
                            <b>כתובת
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCust" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblDep" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblCity" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblAddress" runat="server" />
                            <span id="spnDriver" runat="server" style="display: none"></span>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Repeater ID="rptResults" runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" width="800px" rules="all" border="1" style="display: inline-table">
                        <tr>
                            <th scope="col" style="width: 130px">תיק/ארגז
                            </th>
                            <th scope="col" style="width: 180px">תאור
                            </th>
                            <th scope="col">ת.ז.
                            </th>
                            <th scope="col">שם
                            </th>
                            <th scope="col" style="width: 150px">תאריך
                            </th>
                            <th scope="col" style="width: 130px">סטטוס
                            </th>

                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblCustomerId" runat="server" Text='<%# Eval("FileID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblContactName" runat="server" Text='<%# Eval("Description1") %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Identity") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("FullName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("InsertDate") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("StatusID")!=null && (int)Eval("StatusID")==(int)MA_DAL.MA_HELPER.Consts.enFileStatus.notFound?"לא אותר":"" %>' />

                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <table cellspacing="0" style="width: 800px; text-align: right; display: inline-table" rules="all" border="1">
                <tr>
                    <td style="width: 130px">תיבות ריקות</td>
                    <td style="width: 180px">
                        <asp:Label ID="sumNewBoxe" runat="server" /></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                </tr>
                <tr>
                    <td>החזרות</td>
                    <td>
                        <asp:Label ID="sumRetriveBoxes" runat="server" /></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                </tr>
                <tr>
                    <td>פינויים</td>
                    <td>
                        <asp:Label ID="sumFullBoxes" runat="server" /></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                </tr>
                <tr>
                    <td>גריסות</td>
                    <td>
                        <asp:Label ID="sumExterminateBoxes" runat="server" /></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>

                </tr>
                <tr>
                    <td colspan="5" style="width: 800px">
                        <span>הערה להזמנה:</span>
                        <asp:Label ID="lblShippingComment" runat="server" />
                    </td>

                </tr>
                <tr>
                    <td colspan="3" style="border: none"></td>
                    <td style="width: 150px">שם מלא</td>
                    <td style="width: 130px">
                        <asp:DropDownList ID="ddlReceivedBy" runat="server">
                        </asp:DropDownList>
                        <span id="spnReceivedBy" runat="server" style="display: none"></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border: none"></td>
                    <td style="width: 150px">תאריך</td>
                    <td style="width: 130px">
                        <label id="lblReceivedDate" runat="server"></label>
                    </td>
                </tr>
            </table>
                </div>
            <table style="float: left; width: 100%">
                <tr>
                    <td colspan="2">
                        <div id="dvErrorMessage" runat="server" style="display: none" class="dvErrorMessage"></div>
                        <div id="dvSucceedMessage" runat="server" style="display: none" class="dvSucceedMessage"></div>

                    </td>
                </tr>
                <tr>
                    <td style="width: 662px; text-align: right;">
                        <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnUpdate" Text="חתום" runat="server" OnClientClick="return btnUpdate_Click()" OnClick="btnUpdate_Click" Style="margin-bottom: 10px;" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 662px; text-align: right;"></td>
                    <td>
                        <asp:Button ID="btnPrint" Text="הדפס" runat="server" OnClientClick="printTable()" Style="margin-bottom: 10px;" />
                    </td>

                </tr>
            </table>
         </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
