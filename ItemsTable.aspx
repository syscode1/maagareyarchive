﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ItemsTable.aspx.cs" Inherits="MaagareyArchive.ItemsTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }

        function confirmRemove()
        {
            var isRemove = confirm("שים לב! הסרה של פריט תסיר את הפריט הנבחר מכל הספקים המספקים אותו , האם אתה בטוח?");
            return isRemove;
        }
    </script>
    <h1 id="header" runat="server">טבלת פריטים</h1>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table style="width: 400px; display: inline;">
                <tr>
                    <td>
                        <asp:Label Text="שם פריט" runat="server" />
                        <asp:TextBox ID="txtItemName" Width="200px" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnAddItem" Text="הוסף לטבלה" OnClick="btnAddItem_Click" Width="100px" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataGrid ID="dgResults" runat="server"
                            AutoGenerateColumns="false" CssClass="grid" Width="400px">
                            <%--DataKeyField="FileID"--%>

                            <HeaderStyle CssClass="header" />

                            <AlternatingItemStyle CssClass="alt" />
                            <ItemStyle CssClass="row" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="מס'" ItemStyle-CssClass="ltr">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate><%# rownum++  %></ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="פריט" ItemStyle-CssClass="ltr">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <span id="itemName" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ItemName")).ToString()%> </span>
                                   <span id="itemID" style="display:none;visibility:hidden" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ItemID"))==null?"":(DataBinder.Eval(Container.DataItem, "ItemID")).ToString()%></span>
                                         </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="הסר" ItemStyle-CssClass="ltr" SortExpression="Date">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbRemove" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                            </Columns>
                        </asp:DataGrid>

                    </td>
                </tr>
            </table>

            <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 462px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exit()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnRemove" Text="עדכן הסרה" OnClientClick="return confirmRemove();" OnClick="btnRemove_Click" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
