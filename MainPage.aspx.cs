﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class MainPage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearchOrderMaterial_Click(object sender, EventArgs e)
        {
            Response.Redirect("SearchOrderMaterial.aspx");
        }

        protected void btnOrderSupplieAndCollecting_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderSuppliesAndCollecting.aspx");
        }

        protected void btnSendMessageToArchive_Click(object sender, EventArgs e)
        {
            Response.Redirect("SendMessageToArchive.aspx");
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
        }

        protected void btnExterminateHistoryAndRequiering_Click(object sender, EventArgs e)
        {
            Response.Redirect("ExterminatedHistoryAndRequiering.aspx");
        }
    }
}