﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="UploadFilesExcel.aspx.cs" Inherits="MaagareyArchive.UploadFilesExcel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function upload() {
            debugger
            document.getElementById('<%=lblUploading.ClientID%>').style.display = "";
            $(".se-pre-con")[0].style.display = "";//
        }
    </script>
    <h1 >טעינת קובץ אקסל</h1>

            <asp:LinkButton Text="הורד תבנית קובץ להעלאה" OnClick="Download_Click" style="text-align:right;display:grid" runat="server" />
                <asp:UpdatePanel runat="server" ID="up">
                    <ContentTemplate>
    <table style="text-align: right;" cellpading="10" cellspacing="10">
        <tr>
            <td>
                <asp:FileUpload  ID="FileUpload1" runat="server"  />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="האם הקובץ מכיל כותרת ?" />
            </td>
            <td>
                <asp:RadioButtonList ID="rbHDR" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="כן" Value="Yes" Selected="True">
                    </asp:ListItem>
                    <asp:ListItem Text="לא" Value="No"></asp:ListItem>
                </asp:RadioButtonList>

            </td>
        </tr>
        <tr>
            <td colspan="2">

                <asp:Button ID="btnUpload" runat="server" Text="טען קובץ"
                    OnClick="btnUpload_Click" OnClientClick="return upload()" style="float:right !important"/>
                <asp:Label Text="טוען..." ID="lblUploading" runat="server" style="display:none;float:left;margin-top:5px"/>
                <div class="se-pre-con" style="display:none"></div>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <div style="width:1200px;overflow:scroll">
                <asp:GridView ID="GridView1" runat="server" Width="4000px"
                    OnPageIndexChanging="PageIndexChanging" PageSize="200" AllowPaging="true">
                </asp:GridView>

                </div>
            </td>
        </tr>
    </table> 
    <asp:Button Text="שמור" ID="btnSave" OnClick="btnSave_Click" runat="server" />

                       <div id="dvSucceedMessage" runat="server" style="padding-top: 14px;" class="dvSucceedMessage"></div>
    <br />
                         <div id="dvErrorMessage" runat="server" class="dvErrorMessage" ></div>
                   </ContentTemplate>
                               <Triggers>
               <asp:PostBackTrigger ControlID="btnUpload"  />
                                   </Triggers>
                </asp:UpdatePanel>
      <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
