﻿using dg.Utilities;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserEntity"] != null)
            {
                Users user = Session["UserEntity"] as Users;
                lnkmainPage.HRef = user.ArchiveWorker == true ? "MainPageArchive.aspx" : "MainPage.aspx";
                if (!Page.IsPostBack)
                {
                    lnkbtnLogout.Text += user.FullName + " , " + user.UserID;
                    int unread = EventController.getNumOfUnreadEvents(user.UserID,user.Job);
                    lblUnreadEvents.Text = string.Format("({0})", unread.ToString());
                }
                if (user.ArchiveWorker!=true)
                {
                    hideArchiveButtons();
                }
            }
        }

        private void hideArchiveButtons()
        {
            btnExtraction.Visible = false;
            btnArchiveMaintenance.Visible = false;
            btnManageSuppliers.Visible = false;
            btnAccount.Visible = false;
            btnRetrive.Visible = false;
            btnEvents.Visible = false;
            btnWorkOrdering.Visible = false;

        }

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
        public void LimitToPermissions(Users user, params string[] permissions)
        {
            if (permissions==null || permissions.Length == 0 || Permissions.UserHasAnyPermissionIn(user, permissions)) return;
            Response.Redirect("404.aspx");
        }

        protected void btnManageSuppliers_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageSuppliers.aspx");
        }

        protected void btnSearchFile_Click(object sender, EventArgs e)
        {
            Response.Redirect("SearchOrderMaterial.aspx");
        }

        protected void btnArchiveMaintenance_Click(object sender, EventArgs e)
        {
            Response.Redirect("ArchiveMaintenance.aspx");
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reports.aspx");
        }


        protected void btnInputData_Click(object sender, EventArgs e)
        {
            Response.Redirect("InputData.aspx");
        }

        protected void btnExtraction_Click(object sender, EventArgs e)
        {
            Response.Redirect("Extraction.aspx");
        }

        protected void btnScanExtractFiles_Click(object sender, EventArgs e)
        {
            Response.Redirect("ScanExtractFiles.aspx");
        }

        protected void btnRetrive_Click(object sender, EventArgs e)
        {
            Response.Redirect("ScanRetrives.aspx");
        }

        protected void btnInputBoxes_Click(object sender, EventArgs e)
        {
            Response.Redirect("InputBoxes.aspx");
        }

        protected void btnDeleveryConfirmation_Click(object sender, EventArgs e)
        {
            Response.Redirect("DeleveryConfirmation.aspx");
        }

        protected void btnWorkOrdering_Click(object sender, EventArgs e)
        {
            Response.Redirect("WorkOrderingMain.aspx");
        }

        protected void btnCustomerScreen_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerScreen.aspx");
        }

        protected void btnEvents_Click(object sender, EventArgs e)
        {
            Response.Redirect("EventsPage.aspx");
        }

        protected void btnOrderSuppliesAndCollecting_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderSuppliesAndCollecting.aspx");
        }

        protected void btnHomePage_Click(object sender, EventArgs e)
        {
            Users user = Session["UserEntity"] as Users;
            Response.Redirect(user.ArchiveWorker == true ? "MainPageArchive.aspx" : "MainPage.aspx");

        }
    }

}
