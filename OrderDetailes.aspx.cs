﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class OrderDetailes : BasePage
    {
        List<OrdersUI> orders
        {
            get
            {
                if (Session["OrderDetailes_data"] != null)
                    return Session["OrderDetailes_data"] as List<OrdersUI>;
                return null;
            }
            set
            {
                Session["OrderDetailes_data"] = value;
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            txtFileBoxID.Focus();
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["order_id"]))
                    header.InnerText += Request.QueryString["order_id"].Replace("'", "");
                else if (!string.IsNullOrEmpty(Request.QueryString["fromW"]) && !string.IsNullOrEmpty(Request.QueryString["toW"]))
                    if (Request.QueryString["fromW"] == Request.QueryString["toW"])
                    {
                        header.InnerText += Consts.ORDER_WAREHOSE + Request.QueryString["fromW"];
                    }
                    else
                    {
                        header.InnerText += Consts.ORDER_WAREHOSES.Replace("XXX", Request.QueryString["fromW"]).Replace("YYY", Request.QueryString["toW"]);
                    }

                bindData();

            }

                OrderedDictionary data = UserController.getUsersByWorkers(Consts.enJobs.Extractor);
                if (!data.Contains(0)) data.Insert(0, 0, Consts.SELECT);
                fillDdlOrderDict(data, ddlEmployee);
        }
        private void bindData()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["order_id"]))
            {
                bindDataByOrder();
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["fromW"]) && !string.IsNullOrEmpty(Request.QueryString["toW"]))
                bindDataByWarehouse();
        }
        private void bindDataByOrder()
        {
            string[] orderID = Request.QueryString["order_id"].Replace("'", "").Split(',');
            orders = OrdersController.getOrdersByIDs(orderID);
            dgResults.DataSource = orders;
            dgResults.DataBind();
        }
        private void bindDataByWarehouse()
        {
            int? from = Helper.getInt(Request.QueryString["fromW"]);
            int? to = Helper.getInt(Request.QueryString["toW"]);

            orders = OrdersController.getOrdersByIDs(new string[] { }, from.Value, to.Value);
            dgResults.DataSource = orders;
            dgResults.DataBind();
        }
        //protected void rptDetailes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string orderID = (e.Item.DataItem as OrdersUI).OrderID;
        //        DataGrid dg = e.Item.FindControl("dgResults") as DataGrid;
        //        dg.AutoGenerateColumns = false;
        //        dg.DataSource = orders.Where(o => o.OrderID == orderID);
        //        dg.DataBind();
        //    }
        //}

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            List<string[]> ids = new List<string[]>(); ;

            foreach (DataGridItem item in dgResults.Items)
            {
                if ((item.FindControl("cbNotExist") as CheckBox).Checked)
                {
                    string fileID = (item.FindControl("spnFileID") as HtmlGenericControl).InnerText;
                    string boxID = (item.FindControl("spnBoxID") as HtmlGenericControl).InnerText;
                    int customerID = Helper.getInt((item.FindControl("spnCustomerID") as HtmlGenericControl).InnerText).Value;
                    int departmentID = Helper.getInt((item.FindControl("spnDepartmentID") as HtmlGenericControl).InnerText).Value;
                    int? workerID = Helper.getInt(ddlEmployee.SelectedValue);
                    Events ev = new Events()
                    {
                        CustomerID = customerID,
                        DepartmentID = departmentID,
                        Date = DateTime.Now,
                        EventType = (int)Consts.enEventTypes.repeatedRequest,
                        FileID = fileID,
                        UserID = workerID,
                    };
                    EventController.insert(ev);

                    FileStatusController.UpdateInsert(new FileStatus()
                    {
                        FieldID = fileID,
                        StatusDate = DateTime.Now,
                        LoginID = workerID,
                        StatusID = (int)Consts.enFileStatus.notFound,

                    });
                    FileStatusArchiveController.Insert(new FileStatusArchive()
                    {
                        FieldID = fileID,
                        StatusDate = DateTime.Now,
                        LoginID = workerID,
                        StatusID = (int)Consts.enFileStatus.notFound,
                    });
                    ids.Add(new string[] { fileID, boxID });
                }
            }
            bindData();

        }

        protected void btnPrintlable_Click(object sender, EventArgs e)
        {
            Button btn = (Button)(sender);
            var arguments = btn.CommandArgument;

            if (btn.CommandName == "printLable")
            {
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                string fileID = commandArgs[0];
                string boxID = commandArgs[1];
                //TODO print lable
                FilesController.updatePrintLable(boxID, fileID);
            }

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //TODO print grid + orderID -not founf, print lable
            foreach (DataGridItem item in dgResults.Items)
            {
                if ((item.FindControl("cbCheck") as HtmlInputCheckBox).Checked)
                {
                    string numInOrder = (item.FindControl("spnNumInOrder") as HtmlGenericControl).InnerText;
                    string orderID = (item.FindControl("spnOrderID") as HtmlGenericControl).InnerText;
                    OrdersController.updatePrintOrder(orderID, Helper.getIntNotNull(numInOrder), hfSelectedEmployee.Value, true);
                }
                //
            }
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "Print();", true);// TODO add lable print
        }

        protected void btnReload_Click(object sender, EventArgs e)
        {
            bindData();
        }

        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    OrdersUI order = (OrdersUI)e.Item.DataItem;

                    if (!string.IsNullOrEmpty(order.EmployID))
                    {
                        HtmlInputCheckBox cbCheck = (e.Item.FindControl("cbCheck") as HtmlInputCheckBox);
                        cbCheck.Disabled = true;
                        cbCheck.Checked = false;
                        e.Item.BackColor = Color.Gray;
                    }
                    else
                    {
                        (e.Item.FindControl("cbRemove") as HtmlInputCheckBox).Disabled = true;

                    }

                }
            }
            catch (Exception ex)
            {

            }

        }


        protected void btnInputFileBoxID_Click(object sender, EventArgs e)
        {
            string orderID = OrdersController.getOrderByBoxFile(txtFileBoxID.Value, txtFileBoxID.Value);
            if (string.IsNullOrEmpty(orderID))
            {
                dvScanErrorMessage.InnerText = "תיק/ארגז לא קיים בהזמנה";
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "playSound('" + ParametersController.GetParameterValue(Consts.Parameters.PARAM_SOUND_ALARM_PATH_TEMPLATE) + "')", true);
            }
            else
            {
                OrdersController.scanOrder(orderID, txtFileBoxID.Value, txtFileBoxID.Value, user.UserID);
                bindData();
                dvScanErrorMessage.InnerText = "";
            }
            txtFileBoxID.Value = string.Empty;
        }

        protected void btRemove_Click(object sender, EventArgs e)
        {
            foreach (DataGridItem item in dgResults.Items)
            {
                if ((item.FindControl("cbRemove") as HtmlInputCheckBox).Checked)
                {
                    string numInOrder = (item.FindControl("spnNumInOrder") as HtmlGenericControl).InnerText;
                    string orderID = (item.FindControl("spnOrderID") as HtmlGenericControl).InnerText;
                    OrdersController.updatePrintOrder(orderID, Helper.getIntNotNull(numInOrder), "", false);

                }
            }
            bindData();
        }

        [System.Web.Services.WebMethod]
        public static string updateShelfID(string boxID, string newShelfID)
        {
            if (!string.IsNullOrEmpty(boxID) && !string.IsNullOrEmpty(newShelfID) && newShelfID.Length == 9)
            {
                int userID = HttpContext.Current.Session["UserEntity"] != null ? (HttpContext.Current.Session["UserEntity"] as Users).UserID : 0;
                return BoxesController.updateShelfID(boxID, newShelfID,userID) ? "1" : "0";
            }
            return "0";
        }

        [System.Web.Services.WebMethod]
        public static string getShelfHistory(string boxID)
        {
            if (!string.IsNullOrEmpty(boxID))
            {
                return JsonConvert.SerializeObject(BoxHistoryController.getHistoryByBoxID(boxID), Newtonsoft.Json.Formatting.None);
            }
            return "0";
        }
    }
}