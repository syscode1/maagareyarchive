﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DailyReportDetailed.aspx.cs" Inherits="MaagareyArchive.DailyReportDetailed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        דו"ח יומי מפורט
    </h1>
<script>
            $(document).ready(function () {
            document.getElementById('<%=txtFromDate.ClientID%>').value = '<%=DateTime.Now.ToString("yyyy-MM-01")%>';
            document.getElementById('<%=txtToDate.ClientID%>').value = '<%=DateTime.Now.ToString("yyyy-MM-dd")%>';

            });

    
        function popup(mylink, windowname,href) {
            if (!window.focus)
                return true;
            var cust = document.getElementById('<%=ddlCustomer.ClientID%>').value;
            if (cust == "0")
            {
                document.getElementById('dvErrorMessage').innerText = "יש לבחור לקוח";
                document.getElementById('dvErrorMessage').style.display = "";
                return false;
            }
            document.getElementById('dvErrorMessage').innerText = "";
            var w = 1200;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            var from=document.getElementById('<%=txtFromDate.ClientID%>').value ;
            var to = document.getElementById('<%=txtToDate.ClientID%>').value;
            window.open(href + "?custID=" + cust + "&from=" + from + "&to=" + to, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }
</script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
    <table style="display:inline-grid">
        <tr>
            <td>
                <asp:DropDownList ID="ddlCustomer" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <span>מתאריך </span>
                <br />
                <input type="date" runat="server" id="txtFromDate" />
            </td>
            <td>
                <span>עד תאריך</span>
                <br />
                <input type="date" runat="server" id="txtToDate" />
            </td>
        </tr>
    </table>
<div style="text-align: -webkit-center;
">
    <table cellspacing="10">
        <tr>
            <td> 
                <asp:Button Text="גריסות"  runat="server" OnClientClick="return popup(this, '','ExportExterminated.aspx')" />
            </td>
        </tr>
        <tr>
            <td> 
                <asp:Button Text="שליפות" runat="server" OnClientClick="return popup(this, '','ExportShipping.aspx')"/>
            </td>
        </tr>
        <tr>
            <td> 
                <asp:Button Text="החזרות" runat="server" OnClientClick="return popup(this, '','ExportRetrives.aspx')"/>
            </td>
        </tr>
    </table>
<div  id="dvErrorMessage" class="dvErrorMessage"  ></div>


     <table style="margin: 0px;width:100%; border-spacing: 0px; ">
                    <tr>
                        <td style="text-align: -webkit-center;">
                            <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />

                        </td>
                        <td>
  
                        </td>
                    </tr>
                </table>        
</div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
