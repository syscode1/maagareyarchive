﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerBoxesSale.aspx.cs" Inherits="MaagareyArchive.CustomerBoxesSale" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>מכירת ארגזים ללקוח</h1>
    <table>
        <tr>
            <td>
                <asp:Label Text="לקוח" runat="server" /></td>
            <td>
                <asp:DropDownList runat="server" ID="ddlCustomers">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="כמות ארגזים" runat="server" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtAmount" onkeypress="return isNumberKey(event)"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="re2" class="dvErrorMessage" ControlToValidate="ddlCustomers" runat="Server" InitialValue="0" ErrorMessage="נא לבחור לקוח"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="re3" class="dvErrorMessage" ControlToValidate="txtAmount" runat="Server" ErrorMessage="יש להזין כמות"></asp:RequiredFieldValidator>

    <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display: none"></span>
    <span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display: none"></span>

    <table style="margin-top: 24px; width: 875px">
        <tr>
            <td style="width: 462px; text-align: right;">
                <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
            </td>
            <td>
                <asp:Button ID="btnSave" Text="שמור" OnClick="btnSave_Click" runat="server" />
            </td>

        </tr>
    </table>
</asp:Content>
