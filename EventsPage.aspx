﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EventsPage.aspx.cs" Inherits="MaagareyArchive.EventsPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>משימות ממתינות בתור לטיפול</h1>
    <script>
        $(function () {
            $("[id*=tblResult] td").click(function () {
               
                DisplayDetails($(this).closest("tr"));
            });
        });
        function DisplayDetails(row) {
            
            var src = 'EditViewEvent.aspx';
            if (row != '') {
                var rowID = $("td", row).eq(0)[0].innerText;
                src += '?row_id=' + rowID;

            }
            showLoader();
            $("[id*=ifrEvent]")[0].src =src;
            return false;
        //    popup('EditViewEvent.aspx', '', rowID)
        }
        function showLoader() {
             var updateProgress = $find("<%= updProgress.ClientID %>");
        window.setTimeout(function () {
            updateProgress.set_visible(true);
        }, 100);
        }
        function hideLoader() {
             var updateProgress = $find("<%= updProgress.ClientID %>");
        window.setTimeout(function () {
            updateProgress.set_visible(false);
        }, 100);
        }
        function popup(href, windowname, RowID) {
            if (!window.focus)
                return true;
            var href;
            var w = 1200;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open(href + (RowID != "0" ? "?row_id=" + RowID : ""), windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }
        function eventClick(rowID,tr) {
            jQuery.ajax({
                url: 'EventsPage.aspx/updateReadEvent',
                type: "POST",
                data: "{rowID : '" + rowID +"'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.d == "1") {
                        debugger
                        $("[id*=tdRowID]", tr)[0].style.fontWeight = "unset";
                        $("[id*=tdEventTypeName]", tr)[0].style.fontWeight = "unset";
                    }
                    hideLoader();
                },
                failure: function (msg) { hideLoader(); }
            });
        }
    </script>
    <p>
        <asp:UpdatePanel runat="server" ID="up">
            <ContentTemplate>
    <asp:ImageButton ID="btnAddRow" Style="margin-top: 5px; float: right;" OnClientClick="return DisplayDetails('')" ImageUrl="resources/images/add.png" Height="35px" runat="server" />
    <span style="float: right; margin-top: 10px; margin-right: 5px; font-weight: bold; font-size: 19px; color: #004800;">פתח משימה חדשה</span>
        </p>
    <br />
    <br />
    <br />
  <%--  <asp:GridView ID="dgResultsR" runat="server" CssClass="grid"
        EnableViewState="true" Width="1038px"
        AutoGenerateColumns="false"
        EnablePersistedSelection="true"
        DataKeyNames="RowID">
        <HeaderStyle CssClass="header" />

        <AlternatingRowStyle CssClass="alt" />
        <RowStyle CssClass="row" />
        <%--DataKeyField="FileID"--%>
        <%--<Columns>
            <asp:TemplateField HeaderText="משימה מספר" ItemStyle-CssClass="ltr" SortExpression="Description1">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span>
                        <span><%#(Eval("RowID"))%> </span>
                    </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="מאת" ItemStyle-CssClass="ltr" SortExpression="Description1">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span>
                        <span><%#(Eval("FullName"))%> </span>
                    </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סוג משימה" ItemStyle-CssClass="ltr" SortExpression="Description1">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span id="Span1" runat="server">
                        <span><%#(Eval("EventTypeName"))%> </span>
                    </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="לקוח" ItemStyle-CssClass="ltr" SortExpression="Description1">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span id="Span2" runat="server">
                        <span><%#(Eval("CustomerName"))%> </span>
                    </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="פרטי תיק" ItemStyle-CssClass="ltr" SortExpression="Description1">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span id="Span3" runat="server">
                        <span><%#(Eval("FileID"))%> </span>
                    </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="תוכן משימה" ItemStyle-CssClass="ltr" SortExpression="Description1">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span id="Span4" runat="server">
                        <span style="pointer-events: none"><%#Eval("EventDescription") %> <%#Eval("EmailBody") %> </span>
                    </span>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>

    </asp:GridView>--%>
    <div style="clear:both">
    <div style="float:right">
    <asp:Repeater ID="rptResult" runat="server" OnItemDataBound="rptResult_ItemDataBound">
        <HeaderTemplate>
         <table id="tblResult" style="border: 1px solid #94d094;width: 200px;border-radius: 7px;">
        </HeaderTemplate>
        <ItemTemplate>
                <tr style="height: 30px;" class="event_tr" onclick="eventClick('<%#DataBinder.Eval(Container.DataItem, "RowID") %>',this)">
                    <td id="tdRowID" runat="server" >    
                       <%#(Eval("RowID"))%>
                    </td>   
                    <td id="tdEventTypeName" runat="server">    
                         <%#(Eval("EventTypeName"))%> 
                    </td>   
                </tr>
  
        </ItemTemplate>
        <FooterTemplate>
            </table>          
        </FooterTemplate>
    </asp:Repeater>
        </div>
    <div style="width:720px;height:600px;float:left">
        <iframe id="ifrEvent" style="height:100%;width:720px;border:0px" ></iframe>
    </div>
    </div>
    <table style="margin-top: 10px; border-spacing: 0px; float: left; width: 100%">
        <tr>
            <td style="width: 240px; text-align: right;">
                <input type="button" value="יציאה" onclick="exit()" runat="server" />
            </td>

        </tr>
    </table>
        </ContentTemplate>
        </asp:UpdatePanel>
       <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
