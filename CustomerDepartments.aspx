﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerDepartments.aspx.cs" Inherits="MaagareyArchive.CustomerDepartments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script>

        function onSaveClick() {
            var isValid = true;
            dgItems = $('#<%= dgResults.ClientID%>')[0];
            for (i = 0; i < dgItems.rows.length && isValid == true; i++) {
                if ($("[id*= 'cbCheck']", dgItems.rows[i]).length > 0) {
                    if ($("[id*= 'cbCheck']", dgItems.rows[i])[0].checked == true) {
                        if ($("[id*= 'txtPrice']", dgItems.rows[i])[0].value <= 0) {
                            isValid = false;

                        }
                    }
                }
            }
            dvErrorMessage = $('#<%= dvErrorMessage.ClientID%>')[0];
            dvSucceedMessage = $('#<%= dvSucceedMessage.ClientID%>')[0];
            if (isValid == false) {
                dvErrorMessage.innerText = "יש למלא מחיר לכל פריט מסומן";
                dvErrorMessage.style.display = "";
                dvSucceedMessage.style.display = "none";

            }
            else {
                dvErrorMessage.style.display = "none";
            }


            if ($('[id*=txtAddress]')[0].value.trim() == "" ||
                $('[id*=txtContactName]')[0].value.trim() == "" ||
                $('[id*=txtPhone1]')[0].value.trim() == "")
                isValid = false;

            if (isValid == false) {
                dvErrorMessage.style.display = "";
                dvErrorMessage.innerText = "יש למלאות את כל שדות הכתובת";
            }

            return isValid;
        }

    </script>
    <h1 id="header" runat="server">מחלקות ללקוח</h1>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
          
            <table>
                <tr>
                    <td>

                        <div style="clear: both">

                            <div id="dvSupplierName" runat="server" style="float: right; margin-top: 3px; margin-right: 20px;">
                                <label>בחר לקוח</label>

                                <asp:DropDownList ID="ddlCustomerName" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                            <div id="dvSupplierID" style="float: right; margin-top: 3px; margin-right: 20px;" runat="server">
                                <label>מספר לקוח</label>
                                <asp:DropDownList ID="ddlCustomeID" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerID_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>
                          <div style="overflow:auto">
                        <asp:DataGrid ID="dgResults" runat="server"
                            AutoGenerateColumns="false" CssClass="grid" Width="522px"                            
                            Style="margin-bottom: 30px">


                            <HeaderStyle CssClass="header" />

                            <AlternatingItemStyle CssClass="alt" />
                            <ItemStyle CssClass="row" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="מחלקה" ItemStyle-CssClass="ltr">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <span id="DepartmentName" runat="server"><%#(DataBinder.Eval(Container.DataItem, "DepartmentName")).ToString()%> </span>
                                        <span id="DepartmentID" style="display: none; visibility: hidden" runat="server"><%#(DataBinder.Eval(Container.DataItem, "DepartmentID"))==null?"":(DataBinder.Eval(Container.DataItem, "DepartmentID")).ToString()%></span>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="בחר" ItemStyle-CssClass="ltr">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCheck" runat="server" Checked="true" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="איש קשר לגביה" ItemStyle-CssClass="ltr" HeaderStyle-Width="123px">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <input id="txtCollectionName" maxlength="20" runat="server" class="form-control" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="טלפון לגביה" ItemStyle-CssClass="ltr" HeaderStyle-Width="123px">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <input id="txtCollectionPhone" maxlength="20" runat="server" class="form-control" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ח.פ." ItemStyle-CssClass="ltr" HeaderStyle-Width="90px">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <input id="txtID" onkeypress="return isNumberKey(event,9)" runat="server" class="form-control" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="תשלום עצמי" ItemStyle-CssClass="ltr" HeaderStyle-Width="35px">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbSelfPayment" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </table>
            <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 462px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnSave" Text="שמור" OnClientClick="return onSaveClick();" OnClick="btnSave_Click" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                          <input id="btnBackCustomer" type="button" value="חזרה למסך לקוח" onclick="exitCustomerScreen()" runat="server" style="display:none" />
                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>
                <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display: none"></span><span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display: none"></span></br/>
                     </br/>
                 </caption>
                    </td>
                </tr>
                <br></br>

            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
      <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
