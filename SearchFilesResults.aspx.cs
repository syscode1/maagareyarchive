﻿using DocumentFormat.OpenXml.Drawing.Charts;
using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace MaagareyArchive
{
    public partial class SearchFilesResults : BasePage
    {
        public int rownum = 1;
        List<OrderBufferSearch> obFiles;
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private SearchFileParams SearchFileParams
        {
            get
            {
                if (Session["SearchOrderMaterial_Params"] == null)
                    return null;
                return Session["SearchOrderMaterial_Params"] as SearchFileParams;
            }
            set
            {
                Session["SearchOrderMaterial_Params"] = value;
            }

        }

  private List<FileUI> files
        {
            get
            {
                if (Session["SearchOrderMaterial_Results"] == null)
                    return null;
                return Session["SearchOrderMaterial_Results"] as List<FileUI>;
            }
            set
            {
                Session["SearchOrderMaterial_Results"] = value;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                loadData();
            
            }
        }

        private void loadData()
        {
            if (user.ArchiveWorker == true || SearchFileParams.DepartmentID > 0)
            {
                lblCustName.InnerText = SearchFileParams.CustomerName;
                lblCustNum.InnerText = SearchFileParams.CustomerID.ToString();
                lblDep.InnerText = SearchFileParams.DepartmentName;

                files = FilesController.SearchFiles(SearchFileParams.CustomerID,
                                                    SearchFileParams.DepartmentID,
                                                    SearchFileParams.CustomerBoxNum,
                                                    SearchFileParams.FileID,
                                                    SearchFileParams.SubjectID,
                                                    SearchFileParams.BoxID,
                                                    SearchFileParams.BoxDescription,
                                                    SearchFileParams.AdditionalDepartmentFields,
                                                    user.ArchiveWorker==true? true :false);
                if (files.Count > 0)
                {

                    if(SearchFileParams.DepartmentID > 0)
                    {
                        List<FileFieldsUI> fileField = FileFieldsController.getFileFieldsByDepartment(SearchFileParams.DepartmentID);
                        foreach (var item in fileField)
                        {

                            BoundField field = new BoundField();
                            if (item.Type == (int)Consts.enFieldType.date)
                                field.DataFormatString = "{0:d}";
                            field.DataField = item.Name.Trim();
                            field.HeaderStyle.Width = Unit.Pixel(100);
                            field.ItemStyle.Width = Unit.Pixel(100);
                            dgResults.Columns.Add(field);
                            HtmlTableCell tc = new HtmlTableCell("th");
                            tc.InnerText = item.Title;
                            tc.Width = "100px";
                            dummyHeader.Rows[0].Cells.Add(tc);
                        }
                    }
                    else
                    {
                    List<AllFields> allFields = FileFieldsController.geAllFields();
                        foreach (var item in allFields)
                        {

                            BoundField field = new BoundField();
                            if (item.Type == (int)Consts.enFieldType.date)
                                field.DataFormatString = "{0:d}";
                            field.DataField = item.Name.Trim();
                            field.HeaderStyle.Width = Unit.Pixel(100);
                            field.ItemStyle.Width = Unit.Pixel(100);
                            dgResults.Columns.Add(field);
                            HtmlTableCell tc = new HtmlTableCell("th");
                            tc.InnerText = item.Title;
                            tc.Width = "100px";
                            dummyHeader.Rows[0].Cells.Add(tc);
                        }
                    }

                    dgResults.DataSource = files;
                       

                    dgResults.DataBind();
                    // GridView1.DataSource = files;


                    //  GridView1.DataBind();
                    dgResults.HeaderRow.Attributes["style"] = "display:none";
                    dgResults.UseAccessibleHeader = true;
                    dgResults.HeaderRow.TableSection = TableRowSection.TableHeader;

                }
                else
                {
                    dummyHeader.Style["display"] = "none";
                    dvMessage.InnerText = Consts.NO_DATA_TO_DISPLAY;
                }
            }

        }
        public static string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }

        protected void btnAddAndBack_Click(object sender, EventArgs e)
        {
            
            addToOrderBuffer();
            Response.Redirect("SearchOrderMaterial.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            addToOrderBuffer();
            Response.Redirect("ViewOrder.aspx");
        }

        private void addToOrderBuffer()
        {
            try
            {
                List<OrderBufferUI> orderBuffer = new List<OrderBufferUI>();

                foreach (GridViewRow item in dgResults.Rows)
                {

                    if ((item.FindControl("cbBox") as CheckBox).Checked)
                    {
                        //List<FileUI> filesBox = FilesController.getFilesUIByBox((item.FindControl("boxID") as HtmlGenericControl).InnerText.Trim());
                        //if (filesBox.Count == 0)
                        {
                            bool email = (item.FindControl("cbEmail") as CheckBox).Checked;
                            bool fax = (item.FindControl("cbFax") as CheckBox).Checked;
                            orderBuffer.Add(new OrderBufferUI()
                            {
                                BoxID = (item.FindControl("boxID") as HtmlGenericControl).InnerText.Trim(),
                                CustomerID = (int)Helper.getInt((item.FindControl("customerID") as HtmlGenericControl).InnerText.Trim()),
                                DepartmentID = int.Parse((item.FindControl("departmentID") as HtmlGenericControl).InnerText),
                                FileID = "",
                                Urgent = (item.FindControl("cbUrgent") as CheckBox).Checked,
                                Email = email,
                                Fax = fax,
                                Comment = email || fax ? (item.FindControl("hfComment") as HiddenField).Value : ""

                            });
                        //}
                        //else
                        //{
                        //    foreach (FileUI fileB in filesBox)
                        //    {
                        //        if (fileB.StatusID == (int)Consts.enFileStatus.created ||
                        //            fileB.StatusID == (int)Consts.enFileStatus.inArchive ||
                        //            fileB.StatusID == (int)Consts.enFileStatus.waitingForShelf)
                        //        {
                        //            orderBuffer.Add(new OrderBufferUI()
                        //            {
                        //                BoxID = fileB.BoxID.Trim(),
                        //                CustomerID = (int)fileB.CustomerID,
                        //                DepartmentID = (int)fileB.DepartmentID,
                        //                FileID = fileB.FileID.Trim(),
                        //                Urgent = (item.FindControl("cbUrgent") as CheckBox).Checked,
                        //                Email = (item.FindControl("cbEmail") as CheckBox).Checked,
                        //                Fax = (item.FindControl("cbFax") as CheckBox).Checked
                        //            });

                        //        }
                        //    }
                            
                        }

                    }
                    else if ((item.FindControl("cbCheck") as CheckBox).Checked)
                    {
                        string fileID = (item.FindControl("fileID") as HtmlGenericControl).InnerText.Trim();
                        string boxID=(item.FindControl("boxID") as HtmlGenericControl).InnerText.Trim();
                        if (orderBuffer.Where(o => (string.IsNullOrEmpty(fileID) && o.BoxID == boxID) || (!string.IsNullOrEmpty(fileID) && o.FileID == fileID)).Count() == 0)
                        {
                            bool email = (item.FindControl("cbEmail") as CheckBox).Checked;
                            bool fax = (item.FindControl("cbFax") as CheckBox).Checked;
                            orderBuffer.Add(new OrderBufferUI()
                            {
                                BoxID = (item.FindControl("boxID") as HtmlGenericControl).InnerText.Trim(),
                                CustomerID = (int)Helper.getInt((item.FindControl("customerID") as HtmlGenericControl).InnerText.Trim()),
                                DepartmentID = int.Parse((item.FindControl("departmentID") as HtmlGenericControl).InnerText),
                                FileID = fileID,
                                Urgent = (item.FindControl("cbUrgent") as CheckBox).Checked,
                                Email = email,
                                Fax = fax,
                                Comment =email || fax ? (item.FindControl("hfComment") as HiddenField).Value : ""
                            });
                        }
                    }
                }
                if (orderBuffer.Count > 0)
                {
                    OrderBufferController.Insert(orderBuffer, user.UserID);
                   
                }

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        protected void dgResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType ==DataControlRowType.DataRow)
                {
                    // if (((FileUI)e.Item.DataItem).FileID != null)
                    // {
                    FileUI file = (FileUI)e.Row.DataItem;
                    if (obFiles == null)
                        obFiles = OrderBufferController.GetOrderBufferFiles(user.UserID);
                    if (obFiles.Where(x => x.FileID == (file.FileID != null ? file.FileID.Trim() : null) ||
                                           x.BoxID == (file.bBoxID != null ? file.bBoxID.Trim() : null) && string.IsNullOrEmpty(x.FileID)).Count() > 0)
                    {
                        e.Row.BackColor = Color.LightGray;
                        e.Row.Enabled = false;
                    }
                    if (file.Scan == true)
                        e.Row.BackColor = Color.LightBlue;
                    if (file.Exterminate != null && file.Exterminate == true)
                    {
                        if (user.ArchiveWorker == true)
                            e.Row.Visible = false;
                        else
                        {
                            e.Row.BackColor = Color.Red;
                            e.Row.Enabled = false;
                        }
                    }
                    if (file.bLocation == true)
                    {
                        e.Row.BackColor = Color.LightGray;
                        e.Row.Enabled = false;
                    }
                    if (file.FileID != null)
                    {
                        switch (file.StatusID)
                        {
                            case (int)Consts.enFileStatus.notFound:
                                if (user.ArchiveWorker == true)
                                {
                                    e.Row.BackColor = Color.Yellow;
                                    if (user.PermForRetrive != true)
                                        e.Row.Enabled = false;
                                }
                                else
                                {
                                    e.Row.BackColor = Color.LightGray;
                                    e.Row.Enabled = false;
                                }
                                break;
                            case (int)Consts.enFileStatus.created://נוצר
                                e.Row.BackColor = Color.LightGray;
                                e.Row.Enabled = false;
                                break;
                            case (int)Consts.enFileStatus.inOrder://בהזמנה
                                e.Row.BackColor = Color.LightGray;
                                e.Row.Enabled = false;
                                break;
                            case (int)Consts.enFileStatus.sentToCustomer://נשלח ללקוח
                                e.Row.BackColor = Color.LightGray;
                                e.Row.Enabled = false;
                                break;
                            case (int)Consts.enFileStatus.exterminated://נגרס
                                if (user.ArchiveWorker == true)
                                    e.Row.Visible = false;
                                else
                                {
                                    e.Row.BackColor = Color.Red;
                                    e.Row.Enabled = false;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            // }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        
    }
}