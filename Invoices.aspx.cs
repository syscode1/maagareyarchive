﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class Invoices : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.administrator }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Helper.getInt(ParametersController.GetParameterValue(Consts.Parameters.PARAM_LAST_INVOICE_MONTH).Substring(0, 2)) == DateTime.Now.Month - 1
                && Helper.getInt(ParametersController.GetParameterValue(Consts.Parameters.PARAM_LAST_INVOICE_MONTH).Substring(2, 4)) == DateTime.Now.Year)
            {
                string message = Consts.INVOICES_CREATED_MESSAGE.Replace("XXX", ParametersController.GetParameterValue(Consts.Parameters.PARAM_LAST_INVOICE_MONTH).Substring(0, 2)).Replace("YYY", ParametersController.GetParameterValue(Consts.Parameters.PARAM_LAST_INVOICE_MONTH).Substring(2, 4));
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "alert('" + message + "')", true);
            }
            else
            {


                ParametersController.UpdateParameterValue(Consts.Parameters.PARAM_LAST_INVOICE_MONTH, (DateTime.Now.Month - 1 < 10 ? "0" + (DateTime.Now.Month - 1).ToString() : (DateTime.Now.Month - 1).ToString()) + DateTime.Now.Year.ToString());
                List<Account> accounts = new List<Account>();
                ArchiveEntities context = new ArchiveEntities();
                Dictionary<Customers, int> custDep = DepartmentController.GetCustomerDepartmentsForInvoice();
                int currentMonth = DateTime.Now.Month - 1;
                foreach (var cust in custDep)
                {
                    DateTime dateOfContruct = cust.Key.DateOfContract.Value;
                    Consts.enPOP pop = (Consts.enPOP)System.Enum.Parse(typeof(Consts.enPOP), cust.Key.PeriodOfPayment.ToString());
                    Consts.enMOP mop = (Consts.enMOP)System.Enum.Parse(typeof(Consts.enMOP), cust.Key.MethodOfPayment.ToString());
                    int numOfMonthToDiv = pop == Consts.enPOP.Monthly ? 12 : (pop == Consts.enPOP.HalfOfYear ? 2 : (pop == Consts.enPOP.Quater ? 4 : 1));

                    Account account = new Account();
                    account.CustomerID = cust.Key.CustomerID;
                    account.DepartmentID = cust.Value == 0 ? null : (int?)cust.Value;
                    bool isToPayBoxes = isPayBoxes(pop, dateOfContruct, currentMonth);
                    if (isToPayBoxes == true)
                    {
                        List<Boxes> boxes = BoxesController.getExistBoxes(cust.Key.CustomerID, cust.Value == 0 ? new List<int>() : new List<int>() { cust.Value });
                        List<Boxes> boxesExterm = BoxesController.getExtermBoxes(cust.Key.CustomerID, cust.Value == 0 ? new List<int>() : new List<int>() { cust.Value });

                        calcPriceOfBoxesToPay(mop, cust.Key, cust.Value, dateOfContruct, currentMonth, account, numOfMonthToDiv, boxes, boxesExterm);
                        calcOtherPrices(cust.Key, account, currentMonth, numOfMonthToDiv,boxesExterm);
                        AccountController.insert(account);
                    }
                }

            }
        }

        private void calcOtherPrices(Customers cust, Account account, int currentMonth, int numOfMonthToDiv,List<Boxes> extermBoxes)
        {
            List<Boxes> allBoxes = BoxesController.getAllBoxes(cust.CustomerID);

            account.Transport = cust.Transport;
            account.TransportAmount = calcTransport(allBoxes,currentMonth,numOfMonthToDiv);

            account.Crushing = cust.Crushing;
            account.CrushingAmount = calcCrushing(extermBoxes,currentMonth,numOfMonthToDiv);

            account.CrushingAndTransport = cust.CrushingAndTransport;
            account.CrushingAndTransportAmount = calcCrushing(extermBoxes, currentMonth, numOfMonthToDiv);

            account.FixedPayment = cust.FixedPayment;

            account.Retrive = cust.Retrive;
            account.RetriveAmount = calcRetrive(OrdersController.getOrderByCust(cust.CustomerID), cust);

            account.UrgentRetrive = cust.UrgentRetrive;
            account.UrgentRetriveAmount = calcRetriveUrgent(OrdersController.getOrderByCust(cust.CustomerID), cust);

            //account.Scan = cust.UrgentRetrive;
            // account.ScanAmount = calcScan(OrdersController.getOrderByCust(cust.CustomerID), cust);

            account.EmptyBox = cust.EmptyBox;
            account.EmptyBoxAmount = BoxesController.calcNumOfEmptyBoxes(cust); 

        }

        private bool isPayBoxes(Consts.enPOP pop, DateTime dateOfContruct, int currentMonth)
        {

            switch (pop)
            {
                case Consts.enPOP.Monthly:
                    return true;
                case Consts.enPOP.Quater:
                    if (dateOfContruct.Month == currentMonth ||
                         dateOfContruct.Month - 3 == currentMonth ||
                         dateOfContruct.Month + 3 == currentMonth ||
                         dateOfContruct.Month - 6 == currentMonth ||
                         dateOfContruct.Month + 6 == currentMonth ||
                         dateOfContruct.Month - 9 == currentMonth ||
                         dateOfContruct.Month + 9 == currentMonth)
                        return true;
                    break;
                case Consts.enPOP.HalfOfYear:
                    if (dateOfContruct.Month == currentMonth ||
                        dateOfContruct.Month - 6 == currentMonth ||
                        dateOfContruct.Month + 6 == currentMonth)
                        return true;
                    break;
                case Consts.enPOP.Yearly:
                    if (dateOfContruct.Month == currentMonth)
                        return true;
                    break;
                default:
                    break;
            }

            return false;

        }

        private void calcPriceOfBoxesToPay(Consts.enMOP mop, Customers cust, int dep, DateTime dateOfContruct, int currentMonth, Account account, int numOfMonthToDiv,List<Boxes> boxes,List<Boxes> boxesExterm)
        {

            account.BoxPrice1 = cust.BoxPrice1 / numOfMonthToDiv;
            account.BoxPrice2 = cust.BoxPrice2 / numOfMonthToDiv;
            account.BoxPrice3 = cust.BoxPrice3 / numOfMonthToDiv;
            account.BoxPrice4 = cust.BoxPrice4 / numOfMonthToDiv;
            switch (mop)
            {
                case Consts.enMOP.Next:
                    account.BoxPrice1Amount = boxes.Where(b => b.BoxType == 1).Count();
                    account.BoxPrice1AmountNew = calcBoxAmountByDayesNext(account.BoxPrice1, boxes.Where(b => b.BoxType == 1).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice1AmountExterm = (calcBoxExtermByDayesNext(account.BoxPrice1, boxesExterm.Where(b => b.BoxType == 1 ).ToList(), currentMonth, 365 / numOfMonthToDiv))*-1;

                    account.BoxPrice2Amount = boxes.Where(b => b.BoxType == 2 ).Count();
                    account.BoxPrice2AmountNew = calcBoxAmountByDayesNext(account.BoxPrice2, boxes.Where(b => b.BoxType == 2).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice2AmountExterm = (calcBoxExtermByDayesNext(account.BoxPrice2, boxesExterm.Where(b => b.BoxType == 2).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice3Amount = boxes.Where(b => b.BoxType == 3 ).Count();
                    account.BoxPrice3AmountNew = calcBoxAmountByDayesNext(account.BoxPrice3, boxes.Where(b => b.BoxType == 3).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice3AmountExterm = (calcBoxExtermByDayesNext(account.BoxPrice3, boxesExterm.Where(b => b.BoxType == 3 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice4Amount = boxes.Where(b => b.BoxType == 4 ).Count();
                    account.BoxPrice4AmountNew = calcBoxAmountByDayesNext(account.BoxPrice4, boxes.Where(b => b.BoxType == 4).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice4AmountExterm = (calcBoxExtermByDayesNext(account.BoxPrice4, boxesExterm.Where(b => b.BoxType == 4 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    break;
                case Consts.enMOP.Back:
                    account.BoxPrice1Amount = calcBoxAmountByDayesBack(account.BoxPrice1, boxes.Where(b => b.BoxType == 1 ).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice1AmountExterm = (calcBoxExtermByDayesBack(account.BoxPrice1, boxesExterm.Where(b => b.BoxType == 1 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice2Amount = calcBoxAmountByDayesBack(account.BoxPrice2, boxes.Where(b => b.BoxType == 2 ).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice2AmountExterm = (calcBoxExtermByDayesBack(account.BoxPrice2, boxesExterm.Where(b => b.BoxType == 2 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice3Amount = calcBoxAmountByDayesBack(account.BoxPrice3, boxes.Where(b => b.BoxType == 3 ).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice3AmountExterm = (calcBoxExtermByDayesBack(account.BoxPrice3, boxesExterm.Where(b => b.BoxType == 3 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice4Amount = calcBoxAmountByDayesBack(account.BoxPrice4, boxes.Where(b => b.BoxType == 4 ).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice4AmountExterm = (calcBoxExtermByDayesBack(account.BoxPrice4, boxesExterm.Where(b => b.BoxType == 4 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;
                    break;
                case Consts.enMOP.Britman:
                    account.BoxPrice1Amount = boxes.Where(b => b.BoxType == 1 ).Count();
                    account.BoxPrice1AmountNew = calcBoxAmountByDayesBritman(account.BoxPrice1, boxes.Where(b => b.BoxType == 1).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice1AmountExterm = (calcBoxExtermByDayesBritman(account.BoxPrice1, boxesExterm.Where(b => b.BoxType == 1 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice2Amount = boxes.Where(b => b.BoxType == 2 ).Count();
                    account.BoxPrice2AmountNew = calcBoxAmountByDayesBritman(account.BoxPrice2, boxes.Where(b => b.BoxType == 2).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice2AmountExterm = (calcBoxExtermByDayesBritman(account.BoxPrice2, boxesExterm.Where(b => b.BoxType == 2 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice3Amount = boxes.Where(b => b.BoxType == 3 ).Count();
                    account.BoxPrice3AmountNew = calcBoxAmountByDayesBritman(account.BoxPrice3, boxes.Where(b => b.BoxType == 3).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice3AmountExterm = (calcBoxExtermByDayesBritman(account.BoxPrice3, boxesExterm.Where(b => b.BoxType == 3 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;

                    account.BoxPrice4Amount = boxes.Where(b => b.BoxType == 4 ).Count();
                    account.BoxPrice4AmountNew = calcBoxAmountByDayesBritman(account.BoxPrice4, boxes.Where(b => b.BoxType == 4).ToList(), currentMonth, 365 / numOfMonthToDiv);
                    account.BoxPrice4AmountExterm = (calcBoxExtermByDayesBritman(account.BoxPrice4, boxesExterm.Where(b => b.BoxType == 4 ).ToList(), currentMonth, 365 / numOfMonthToDiv)) * -1;
                    break;

                case Consts.enMOP.Osem:
                    if (DateTime.Now.Month == cust.DateOfContract.Value.Month)
                    {
                        cust = CustomersController.updateBoxAmountOsem(cust, boxes.Where(b => b.BoxType == 1 ).Count(),
                                                                             boxes.Where(b => b.BoxType == 2 ).Count(),
                                                                             boxes.Where(b => b.BoxType == 3 ).Count(),
                                                                             boxes.Where(b => b.BoxType == 4 ).Count());
                    }
                    account.BoxPrice1Amount = cust.BoxAmountOsem1;
                    account.BoxPrice2Amount = cust.BoxAmountOsem2;
                    account.BoxPrice3Amount = cust.BoxAmountOsem3;
                    account.BoxPrice4Amount = cust.BoxAmountOsem4;

                    dateOfContruct = new DateTime(cust.DateOfContract.Value.Day, cust.DateOfContract.Value.Month,DateTime.Now.Month> cust.DateOfContract.Value.Month? DateTime.Now.Year+1:DateTime.Now.Year);

                    account.BoxPrice1AmountNew = calcBoxAmountByDayesOsem(account.BoxPrice1, boxes.Where(b => b.BoxType == 1).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct);
                    account.BoxPrice2AmountNew = calcBoxAmountByDayesOsem(account.BoxPrice2, boxes.Where(b => b.BoxType == 2).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct);
                    account.BoxPrice3AmountNew = calcBoxAmountByDayesOsem(account.BoxPrice3, boxes.Where(b => b.BoxType == 3).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct);
                    account.BoxPrice4AmountNew = calcBoxAmountByDayesOsem(account.BoxPrice4, boxes.Where(b => b.BoxType == 4).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct);

                    account.BoxPrice1AmountExterm= (calcBoxExtermByDayesOsem(account.BoxPrice1, boxesExterm.Where(b => b.BoxType == 1 ).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct)) * -1;
                    account.BoxPrice2AmountExterm =( calcBoxExtermByDayesOsem(account.BoxPrice2, boxesExterm.Where(b => b.BoxType == 2 ).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct)) * -1;
                    account.BoxPrice3AmountExterm =( calcBoxExtermByDayesOsem(account.BoxPrice3, boxesExterm.Where(b => b.BoxType == 3 ).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct)) * -1;
                    account.BoxPrice4AmountExterm = (calcBoxExtermByDayesOsem(account.BoxPrice4, boxesExterm.Where(b => b.BoxType == 4 ).ToList(), currentMonth, 365 / numOfMonthToDiv, dateOfContruct)) * -1;

                    break;
                default:
                    break;
            }
        }

        private int calcBoxAmountByDayesNext(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                int numOfDayes = box.InsertDate.Value.CompareTo(currentDate);
                sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? 0 : numOfDayes;
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }
      private int calcBoxExtermByDayesNext(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                if (box.ExterminateDate != null)
                {
                    int numOfDayes = box.ExterminateDate.Value.CompareTo(currentDate);
                    sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? 0 : numOfDayes;
                }
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }
        private int calcBoxAmountByDayesBack(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                int numOfDayes = box.InsertDate.Value.CompareTo(currentDate);
                sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? numOfPeriodDayes : numOfDayes;
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }
        private int calcBoxExtermByDayesBack(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                int numOfDayes = box.ExterminateDate.Value.CompareTo(currentDate);
                sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? numOfPeriodDayes : numOfDayes;
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }
        private int calcBoxAmountByDayesBritman(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                DateTime insertDate = box.InsertDate.Value.AddMonths(1);
                DateTime date= new DateTime(box.InsertDate.Value.Year, box.InsertDate.Value.Month, 1);
                int numOfDayes = date.CompareTo(currentDate);
                sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? 0 : numOfDayes;
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }
         private int calcBoxExtermByDayesBritman(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                DateTime insertDate = box.ExterminateDate.Value.AddMonths(1);
                DateTime date= new DateTime(box.ExterminateDate.Value.Year, box.ExterminateDate.Value.Month, 1);
                int numOfDayes = date.CompareTo(currentDate);
                sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? 0 : numOfDayes;
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }
      private int calcBoxAmountByDayesOsem(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes,DateTime dateOfContract)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                int numOfDayes = box.InsertDate.Value.CompareTo(currentDate);
                sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? 0 : dateOfContract.CompareTo(box.InsertDate.Value);
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }
        private int calcBoxExtermByDayesOsem(decimal? boxPrice, List<Boxes> boxes, int currentMonth, int numOfPeriodDayes,DateTime dateOfContract)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumNumOfDayes = 0;
            foreach (var box in boxes)
            {
                int numOfDayes = box.ExterminateDate.Value.CompareTo(currentDate);
                sumNumOfDayes += numOfDayes >= numOfPeriodDayes ? 0 : dateOfContract.CompareTo(box.ExterminateDate.Value);
            }
            return sumNumOfDayes / numOfPeriodDayes;
        }

        private int calcTransport( List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumBoxes = 0;
            foreach (var box in boxes)
            {
                int numOfDayes = box.InsertDate.Value.CompareTo(currentDate);
                sumBoxes += numOfDayes >= numOfPeriodDayes ? 0 : 1;
            }
            return sumBoxes ;
        }

        private int calcCrushing(List<Boxes> boxes, int currentMonth, int numOfPeriodDayes)
        {
            DateTime currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            int sumBoxes = 0;
            foreach (var box in boxes)
            {
                int numOfDayes = box.Exterminate.Value.CompareTo(currentDate);
                sumBoxes += numOfDayes >= numOfPeriodDayes ? 0 : 1;
            }
            return sumBoxes;
        }
        private int calcRetrive(List<Orders> orders,Customers cust)
        {
            DateTime dateOfContruct = new DateTime(cust.DateOfContract.Value.Day, cust.DateOfContract.Value.Month, DateTime.Now.Month > cust.DateOfContract.Value.Month ? DateTime.Now.Year : DateTime.Now.Year - 1);
            int ord = orders.Where(x => x.BoxID != null && x.Urgent != true && x.Date>dateOfContruct).Count();
            int account = AccountController.get().Where(x => x.InsertDate > dateOfContruct).Select(x => x.RetriveAmount.Value).Sum();
            ord = ord - cust.MaxRetrive.Value-account;
            return ord;
        }
        private int calcRetriveUrgent(List<Orders> orders,Customers cust)
        {
            DateTime dateOfContruct = new DateTime(cust.DateOfContract.Value.Day, cust.DateOfContract.Value.Month, DateTime.Now.Month > cust.DateOfContract.Value.Month ? DateTime.Now.Year : DateTime.Now.Year - 1);
            int ord = orders.Where(x => x.BoxID != null && x.Urgent == true && x.Date>dateOfContruct).Count();
            int account = AccountController.get().Where(x => x.InsertDate > dateOfContruct).Select(x => x.UrgentRetriveAmount.Value).Sum();
            ord = ord - cust.maxUrgentRetrive.Value-account;
            return ord;
        }

    }
}
    