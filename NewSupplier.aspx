﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="NewSupplier.aspx.cs" Inherits="MaagareyArchive.NewSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script> 
        function onSaveClick() {
            var isValid = true;
            dgItems = $('#<%= dgResults.ClientID%>')[0];
            for (i = 0; i < dgItems.rows.length && isValid == true; i++)
            {
                if ($("[id*= 'cbCheck']", dgItems.rows[i]).length > 0) {
                    if ($("[id*= 'cbCheck']", dgItems.rows[i])[0].checked == true) {
                        if ($("[id*= 'txtPrice']", dgItems.rows[i])[0].value <= 0) {
                            isValid = false;

                        }
                    }
                }
            }
            
            dvErrorMessage = $('#<%= dvErrorMessage.ClientID%>')[0];
            dvSucceedMessage = $('#<%= dvSucceedMessage.ClientID%>')[0];
            if( isValid == false)
            {
                dvErrorMessage.innerText = "יש למלא מחיר לכל פריט מסומן";
                dvErrorMessage.style.display = "";
                dvSucceedMessage.style.display = "none";
                
            }
            else
            {
                dvErrorMessage.style.display = "none";
            }

            if (document.getElementById('<%= txtSupplierName.ClientID%>').value.trim() == "") {
                isValid = false;

                if (isValid == false) {
                    dvErrorMessage.style.display = "";
                    dvErrorMessage.innerText = "יש למלאות שם ספק";
                }
            }

            return isValid;
        }
    </script>
    <h1 id="header" runat="server">הסכם ספק</h1>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>

                        <div style="clear: both">
                            <div style="float: right; margin-top: 20px;">
                                <asp:RadioButton Text="ספק חדש" GroupName="supplierType" Checked="true" AutoPostBack="true" OnCheckedChanged="rbSupplierExist_CheckedChanged" runat="server" />
                                <asp:RadioButton Text="ספק קיים" ID="rbSupplierExist" AutoPostBack="true" OnCheckedChanged="rbSupplierExist_CheckedChanged" GroupName="supplierType" runat="server" />
                            </div>

                            <div id="dvSupplierName" runat="server" style="display: none; float: right; margin-top: 3px; margin-right: 20px;">
                                <label>בחר ספק</label>

                                <asp:DropDownList ID="ddlSupplierName" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                            <div id="dvSupplierID" style="float: right; margin-top: 3px; margin-right: 20px; display: none" runat="server">
                                <label>מספר ספק</label>
                                <asp:DropDownList ID="ddlSupplierID" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplierID_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                            <div runat="server" style="float: right; margin-top: 3px; margin-right: 20px;">
                                <span style="color:red">*</span>
                                <label>שם ספק</label>
                                <asp:TextBox ID="txtSupplierName" runat="server" />
                            </div>
                        </div>

                    </td>
                </tr>
                <tr>
                    <asp:DataGrid ID="dgResults" runat="server"
                        AutoGenerateColumns="false" CssClass="grid" Width="400px" Style="margin-bottom: 30px">


                        <HeaderStyle CssClass="header" />

                        <AlternatingItemStyle CssClass="alt" />
                        <ItemStyle CssClass="row" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="מס'" ItemStyle-CssClass="ltr">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate><%# rownum++  %></ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="פריט" ItemStyle-CssClass="ltr">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <span id="itemName" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ItemName")).ToString()%> </span>
                                    <span id="itemID" style="display: none; visibility: hidden" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ItemID"))==null?"":(DataBinder.Eval(Container.DataItem, "ItemID")).ToString()%></span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="בחר" ItemStyle-CssClass="ltr">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCheck" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="מחיר" ItemStyle-CssClass="ltr">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPrice" runat="server"  onkeypress="return isNumberKey(event)" />
                                </ItemTemplate>
                            </asp:TemplateColumn>

                        </Columns>
                    </asp:DataGrid>

                </tr>
                <table style="display: inline-block; line-height: 20px;">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="עיר" />
                            <asp:DropDownList ID="ddlCities" runat="server" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="כתובת" />
                            <input id="txtAddress" runat="server" class="form-control" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="תיאור" />
                            <input id="txtDescription" runat="server" class="form-control" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="איש קשר" />
                            <input id="txtContactName" runat="server" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server"  Text="טלפון 1" />
                            <input id="txtPhone1" runat="server"  onkeypress="return isNumberKey(event,10)" class="form-control" />
                        </td>
                        <td>
                            <asp:Label runat="server"  Text="טלפון 2" />
                            <input id="txtPhone2" runat="server"  onkeypress="return isNumberKey(event,10)"  class="form-control" />
                        </td>
                        <td>
                            <asp:Label runat="server"  Text="פקס" />
                            <input id="txtFax" runat="server"  onkeypress="return isNumberKey(event,10)" class="form-control"  />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="מייל" />
                            <input id="txtMail" runat="server" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="תנאי אשראי" />
                            <asp:DropDownList ID="ddlTemOfPayment" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="תאריך חתימת ההסכם" />
                            <input id="txtDateOfContract" disabled="disabled" runat="server" class="form-control" />
                        </td>
                        <td colspan="2">
                            <asp:Label runat="server" Text="הערות" />
                            <input id="txtComment" runat="server" onkeypress="return maxCharLength(event,256)" class="form-control" />
                        </td>
                    </tr>
                </table>
                <tr>
                </tr>
            </table>
             <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 462px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnSave" Text="שמור" OnClientClick="return onSaveClick();" OnClick="btnSave_Click" runat="server" />
                    </td>
                </tr>
                     <br></br>
                     <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display:none"></span><span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display:none"></span></br/>
                     </br/>
                 </caption>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
