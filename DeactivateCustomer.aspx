﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DeactivateCustomer.aspx.cs" Inherits="MaagareyArchive.DeactivateCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>גריעת לקוח</h1>
    <asp:DropDownList runat="server" ID="ddlCustomers" Width="250px" >

    </asp:DropDownList>
    <br/>
            <asp:RequiredFieldValidator ID="re2" class="dvErrorMessage" ControlToValidate="ddlCustomers" runat="Server" InitialValue="0" ErrorMessage="נא לבחור לקוח"></asp:RequiredFieldValidator>
<br />
 <table>
                <tr>
                    <td>
                       <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display:none"></span>
                       <span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display:none"></span>
                       <br/> 

                    </td>
                </tr>
            </table>
                 <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 462px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnDeactivate" Text="גריעת לקוח" OnClick="btnDeactivate_Click" runat="server" />
                    </td>
                </tr>
                 </table>
</asp:Content>
