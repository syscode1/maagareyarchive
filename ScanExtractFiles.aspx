﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanExtractFiles.aspx.cs" Inherits="MaagareyArchive.ScanExtractFiles" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>סריקת תיקים וארגזים שנשלפו
    </h1>
    <script>
        var soundObject = null;
        var isStop;
        var interval = null;
        function inputFile(evt) {
            debugger
            var dvError = document.getElementById('<%= dvErrorMessage.ClientID%>');
            if (evt.currentTarget.value.length > 13 || evt.currentTarget.value.length < 13) {
                {
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    return false;
                }

            }
            if (evt.currentTarget.value.length == 13) {
                if (evt.currentTarget.value.indexOf("261") != 0 &&
                    evt.currentTarget.value.indexOf("262") != 0) {
                    playSound();
                    dvError.style.display = "";
                    dvError.innerText = "מספר לא תקין";
                    evt.currentTarget.value = "";
                    return false;
                }
                else {
                    stop();
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    document.getElementById('<%=btnInputFileBoxID.ClientID %>').click();
                    return true;
                }

            }
        }

        function playSound() {
            debugger
            setTimeout("stop()", 30000);
            isStop = false;
            play(document.getElementById('<%= hfSoundPath.ClientID%>').value);
            blink();
        }



        function stop() {
            if (soundObject != null) {
                document.body.removeChild(soundObject);
                soundObject.removed = true;
                soundObject = null;
                isStop = true;
                clearInterval(interval);
                $('#blinkDiv')[0].style.visibility = "hidden";
            }
        }
        function play(src) {
            if (soundObject != null) {
                document.body.removeChild(soundObject);
                soundObject.removed = true;
                soundObject = null;
            }
            if (isStop == false) {
                setTimeout("play('" + src + "')", 3000);
            }
            else
                isStop = false;

            soundObject = document.createElement("embed");
            soundObject.setAttribute("src", src);

            soundObject.setAttribute("hidden", true);
            soundObject.setAttribute("autostart", true);
            soundObject.setAttribute("loop", true);
            document.body.appendChild(soundObject);
        }
        function blink() {
            var el = $('#blinkDiv');
            interval = window.setInterval(function () {
                el.toggleClass('blinking');
            }, 500);
        }
    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfSoundPath" Value="" runat="server" />
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <div id="blinkDiv" name="blinkDiv">
                            <asp:Image ImageUrl="resources/images/light.png" Style="width: 25px" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
            <div>
                <span>סרוק תיק או ארגז</span>

                <input type="text" id="txtFileBoxID" oninput="return inputFile(event)" maxlength="13" onkeypress="return isNumberKey(event)" runat="server"
                    style="width: 150px" />
                <asp:Button Style="display: none" ID="btnInputFileBoxID" runat="server" UseSubmitBehavior="true" OnClick="btnInputFileBoxID_Click" />

                <div id="dvErrorMessage" runat="server"></div>
                <div style="padding: 27px">
                    <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
