﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UsersManager.aspx.cs" Inherits="MaagareyArchive.UsersManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function clearAll() {
            var groupExterm = "input:checkbox";
            $(groupExterm).prop("checked", false);
            return false;
        }
        function checkAll(event, tbl) {
            debugger
            $("input:checkbox", $('#' + tbl)).prop("checked", event.checked);

        }
        function validate() {
            var isValid = true;
            var dvErrorMessage = document.getElementById('<%=dvErrorMessage.ClientID %>');
            dvErrorMessage.style.display = "none";
            dvErrorMessage.innerText = "";
            if (document.getElementById('<%=ddlCustomer.ClientID %>').value == "0") {
                dvErrorMessage.innerText += "יש לבחור לקוח\n";
                dvErrorMessage.style.display = "";
                isValid = false;
            }
            if (document.getElementById('<%=ddlJob.ClientID %>').value == "0") {
                dvErrorMessage.innerText += "יש לבחור תפקיד\n";
                dvErrorMessage.style.display = "";
                isValid = false;
            }
            if (document.getElementById('<%=rbCustomerExist.ClientID %>').checked == true) {
                if (document.getElementById('<%=ddlUsers.ClientID %>').value == "0" ||
                    document.getElementById('<%=ddlUsers.ClientID %>').value == "") {
                    dvErrorMessage.innerText += "יש לבחור משתמש\n";
                    dvErrorMessage.style.display = "";
                    isValid = false;
                }

            }
            else {
                if (document.getElementById('<%=txtUserName.ClientID %>').value.trim() == "" ||
                    document.getElementById('<%=txtPassword.ClientID %>').value.trim() == "") {
                    dvErrorMessage.innerText += "יש להזין שם משתמש וסיסמא\n";
                    dvErrorMessage.style.display = "";
                    isValid = false;
                }
            }
            return isValid;
        }
    </script>
    <h1>ניהול משתמשים
    </h1>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <table style="width: 600px; display: inline-grid;">
                <tr>
                    <td colspan="3">
                        <div style="float: right; margin-top: 20px;">
                            <asp:RadioButton Text="משתמש חדש" AutoPostBack="true" Font-Bold="true" ID="rbNew" runat="server" GroupName="customerType" OnCheckedChanged="rbCustomerExist_CheckedChanged" />
                            <asp:RadioButton Text="משתמש קיים" AutoPostBack="true" Font-Bold="true" ID="rbCustomerExist" runat="server" OnCheckedChanged="rbCustomerExist_CheckedChanged" GroupName="customerType" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>לקוח</span>
                        <asp:DropDownList ID="ddlCustomer" AutoPostBack="true" runat="server" Width="170px" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"></asp:DropDownList>
                    </td>
                    <td>
                        <span>שם משתמש</span>
                        <asp:DropDownList ID="ddlUsers" runat="server" Width="170px" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        <asp:TextBox ID="txtUserName" runat="server" Width="170px" Style="display: none" />
 
                    </td>
                    <td>
                       <span id="userNum" runat="server"></span>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                        
                    </td>
                    <td>
                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>סיסמא</span>
                        <input id="txtPassword" runat="server" style="width: 170px" />

                    </td>
                    <td>
                        <div id="dvSubject" runat="server">
                        <span>נושאים</span>
                        <asp:DropDownList ID="ddlSubject" runat="server" Width="170px"></asp:DropDownList>
                            </div>

                    </td>
                    <td style="width: 189px;">
                        <div id="dvJob" runat="server">
                        <span>תפקיד</span>
                        <asp:DropDownList ID="ddlJob" runat="server" Width="170px"></asp:DropDownList>
                            </div>
                    </td>
                </tr>
            </table>

            <h2>הרשאות להזמנת סוגי חומר</h2>

           
                <table id="tblDep" style="width: 600px; display: inline-grid; text-align: right;" cellpadding="10px">
                    <tr>
                        <td>
                            <label>
                                 <input id="cbAllDep" type="checkbox" runat="server" onchange="checkAll(this,'tblDep')" />
                בחר הכל</label>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">

                <label>
                    <input id="cbType1" type="checkbox" runat="server" />כללי</label>
            </td>
            <td style="width: 150px">
                <label>
                    <input id="cbType2" type="checkbox" runat="server" />הנהלת חשבונות</label>
            </td>
            <td style="width: 150px">
                <label>
                    <input id="cbType3" type="checkbox" runat="server" />תביעות</label>
            </td>
            <td style="width: 200px">
                <label>
                    <input id="cbType4" type="checkbox" runat="server" />רווחה, כ"א ,שכר</label>
            </td>
            <td style="width: 150px">
                <label>
                    <input id="cbType5" type="checkbox" runat="server" />הנדסה</label>
            </td>
        </tr>
            <tr>
                <td>
                    <label>
                        <input id="cbType6" type="checkbox" value="גבייה" runat="server" />גבייה</label>
                </td>
                <td>
                    <label>
                        <input id="cbType7" type="checkbox" value="עריכת דין" runat="server" />עריכת דין</label>
                </td>
                <td>
                    <label>
                        <input id="cbType8" type="checkbox" value="מדע" runat="server" />מדע</label>
                </td>
                <td>
                    <label>
                        <input id="cbType9" type="checkbox" value="לקוחות" runat="server" />לקוחות</label>
                </td>
                <td>
                    <label>
                        <input id="cbType10" type="checkbox" value="תביעות רכב" runat="server" />תביעות רכב</label>
                </td>
            </tr>
            </table>

    <h2>הרשאות לביצוע פעולות</h2>
            <table id="tblAction" style="width: 600px; display: inline-grid; text-align: right;" cellpadding="10px">
                <tr>
                    <td>
                        <input id="cbAllActions" type="checkbox" runat="server" onchange="checkAll(this,'tblAction')" />
                        בחר הכל</label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">

                        <label>
                            <input id="cbReport" type="checkbox" runat="server" />דוחות</label>
                    </td>
                    <td style="width: 150px">
                        <label>
                            <input id="cbExterminate" type="checkbox" runat="server" />גריסה</label>
                    </td>
                    <td style="width: 150px">
                        <label>
                            <input id="cbUrgent" type="checkbox" runat="server" />הזמנה דחופה</label>
                    </td>
                    <td style="width: 200px">
                        <label>
                            <input id="cbOrderSpecial" type="checkbox" runat="server" />הזמנת מתכלים</label>
                    </td>
                    <td style="width: 150px"></td>
                </tr>
            </table>
            <h2>הרשאות לביצוע פעולות</h2>
            <table id="tblActions2" style="width: 600px; display: inline-grid; text-align: right;" cellpadding="10px">
                <tr>
                    <td>
                        <div id="dvAllActions2" runat="server">
                        <input id="cbAllActions2" type="checkbox" runat="server" onchange="checkAll(this,'tblActions2')" />
                        בחר הכל</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        <div id="dvAdmin" runat="server">
                        <label>
                            <input id="cbAdmin" type="checkbox" runat="server" />אדמיניסטרטור</label>

                        </div>
                    </td>
                    <td style="width: 150px">
                         <div id="dvMaintence" runat="server">
                        <label>
                            <input id="cbMaintence" type="checkbox" runat="server" />מסך תחזוקה</label>
                         </div>
                    </td>
                    <td style="width: 150px">
                         <div id="dvCustomerScreen" runat="server">
                        <label>
                            <input id="cbCustomerScreen" type="checkbox" runat="server" />מסך לקוח</label>
                         </div>
                    </td>
                    <td style="width: 200px">
                         <div id="dvCollection" runat="server">
                        <label>
                            <input id="cbCollection" type="checkbox" runat="server" />ממשק גביה והנה"ח</label>
                         </div>
                    </td>
                    <td style="width: 150px">
                         <div id="dvWorksForDrivers" runat="server">
                        <label>
                            <input id="cbWorksForDrivers" type="checkbox" runat="server" />סידור עבודה</label>
                         </div>
                    </td>
                </tr>
                <tr>
                    <td>
                         <div id="dvTyping" runat="server">
                        <label>
                            <input id="cbTyping" type="checkbox" runat="server" />הקלדת חומר</label>
                         </div>
                    </td>
                    <td>
                         <div id="dvRetrive" runat="server">
                        <label>
                            <input id="cbRetrive" type="checkbox" runat="server" />שליפות והחזרות</label>
                         </div>
                    </td>
                    <td>
                         <div id="dvOrderItems" runat="server">
                        <label>
                            <input id="cbOrderItems" type="checkbox" runat="server" />ניהול ספקים</label>
                         </div>
                    </td>
                    <td>
                         <div id="dvSecondRetrive" runat="server">
                        <label>
                            <input id="cbSecondRetrive" type="checkbox" runat="server" />מסך שליפה שניה</label>
                         </div>
                    </td>
                    <td></td>
                </tr>
            </table>
            <div class="h1_sub"></div>
            <table style="line-height: 20px; width: 100%; border-spacing: 25px;">
                <tr>
                    <td>
                        <asp:Label runat="server" Text="עיר" />
                        <br />
                        <asp:DropDownList ID="ddlCities" runat="server" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="כתובת" />
                        <br />
                        <input id="txtAddress" runat="server" onkeypress="return maxCharLength(event,30)" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="תיאור" />
                        <br />
                        <input id="txtDescriptionAddress" onkeypress="return maxCharLength(event,30)" runat="server" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="טלפון 1" />
                        <br />
                        <input id="txtPhone1" runat="server" onkeypress="return isNumberKey(event,10)" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="טלפון 2" />
                        <br />
                        <input id="txtPhone2" runat="server" onkeypress="return isNumberKey(event,10)" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="פקס" />
                        <br />
                        <input id="txtFax" runat="server" onkeypress="return isNumberKey(event,10)" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="מייל" />
                        <br />
                        <input id="txtMail" runat="server" />
                    </td>
                </tr>
            </table>
            <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 388px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
                        <asp:Button ID="Button1" Text="נקה הכל" Width="140px" OnClientClick="return clearAll()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnUpdate" Text="עדכן" OnClientClick="return validate()" OnClick="btnUpdate_Click" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <input id="btnBackCustomer" type="button" value="חזרה למסך לקוח" onclick="exitCustomerScreen()" runat="server" style="display: none" />
                    </td>
                </tr>
            </table>
            <caption>
                <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display: none"></span><span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display: none"></span>
            </caption>
        </ContentTemplate>
    </asp:UpdatePanel>
          <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
