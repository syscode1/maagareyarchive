﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DaylyReport.aspx.cs" Inherits="MaagareyArchive.DaylyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $(document).ready(function () {
            document.getElementById('<%=txtFromDate.ClientID%>').value = '<%=DateTime.Now.ToString("yyyy-MM-dd")%>';
            document.getElementById('<%=txtToDate.ClientID%>').value = '<%=DateTime.Now.ToString("yyyy-MM-dd")%>';

        });

        function popup(mylink, windowname,type) {
            if (!window.focus)
                return true;
            var href;
            var w = 1200;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            var href = "DaylyReportDataPopup.aspx";
            var from=document.getElementById('<%=txtFromDate.ClientID%>').value ;
            var to = document.getElementById('<%=txtToDate.ClientID%>').value;
            window.open(href + "?dataType=" + type +"&from="+from +"&to="+to, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }
    </script>
    <h1>דו"ח יומי</h1>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>  
    <table style="display:inline-grid">
        <tr>
            <td>
                <span>מתאריך </span>
                <br />
                <input type="date" runat="server" id="txtFromDate" />
            </td>
            <td>
                <span>עד תאריך</span>
                <br />
                <input type="date" runat="server" id="txtToDate" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button Text="הצג" ID="showReport" Width="100px" OnClick="showReport_Click" runat="server" />
            </td>
        </tr>
    </table>

    <table style="width:100%">
        <tr>
            <td>
    <table border="1" cellpadding="10" cellspacing="0" style="text-align: right;display:inline-grid">

        <tr>
            <td> 
                <a style="color:black" onclick="return popup(this, '','notExistExtractions')">
                <span><b><u>שליפות שלא נמצאו</u></b></span></a>
            </td>
            <td>
                  
                   <span id="spnNotExistExtractions" runat="server"><b></b></span>
            </td>
        </tr>
        <tr>
            <td>
                <a style="color:black" onclick="return popup(this, '','fullNewBoxes')">
                <span><b><u>תיבות חדשות עם תוכן</u></b></span></a>
            </td>
            <td>
                <span id="spnFullNewBoxes" runat="server"></span>
            </td>
        </tr>
        <tr>
            <td>   
                <a style="color:black" onclick="return popup(this, '','emptyNewBoxes')">
                <span><b><u>תיבות חדשות בלי תוכן</u></b></span></a>
            </td>
            <td>
                <span id="spnEmptyNewBoxes" runat="server"></span>
            </td>
        </tr>
        <tr>
            <td>
                <a style="color:black" onclick="return popup(this, '','newPlacedBoxes')">
                <span><b><u>תיבות חדשות שמוקמו</u></b></span></a>

            </td>
            <td>
                <span id="spnNewPlacedBoxes" runat="server"></span>
            </td>
        </tr>
        <tr>
            <td>
                <a style="color:black" onclick="return popup(this, '','notPlacedBoxes')">
                <span><b><u>תיבות חדשות שטרם מוקמו</u></b></span></a>

            </td>
            <td>
                <span id="spnNewNotPlacedBoxes" runat="server"></span>
            </td>
        </tr>
        <tr style="background-color: lightgray">
            <td>
                <a style="color:black" onclick="return popup(this, '','newBoxes')">
                <span><b><u>סה"כ תיבות חדשות</u></b></span></a>
            </td>
            <td>
                <span id="spnNewBoxes" runat="server"><b></b></span>
            </td>
        </tr>
        <tr>
            <td>
                               <a style="color:black" onclick="return popup(this, '','boxesForExterminate')">
                <span><b><u>גריסות ממתינות לביצוע</u></b></span></a>
            </td>
            <td>
                <span id="spnBoxesForExterminate" runat="server"><b></b></span>
            </td>
        </tr>
    </table>
                </td>
        </tr>
    </table>
    <table style="width:100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:DataGrid ID="dgResults" runat="server"
                    AutoGenerateColumns="false" CssClass="grid"
                    EnableViewState="true" AllowSorting="true"
                    ShowFooter="true" Width="200px" style="margin-top:10px"
                    OnItemDataBound="dgResults_ItemDataBound">
                    <%--DataKeyField="FileID"--%>

                    <HeaderStyle CssClass="header" />

                    <AlternatingItemStyle CssClass="alt" />
                    <ItemStyle CssClass="row" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="מחסן" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(DataBinder.Eval(Container.DataItem, "ShelfID"))%></span>
                            </ItemTemplate>
                            <FooterTemplate>
                                <b>סה"כ:</b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="כמות" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#Convert.ToInt16(DataBinder.Eval(Container.DataItem, "NumBoxCapacity")) -Convert.ToInt16 (DataBinder.Eval(Container.DataItem, "NumBoxesOut"))%></span>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label Font-Bold="true" ID="lblSum" runat="server"  />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>

                </asp:DataGrid>
            </td>

        </tr>
        <tr>
            <td>
                <div style="padding-top:10px">
                    <asp:Label Text="מלאי כולל של תיבות" ID="sumBoxes" BackColor="Yellow" Style="text-align: center" Font-Size="24" runat="server" />

                </div>

            </td>
        </tr>
        <tr>
            <td style="padding-top:30px">
                <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
            </td>
        </tr>
    </table>
             </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
