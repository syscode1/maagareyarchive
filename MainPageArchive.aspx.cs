﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Drawing;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System.Collections;

namespace MaagareyArchive
{
    public partial class MainPageArchive : BasePage
    {
            protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

            protected void Page_Load(object sender, EventArgs e)
            {
                if (!Page.IsPostBack)
                {
                    fillddlCustomers();
                    string customerID = Request.QueryString["custID"];
                    if (!string.IsNullOrEmpty(customerID))
                    {
                        ddlCustomerID.SelectedValue = customerID;
                        ddlCustomerID_SelectedIndexChanged(null, null);
                    }
                   else if(Session["MainPageArchive_customerID"]!=null)
                {
                    if(ddlCustomerID.Items.FindByValue(Session["MainPageArchive_customerID"].ToString())!=null)
                    {

                        ddlCustomerID.SelectedValue = Session["MainPageArchive_customerID"].ToString();
                        ddlCustomerID_SelectedIndexChanged(null, null);
                    }
                }

                }
            }

            private void fillddlCustomers()
            {

            ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
            ddlCustomerID.DataSource = (new MyCache()).CustomersIds;

            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();

            ddlCustomerID.DataTextField = "Value";
            ddlCustomerID.DataValueField = "Key";
            ddlCustomerID.DataBind();
        }
            private void fillDdl(DropDownList ddl, object data)
            {
                ddl.DataSource = data;
                ddl.DataTextField = "Value";
                ddl.DataValueField = "Key";
                ddl.DataBind();

            }

            protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
            {
                ddlCustomerID.SelectedValue = ddlCustomerName.SelectedValue;
                int customerID = Helper.getInt(ddlCustomerName.SelectedValue).Value;
            bindData(customerID);
            }

            protected void ddlCustomerID_SelectedIndexChanged(object sender, EventArgs e)
            {
                ddlCustomerName.SelectedValue = ddlCustomerID.SelectedValue;
                int customerID = Helper.getInt(ddlCustomerID.SelectedValue).Value;
                bindData(customerID);
            }
            private void bindData(int customerID)
            {
            Session["MainPageArchive_customerID"] = customerID;
            Customers cust = CustomersController.getCustomerByID(customerID);
            if (cust != null)
            {
                txtAddress.Value = cust.FullAddress;
                txtContactName.Value = cust.ContactMan;
                //    txtDateOfContract.Value = cust.DateOfContract.ToString();
                //    txtDescription.Text = cust.Description;
                txtDescriptionAddress.Value = cust.DescriptionAddress;
                txtFax.Value = cust.Fax;
                txtMail.Value = cust.Email;
                txtPhone1.Value = cust.Telephon1;
                txtPhone2.Value = cust.Telephon2;
                //      txtPO.Value = cust.PO;
                if (ddlCities.Items.FindByText(cust.CityAddress.TrimStart().TrimEnd()) != null)
                    ddlCities.SelectedValue = cust.CityAddress.TrimStart().TrimEnd();

                int sumBoxes = Convert.ToInt32(BoxesController.getBoxesCount(customerID, new List<int>()));
                txtSumBoxes.Value = sumBoxes.ToString();
                txtSumEmpty.Value = BoxesController.calcNumOfEmptyBoxes(customerID, sumBoxes).ToString();

                dgResultsR.DataSource = OrdersController.getWorkOrders(customerID);
                dgResultsR.DataBind();
            }
            }



            protected void dgResultsR_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    WorkOrderingUI dataItem = e.Row.DataItem as WorkOrderingUI;
                    if (dataItem.Urgent == true)
                        e.Row.BackColor = Color.Pink;
                    if (dataItem.ShippingRowID != null)
                    {
                        e.Row.Enabled = false;
                        e.Row.BackColor = dataItem.Urgent == true ? Color.Pink : Color.LightGray;

                    }
                }
            }
            protected void btnUpdateCust_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("CustomerAgreement.aspx?custID=" + ddlCustomerID.SelectedValue);
            }

            protected void btnmanegeDepartments_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")

                    Response.Redirect("CustomerDepartments.aspx?custID=" + ddlCustomerID.SelectedValue);

            }

            protected void btnManegeUsers_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")

                    Response.Redirect("UsersManagerList.aspx?custID=" + ddlCustomerID.SelectedValue);

            }
            protected void btnOrders_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("ExportOrders.aspx?custID=" + ddlCustomerID.SelectedValue);

            }

            protected void btnExterminate_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("ExterminateRequiering.aspx?custID=" + ddlCustomerID.SelectedValue);
            }

            protected void btnExterminateReport_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("ExportExterminated.aspx?custID=" + ddlCustomerID.SelectedValue);
            }

            protected void btnPrices_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("CustomerAgreement.aspx?custID=" + ddlCustomerID.SelectedValue + "&isDisable=1");
            }

            protected void btnWorkOrdering_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("WorkOrderingHistory.aspx?custID=" + ddlCustomerID.SelectedValue);

            }

            protected void btnDeliveryConfirmation_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("DeleveryConfirmation.aspx?custID=" + ddlCustomerID.SelectedValue);

            }
            protected void btnCustomerDepartment_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("CustomerDepartments.aspx?custID=" + ddlCustomerID.SelectedValue);

            }
            protected void btnCustomerSubject_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("CustomerSubjects.aspx?custID=" + ddlCustomerID.SelectedValue);
            }

            protected void btnCustomerBoxesSale_Click(object sender, EventArgs e)
            {
                if (ddlCustomerID.SelectedValue != "0" || ddlCustomerName.SelectedValue != "0")
                    Response.Redirect("CustomerBoxesSale.aspx?custID=" + ddlCustomerID.SelectedValue);
            }
        

    }
  
}