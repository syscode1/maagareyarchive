﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Extraction.aspx.cs" Inherits="MaagareyArchive.Extraction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <meta http-equiv="refresh" content="180" />
    <script>
        var soundObject = null;
        var isStop;
        var interval = null;
        $(document).ready(function () {
            isStop = false;
            if (document.getElementById('<%= hfIsUrgent.ClientID%>').value != '') {
      <%--           playSound(document.getElementById('<%= hfIsUrgent.ClientID%>').value);--%>
                blink(5000)
            }
        });
        function blink(timeOut) {
            var el = $('#blinkDiv');
            interval = window.setInterval(function () {
                el.toggleClass('blinking');
            }, 500);
            setTimeout(function () { $('#blinkDiv')[0].style.visibility = "hidden"; }, timeOut);
        }
        function playSound(src) {
            debugger
            document.getElementById('<%= hfIsUrgent.ClientID%>').value = '';
            setTimeout("stop()", 2000);
            play(src);
            blink(1500);
            

        }



        function stop() {
            debugger
            if (soundObject != null) {
                document.body.removeChild(soundObject);
                soundObject.removed = true;
                soundObject = null;
                isStop = true;
                clearInterval(interval);
                
            }
            $('#blinkDiv')[0].style.visibility = "hidden";
        }
        function play(src) {
            if (soundObject != null) {
                document.body.removeChild(soundObject);
                soundObject.removed = true;
                soundObject = null;
            }
            if (isStop == false) {
                setTimeout("play('" + src + "')", 0);
            }
            else
                isStop = false;

            soundObject = document.createElement("embed");
            soundObject.setAttribute("src", src);

            soundObject.setAttribute("hidden", true);
            soundObject.setAttribute("autostart", true);
            soundObject.setAttribute("loop", true);
            document.body.appendChild(soundObject);

        }
        function check_click(event) {
            if (event.checked) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var groupUrgent = "input:checkbox[group='urgent']";
                var groupRegular = "input:checkbox[group='regular']";
                var groupExterm = "input:checkbox[group='exterm']";

                var group = event.attributes["group"].value;
                if (group != 'urgent')
                    $(groupUrgent).prop("checked", false);
                if (group != 'regular')
                    $(groupRegular).prop("checked", false);
                if (group != 'exterm')
                    $(groupExterm).prop("checked", false);
            }
        }

        function regular_click(select) {
            debugger
            var groupRegular = "input:checkbox:enabled[group='regular']";
            $(groupRegular).prop("checked", select);


            var groupUrgent = "input:checkbox[group='urgent']";
            var groupExterm = "input:checkbox[group='exterm']";

            $(groupExterm).prop("checked", false);
            $(groupUrgent).prop("checked", false);

            return false;
        }

        function exterm_click(select) {
            var groupExterm = "input:checkbox[group='exterm']";
            $(groupExterm).prop("checked", select);

            var groupUrgent = "input:checkbox[group='urgent']";
            var groupRegular = "input:checkbox[group='regular']";

            $(groupRegular).prop("checked", false);
            $(groupUrgent).prop("checked", false);

            return false;
        }
        function ddlCustomer_change(event)
        {
            var allCheck = "input:checkbox[id*='cbCheck']";
            $(allCheck).prop("checked", false);
            var customerGroup = "input:checkbox:enabled[customer='" + event.value + "']";
            $(customerGroup).prop("checked", true);
            
        }
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }

        function inputFile(evt) {
            
            var dvError = document.getElementById('<%= dvErrorMessage.ClientID%>');
            if (evt.currentTarget.value.length > 13 || evt.currentTarget.value.length < 13) {
                {
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    return false;
                }

            }
            if (evt.currentTarget.value.length == 13) {
                if (evt.currentTarget.value.indexOf("261") != 0 &&
                    evt.currentTarget.value.indexOf("262") != 0) {
                    blink()
                    dvError.style.display = "";
                    dvError.innerText = "מספר לא תקין";
                    evt.currentTarget.value = "";
                    return false;
                }
                else {
                    stop();
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    document.getElementById('<%=btnInputFileBoxID.ClientID %>').click();
                    return true;
                }

            }
        }




 
    </script>
    <asp:HiddenField ID="hfIsUrgent" Value="" runat="server" />
    <h1>שליפות
    </h1>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
    <table width="100%">
        <tr>
            <td colspan="3">
                <div id="blinkDiv" name="blinkDiv">
                    <asp:Image ImageUrl="resources/images/light.png" Style="width: 25px" runat="server" />
                </div>
            </td>
        </tr>
        <tr>

            <td style="width: 200px">
                <table>
                    <tr>
                        <td>
                            <div style="float: right; border-style: solid; margin: 5px; padding: 5px; width: 160px;">
                                <span style="float: right">סה"כ תיקים לשליפה:</span>
                                <asp:Label ID="sumOfFilesToRetrieve" Font-Bold="true" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: right; border-style: solid; margin: 5px; padding: 5px; width: 160px;">
                                <span style="float: right">סה"כ ארגזים לביעור:</span>
                                <asp:Label ID="sumOfBoxesToExterm" Font-Bold="true" runat="server" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <div>
                    <table border="1" id="tblWarehouse" runat="server" style="height: 79px; border-style: solid; border-width: 3px; border-color: black; width: 400px;">
                        <tr>
                            <td style="background-color: azure">מחסן</td>
                        </tr>
                        <tr>
                            <td style="background-color: azure">כמות בטיפול</td>
                        </tr>
                        <tr>
                            <td style="background-color: azure">כמות לא מהוקצע</td>
                        </tr>
                    </table>
                </div>
            </td>

            <td>
                            <asp:HiddenField ID="hfSoundPath" Value="" runat="server" />
             <div style="height: 71px;border-style: solid; border-width: 4px;border-color: black;">
                 <div class="h1_sub" style="    font-weight: bold;    font-size: 15px;margin-bottom:2px;width:92%">
סריקת תיקים שנשלפו</div>
                <span>סרוק תיק או ארגז</span>

                <input type="text" id="txtFileBoxID" oninput="return inputFile(event)" maxlength="13" onkeypress="return isNumberKey(event)" runat="server"
                    style="width: 150px" />
                <asp:Button Style="display: none" ID="btnInputFileBoxID" runat="server" UseSubmitBehavior="true" OnClick="btnInputFileBoxID_Click" />

                <div id="dvErrorMessage" runat="server"></div>
            </div>
            </td>
        </tr>
    </table>


    <div>
        <asp:DataGrid ID="dgResults" runat="server"
            AutoGenerateColumns="false" CssClass="grid"
            EnableViewState="true" AllowSorting="true" OnItemDataBound="dgResults_ItemDataBound">
            <%--DataKeyField="FileID"--%>

            <HeaderStyle CssClass="header" />

            <AlternatingItemStyle CssClass="alt" />
            <ItemStyle CssClass="row" />
            <Columns>
                <asp:TemplateColumn HeaderText="בחר" ItemStyle-CssClass="ltr">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate>
                        <input type="checkbox" group='<%#DataBinder.Eval(Container.DataItem, "Urgent")!=null && (bool)DataBinder.Eval(Container.DataItem, "Urgent")==true? "urgent":
                                (DataBinder.Eval(Container.DataItem, "isExterm")!=null && (bool)DataBinder.Eval(Container.DataItem, "isExterm")==true?"exterm" : "regular")%>'
                            customer='<%# DataBinder.Eval(Container.DataItem, "CustomerID")%>'
                            id="cbCheck" runat="server" onclick="check_click(this)" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="הזמנה" ItemStyle-CssClass="ltr" SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><a  href="<%# href+ DataBinder.Eval(Container.DataItem, "OrderID")%> " ><span id="spnOrderID" runat="server"><%#DataBinder.Eval(Container.DataItem, "OrderID")%></span> </a></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="לקוח" ItemStyle-CssClass="ltr" SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "CustomerID")%></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="כמות" ItemStyle-CssClass="ltr" SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "count")%></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="זמן הזמנה" ItemStyle-CssClass="ltr" SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Date")%></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="דחיפות" ItemStyle-CssClass="ltr" SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Urgent")!=null && Convert.ToBoolean( DataBinder.Eval(Container.DataItem, "Urgent"))==true?"דחוף":"רגיל" %></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="הערות" ItemStyle-CssClass="ltr" SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "DescriptionAddress")%></ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="הודפס" ItemStyle-CssClass="ltr">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate>
                        <asp:CheckBox ID="cbPrinted" Enabled="false" runat="server" Checked=' <%#DataBinder.Eval(Container.DataItem, "EmployID")!=null && !string.IsNullOrEmpty( DataBinder.Eval(Container.DataItem, "EmployID").ToString())? true :false%>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="סטטוס" ItemStyle-CssClass="ltr">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate>
                        <ItemTemplate><span id="spnEmpName" runat="server">
                            <%# DataBinder.Eval(Container.DataItem, "EmployID")!=null && !string.IsNullOrEmpty( DataBinder.Eval(Container.DataItem, "EmployID").ToString())?"הוקצע" :(DataBinder.Eval(Container.DataItem, "Print")!=null && Convert.ToBoolean( DataBinder.Eval(Container.DataItem, "Print").ToString()) ==true?"חלקי" :"")%></span></ItemTemplate>
                    </ItemTemplate>
                </asp:TemplateColumn>


            </Columns>
        </asp:DataGrid>

    </div>
    <div>
        <table style="margin: 0px; border-spacing: 0px;width:100%">
            <tr>
                <td style=" text-align: right;">
                    <input type="button" value="יציאה" onclick="exit()" runat="server" />

                </td>
                <td >
                    <table style="width: 100%; height: 58px; padding-top: 10px; height: 106px;">
                        <tr>
                            <td ><a onclick="return regular_click(true)" href="#">בחר הכל שליפות</a>

                            </td>
                            <td >
                                <a href="#" onclick="return exterm_click(true);">בחר הכל גריסה</a>

                            </td>
                        </tr>
                        <tr>
                            <td><a onclick="return regular_click(false);" href="#">נקה הכל שליפות</a>

                            </td>
                            <td >
                                <a href="#" onclick="return exterm_click(false);">נקה הכל גריסה</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                                <asp:DropDownList ID="ddlCustomer" onchange="ddlCustomer_change(this)" runat="server" Width="170px">
                                </asp:DropDownList>
                                     <asp:DropDownList ID="ddlWarehouse" OnSelectedIndexChanged="ddlWarehouse_SelectedIndexChanged" AutoPostBack="true"  runat="server" Width="170px">
                                </asp:DropDownList>
                                 <asp:DropDownList ID="ddlSite" OnSelectedIndexChanged="ddlSite_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>

                </td>
                <td>
                    <asp:Button Text="הפק דוח" ID="btnReport" OnClick="btnReport_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />

                </td>
            </tr>
            <tr>
<td></td>
                <td></td>
                <td>
                    <asp:Button Text="הצג הכל" ID="btnShowAll" OnClick="btnShowAll_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />

                </td>
            </tr>
        </table>

    </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
