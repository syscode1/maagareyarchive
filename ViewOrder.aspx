﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ViewOrder.aspx.cs" Inherits="MaagareyArchive.ViewOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

         function urgentOrderCheck(button) {
            if (button.checked) {
                var userUrgentPermission = '<%= user.PermForUrgentDelivery%>';
                if (userUrgentPermission == 'False') {
                    alert("ההרשאה להזמנת חומר דחוף חסומה.")
                    return false;
                }
                else {
                    var isConfirm = confirm("משלוח דחוף מחייב בתשלום נוסף")
                    if (!isConfirm)
                        button.checked = false;
                }
            }

        }

        function radioButtonChange(radioButton) {

            //todo: disable / enable fields
        }
        function orderClick() {
            debugger
            var isValid=true;
            var rb=document.getElementById('<%= rbDepartmentAddress.ClientID%>');
            if(rb.checked)
            {
                var tbl = $("[id*= 'tblAddressDdl']")[0];
                var ddl = $("[id*= 'ddlPlace']")[0];
                if (tbl.style.display == "")
                {
                    if (ddl.value == 0) {
                        isValid = false;
                    }
                }
            }
            else
            {
                var rbNew = document.getElementById('<%= rbNewAddress.ClientID%>');
                if (rbNew.checked && ( document.getElementById('<%= txtAddress.ClientID%>').value.trim() == "" ||
                    document.getElementById('<%= txtContactName.ClientID%>').value.trim() == "" ||
                    document.getElementById('<%= txtDescription.ClientID%>').value.trim() == "" ||
                    document.getElementById('<%= txtPhone.ClientID%>').value.trim() == ""))
                    isValid = false;
            }
            if (isValid == false) {
                dvErrorMessage.style.display = "";
                dvErrorMessage.innerText = "יש למלאות את כל שדות הכתובת";
            }
            return isValid;
        }
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }
        function popup(mylink, windowname, fileID) {
            if (!window.focus)
                return true;
            var href;
            var w = 500;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            if (typeof (mylink) == 'string')
                href = mylink;
            else href = mylink.href;
            window.open(href + "?fileID=" + fileID, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }
        function checkRb(id) {
            debugger
            if (id == 'departmentAddress')
            {
                document.getElementById('<%= rbDepartmentAddress.ClientID%>').checked = true;
            }
            else if (id == 'newAddress') {
                document.getElementById('<%= rbNewAddress.ClientID%>').checked = true;
            }
        }
    </script>
    <asp:UpdatePanel id="up" runat="server">
        <ContentTemplate>
            <h1>הזמנה</h1>
            <asp:DataGrid ID="dgResults" runat="server"
                AutoGenerateColumns="false" CssClass="grid"
                EnableViewState="true">
                <%--DataKeyField="FileID"--%>
                <HeaderStyle CssClass="header" />

                <AlternatingItemStyle CssClass="alt" />
                <ItemStyle CssClass="row" />
                <Columns>
                    <asp:TemplateColumn HeaderText="מס'" ItemStyle-CssClass="ltr">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate><%# rownum++ %></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="מחלקה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="departmentID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "DepartmentID")).ToString()%> </span>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="תיק" ItemStyle-CssClass="ltr" SortExpression="FileID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <a href="FileStatusHistory.aspx" onclick="return popup(this, 'notes','<%#(DataBinder.Eval(Container.DataItem, "FileID"))%>')">
                                <span id="fileID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "FileID"))==null?"":(DataBinder.Eval(Container.DataItem, "FileID")).ToString()%></span></a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="ארגז" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="boxID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "BoxID")).ToString()%> </span>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="תאור" ItemStyle-CssClass="ltr" SortExpression="Description1">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Description1")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="משנה" ItemStyle-CssClass="ltr" SortExpression="StartYear">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "StartYear")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="עד שנה" ItemStyle-CssClass="ltr" SortExpression="EndYear">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "EndYear")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="ממספר" ItemStyle-CssClass="ltr" SortExpression="ScanNumStart">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "ScanNumStart")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="עד מספר" ItemStyle-CssClass="ltr" SortExpression="ScanNumEnd">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "ScanNumEnd")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="מייל" ItemStyle-CssClass="ltr" SortExpression="ScanNumEnd">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <asp:CheckBox Enabled="false" runat="server" Checked='<%#(DataBinder.Eval(Container.DataItem, "Email")!=null && Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Email"))==true)? true :false%>'/>
                            </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="פקס" ItemStyle-CssClass="ltr" SortExpression="ScanNumEnd">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                               <asp:CheckBox Enabled="false" runat="server" Checked='<%#(DataBinder.Eval(Container.DataItem, "Fax")!=null && Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Fax"))==true)? true :false%>'/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="הערות" ItemStyle-CssClass="ltr" SortExpression="ScanNumEnd">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "Comment")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="הסר" ItemStyle-CssClass="ltr" ItemStyle-BackColor="#90EE90">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="cbRemove" runat="server" /></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="דחוף" ItemStyle-CssClass="ltr" ItemStyle-BackColor="#fbc5c2">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="cbUrgent" Checked='<%# (DataBinder.Eval(Container.DataItem, "Urgent")!=null?(bool)DataBinder.Eval(Container.DataItem, "Urgent"):false)%>'  OnClick="return urgentOrderCheck(this)" runat="server" />
                        </ItemTemplate>
                        <%--OnClick="urgentOrderCheck(this)"--%>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>

            <div>
                <table style="padding: 20px;">
                    <tr>
                        <td style="padding: 22px;">
                            <asp:RadioButton ID="rbDepartmentAddress" runat="server" GroupName="addressGroup" onclick="radioButtonChange(this)" Checked="true" />
                        </td>
                        <td colspan="5" style="text-align: right; direction: rtl;">
                            <table id="tblAddressDdl" runat="server" style="display: none">
                                <tr>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlPlace" onfocus="checkRb('departmentAddress')">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbNewAddress" runat="server" GroupName="addressGroup" onclick="radioButtonChange(this)" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCities" runat="server" onfocus="checkRb('newAddress')" />
                        </td>
                        <td>
                            <span style="color:red;color: red; vertical-align: top;" >*</span>
                        </td>
                        <td>

                            <input id="txtAddress" runat="server" placeholder="כתובת" onfocus="inputFocus(this)" class="form-control" />
                        </td>
                        <td>
                            <span style="color:red;color: red; vertical-align: top;" >*</span>
                        </td>
                        <td>
                            <input id="txtDescription" runat="server" placeholder="תיאור" onfocus="inputFocus(this)" class="form-control" />
                        </td>
                       <td>
                            <span style="color:red;color: red; vertical-align: top;" >*</span>
                        </td>
                        <td>
                            <input id="txtContactName" runat="server" placeholder="איש קשר" onfocus="inputFocus(this)" class="form-control" />
                        </td>
                        <td>
                            <span style="color:red;color: red; vertical-align: top;" >*</span>
                        </td>
                        <td>
                            <input id="txtPhone" runat="server" placeholder="טלפון"  onkeypress="return isNumberKey(event,10)" onfocus="inputFocus(this)" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="9">
                                                       <input id="txtComment" runat="server" placeholder="הערות להזמנה"  onkeypress="return maxCharLength(event,64)" onfocus="inputFocus(this)" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:10px">
                            <asp:RadioButton ID="rbOffice" runat="server" GroupName="addressGroup" onclick="radioButtonChange(this)" Checked="false" />
                        </td>
                        <td colspan="5" style="text-align: right; direction: rtl;">
                            <asp:Label Text="משרד" runat="server" />
                        </td>
                        </tr>
                    <tr>
                        <td >
                            <asp:RadioButton ID="rbFiling" runat="server" GroupName="addressGroup" onclick="radioButtonChange(this)" Checked="false" />
                        </td>
                        <td colspan="5" style="text-align: right; direction: rtl;">
                            <asp:Label Text="תיוקים" runat="server" />
                        </td>
                        </tr>
                </table>



            </div>
             <span id="dvErrorMessage" class="dvErrorMessage" style="display:none"></span>
            <table style="float: left;width:100%">
                <tr>
                     <td style="width: 462px;text-align: right;">
                    <input type="button" value="יציאה" onclick="exit()" runat="server" />
                </td>
                    <td>
                        <asp:Button ID="btnRefresh" Text="רענן" runat="server" OnClick="btnRefresh_Click" Style="margin-bottom: 10px; width: 130px" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td  style="float:left">
                        <asp:Button ID="btnOrder" Text="הזמן" runat="server" OnClientClick="return orderClick()" OnClick="btnOrder_Click" Style="margin-bottom: 10px; width: 130px" />
                    </td>
                    <%--OnClientClick="return getSelectedFiles()"--%>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

     <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
