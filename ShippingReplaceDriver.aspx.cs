﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ShippingReplaceDriver : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
if(!Page.IsPostBack)
            {
                int rowID = string.IsNullOrEmpty(Request.QueryString["rowID"]) ? 0 : Helper.getIntNotNull(Request.QueryString["rowID"]);
                ShippingsUI shipping= ShippingController.getSipping(rowID);
                if (shipping != null)
                {
                    dgResultsR.DataSource = ShippingController.getGroupShippings(DateTime.MinValue, DateTime.MaxValue, "").Where(x => x.ShippingID != shipping.ShippingID);
                    dgResultsR.DataBind();
                }
            }
        }

        protected void cbMove_CheckedChanged(object sender, EventArgs e)
        {
            string newShippingID = hfShippngChecked.Value;
            int rowID = string.IsNullOrEmpty(Request.QueryString["rowID"]) ? 0 : Helper.getIntNotNull(Request.QueryString["rowID"]);
            List<ShippingsUI> shippings = ShippingController.getSippings(newShippingID);
            if (shippings.Count > 0 && rowID > 0)
            {

                int numInShipping = ShippingController.getSippings(newShippingID).Select(x => x.NumInShipping).Max();
                ShippingController.updateShippingDriver(rowID, newShippingID, numInShipping + 1, hfDriverChecked.Value);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "window.opener.location.reload();window.close()", true);

            }
        }
    }
}