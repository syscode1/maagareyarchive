﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="CustomerMail.aspx.cs" Inherits="MaagareyArchive.CustomerMail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
               <asp:DataGrid ID="dgResults" runat="server"
                    AutoGenerateColumns="false" CssClass="grid"
                    EnableViewState="true" AllowSorting="true"
                    ShowFooter="true" Width="200px" style="margin-top:10px">
                    <%--DataKeyField="FileID"--%>

                    <HeaderStyle CssClass="header" />

                    <AlternatingItemStyle CssClass="alt" />
                    <ItemStyle CssClass="row" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="תאריך" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(DataBinder.Eval(Container.DataItem, "Date"))%></span>
                            </ItemTemplate>
                            <FooterTemplate>
                                <b>סה"כ:</b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="מחלקה" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#DataBinder.Eval(Container.DataItem, "DepartmentID")%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="שם" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#DataBinder.Eval(Container.DataItem, "UserName")%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="נושא" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#DataBinder.Eval(Container.DataItem, "EmailSubject")%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="גוף" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#DataBinder.Eval(Container.DataItem, "EmailBody")%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>

                </asp:DataGrid>
</asp:Content>
