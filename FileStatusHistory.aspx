﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" CodeBehind="FileStatusHistory.aspx.cs" Inherits="MaagareyArchive.FileStatusHistory" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        function popup( shippingID) {
            if (!window.focus)
                return true;
            var href;
            var w = 800;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open("ShippingReceivedReport.aspx" + "?shippingID=" + shippingID, '', 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }

    </script>
    <h1 id="header" runat="server"> </h1>
    <div>
         <asp:DataGrid  ID="dgResults" runat="server"
             AutoGenerateColumns="false"  CssClass="gridPopup"
             EnableViewState="true" ><%--DataKeyField="FileID"--%>

            <HeaderStyle CssClass="header"/>
           
            <AlternatingItemStyle CssClass="alt" />
            <ItemStyle CssClass="row" />
            <Columns>
                <asp:TemplateColumn HeaderText="מס'" ItemStyle-CssClass="ltr">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%# rownum++  %></ItemTemplate>
                </asp:TemplateColumn>  
                   <asp:TemplateColumn HeaderText="תאריך" ItemStyle-CssClass="ltr" SortExpression="FileID">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate>
                        <%#(DataBinder.Eval(Container.DataItem, "StatusDate")).ToString()%></ItemTemplate>
                </asp:TemplateColumn>  
                  <asp:TemplateColumn HeaderText="משתמש" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#(DataBinder.Eval(Container.DataItem, "FullName")).ToString()%></ItemTemplate>
                </asp:TemplateColumn>  
                  <asp:TemplateColumn HeaderText="סטטוס" ItemStyle-CssClass="ltr"  SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "StatusName").ToString()%></ItemTemplate>
                </asp:TemplateColumn>  
                                  <asp:TemplateColumn HeaderText="הערות" ItemStyle-CssClass="ltr"  SortExpression="Description1">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Comment")%></ItemTemplate>
                </asp:TemplateColumn>  
                  <asp:TemplateColumn HeaderText="תעודת משלוח" ItemStyle-CssClass="ltr"  SortExpression="StartYear">
                    <HeaderStyle CssClass="t-center" />
                    <ItemStyle CssClass="t-natural va-middle" />
                      <ItemTemplate>
                       <u>   <a style="text-underline-position:below" onclick="popup( <%# (DataBinder.Eval(Container.DataItem, "ShippingID"))%>)">
                              <%# (DataBinder.Eval(Container.DataItem, "ShippingID"))%></a></u>
                      </ItemTemplate>
                </asp:TemplateColumn>  
                </Columns>
                </asp:DataGrid>
    </div>
</asp:Content>
