﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="SuppliersOrderTracing_popup.aspx.cs" Inherits="MaagareyArchive.SuppliersOrderTracing_popup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function closePopup() {
            window.opener.location.reload();
            window.close();
        }

        function print() {
            window.frames["print_frame"].document.body.innerHTML = document.getElementById("dvDetailes").innerHTML + document.getElementById("<%=dvGridResults.ClientID%>").innerHTML;
            window.frames["print_frame"].window.focus();
            window.frames["print_frame"].window.print();
        }
    </script>
    <table id="tblResults" runat="server" style="display:inline;width:100%">
        <tr>
            <td>
                <div id="dvDetailes" style="direction: rtl">
                    <table style="display: inline">
                        <tr>
                            <td style="padding-left: 50px;">
                                <label style="font-size: 18px; font-weight: bold;">ספק:</label>
                                <label style="font-size: 18px;" id="lblSupplier" runat="server"></label>

                            </td>
                            <td>
                                <label style="font-size: 18px; font-weight: bold;">הזמנה מספר:</label>
                                <label style="font-size: 18px;" id="lblOrderNum" runat="server"></label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>

            <td>
                <div id="dvGridResults" runat="server" style="direction: rtl">
                    <asp:GridView ID="dgResults" runat="server" Width="550px"
                        AutoGenerateColumns="false" CssClass="grid"
                        EnableViewState="true">
                        <%--DataKeyField="FileID"--%>

                        <HeaderStyle CssClass="header" />

                        <AlternatingRowStyle CssClass="alt" />
                        <RowStyle CssClass="row" />
                        <Columns>
                            <asp:TemplateField HeaderText="מס'" ItemStyle-CssClass="ltr">
                                <ItemTemplate>
                                    <%# rownum++  %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="פריט" ItemStyle-CssClass="ltr">
                                <ItemTemplate>
                                    <span id="spnItemName" runat="server">
                                        <%#(DataBinder.Eval(Container.DataItem,  "ItemName")).ToString()%>
                                    </span>
                                    <span id="spnNumInOrder" runat="server" style="display: none">
                                        <%#(DataBinder.Eval(Container.DataItem, "NumInOrder")).ToString()%>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="כמות" ItemStyle-CssClass="ltr">
                                <ItemTemplate>
                                    <span id="spnItemAmount" runat="server">
                                        <%#(DataBinder.Eval(Container.DataItem, "Amount")).ToString()%>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="הערות" ItemStyle-CssClass="ltr">
                                <ItemTemplate>
                                    <span id="spnItemRemark" runat="server">
                                        <%#(DataBinder.Eval(Container.DataItem, "Remark")).ToString()%>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="סופק" ItemStyle-CssClass="ltr">
                                <ItemTemplate>
                                    <div runat="server">
                                        <asp:TextBox ID="txtAmountProvided" runat="server" Text='<%#(DataBinder.Eval(Container.DataItem, "AmountProvided")).ToString()%>'></asp:TextBox>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
    <div id="dvPrintStyle" style="display:none;font-family: arial,sans-serif; direction: rtl">
        <asp:Image ID="imgLogo" ImageUrl="~/resources/images/Logo.png" runat="server" />
     <div style="height:99%; width: 99%; border: solid;border-width:1px;">
    
            <div style="padding-right: 48px; width: 770px;font-family: arial,sans-serif; direction: rtl;float:right" id="dvHeader" runat="server">
                               <div  style ="float: left;position:absolute;    margin-top: 20px; top:25px; left: 70px;vertical-align:top" runat = 'server' >
                    logo
                </div>
                 <p style="font-size: 37px; margin-bottom: 0px; color: #339966; font-weight: bold;">מאגרי ארכיב וגניזה בע"מ</p>

                <p style="font-size: 21px; margin-top: 3px; color: #339966; font-weight: bold;">שרותי ארכיון</p>


                <p style="font-size: 22px;">טופס הזמנת מוצר</p>


                <div style="width: 550px">
                    <div style="float: right">
                        <span>הזמנת רכש מס':</span>
                        <span>spnOrderNum</span>

                    </div>
                    <div style="float: left">
                        <span>תאריך:</span>
                        <span>spnOrderDate</span>

                    </div>

                </div>
                <div style="height: 36px"></div>
                <div style="width: 550px; padding-bottom: 63px">
                    <div style="float: right">
                        <span>ספק:</span>
                        <span>spnSupplier</span>

                    </div>
                    <div style="float: left;">
                        <span>טלפון:</span>
                        <span>spnPhone</span>
                        <span>פקס:</span>
                        <span>spnFax</span>
                    </div>

                </div>
            </div>
     
       <div id="dvFooter" runat="server" style="direction:rtl" >
            <p style="padding-top:20px">** ספק, נא ציין מועד אספקה</p>
            <div>

            </div>
            <div style="text-align:center;font-weight:bold;width:200px;padding-top:30px;">
                <p>תודה מראש,</p>
                <p>מאגרי ארכיב וגניזה בע"מ</p>
            </div>

            <div id="dvFooterButtom" runat="server" style="text-align: center;position:absolute;width: 100%;right: 0px;bottom: 0px;">
                <p>
                    ת.ד. 33 בני עי"ש 608600  . טל : 08-9356037 / 08-9419401  .  פקס : 08-9349491
                </p>
                <p>
                    כתובת דואר אלקטרונית :       archive.ream@gmail.com  אתר אינטרנט : www.m-archive.co.il
                </p>
            </div>
      
   </div>
             
          </div>
</div>
    

            </td>
        </tr>
        <tr>
            <td>
                <table style="margin: 0px; border-spacing: 0px;width:100%">
                    <tr>
                        <td style="text-align: right;">
                            <input type="button" value="סגור" onclick="closePopup()" runat="server" />
                        </td>
                        <td>
                            <asp:Button Text="עדכן" ID="btnUpdate" OnClick="btnUpdate_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        
                        <td colspan="2" >

                            <asp:Button Text="הדפס" ID="btnPrint" OnClick="btnPrint_Click" Style="float: right !important; margin: 10px" Width="85px" runat="server" />
                            <asp:Button Text="שמור בשם" ID="btnSaveToFile" OnClick="btnSaveToFile_Click" Style="float: none !important; margin: 10px" Width="85px" runat="server" />
                            <asp:Button Text="שלח במייל" ID="btnSendMail" OnClick="btnSendMail_Click" Style="float: left !important; margin: 10px" Width="85px" runat="server" />


                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div runat="server" class="dvSucceedMessage">
                                <span id="dvErrorMessage" runat="server" class="dvSucceedMessage"></span>
                            </div>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
    </table>
    
</asp:Content>
