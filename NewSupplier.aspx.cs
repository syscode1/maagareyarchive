﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class NewSupplier : BasePage
    {
        public int rownum = 1;

        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindGridData();
                fillDdl();
            }
        }

        private void fillDdl()
        {
            var listCache = new MyCache();
            ddlCities.DataSource = listCache.Cities;
            ddlCities.DataBind();

            OrderedDictionary data;
            data = SuppliersController.getSuppliersList();
            data.Insert(0, 0, Consts.SELECT);
            ddlSupplierName.DataSource = data;
            ddlSupplierName.DataTextField = "Value";
            ddlSupplierName.DataValueField = "Key";
            ddlSupplierName.DataBind();

            ddlSupplierID.DataSource = data;
            ddlSupplierID.DataTextField = "Key";
            ddlSupplierID.DataValueField = "Key";
            ddlSupplierID.DataBind();


            fillDdl(PaymentController.getPeriodOfPaymentList(), ddlTemOfPayment);


        }

        private void bindGridData()
        {
            dgResults.DataSource = ItemsController.getAllItems();
            dgResults.DataBind();
        }

        protected void rbSupplierExist_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSupplierExist.Checked)
            {
                dvSupplierName.Style["display"] = "";
                dvSupplierID.Style["display"] = "";

            }
            else
            {
                dvSupplierName.Style["display"] = "none";
                dvSupplierID.Style["display"] = "none";

            }
            clearControls();
        }

        protected void ddlSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSupplierName.SelectedValue = ddlSupplierID.SelectedValue;
            fillControlsData();
        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlSupplierID.SelectedValue = ddlSupplierName.SelectedValue;
            clearControls();
            fillControlsData();
        }

        private void fillControlsData()
        {
            Suppliers supplier = SuppliersController.getSupplierById(Convert.ToInt32(ddlSupplierID.SelectedValue));
            txtAddress.Value = supplier.FullAddress;
            txtComment.Value = supplier.Description;
            txtContactName.Value = supplier.ContactMan;
            txtDateOfContract.Value = supplier.DateOfContract != null ? supplier.DateOfContract.ToString() : "";
            txtDescription.Value = supplier.DescriptionAddress;
            txtFax.Value = supplier.Fax;
            txtMail.Value = supplier.Email;
            txtPhone1.Value = supplier.Telephone1;
            txtPhone2.Value = supplier.Telephone2;
            txtSupplierName.Text = supplier.SupplierName;
            if (!string.IsNullOrEmpty(supplier.CityAddress.Trim()))
            {
                int? index = ddlCities.Items.IndexOf(new ListItem(supplier.CityAddress.Trim()));
                ddlCities.SelectedIndex = index != null && index >= 0 ? (int)index : 0;
            }
            if (supplier.TermOfPayment != null)
                ddlTemOfPayment.SelectedValue = supplier.TermOfPayment.ToString();
            List<SuppliersItems> supplierItems = SuppliersItemsController.Get(supplier.SupplierID);
            foreach (DataGridItem item in dgResults.Items)
            {
                int? itemId = Helper.getInt((item.FindControl("itemID") as HtmlGenericControl).InnerText);
                if (itemId != null)
                {
                    SuppliersItems sItem = supplierItems.Where(s => s.ItemID == itemId).FirstOrDefault();
                    (item.FindControl("cbCheck") as CheckBox).Checked = sItem != null ? true : false;
                    (item.FindControl("txtPrice") as TextBox).Text = sItem != null && sItem.SupplierPrice !=null? ((decimal)sItem.SupplierPrice).ToString("N2") : "";

                }
            }
        }

        private void clearControls()
        {
            Suppliers supplier = SuppliersController.getSupplierById(Convert.ToInt32(ddlSupplierID.SelectedValue));
            txtAddress.Value = string.Empty;
            txtComment.Value = string.Empty;
            txtContactName.Value = string.Empty;
            txtDateOfContract.Value = DateTime.Now.ToShortDateString();
            txtDescription.Value = string.Empty;
            txtFax.Value = string.Empty;
            txtMail.Value = string.Empty;
            txtPhone1.Value = string.Empty;
            txtPhone2.Value = string.Empty;
            txtSupplierName.Text = string.Empty;
            ddlCities.ClearSelection();
            ddlTemOfPayment.ClearSelection();
            bindGridData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {


            int? supplierId = Helper.getInt(ddlSupplierID.SelectedValue);
            if ((rbSupplierExist.Checked &&(supplierId == null || supplierId <= 0)) || string.IsNullOrEmpty(txtSupplierName.Text))
            {
                showIndicationMessage(false, Consts.MISSING_SUPPLIER_NAME);
            }
            else
            {
                Suppliers supplier = fillSupplier();

                bool isSucceed = false;
                if (rbSupplierExist.Checked)
                {
                    supplier.SupplierID = (int)supplierId;
                    isSucceed= SuppliersController.Update(supplier);
                }
                else
                {
                    supplierId = SuppliersController.Insert(supplier);
                    isSucceed = supplierId > 0?true:false;
                }
                if (isSucceed)
                {
                    List<SuppliersItems> itemsToAddUpdate = new List<SuppliersItems>();
                    List<SuppliersItems> itemsToDelete = new List<SuppliersItems>();

                    fillItemsToAddUpdate((int)supplierId, out itemsToAddUpdate, out itemsToDelete);
                    SuppliersItemsController.InsertUpdate(itemsToAddUpdate);
                    SuppliersItemsController.Delete(itemsToDelete);
                }

                string message = isSucceed ? Consts.SAVED_SUCCEED : Consts.SAVED_FAIL;
                showIndicationMessage(isSucceed, message);
                if(isSucceed && rbSupplierExist.Checked==false)
                {
                    clearControls();
                    fillDdl();
                }
                    
            }

        }

        private void fillItemsToAddUpdate(int supplierId,out List<SuppliersItems> itemsToAddUpdate,out List<SuppliersItems> itemsToDelete)
        {
            itemsToAddUpdate = new List<SuppliersItems>();  
            itemsToDelete = new List<SuppliersItems>();      
            foreach (DataGridItem item in dgResults.Items)
            {
                if ((item.FindControl("cbCheck") as CheckBox).Checked == true)
                    itemsToAddUpdate.Add(new SuppliersItems()
                    {
                        SupplierId = supplierId,
                        ItemID = (int)Helper.getInt((item.FindControl("itemID") as HtmlGenericControl).InnerText),
                        SupplierPrice=Helper.getDecimal((item.FindControl("txtPrice") as TextBox).Text),
                        IsActive=true,
                        UpdatedDate=DateTime.Now,
                        
                    });
                else
                {
                    if(rbSupplierExist.Checked)
                        itemsToDelete.Add(new SuppliersItems()
                        {
                            SupplierId = supplierId,
                            ItemID = (int)Helper.getInt((item.FindControl("itemID") as HtmlGenericControl).InnerText),
                            SupplierPrice = Helper.getDecimal((item.FindControl("txtPrice") as TextBox).Text),
                            IsActive = true,
                            UpdatedDate = DateTime.Now,
                        });
                }
            }
           
        }

        private Suppliers fillSupplier()
        {
            Suppliers supplier = new Suppliers();
            supplier.FullAddress = txtAddress.Value;
            supplier.Description = txtComment.Value;
            supplier.ContactMan = txtContactName.Value;
            supplier.DateOfContract =rbSupplierExist.Checked && !string.IsNullOrEmpty(txtDateOfContract.Value) ? Helper.getDate(txtDateOfContract.Value):DateTime.Now;
            supplier.DescriptionAddress = txtDescription.Value;
            supplier.Fax = txtFax.Value.Trim();
            supplier.Email = txtMail.Value;
            supplier.Telephone1 = txtPhone1.Value.Trim();
            supplier.Telephone2 = txtPhone2.Value.Trim();
            supplier.SupplierName = txtSupplierName.Text;
            supplier.CityAddress = ddlCities.SelectedItem.Text;
            supplier.TermOfPayment = Helper.getInt(ddlTemOfPayment.SelectedValue);
            return supplier;
        }

        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }
    }
}
