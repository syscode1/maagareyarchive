﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class UsersManagerList : BasePage
    {
        public int rownum = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillCustomerDdl();

                string customerID = Request.QueryString["custID"];
                if (!string.IsNullOrEmpty(customerID))
                {
                    ddlCustomerName.SelectedValue = customerID;
                    ddlCustomerName_SelectedIndexChanged(null, null);
                }
                else
                    ddlCustomerName_SelectedIndexChanged(null, null);
            }

        }

        private void fillCustomerDdl()
        {
            ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();
        }


        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
                bindData();
        }
        private void bindData()
        {
            int selectedCustomer;
            selectedCustomer = Helper.getInt(ddlCustomerName.SelectedValue).Value;

            List<Users> users = UserController.getAllUsersList(selectedCustomer);//,selectedUser);
            dvMessage.Style["display"] = users != null && users.Count > 0 ? "none" : "";
            dgResults.DataSource = users;
            dgResults.DataBind();

            dgResults.UseAccessibleHeader = true;
            dgResults.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "setSortedTable()", true);
        }

    }
}