﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SendMessageToArchive.aspx.cs" Inherits="MaagareyArchive.SendMessageToArchive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $(document).ready(function () {
            $('.file-upload').on('change', function (evt) {
                if (this.files.length > 0 && (this.files[0].size / 1024 / 1024) > 3.99) {
                    $("#IsValid").val("false");
                } else {
                    $("#IsValid").val("true");
                }
                console.log(this.files[0].size);
            });
        });
            function Validate()
        {
        if ($("#IsValid").val() == "true") return true;
        alert('הקובץ המצורף גדול מידי, יש לבחור קובץ בגודל של עד 4  GB');
        return false;
    }
    </script>
    <h1>שליחת הודעה לארכיון</h1>
         <input type="hidden" id="IsValid" value="true"/>
     <div  runat="server" class="dvSucceedMessage">
                
            </div>
    <span id="dvErrorMessage" runat="server" class="dvErrorMessage">

                </span>
    <table style="width:100%" runat="server">
        <tr>
            <td > 
            </td>
            <td style="text-align:left">
                <asp:RadioButton ID="rbUrgent_true" Text="דחוף" GroupName="urgent"  runat="server" />
                <asp:RadioButton ID="rbUrgent_false" Text="רגיל" Checked="true" GroupName="urgent" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <span>אל:</span>
            </td> 
            <td>
                <asp:TextBox ID="txtTo" runat="server" />
            </td>
        </tr>
        <tr>
             <td>
                <span>נושא:</span>
            </td> 
            <td>
                <asp:TextBox ID="txtSubject" onkeypress="return maxCharLength(event,67)"  runat="server" />
            </td>
        </tr>
        <tr>
           <td>
                <span>תוכן:</span>
            </td> 
            <td>
                <asp:TextBox ID="txtBody"  TextMode="MultiLine" onkeypress="return maxCharLength(event,4080)" runat="server" />
            </td>
        </tr>
        <tr>
             <td>
                <span>צרף קובץ:</span>
            </td> 
            <td>
               <asp:FileUpload runat="server" style="float:right" CssClass="file-upload"  ID="AttachedFile" />
            </td>
        </tr>
    </table>
     <table style="float: left;">
         <tr>
                <td>
                   <asp:Button ID="btnSend" Text="שלח" runat="server"  OnClientClick="return Validate();" OnClick="btnSend_Click" style="margin-bottom:10px;width:130px" />
                </td><%--OnClientClick="return getSelectedFiles()"--%>
            </tr>
        </table>

</asp:Content>



