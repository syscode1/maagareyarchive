﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" CodeBehind="OrderSuppliesAndCollecting.aspx.cs" Inherits="MaagareyArchive.OrderSuppliesAndCollecting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            enableDatepicker();
        });

        $(function () {
            enableDatepicker();
        });


        function enableDatepicker() {
            $("#datepicker").datepicker({ dateFormat: "dd-mm-yy" });
        }

        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }

    </script>
    <h1>פינויים ואספקות</h1>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>

            <div id="dvCustomer" runat="server">
                <table>
                    <tr>

                        <td>לקוח                
                            <asp:DropDownList ID="ddlCustomer" runat="server" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>

                        </td>
                    </tr>

                </table>




            </div>
            <div runat="server" class="dvErrorMessage">
                <span id="dvErrorMessage" runat="server" class="dvErrorMessage"></span>
            </div>
            <div id="dvData" runat="server">
                <table style="display: inline-block">
                    <tr>
                        <td>
                            <asp:DataGrid ID="dgResults" runat="server" Width="650px"
                                AutoGenerateColumns="false" CssClass="grid"
                                EnableViewState="true" OnItemDataBound="dgResults_ItemDataBound">
                                <%--DataKeyField="FileID"--%>

                                <HeaderStyle CssClass="header" />

                                <AlternatingItemStyle CssClass="alt" />
                                <ItemStyle CssClass="row" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="אתר" ItemStyle-CssClass="ltr">
                                        <HeaderStyle CssClass="t-center" />
                                        <ItemStyle CssClass="t-natural va-middle" />
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPlace">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="תיבות" ItemStyle-CssClass="ltr">
                                        <HeaderStyle CssClass="t-center" />
                                        <ItemStyle CssClass="t-natural va-middle" />
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtEmptyBoxes" onkeypress="return isNumberKey(event)" Text='<%#Convert.ToInt16((DataBinder.Eval(Container.DataItem, "EmptyBoxes")))==0?"":(DataBinder.Eval(Container.DataItem, "EmptyBoxes"))%>' Width="60px" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="פינוי" ItemStyle-CssClass="ltr">
                                        <HeaderStyle CssClass="t-center" />
                                        <ItemStyle CssClass="t-natural va-middle" />
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtFullBoxes" onkeypress="return isNumberKey(event)" Text='<%#Convert.ToInt16((DataBinder.Eval(Container.DataItem, "FullBoxes")))==0?"":(DataBinder.Eval(Container.DataItem, "FullBoxes"))%>' Width="60px" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="החזרות" ItemStyle-CssClass="ltr">
                                        <HeaderStyle CssClass="t-center" />
                                        <ItemStyle CssClass="t-natural va-middle" />
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtNumRetriveBoxes" onkeypress="return isNumberKey(event)" Text='<%#Convert.ToInt16((DataBinder.Eval(Container.DataItem, "RetriveBoxes")))==0?"":(DataBinder.Eval(Container.DataItem, "RetriveBoxes"))%>' Width="60px" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="גריסה" ItemStyle-CssClass="ltr" SortExpression="StartYear">
                                        <HeaderStyle CssClass="t-center" />
                                        <ItemStyle CssClass="t-natural va-middle" />
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" onkeypress="return isNumberKey(event)" ID="txtNumExterminateBoxes" Text='<%#Convert.ToInt16((DataBinder.Eval(Container.DataItem, "ExterminateBoxes")))==0?"":(DataBinder.Eval(Container.DataItem, "ExterminateBoxes"))%>' Width="60px" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <td style="display: inline-block;">

                            <asp:ImageButton ID="btnAddRow" Style="margin-top: 65px;" OnClick="btnAddRow_Click" ImageUrl="resources/images/add.png" Height="35px" runat="server" />
                            <%--   <asp:ImageButton ImageUrl="resources/images/remove.png" Height="35px" runat="server" />--%>
                        </td>
                    </tr>
                </table>

                <table style="display: inline-block; line-height: 50px;">
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlCities" runat="server" />
                        </td>
                        <td>
                            <input id="txtAddress" runat="server" placeholder="כתובת" class="form-control" />
                        </td>
                        <td>
                            <input id="txtDescription" runat="server" placeholder="תיאור" class="form-control" />
                        </td>
                        <td>
                            <input id="txtContactName" runat="server" placeholder="איש קשר" class="form-control" />
                        </td>
                        <%--                 <td>
                      <input  id="txtPhone" runat="server"  placeholder="טלפון"  class="form-control" />
                 </td>--%>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <input id="txtComment" runat="server" placeholder="הערות" onkeypress="return maxCharLength(event,256)" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <input type="text" placeholder="לתאריך" id="datepicker">
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlEvents"></asp:DropDownList>
                        </td>
                    </tr>
                </table>

                <table style="width: 100%">
                    <tr>
                        <td>
                            <asp:Button ID="btnOrder" Text="הזמן" runat="server" OnClick="btnOrder_Click" Style="margin-bottom: -35px; width: 130px" />
                        </td>
                        <%--OnClientClick="return getSelectedFiles()"--%>
                    </tr>
                </table>
                <br />
                <br />
                <div class="h1_sub">פינויים ואספקות היסטוריים</div>
                <asp:DataGrid ID="dgSuppliersHistory" runat="server" Width="650px"
                    AutoGenerateColumns="false" CssClass="grid"
                    EnableViewState="true" OnItemDataBound="dgResults_ItemDataBound">
                    <%--DataKeyField="FileID"--%>

                    <HeaderStyle CssClass="header" />

                    <AlternatingItemStyle CssClass="alt" />
                    <ItemStyle CssClass="row" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="אתר">
                            <ItemTemplate>
                                <%# Eval("CityAddress") + " " + Eval("FullAddress")+ " " + Eval("DescriptionAddress")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="תאריך" DataField="Date"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="תיבות" DataField="NumNewBoxOrder"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="פינוי" DataField="NumFullBoxes"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="החזרות" DataField="NumRetriveBoxes"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="גריסה" DataField="NumExterminateBoxes"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
            <br />
            <br />
            <div style="float: right; margin-top: -3px">
                <input type="button" value="יציאה" onclick="exit()" style="margin-left: 10px; width: 180px" runat="server" />

            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
