﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ExtractionsNotFound : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            string customerID = Request.QueryString["custID"];
            if (!string.IsNullOrEmpty(customerID))
            {
                dgResults.DataSource = FileStatusController.GetByCustomer(Helper.getInt( customerID).Value, Consts.enFileStatus.notFound);
                dgResults.DataBind();
            }
        }
    }
}