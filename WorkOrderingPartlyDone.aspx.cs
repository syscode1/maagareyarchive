﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class WorkOrderingPartlyDone : BasePage
    {

        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindData();
            }

        }

        private void bindData()
        {
            string rowId = Request.QueryString["rowId"];
            if (!string.IsNullOrEmpty(rowId))
            {
                ShippingsUI shipping = ShippingController.getSipping(Helper.getInt(rowId).Value);
                if (shipping != null)
                {
                    dgResultsR.DataSource =new List<ShippingsUI>() { shipping };
                    dgResultsR.DataBind();
                    OrderedDictionary data = new OrderedDictionary();
                    List<int> dep = shipping.DepartmentID != 0 ? new List<int>() { shipping.DepartmentID } :null;
                    if (dep == null)
                    {
                        data = UserController.getAllUsers(shipping.CustomerID);
                    }
                    else
                    {
                        data = UserController.getUsersByCustomer(shipping.CustomerID);
                    }
                    data.Insert(0, 0, Consts.ALL_USERS);
                    fillDdlOrderDict(data, ddlUser);
                    ddlUser.SelectedValue = "0";
                }
            }
        }

        protected void dgResultsR_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ShippingsUI dataItem = e.Row.DataItem as ShippingsUI;
                if (dataItem.Urgent == true)
                    e.Row.BackColor = Color.Pink;
                if (dataItem.Color == true)
                    (e.Row.FindControl("txtComment") as TextBox).BackColor = Color.Yellow;
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ArchiveEntities context = new ArchiveEntities();
             if (dgResultsR.Rows.Count>0)
            {
                ArchiveEntities contex = new ArchiveEntities();
                int rowID = Helper.getInt(Request.QueryString["rowId"]).Value;
                Shippings shipping = contex.Shippings.Where(s => s.rowID == rowID).FirstOrDefault();
     
                int SumNewBoxOrder, SumFullBoxes, SumRetriveBoxes, SumExterminateBoxes;
                int orgSumNewBoxOrder, orgSumFullBoxes, orgSumRetriveBoxes, orgSumExterminateBoxes;
                int subSumNewBoxOrder, subSumFullBoxes, subSumRetriveBoxes, subSumExterminateBoxes;
                bool cbSumNewBoxOrder, cbSumFullBoxes, cbSumRetriveBoxes, cbSumExterminateBoxes;

                orgSumNewBoxOrder = Helper.getIntNotNull((dgResultsR.Rows[0].FindControl("spnSumNewBoxOrder") as HtmlGenericControl).InnerText);
                orgSumFullBoxes = Helper.getIntNotNull((dgResultsR.Rows[0].FindControl("spnSumFullBoxes") as HtmlGenericControl).InnerText);
                orgSumRetriveBoxes = Helper.getIntNotNull((dgResultsR.Rows[0].FindControl("spnSumRetriveBoxes") as HtmlGenericControl).InnerText);
                orgSumExterminateBoxes = Helper.getIntNotNull((dgResultsR.Rows[0].FindControl("spnSumExterminateBoxes") as HtmlGenericControl).InnerText);


                SumNewBoxOrder = Helper.getInt((dgResultsR.Rows[0].FindControl("txtSumNewBoxOrder") as TextBox).Text).Value;
                SumFullBoxes= Helper.getInt((dgResultsR.Rows[0].FindControl("txtSumFullBoxes") as TextBox).Text).Value;
                SumRetriveBoxes= Helper.getInt((dgResultsR.Rows[0].FindControl("txtSumRetriveBoxes") as TextBox).Text).Value;
                SumExterminateBoxes= Helper.getInt((dgResultsR.Rows[0].FindControl("txtSumExterminateBoxes") as TextBox).Text).Value;

                cbSumNewBoxOrder = (dgResultsR.Rows[0].FindControl("cbSumNewBoxOrder") as CheckBox).Checked;
                cbSumFullBoxes = (dgResultsR.Rows[0].FindControl("cbSumFullBoxes") as CheckBox).Checked;
                cbSumRetriveBoxes = (dgResultsR.Rows[0].FindControl("cbSumRetriveBoxes") as CheckBox).Checked;
                cbSumExterminateBoxes = (dgResultsR.Rows[0].FindControl("cbSumExterminateBoxes") as CheckBox).Checked;


                subSumNewBoxOrder = orgSumNewBoxOrder - SumNewBoxOrder;
                subSumFullBoxes = orgSumFullBoxes - SumFullBoxes;
                subSumRetriveBoxes = orgSumRetriveBoxes - SumRetriveBoxes;
                subSumExterminateBoxes = orgSumExterminateBoxes - SumExterminateBoxes;

                int selectedUser = Helper.getIntNotNull(ddlUser.SelectedValue);

                shipping.DoneStatus = (int)Consts.enDoneStatus.PartlyDone;
                shipping.ReceivedBy = ddlUser.SelectedValue;
                shipping.ReceivedDate = DateTime.Now;
                shipping.SumExterminateBoxes = SumExterminateBoxes;
                shipping.SumFullBoxes = SumFullBoxes;
                shipping.SumNewBoxOrder = SumNewBoxOrder;
                shipping.SumRetriveBoxes = SumRetriveBoxes;
                shipping.DeliveryID = Helper.getDeliveryID(user.UserID);
                contex.SaveChanges();

                OrdersController.updateOrderStatusDone(rowID, selectedUser);

                string orderID = "";
                if (subSumNewBoxOrder > 0 ||
                   subSumFullBoxes > 0 ||
                   subSumRetriveBoxes > 0 ||
                   subSumExterminateBoxes > 0)
                {

                    if ((subSumNewBoxOrder > 0 && cbSumNewBoxOrder == false) ||
                   (subSumFullBoxes > 0 && cbSumFullBoxes == false) ||
                   (subSumRetriveBoxes > 0 && cbSumRetriveBoxes == false) ||
                   (subSumExterminateBoxes > 0 && cbSumExterminateBoxes == false))
                    {
                         orderID = Helper.getOrderID(user.UserID);
                        OrdersController.Insert(new Orders()
                        {
                            CityAddress = shipping.CityAddress,
                            Comment = shipping.Comment + Consts.PARTLY_DONE_COMMENT,
                            ContactMan = shipping.ContactMan,
                            CustomerID = shipping.CustomerID,
                            DepartmentID = shipping.DepartmentID,
                            FullAddress = shipping.FullAddress,
                            LoginID = selectedUser,
                            Date = DateTime.Now,
                            Done = false,
                            NumInOrder = 1,
                            NumExterminateBoxes = subSumExterminateBoxes > 0 && cbSumExterminateBoxes == false ? subSumExterminateBoxes : 0,
                            NumFullBoxes = subSumFullBoxes > 0 && cbSumFullBoxes == false ? subSumFullBoxes : 0,
                            NumNewBoxOrder = subSumNewBoxOrder > 0 && cbSumNewBoxOrder == false ? subSumNewBoxOrder : 0,
                            NumRetriveBoxes = subSumRetriveBoxes > 0 && cbSumRetriveBoxes == false ? subSumRetriveBoxes : 0,
                            Telephone1 = shipping.Telephone1,
                            Telephone2 = shipping.Telephone2,
                            Urgent = shipping.Urgent,
                            OrderID=orderID,
                        });
                    }
                     orderID = Helper.getOrderID(user.UserID);
                    OrdersController.Insert(new Orders()
                    {
                        CityAddress = shipping.CityAddress,
                        Comment = shipping.Comment + Consts.PARTLY_DONE_COMMENT,
                        ContactMan = shipping.ContactMan,
                        CustomerID = shipping.CustomerID,
                        DepartmentID = shipping.DepartmentID,
                        FullAddress = shipping.FullAddress,
                        LoginID = selectedUser,
                        Date = DateTime.Now,
                        Done = true,
                        ShippingRowID = shipping.rowID,
                        NumInOrder = 1,
                        NumExterminateBoxes = subSumExterminateBoxes > 0 ? subSumExterminateBoxes * -1 : 0,
                        NumFullBoxes = subSumFullBoxes > 0 ? subSumFullBoxes * -1 : 0,
                        NumNewBoxOrder = subSumNewBoxOrder > 0 ? subSumNewBoxOrder * -1 : 0,
                        NumRetriveBoxes = subSumRetriveBoxes > 0 ? subSumRetriveBoxes * -1 : 0,
                        Telephone1 = shipping.Telephone1,
                        Telephone2 = shipping.Telephone2,
                        Urgent = shipping.Urgent,
                        OrderID = orderID,
                    });
                }
                    
                if(subSumNewBoxOrder < 0 ||
                   subSumFullBoxes < 0 ||
                   subSumRetriveBoxes < 0 ||
                   subSumExterminateBoxes < 0)
                {
                     orderID = Helper.getOrderID(user.UserID);
                    OrdersController.Insert(new Orders()
                    {
                        CityAddress = shipping.CityAddress,
                        Comment = shipping.Comment + Consts.PARTLY_DONE_COMMENT,
                        ContactMan = shipping.ContactMan,
                        CustomerID = shipping.CustomerID,
                        DepartmentID = shipping.DepartmentID,
                        FullAddress = shipping.FullAddress,
                        LoginID = selectedUser,
                        Date = DateTime.Now,
                        Done = true,
                        ShippingRowID = shipping.rowID,
                        NumInOrder = 1,
                        NumExterminateBoxes = subSumExterminateBoxes < 0 ? subSumExterminateBoxes * -1 : 0,
                        NumFullBoxes = subSumFullBoxes < 0 ? subSumFullBoxes * -1 : 0,
                        NumNewBoxOrder = subSumNewBoxOrder < 0 ? subSumNewBoxOrder * -1 : 0,
                        NumRetriveBoxes = subSumRetriveBoxes < 0 ? subSumRetriveBoxes * -1 : 0,
                        Telephone1 = shipping.Telephone1,
                        Telephone2 = shipping.Telephone2,
                        Urgent = shipping.Urgent,
                        OrderID = orderID,
                    });
                }
            }
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "window.opener.location.reload();window.close()", true); 
        }

        private void insertPositiveSum(Shippings shipping)
        {


        }
    }
}