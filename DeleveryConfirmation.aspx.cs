﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class DeleveryConfirmation : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillCustomerDdl();
                fillDepartmentDdl(0);

                string customerID = Request.QueryString["custID"];
                if (!string.IsNullOrEmpty(customerID))
                {
                    ddlCustomerName.SelectedValue = customerID;
                    btnBackCustomer.Style["display"] = "";
                    ddlCustomerName_SelectedIndexChanged(null, null);
                }
                else
                    bindUserData();
                //  fillUsersDdl(0, 0);
            }
        }

        private void fillCustomerDdl()
        {
            ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();
        }
        private void fillDepartmentDdl(int customerID)
        {
            OrderedDictionary data = new OrderedDictionary(); ;

            data = DepartmentController.GetCustomerDepartmentsDictionary(customerID);
            data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
            fillDdlOrderDict(data, ddlDepartment);
            ddlDepartment.SelectedValue = "0";
        }

        //private void fillUsersDdl(int customerID, int departmentID)
        //{
        //    OrderedDictionary data = new OrderedDictionary();
        //    data = UserController.getUsersByDepartment(customerID, new List<int>() { departmentID });
        //    data.Insert(0, 0, Consts.ALL_USERS);
        //    fillDdlOrderDict(data, ddlUser);
        //    ddlUser.SelectedValue = "0";
        //}
        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedCustomer = Helper.getInt(ddlCustomerName.SelectedValue).Value;
            if (selectedCustomer > 0)
                fillDepartmentDdl(selectedCustomer);
            bindData();
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedCustomer = Helper.getInt(ddlCustomerName.SelectedValue).Value;
            int selectedDep = Helper.getInt(ddlDepartment.SelectedValue).Value;
            if (selectedCustomer > 0 )
            //    fillUsersDdl(selectedCustomer, selectedDep);
            bindData();
        }


        private void bindData()
        {
            int selectedCustomer, selectedDep=0;
            selectedCustomer = Helper.getInt(ddlCustomerName.SelectedValue).Value;
            selectedDep = Helper.getInt(ddlDepartment.SelectedValue).Value;

            List<ShippingsUI> shippings = ShippingController.getSippings(selectedCustomer, selectedDep);//,selectedUser);
            dvMessage.Style["display"] = shippings != null && shippings.Count > 0 ? "none" : "";
            dgResultsR.DataSource = shippings;
                dgResultsR.DataBind();
        }
       private void bindUserData()
        {

            List<ShippingsUI> shippings = ShippingController.getSippings(0, 0,user.UserID);//,selectedUser);
            dvMessage.Style["display"] = shippings != null && shippings.Count > 0 ? "none" : "";
            dgResultsR.DataSource = shippings;
                dgResultsR.DataBind();
        }

    }
}