﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ScanRetrives.aspx.cs" Inherits="MaagareyArchive.ScanRetrives" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function inputFile(evt) {
            var dvError = document.getElementById('<%= dvErrorMessage.ClientID%>');
            if (evt.currentTarget.value.length > 13 || evt.currentTarget.value.length < 13) {
                {
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    return false;
                }

            }
            if (evt.currentTarget.value.length == 13) {
                if (evt.currentTarget.value.indexOf("261") != 0 &&
                    evt.currentTarget.value.indexOf("262") != 0) {
                    dvError.style.display = "";
                    dvError.innerText = "מספר לא תקין";
                    evt.currentTarget.value = "";
                    return false;
                }
                else {
                    dvError.style.display = "none";
                    dvError.innerText = "";
                    document.getElementById('<%=btnInputFileBoxID.ClientID %>').click();
                    return true;
                }

            }
        }

        function CallPrint() {
            debugger
            var prtContent = document.getElementById('dvPrint');
            var tableRightStyle = $('#<%= dgResults.ClientID %>', prtContent)[0].style.right;
            $('#<%= dgResults.ClientID %>', prtContent)[0].style.right = "0px";
                    prtContent.border = 1; //set no border here
                    var rows = $('tr', prtContent);
                    var lastRowSpan=0;
                    for (i = 0; i < rows.length; i++) {
                        if ($('td', rows[i]).length > 0) {
                            if ($('td', rows[i])[0].attributes["rowspan"] != undefined)
                                lastRowSpan = i;
                        if ($('[id*=spnPrintRetrivesDate]', rows[i]).length > 0
                            && $('[id*=spnPrintRetrivesDate]', rows[i])[0].innerText != "") {
                            rows[i].closest("tr").style.display = "none";

                            $('td', rows[lastRowSpan])[0].attributes["rowspan"].value = $('td', rows[lastRowSpan])[0].attributes["rowspan"].value - 1;
                                
                            }

                        }
                       
                    }
                    var WinPrint = window.open('', '', 'left=0,top=100,width=1500,height=900,toolbar=0,scrollbars=1,status=0,resizable=1');
                    WinPrint.document.write("<html><body style='-webkit-print-color-adjust:exact;'>" + prtContent.outerHTML + "</body></html>");

                    var css = '@page { size: landscape; }',
        head = WinPrint.document.head || document.getElementsByTagName('head')[0],
        style = WinPrint.document.createElement('style');

                    style.type = 'text/css';
                    style.media = 'print';

                    if (style.styleSheet) {
                        style.styleSheet.cssText = css;
                    } else {
                        style.appendChild(WinPrint.document.createTextNode(css));
                    }
                    head.appendChild(style);

                    WinPrint.document.close();
                    WinPrint.focus();
                    WinPrint.print();
                    WinPrint.close();
                    $('#<%= dgResults.ClientID %>', prtContent)[0].style.right = tableRightStyle;
            return true;

        }
    </script>
    <h1>החזרות
    </h1>
    <div>
        <span>סרוק תיק</span>

        <input type="text" id="txtFileBoxID" oninput="return inputFile(event)" maxlength="13" onkeypress="return isNumberKey(event)" runat="server"
            style="width: 150px" />
        <asp:Button Style="display: none" ID="btnInputFileBoxID" runat="server" UseSubmitBehavior="true" OnClick="btnInputFileBoxID_Click" />

        <div id="dvErrorMessage" runat="server"></div>
    </div>

    <div>
        <asp:UpdatePanel runat="server" ID="up">
            <ContentTemplate>
                <div style="padding-bottom: 15px;">
                    <div class="h1_sub"></div>
                    <span style="font-weight: bold; text-align: right; display: grid; padding-top: 15px;">סריקה אחרונה: </span>
                    <asp:GridView ID="dgLastResult" runat="server"
                        AutoGenerateColumns="false" CssClass="grid"
                        EnableViewState="true" Style="margin-top: 15px !important;"
                        OnDataBound="dgResults_DataBound">
                        <%--DataKeyField="FileID"--%>
                        <HeaderStyle CssClass="header" />

                        <AlternatingRowStyle CssClass="alt" />
                        <RowStyle CssClass="row" Font-Bold="true" BackColor="Yellow" />

                        <Columns>
                            <asp:BoundField DataField="Warehouse" HeaderText="מחסן" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="Row" HeaderText="שורה" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="Cell" HeaderText="תא" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="Shelf" HeaderText="מדף" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="CustomerID" HeaderText="קוד לקוח" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="CustomerBoxNum"  HeaderText="מספר ארגז לקוח" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />

                            <asp:BoundField DataField="FileID" HeaderText="תיק" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="BoxID" HeaderText="ארגז" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="h1_sub"></div>
                <div id="dvPrint" style="direction: rtl; text-align: center;">
                    <asp:GridView ID="dgResults" runat="server"
                        AutoGenerateColumns="false" CssClass="grid"
                        EnableViewState="true"
                        OnDataBound="dgResults_DataBound">
                        <%--DataKeyField="FileID"--%>
                        <HeaderStyle CssClass="header" />

                        <AlternatingRowStyle CssClass="alt" />
                        <RowStyle CssClass="row" />

                        <Columns>
                            <asp:BoundField DataField="Warehouse" HeaderText="מחסן" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="Row" HeaderText="שורה" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="Cell" HeaderText="תא" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="Shelf" HeaderText="מדף" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="CustomerID" HeaderText="קוד לקוח" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />
                            <asp:BoundField DataField="CustomerBoxNum"  HeaderText="מספר ארגז לקוח" ControlStyle-BorderColor="#929191" ItemStyle-CssClass="removeBorder" HeaderStyle-CssClass="t-center" />

                            <asp:TemplateField HeaderText="תיק" >
                                <ItemTemplate >
                                    <span id="spnFileID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "FileID")).ToString()%></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ארגז">
                                <ItemTemplate>
                                    <span id="spnBoxID" runat="server"><%#(Eval("BoxID"))%></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="הודפס">
                                <ItemTemplate>
                                    <span id="spnPrintRetrivesDate" runat="server"><%#(Eval("PrintRetrivesDate"))%></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <table style="float: left; width: 100%">
                    <tr>
                        <td style="width: 662px; text-align: right;">
                            <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />
                        </td>
                        <td style="float: left">
                            <asp:Button ID="btnPrint" Text="הדפס" runat="server" OnClientClick="return CallPrint()" OnClick="btnPrint_Click" Style="margin-bottom: 10px; width: 130px" />

                        </td>
                    </tr>

                </table>


            </ContentTemplate>
        </asp:UpdatePanel>


    </div>
    <style>
        .removeBorder {
            border-bottom-style: none !important;
        }
    </style>

      <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
