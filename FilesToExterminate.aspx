﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" EnableEventValidation="false" CodeBehind="FilesToExterminate.aspx.cs" Inherits="MaagareyArchive.FilesToExterminate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $(document).ready(function () {

            $('#overlay_form').submit(function () {
                $('input:checkbox:checked').each(function () {
                    $("#text").text($("#text").text() + $(this).val() + " ,");

                });
                $('#overlay_form').fadeOut(500);

                return false;
            });
        });

        function exterminate() {
            window.opener.$("[id*='" + "cbExterminate_" + getParameterByName('rowIndex') + "']")[0].checked = true;
            window.opener.$("[id*='" + "cbExterminate_" + getParameterByName('rowIndex') + "']")[0].closest('tr').style.backgroundColor = "white";
            window.close();
        }

        function dontExterminate() {
            window.opener.$("[id*='" + "cbExterminate_" + getParameterByName('rowIndex') + "']")[0].closest('tr').style.backgroundColor = "white";
            window.opener.$("[id*='" + "cbExterminate_" + getParameterByName('rowIndex') + "']")[0].closest('tr').style.display = "none"
            window.opener.$("[id*='" + "cbExterminate_" + getParameterByName('rowIndex') + "']")[0].checked = false;
            window.close();
        }

        function getParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>
    <form action="" id="overlay_form" method="post" style="display: none">
        <h1 id="header" runat="server"></h1>
        <div id="dvGridResults">
            <asp:DataGrid ID="dgResults" runat="server"
                AutoGenerateColumns="false" CssClass="grid"
                OnItemDataBound="dgResults_ItemDataBound">
                <HeaderStyle CssClass="header" />

                <AlternatingItemStyle CssClass="alt" />
                <ItemStyle CssClass="row" />
                <Columns>
                    <asp:TemplateColumn HeaderText="מספר תיק" ItemStyle-CssClass="ltr">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="fileID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "FileID"))==null?"":(DataBinder.Eval(Container.DataItem, "FileID")).ToString()%></span>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="שנת ביעור" ItemStyle-CssClass="ltr">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="ExterminateYear" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ExterminateYear")).ToString()%> </span>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="תיאור ארגז" ItemStyle-CssClass="ltr">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="numOfFiles" runat="server"><%#(DataBinder.Eval(Container.DataItem, "Description1")).ToString()%> </span>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="מספר ארגז לקוח" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="fullName" runat="server"><%#(DataBinder.Eval(Container.DataItem, "CustomerBoxNum")).ToString()%> </span>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>
            <table style="width: 100%; border-spacing: 20px">
                <tr>
                    <td style="float: left; margin-left: 5px;">
                        <asp:Button ID="btnDontExterminate" Text="אל תגרוס" runat="server" OnClientClick="dontExterminate()" />
                    </td>
                    <td style="width: 100px">
                        <asp:Button ID="btnExterminate" Text="גרוס בכל זאת" OnClientClick="exterminate()" runat="server" />
                    </td>
                </tr>
            </table>
        </div>

    </form>
</asp:Content>
