﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="ShippingReceivedReport.aspx.cs" Inherits="MaagareyArchive.ShippingReceivedReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <h1 id="header" runat="server" > </h1>
    <div style="overflow-x:auto;width:800px">
        <asp:DataGrid  ID="dgResults" runat="server"
             AutoGenerateColumns="false"  CssClass="gridPopup"
             EnableViewState="true" style="width: auto;"
            OnItemDataBound="dgResults_ItemDataBound">
            <HeaderStyle CssClass="header"/>
            <AlternatingItemStyle CssClass="alt" />
            <ItemStyle CssClass="row" />
            <Columns>
            </Columns>
        </asp:DataGrid>
    </div>
</asp:Content>
