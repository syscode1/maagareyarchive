﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ExportRetrives : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
  (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                OrderedDictionary data;
                if (user.ArchiveWorker == true)
                {

                    trCustomers.Visible = true;
                    fillCustomerDdl();
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedItem.Value))
                    {
                        data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomer.SelectedItem.Value));
                        data.Insert(0, 0, Consts.ALL_DEPARTMENTS);

                        string customerID = Request.QueryString["custID"];
                        if (!string.IsNullOrEmpty(customerID))
                        {
                            ddlCustomer.SelectedValue = customerID;
                            if (Request.QueryString["from"] != null)
                                txtFromDate.Value = Request.QueryString["from"];
                            if (Request.QueryString["to"] != null)
                                txtToDate.Value = Request.QueryString["to"];
                            btnShow_Click(null, null);
                        }

                    }
                }
                else {
                    trCustomers.Visible = false;
                    data = DepartmentController.GetUserDepartmentsDictionary(user);
                    data.Insert(0, 0, Consts.ALL_DEPARTMENTS);

                }

            }
        }

        private void fillCustomerDdl()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            dgResults.DataSource = getData().Keys.ToList();//.Select(a => new { a.BoxID, a.BoxType,a.BoxDescription,a.CustomerBoxNum,a.CustomerID,a.DepartmentID,a.Exterminate,a.ExterminateDate,a.ExterminateYear,a.InsertDate,a.LablePrint,a.Location,a.LoginID,a.Mignaza });
            dgResults.AutoGenerateColumns = true;
            dgResults.DataBind();
            btnPrint.Visible = true;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<Files, FileStatus> boxes = getData();
                Dictionary<string, string> param = new Dictionary<string, string>();

                param.Add(ExcelHeaders.ResourceManager.GetString("Customer"), ddlCustomer.SelectedItem != null ? ddlCustomer.SelectedItem.Text : user.CustomerID.ToString());
                param.Add(ExcelHeaders.ResourceManager.GetString("FromDate"), txtFromDate.Value);
                param.Add(ExcelHeaders.ResourceManager.GetString("ToDate"), txtToDate.Value);
                param.Add(ExcelHeaders.ResourceManager.GetString("NumOfBoxes"), boxes.Count.ToString());
                Shippings s = new Shippings();
                exportToExcell(boxes.Keys.ToList(), "export_shipping_data", param, new string[] { nameof(s.LoginID), nameof(s.DepartmentID) });
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }




        private Dictionary<Files, FileStatus> getData()
        {
            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
            DateTime? fromShippingDate = Helper.getDate(txtFromDate.Value);
            DateTime? toShgippingDate = Helper.getDate(txtToDate.Value);
            return FileStatusController.GetFilesByCustomer(customerID, Consts.enFileStatus.inArchive, fromShippingDate==null?DateTime.MinValue: fromShippingDate.Value,toShgippingDate==null?DateTime.MaxValue: toShgippingDate.Value);

        }



        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                foreach (TableCell item in e.Item.Cells)
                {
                    string header = ExcelHeaders.ResourceManager.GetString(item.Text);
                    item.Text = string.IsNullOrEmpty(header) ? item.Text : header;
                }
        }
    }
}