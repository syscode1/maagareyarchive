﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ArchiveStructure.aspx.cs" Inherits="MaagareyArchive.ArchiveStructure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>מבנה מגנזה</h1>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
    <table cellpadding="10" cellspacing="10" style="display:inline">
        <tr>
            <td>
                <span>ממחסן</span>
                <asp:DropDownList ID="ddlFromWarehouse" runat="server">
                </asp:DropDownList>

            </td>
            <td>
                <span>עד מחסן</span>
                <asp:DropDownList ID="ddlToWarehouse" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <span>משורה</span>
                <asp:TextBox ID="txtFromRow" onkeypress="return isNumberKey(event,2)"   runat="server">
                </asp:TextBox>
            </td>
            <td>
                <span>עד שורה</span>
                <asp:TextBox ID="txtToRow" onkeypress="return isNumberKey(event,2)" runat="server">
                </asp:TextBox>
            </td>
        </tr>
                <tr>
            <td>
                <span>מתא</span>

                <asp:TextBox ID="txtFromCell"  Width="100px"  onkeypress="return maxCharLength(event,1)" runat="server">
                </asp:TextBox>
                <asp:DropDownList ID="ddlFromCell" Width="50px"  runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <span>עד תא</span>

                <asp:TextBox ID="txtToCell"  Width="100px"  onkeypress="return maxCharLength(event,1)" runat="server">
                </asp:TextBox>
                <asp:DropDownList ID="ddlToCell" Width="50px"  runat="server">
                </asp:DropDownList>
            </td>
        </tr>
                
                <tr>
            <td>
                <span>ממדף</span>
                <asp:TextBox ID="txtFromShelf" onkeypress="return isNumberKey(event,2)" runat="server">
                </asp:TextBox>
            </td>
            <td>
                <span>עד מדף</span>
                <asp:TextBox ID="txtToShelf" onkeypress="return isNumberKey(event,2)" runat="server">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
              <span>כמות</span>
                <asp:TextBox ID="txtAmount" onkeypress="return isNumberKey(event)" Width="100px" runat="server">
                </asp:TextBox>
            </td>
        </tr>
    </table>
    <table style="width: 100%;">
        <tr>
            <td style="float:right">
                <asp:Button Text="חזרה לתפריט הראשי" OnClientClick="window.location='ArchiveMaintenance.aspx'; return false;" runat="server" />
            </td>
            <td style="float:left">
                <asp:Button ID="txtInsert" Text="הכנס" runat="server" OnClick="txtInsert_Click" />
            </td>
        </tr>
    </table>
            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
