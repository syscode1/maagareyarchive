﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" EnableEventValidation="false" CodeBehind="ExterminateRequiering.aspx.cs" Inherits="MaagareyArchive.ExterminateRequiering" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>המלצה לגריסה
    </h1>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            enableDatepicker();
        });

        $(function () {
            enableDatepicker();
        });


        function enableDatepicker() {
            $('#<%=txtFromInsertDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
            $('#<%=txtToInsertDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
        }


        function print() {
            window.frames["print_frame"].document.body.innerHTML = document.getElementById("dvGridResults").innerHTML;
            window.frames["print_frame"].window.focus();
            window.frames["print_frame"].window.print();

        }

        function popup(mylink, windowname, boxID) {
            if (!window.focus)
                return true;
            var toYear = $('#<%=txtToYear.ClientID%>')[0].value;
            var href;
            var w = 1000;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            if (typeof (mylink) == 'string')
                href = mylink;
            else href = mylink.href;
            var rowIndex = mylink.closest('tr').rowIndex - 1;
            window.open(href + "?boxID=" + boxID + "&toYear=" + toYear + "&rowIndex=" + rowIndex, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }

        function showPasswordDialog() {
            $("#dialog")[0].style.visibility = "visible";
            $("#dialog")[0].style.display = "";
            $("#dialog").dialog({
                position: {
                    my: "left-300 bottom+100",
                    at: "left bottom",
                    of: "#iconHelpNewPass"
                }, height: 100
            });
        }


        var validFilesTypes = ["xls", "xlsx"];
        function ValidateFile() {
            var isValidFile = true;
            var file = document.getElementById("<%=FileUpload1.ClientID%>");
            var path = file.value;
            if (path != "") {
                var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();
                isValidFile = false;
                for (var i = 0; i < validFilesTypes.length; i++) {
                    if (ext == validFilesTypes[i]) {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile) {
                    showErrorMessage(" קובץ לא תקין , אנא בחר קובץ עם" +
                     " סיומת:\n\n" + validFilesTypes.join(", "));
                }
            }
            if (isValidFile == true)
                return confirmClear();
            return false;
        }
        function confirmClear() {
            return  confirm("עצור! לחיצה על אישור תמחק את הטבלה הנוכחית ותטען במקומה את תוצאות החיפוש האם להמשיך?");

        }
        function validateBeforeExterminate() {
            var grid = document.getElementById("<%=dgResults.ClientID%>");
            var rows = grid.rows;
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].style.backgroundColor.toLowerCase() == "red") {
                    showErrorMessage(" אין אפשרות להזמין תיקים לגריסה אם שנת הביעור שלהם  גדולה מהשנה שהוגדרה  <br /> או מהשנה הנוכחית \n\n, יש ללחוץ על מספר הארגז ולבחור האם לגרוס ");
                    return false;
                }

            }
            return true;
        }

        function showErrorMessage(text) {
            var label = document.getElementById("<%=dvErrorMessage.ClientID%>");
        label.style.color = "red";
        label.innerHTML = text;
        label.style.backgroundColor = "rgba(253, 157, 157, 0.16)";
    }
    function ExtermSelectAll(checkBox) {
        var checkboxAll = $("[id*= 'cbExterminate']");
        for (i = 0; i < checkboxAll.length; i++)
            checkboxAll[i].checked = checkBox.checked;
    }
    function cbExterminateChange(checkBox) {
        var userID=<%= user.UserID %>;
            var boxId=checkBox.parentNode.attributes.boxID.value;
            jQuery.ajax({
                url: 'ExterminateRequiering.aspx/exterminateSelectChange',
                type: "POST",
                data: "{boxID : '" + boxId + "',userID: '" + userID + "',isSelect: '" + checkBox.checked + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (data) {
                },
                failure: function (msg) {  }
            });
        }

        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                        window.location = "MainPageArchive.aspx";
                    else
                        window.location = "MainPage.aspx";
                }

    </script>
    <style>
        .row-white {
            background-color: white !important;
        }
    </style>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table style="border-spacing: 10px; display: inline">
                <tr id="trCustomers" runat="server">
                    <td>
                        <span>לקוח</span>
                        <br />

                        <asp:DropDownList ID="ddlCustomer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <span>מחלקה</span>
                        <br />

                        <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <%-- <td>
                        <span>מתאריך גריסה </span>
                        <br />
                        <input type="text" runat="server" id="txtFromExterminateDate" />
                    </td>
                    <td>
                        <span>עד תאריך גריסה</span>
                        <br />
                        <input type="text" runat="server" id="txtToExterminateDate" />
                    </td>--%>
                    <td>
                        <span>מתאריך כניסה לארכיון </span>
                        <br />
                        <input type="text" runat="server" id="txtFromInsertDate" />
                    </td>
                    <td>
                        <span>עד תאריך כניסה לארכיון</span>
                        <br />
                        <input type="text" runat="server" id="txtToInsertDate" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <span>עד שנה </span>
                        <br />
                        <asp:TextBox runat="server" ID="txtToYear" oncick="return isNumberKey(event)" />
                    </td>

                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;">
                        <h1 style="-webkit-margin-before: 0 !important;">קליטה מקובץ
                        </h1>
                        <div id="dialog" title="דרישות קובץ" style="visibility: hidden; display: none">
                            <p>יש לבחור קובץ אקסל (2003 ומעלה) , בקובץ יש לכלול עמודה שהכותרת שלה היא BoxID ומכילה את רשימת הארגזים</p>
                        </div>
                        <i id="iconHelpNewPass" class="fa fa-question-circle" style="font-size: 24px; cursor: pointer" title="חוקיות הקובץ" onclick="showPasswordDialog()"></i>

                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <div runat="server" class="dvErrorMessage">
                            <span id="dvErrorMessage" runat="server" class="dvErrorMessage"></span>

                        </div>
                    </td>
                </tr>
            </table>


        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="width: 100%; float: left;">
        <%--        <asp:Button Text="יצא" ID="btnExport" OnClick="btnExport_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />--%>

        <table style="margin: 0px; width: 100%; border-spacing: 0px; float: left; padding-bottom: 10px">
            <tr>
                <td style="width: 620px; text-align: right;">
                    <input type="button" value="יציאה" onclick="exit()" runat="server" />

                </td>
                <td>
                    <asp:Button Text="הצג" ID="btnShow" Style="float: left; margin: 10px" Width="85px" OnClientClick="return ValidateFile();" OnClick="btnShow_Click" runat="server" />

                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <input id="btnBackCustomer" type="button" value="חזרה למסך לקוח" onclick="exitCustomerScreen()" runat="server" style="display: none" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <table style="margin: 0px; border-spacing: 0px; display: inline;">

                    <tr>
                        <td colspan="2">
                            <div id="dvGridResults">
                                <asp:DataGrid ID="dgResults" runat="server" edit
                                    AutoGenerateColumns="false" CssClass="grid"
                                    OnItemDataBound="dgResults_ItemDataBound" Style="margin-top: 0px !important">
                                    <HeaderStyle CssClass="header" />

                                    <AlternatingItemStyle CssClass="alt" />
                                    <ItemStyle CssClass="row" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="מספר ארגז" ItemStyle-CssClass="ltr" SortExpression="FileID">
                                            <HeaderStyle CssClass="t-center" />

                                            <ItemStyle CssClass="t-natural va-middle" />
                                            <ItemTemplate>
                                                <a href="FilesToExterminate.aspx" onclick="return popup(this, 'notes','<%#(DataBinder.Eval(Container.DataItem, "BoxID"))%>')">
                                                    <span id="boxID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "BoxID"))==null?"":(DataBinder.Eval(Container.DataItem, "BoxID")).ToString()%></span></a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="מחלקה" ItemStyle-CssClass="ltr">
                                            <HeaderStyle CssClass="t-center" />
                                            <ItemStyle CssClass="t-natural va-middle" />
                                            <ItemTemplate>
                                                <span id="departmentName" runat="server"><%#(DataBinder.Eval(Container.DataItem, "DepartmentName"))==null?"":(DataBinder.Eval(Container.DataItem, "DepartmentName")).ToString()%></span></a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="שנת ביעור" ItemStyle-CssClass="ltr" SortExpression="Date">
                                            <HeaderStyle CssClass="t-center" />
                                            <ItemStyle CssClass="t-natural va-middle" />
                                            <ItemTemplate>
                                                <span id="date" runat="server"><%#DataBinder.Eval(Container.DataItem, "ExterminateYear")==null?"":(DataBinder.Eval(Container.DataItem, "ExterminateYear")).ToString()%> </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="תיאור ארגז" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                                            <HeaderStyle CssClass="t-center" />
                                            <ItemStyle CssClass="t-natural va-middle" />
                                            <ItemTemplate>
                                                <span id="numOfFiles" runat="server"><%#(DataBinder.Eval(Container.DataItem, "BoxDescription")).ToString()%> </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="מספר ארגז לקוח" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                                            <HeaderStyle CssClass="t-center" />
                                            <ItemStyle CssClass="t-natural va-middle" />
                                            <ItemTemplate>
                                                <span id="fullName" runat="server"><%#DataBinder.Eval(Container.DataItem, "CustomerBoxNum")!=null?(DataBinder.Eval(Container.DataItem, "CustomerBoxNum")).ToString():""%> </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-CssClass="ltr" SortExpression="BoxID">
                                            <HeaderStyle CssClass="t-center" />
                                            <HeaderTemplate>
                                                ביעור
                                                  <input type="checkbox" name="name" id="cbExtermAll" onclick="ExtermSelectAll(this)" value=" " />
                                            </HeaderTemplate>
                                            <ItemStyle CssClass="t-natural va-middle" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbExterminate" Checked='<%#(DataBinder.Eval(Container.DataItem, "IsSelect"))%>' runat="server" boxID='<%#(DataBinder.Eval(Container.DataItem, "BoxID"))%>' onclick='cbExterminateChange(this,0)' />
                                                <span id="departmentID" runat="server" style="display: none"><%#(DataBinder.Eval(Container.DataItem, "DepartmentID")).ToString()%> </span>
                                                <span id="customerID" runat="server" style="display: none"><%#(DataBinder.Eval(Container.DataItem, "CustomerID")).ToString()%> </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>

                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSendToExterm" Style="left: -98px; width: 185px; margin-top: 20px;" Text="שלח הזמנה לגריסה" Visible="false" runat="server" OnClick="btnSendToExterm_Click" OnClientClick="return validateBeforeExterminate();" />
                            <div id="dvClearTable" runat="server" style="float: left; padding-top: 20px;">
                                <button type="submit" runat="server" id="btnClearData" onserverclick="btnClearData_ServerClick" style="background-color: transparent; border-width: 0">
                                    <img style="height: 19px;" src="resources/images/remove.ico" />
                                    נקה טבלה</button>
                            </div>
                        </td>
                    </tr>
                    <%--visibility:hidden;display:none--%>
                </table>
                <iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
