﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class FilesToExterminate : System.Web.UI.Page
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!Page.IsPostBack)
            {
                string boxID=Request.QueryString["boxID"];
                if (!string.IsNullOrEmpty(boxID))
                {
                    header.InnerText = " פירוט תיקים בארגז: " + boxID;
                }
                
                dgResults.DataSource = FilesController.getFilesByBox(boxID);
                dgResults.DataBind();
            }
        }

        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int toYear = string.IsNullOrEmpty(Request.QueryString["toYear"]) ? DateTime.Now.Year : (int)Helper.getInt(Request.QueryString["toYear"]);
                DataGridItem item = e.Item;
                int? exterminateYear = (item.FindControl("ExterminateYear") as HtmlGenericControl)!= null?Helper.getInt((item.FindControl("ExterminateYear") as HtmlGenericControl).InnerText) :null;
                if (exterminateYear != null && exterminateYear > toYear)
                {
                    item.BackColor = Color.Yellow;
                }
            }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        protected void btnDontExterminate_Click(object sender, EventArgs e)
        {

        }
    }
}