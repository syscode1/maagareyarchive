﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class InputData : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.anyUser }; } }

        HtmlInputGenericControl[] TextBoxList;
        public DateTime date;
        private List<AllFields> allFields
        {
            get
            {
                if (Session["InputData_allFields"] == null)
                    Session["InputData_allFields"] = FileFieldsController.geAllFields();
                return Session["InputData_allFields"] as List<AllFields>;
            }
            set
            {
                Session["InputData_allFields"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                txtBoxNum.Focus();
                fillddlCustomers();
                enableFileFields(false, dvInputFiles);
                fillBoxBufferList();
                txtTypingName.Value = user.FullName;

                if(Request.QueryString["boxId"]!=null)
                {
                    txtBoxNum.Text = Request.QueryString["boxId"].ToString();
                    LoadBoxData();
                }
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            dvFileFields.Controls.Clear();
            Files file = new Files();
            TextBoxList = new HtmlInputGenericControl[allFields.Count()];
            int index = 0;
            foreach (AllFields field in allFields)
            {
                TextBoxList[index] = new HtmlInputGenericControl();
                TextBoxList[index].ID = field.ID.ToString();
                TextBoxList[index].Style["display"] = "none";
                TextBoxList[index].Style["width"] = "140px";
                TextBoxList[index].Attributes.Add("fieldName", field.Name);
                dvFileFields.Controls.Add(TextBoxList[index++]);
            }

        }


        [System.Web.Services.WebMethod]
        public static string checkFileExist(string fileID, string customerID, string departmentID)
        {
            Files file = FilesController.getFileByID(fileID);
            if (file == null)
                return "";
            int customer = Helper.getInt(customerID) != null ? Helper.getInt(customerID).Value : 0;
            int? department = Helper.getInt(departmentID);
            if (file.CustomerID == customer && file.DepartmentID == department)
                return file.BoxID;
            return null;
        }

        [System.Web.Services.WebMethod]
        public static string getCustByBox(string boxID)
        {
            Boxes box = BoxesController.getBoxByID(boxID);
            if (box == null)
                return "";
           
            return box.CustomerID.ToString();
        }


        protected void fileID_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem item in rptBoxFiles.Items)
            {
                LinkButton linkbtTitle = item.FindControl("lbInputDataFileID") as LinkButton;
                linkbtTitle.Style["border-bottom"] = "0px";
            }
            ((LinkButton)sender).Style["border-bottom"] = "3px solid #5f9827";
            fillFileData(((LinkButton)sender).Text);


        }

        protected void btnLoadBoxData_Click(object sender, EventArgs e)
        {
            LoadBoxData();
        }

        private void fillBoxBufferList()
        {
//            List<BoxesBufferUI> data = BoxBufferController.getAllNewBoxesBuffer();
//           rptBoxesBuffer.DataSource = data;
//           rptBoxesBuffer.DataBind();
        }
        private void LoadBoxData()
        {
            int oldSelectedCustomer = !string.IsNullOrEmpty( ddlCustomerID.SelectedValue)?Helper.getIntNotNull(ddlCustomerID.SelectedValue) :0;
            List<BoxFileUI> data = BoxesController.getBoxFiles(txtBoxNum.Text,user.CustomerID);
            rptBoxFiles.DataSource = data;
            rptBoxFiles.DataBind();
            // if(validateBoxNum)
            // { 
            enableFileFields(true, dvInputFiles);
            enableFileFields(true, btnCloseFile);
            txtFileID.Value = string.Empty;
            if (data.Count == 0) //box doesnt exist
            {
               // enableFileFields(false, btnSaveBox);
                unlockCustomerDep();
                txtBoxDescription.Value = string.Empty;
                txtExterminatedDate.Value = string.Empty;
                txtCustomerBoxNum.Value = string.Empty;
                enableFileFields(true, txtCustomerBoxNum);
                txtBoxDescription.Focus();
                txtTypingName.Value = user.FullName;
                if (ddlCustomerID.SelectedValue != null && ddlCustomerID.SelectedValue != "0")
                {
                    if (ddlDepartment.SelectedValue == null || ddlCustomerID.SelectedValue == "0")
                        fillddlDepartment();
                    else
                        ddlDepartment_SelectedIndexChanged(null, null);
                    txtCustomerBoxNum.Value = BoxesController.getLastCustomerBoxNum(Helper.getIntNotNull(ddlCustomerID.SelectedValue)).ToString();
                }
                
            }
            else
            {
                if (cbAutoFileNum.Checked == false)
                    txtFileID.Focus();
                
                if (data.Count == 1 && data[0].FileID == null) //empty box
                {
                    lockCustomerDep(data[0].CustomerID, data[0].DepartmentID);
                }
                if (data.Count > 0)
                {
                    fillBoxData(data[0].BoxDescription, data[0].ExterminateYear, data[0].CustomerBoxNum,data[0].boxLoginID);
                    enableFileFields(true, btnSaveBox);
                    if (data.Count > 1 || (data.Count == 1 && data[0].FileID != null))
                    {
                        lockCustomerDep(data[0].CustomerID, data[0].DepartmentID);
                        drowFields(data[0].DepartmentID);
                    }
                }
            }
            //lblNumerator.Text = data == null ? "" : data.Count.ToString();
            // }
            if (string.IsNullOrEmpty(txtBoxNum.Text))//clicked on add new box button
                txtBoxNum.Focus();
        }

        private void fillBoxData(string boxDescription, int? exterminateYear, int? customerBoxNum,int? boxLoginID)
        {
            txtBoxDescription.Value = boxDescription;
            txtExterminatedDate.Value = exterminateYear!=null?exterminateYear.Value.ToString():"";
            if (customerBoxNum!=null)
            {
                txtCustomerBoxNum.Value = customerBoxNum!=null?customerBoxNum.ToString():"";
                enableFileFields(false, txtCustomerBoxNum);
            }
            if (boxLoginID != null)
            {
                Users users = UserController.getUserByUserID(boxLoginID.Value);
                if (user != null)
                      txtTypingName.Value =users.FullName;

            }
            else
             txtTypingName.Value = "";
        }
        private void fillFileData(string fileID)
        {
            if (ddlCustomerID.SelectedValue != null && Helper.getInt(ddlCustomerID.SelectedValue) > 0 &&
                ddlDepartment.SelectedValue != null && Helper.getInt(ddlDepartment.SelectedValue) > 0)
            {
                Files file = FilesController.getFileByID(fileID);
                int customerID = Helper.getInt(ddlCustomerID.SelectedValue).Value;
                int departmentID = Helper.getInt(ddlDepartment.SelectedValue).Value;
                drowFields(departmentID, file);
                enableFileFields(true, dvInputFiles);
                enableFileFields(true, btnSaveFile);
               // enableFileFields(true, btnCloseFile);
                if (file != null)
                {
                    txtFileID.Value = file.FileID;
                    enableFileFields(false, txtFileID);
                    enableFileFields(false, cbAutoFileNum);
                    cbAutoFileNum.Checked = false;
                    lockCustomerDep(customerID, departmentID);
                    if (file.SubjectID != null && file.SubjectID > 0 && ddlSubject.Items.FindByValue(file.SubjectID.ToString())!=null)
                        ddlSubject.SelectedValue = file.SubjectID.ToString();
                }


            }
        }
        private void lockCustomerDep(int? customerID, int departmentID)
        {
            if (customerID != null && customerID > 0)
            {
                if (ddlCustomerID.Items.FindByValue(customerID.Value.ToString()) != null)
                {
                    ddlCustomerID.SelectedValue = customerID.Value.ToString();
                    ddlCustomerName.SelectedValue = customerID.Value.ToString();
                }
                enableFileFields(false, ddlCustomerID);
                enableFileFields(false, ddlCustomerName);
                if (cbAutoFileNum.Checked == true)
                    setAutoFileID(departmentID);
                fillDdlSubject();
                fillddlDepartment();
                ddlDepartment.SelectedValue =ddlDepartment.Items.FindByValue( departmentID.ToString())!=null?departmentID.ToString():null;
                if (departmentID > 0)
                {
                    enableFileFields(false, ddlDepartment);
                    if (string.IsNullOrEmpty(txtFileID.Value))
                    {
                        enableFileFields(true, txtFileID);
                        enableFileFields(true, cbAutoFileNum);
                    }
                }
                else
                {
                    enableFileFields(true, ddlDepartment);
                }
            }
        }
        private void unlockCustomerDep()
        {

           // ddlCustomerID.ClearSelection();
           // ddlCustomerName.ClearSelection();
          //  ddlDepartment.ClearSelection();
          //  ddlDepartment.Items.Clear();

            enableFileFields(true, ddlCustomerID);
            enableFileFields(true, ddlCustomerName);
            enableFileFields(true, ddlDepartment);
          //  enableFileFields(false, txtFileID);
          //  enableFileFields(false, cbAutoFileNum);


        }
        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerID.SelectedValue = ddlCustomerName.SelectedValue;

            if(!string.IsNullOrEmpty( txtBoxNum.Text) && txtBoxNum.Text.Length==13)
                txtCustomerBoxNum.Value = BoxesController.getLastCustomerBoxNum(Helper.getIntNotNull(ddlCustomerName.SelectedValue)).ToString();


            fillddlDepartment();

            fillDdlSubject();

            txtFileID.Value = string.Empty;
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedValue != "0")
            {
                //if (!string.IsNullOrEmpty(txtFileID.Value.Trim()))
                //    {
                //    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction",string.Format( "checkFileExist({0},{1},{2})", txtFileID.Value, ddlCustomerID.SelectedValue, ddlDepartment.SelectedValue), true); 
                //        return;
                //    }
                drowFields(Helper.getInt(ddlDepartment.SelectedValue).Value);
                enableFileFields(true, btnSaveBox);
                enableFileFields(true, btnSaveFile);
                enableFileFields(true, txtFileID);
                enableFileFields(true, cbAutoFileNum);
                if (cbAutoFileNum.Checked == true)
                {
                    setAutoFileID();

                }
                else
                {
                    txtFileID.Focus();
                }
            }

        }
        protected void ddlCustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerName.SelectedValue = ddlCustomerID.SelectedValue;
            if (!string.IsNullOrEmpty(txtBoxNum.Text) && txtBoxNum.Text.Length == 13)
                txtCustomerBoxNum.Value = BoxesController.getLastCustomerBoxNum(Helper.getIntNotNull(ddlCustomerID.SelectedValue)).ToString();

            fillddlDepartment();

            fillDdlSubject();
        }
        private void fillddlDepartment()
        {
            OrderedDictionary data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomerID.SelectedItem.Value));
            data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
            fillDdlOrderDict(data, ddlDepartment);
        }
        private void fillDdlSubject()
        {
            OrderedDictionary data = SubjectsController.getAllSubjectsByCustomerID_OrderDict(Convert.ToInt32(ddlCustomerID.SelectedItem.Value));
            data.Insert(0, 0, Consts.SELECT);
            fillDdlOrderDict(data, ddlSubject);
        }
        private void fillddlCustomers()
        {
            if (user.ArchiveWorker == true)
            {
                ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
                ddlCustomerID.DataSource = (new MyCache()).CustomersIds;
            }
            else
            {
                OrderedDictionary data;
                Customers cust = CustomersController.getCustomerByID(user.CustomerID);
                data = new OrderedDictionary();
                data.Insert(0, cust.CustomerID, cust.CustomerName);
                ddlCustomerName.DataSource = data;
                data = new OrderedDictionary();
                data.Insert(0, cust.CustomerID, cust.CustomerID);
                ddlCustomerID.DataSource = data;
                ddlCustomerName.Enabled = false;
                ddlCustomerID.Enabled = false;
                            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();

            ddlCustomerID.DataTextField = "Value";
            ddlCustomerID.DataValueField = "Key";
            ddlCustomerID.DataBind();

                ddlCustomerID.SelectedValue = cust.CustomerID.ToString();
                fillddlDepartment();
            }

            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();

            ddlCustomerID.DataTextField = "Value";
            ddlCustomerID.DataValueField = "Key";
            ddlCustomerID.DataBind();

        }
        private void fillDdl(DropDownList ddl, object data)
        {
            ddl.DataSource = data;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
        }
        private void enableFileFields(bool enable, WebControl control)
        {
            if (enable)
            {
                control.Style.Clear();
            }
            else
            {
                control.Style["pointer-events"] = "none";
                control.Style["opacity"] = "0.4";
            }
        }
        private void enableFileFields(bool enable, HtmlControl control)
        {
            if (enable)
            {
                control.Style.Remove("pointer-events");
                control.Style.Remove("opacity");
            }
            else
            {
                control.Style["pointer-events"] = "none";
                control.Style["opacity"] = "0.4";
            }
        }

        private void drowFields(int departmentID, Files file = null)
        {
            var fields = FileFieldsController.getFileFieldsByDepartment(departmentID);
            if (fields.Count > 0)
            {
                HtmlTable table = new HtmlTable();

                table.CellPadding = 10;
                HtmlGenericControl gc = new HtmlGenericControl();
                table.Width = "100%";
                table.Height = "100%";
                table.ID = "tblFields";
                HtmlTableRow tr = new HtmlTableRow();
                int index = 0;
                HtmlInputGenericControl tx;


                foreach (var field in fields)
                {
                    
                    HtmlTableCell td = new HtmlTableCell();
                    tx = TextBoxList.Where(t => t.ID == field.FieldID.ToString()).FirstOrDefault();
                    tx.Style["display"] = "";
                    Consts.enFieldType type = (Consts.enFieldType)Enum.Parse(typeof(Consts.enFieldType), field.Type.ToString());
                    addValidationToField(tx, type,field.MaxChars);

                    if (file != null)
                    {
                        fillFieldData(field.Name, file, tx);
                    }
                    else
                    {
                        tx.Value = string.Empty;
                    }
                    if (field.Name.Trim() == "Description1" && cbAutoFileNum.Checked == true)
                        tx.Focus();
                    tx.Attributes.Add("isRequiered", field.IsRequiered.ToString());
                    if (field.Name == "InsertDate")
                    {
                        enableFileFields(false, tx);
                        tx.Disabled = true;
                        if (file == null)
                        {
                            tx.Value = DateTime.Now.ToShortDateString();
                            tx.Attributes["type"] = "text";
                        }
                    }
                    if (field.Name.Trim() == "FullName")
                    {
                        enableFileFields(false, tx);
                        tx.Disabled = true;
                        if (file == null)
                        {
                            tx.Value = user.FullName;
                            tx.Attributes["type"] = "text";
                        }
                    }    
                    HtmlGenericControl lb = new HtmlGenericControl();
                    lb.InnerText = field.Title;
                    lb.InnerHtml += "<br>";
                    if (index == 4)
                    {
                        index = 0;
                        table.Rows.Add(tr);
                        tr = new HtmlTableRow();
                    }
                    td.Controls.Add(lb);
                    td.Controls.Add(tx);
                    // td.Controls.Add(tryTextBox);
                    tr.Cells.Add(td);
                    index++;
                }
                    table.Rows.Add(tr);
                dvFileFields.Controls.Clear();
                if (dvFileFields.FindControl(table.ClientID) != null)
                    dvFileFields.Controls.Remove(table);
                dvFileFields.DataBind();
                dvFileFields.Controls.Add(table);
            }
        }

        private void addValidationToField(HtmlInputGenericControl tx, Consts.enFieldType type,int? maxChars)
        {
            if (type == Consts.enFieldType.number)
            {
                tx.Attributes["type"] = "number";
                tx.Attributes["onkeypress"] = "return isNumberKey(event,"+ maxChars + ");";
            }
            else
            {
                tx.Attributes["type"] = Enum.GetName(typeof(Consts.enFieldType), type);

            }
            if (type == Consts.enFieldType.date)
            {
                tx.Attributes["max"] = "9999-12-31";
                tx.Attributes["min"] = "1800-01-01";
            }
            else if (maxChars != null)
            {
                tx.Attributes["maxlength"] = maxChars.Value.ToString();
            }
           
            
        }

        private void fillFieldData(string name, Files file, HtmlInputGenericControl tx)
        {
            //tx = new TextBox();
            try
            {

                var value = (file.GetType().GetProperty(name.Trim()).GetValue(file));
                if ((file.GetType().GetProperty(name.Trim()).PropertyType == typeof(DateTime?) ||
                    file.GetType().GetProperty(name.Trim()).PropertyType == typeof(DateTime)) && value != null)
                    tx.Value = Helper.getDate(value.ToString()).Value.ToString("yyyy-MM-dd");
                else
                    tx.Value = value != null ? value.ToString() : "";
            }
            catch (Exception)
            {
            }
        }
        protected void btnSaveBox_Click(object sender, EventArgs e)
        {
            string results = validateBox();

            if (!string.IsNullOrEmpty(results))
            {
                showBoxMessage(false, results);
            }
            else
            {
                if (saveBox())
                {
                    txtBoxNum.Text = string.Empty;
                    LoadBoxData();
                    txtBoxNum.Focus();
                }
            }
            enableFileFields(true, btnSaveBox);
        }

        private void clearField()
        {
            foreach (HtmlInputGenericControl item in TextBoxList)
            {
                item.Value = string.Empty;
            }
        }
        private void showBoxMessage(bool isSucceed, string message)
        {
            if (isSucceed)
            {
                dvBoxErrorMessageBottom.Style["display"] = "none";
                dvBoxErrorMessageBottom.InnerText = string.Empty;
            }
            else
            {
                dvBoxErrorMessageBottom.Style["display"] = "";
                dvBoxErrorMessageBottom.InnerText = message;
            }
        }
        private void showFileMessage(bool isSucceed, string message)
        {
            if (isSucceed)
            {
                dvFileErrorMessageBottom.Style["display"] = "none";
                dvFileErrorMessageBottom.InnerText = string.Empty;
            }
            else
            {
                dvFileErrorMessageBottom.Style["display"] = "";
                dvFileErrorMessageBottom.InnerText = message;
            }
        }
        private string validateBox()
        {
            if (!Helper.IsValidBoxID(txtBoxNum.Text))
                return Consts.INVALID_BOX_NUM + "\n";
            return string.Empty;
        }
        private bool saveBox()
        {
            dvBoxErrorMessageBottom.InnerText = string.Empty;
            dvBoxErrorMessageBottom.Style["display"] = "none";
            Boxes ExistBox = BoxesController.validateIfBoxIDExist(txtBoxNum.Text);
            int? custBoxNum = Helper.getInt(txtCustomerBoxNum.Value);
            if (ExistBox == null )
            {
                
                ExistBox = BoxesController.validateIfBoxNumExist(custBoxNum, Helper.getInt(ddlCustomerID.SelectedValue).Value);
                if (ExistBox != null && !string.IsNullOrEmpty(txtCustomerBoxNum.Value))
                {
                    dvBoxErrorMessageBottom.InnerText = Consts.BOX_BUFFER_CUSTOMER_ID_EXIST.Replace("XXX", ExistBox.BoxID);
                    dvBoxErrorMessageBottom.Style["display"] = "";
                    txtCustomerBoxNum.Value = string.Empty;
                    txtCustomerBoxNum.Focus();
                    return false;
                }
            }
            else
            {
                if (ExistBox.CustomerBoxNum != custBoxNum || ExistBox.CustomerID != Helper.getInt(ddlCustomerID.SelectedValue))
                {
                    dvBoxErrorMessageBottom.InnerText =user.ArchiveWorker==true? Consts.BOX_BUFFER_ID_EXIST.Replace("XXX", ddlCustomerID.Text).Replace("YYY", ExistBox.CustomerBoxNum==null?"":ExistBox.CustomerBoxNum.ToString())
                        : Consts.BOX_EXIST;
                    dvBoxErrorMessageBottom.Style["display"] = "";
                    txtCustomerBoxNum.Value = string.Empty;
                    txtCustomerBoxNum.Focus();
                    return false;

                }
            }
            int customerID = Helper.getInt(ddlCustomerID.SelectedValue).Value;
            Customers customer = CustomersController.getCustomerByID(customerID);
            if (customer.IsBoxDescRequiered == true && string.IsNullOrEmpty(txtBoxDescription.Value))
            {
                dvBoxErrorMessageBottom.InnerText = Consts.REQUIERED_FIELD.Replace("XXX", "טקסט לארגז");
                dvBoxErrorMessageBottom.Style["display"] = "";
                txtBoxDescription.Focus();
                return false;
            }
            if (customer.IsExtermYearRequiered == true && (string.IsNullOrEmpty(txtExterminatedDate.Value)|| Helper.getInt(txtExterminatedDate.Value)==0))
            {
                dvBoxErrorMessageBottom.InnerText = Consts.REQUIERED_FIELD.Replace("XXX", "שנת ביעור");
                dvBoxErrorMessageBottom.Style["display"] = "";
                txtExterminatedDate.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtCustomerBoxNum.Value))
            {
                custBoxNum = BoxesController.getLastCustomerBoxNum(customerID);
                hfIsAutoCustBoxNum.Value = "1";
            }

            if (ExistBox==null)
            BoxBufferController.insert(new BoxesBuffer()
            {
                BoxID = txtBoxNum.Text,
                CustomerBoxNum = custBoxNum,
                CustomerID = customerID,
                DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                InsertDate = DateTime.Now,
                ExterminateYear = !string.IsNullOrEmpty(txtExterminatedDate.Value) ? Convert.ToInt16(txtExterminatedDate.Value) : 0,
                BoxDescription = txtBoxDescription.Value,
                Print = false,
            }, user.UserID);

            Boxes b = new Boxes()
            {
                BoxID = txtBoxNum.Text,
                BoxDescription = txtBoxDescription.Value,
                CustomerID =customerID,
                DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                ExterminateYear = !string.IsNullOrEmpty(txtExterminatedDate.Value) ? Convert.ToInt16(txtExterminatedDate.Value) : 0,
                CustomerBoxNum = custBoxNum,
                LoginID = ExistBox==null? user.UserID:ExistBox.LoginID,
                InsertData=2,
            };
             bool isSucceed= BoxesController.insertUpdate(b);
            if(isSucceed)
            {
                fillBoxBufferList();
            }
            return isSucceed;
        }
        protected void btnSaveFile_Click(object sender, EventArgs e)
        {
            showFileMessage(true, "");
            string results = validateFile();
            if (!string.IsNullOrEmpty(results))
                showFileMessage(false, results);
            else
            {
                if (saveBox())
                {
                    Files file = getFile();
                    bool isSucceed = FilesController.insertUpdate(file, user);
                    if (isSucceed)
                    {
                        LoadBoxData();
                        clearField();

                        if(cbAutoFileNum.Checked==false)
                        txtFileID.Focus();
                    }
                }
                else
                    enableFileFields(true, btnSaveBox);
            }

        }

        private string validateFile()
        {
            string result = string.Empty;
            if (!Helper.IsValidFileID(txtFileID.Value))
                result += Consts.INVALID_FILE_NUM + "/n";
                result += validateBox();
            return result;
        }

        private Files getFile()
        {
            Files file = new Files();
            file.FileID = txtFileID.Value;
            int? departmentID = Helper.getInt(ddlDepartment.SelectedValue);
            file.DepartmentID = departmentID;
            file.CustomerID = Helper.getInt(ddlCustomerID.SelectedValue);
            file.BoxID = txtBoxNum.Text;
            int? subjectID = Helper.getInt(ddlSubject.SelectedValue);
            file.SubjectID = subjectID != null ? subjectID.Value : 0;
            //     HtmlTable t = dvFileFields.FindControl("tblFields") as HtmlTable;
            string tvalue = string.Empty;
            string controlid = string.Empty;
            var fields = FileFieldsController.getFileFieldsByDepartment(departmentID.Value);
            for (int i = 0; i < dvFileFields.Controls.Count; i++)
            {
                if (dvFileFields.Controls[i].GetType() == typeof(HtmlInputGenericControl))
                {
                    HtmlInputGenericControl tx = dvFileFields.Controls[i] as HtmlInputGenericControl;
                    if (fields.Where(f => f.FieldID.ToString() == tx.ID).Count() > 0)
                    {
                        PropertyInfo po = file.GetType().GetProperty(tx.Attributes["fieldName"].Trim());
                        try
                        {
                            if ((po.Name.ToLower().StartsWith("to") || po.Name.ToLower().StartsWith("end") ||
                                   po.Name.ToLower().EndsWith("to") || po.Name.ToLower().EndsWith("end")) && string.IsNullOrEmpty(tx.Value)&& i>0)
                            {
                                string prevValue = (dvFileFields.Controls[i - 1] as HtmlInputGenericControl).Value;
                                tx.Value = prevValue;
                            }
                            if (po.PropertyType == typeof(Nullable<Int32>) || po.PropertyType == typeof(Nullable<Int16>))
                                po.SetValue(file, Helper.getInt(tx.Value));
                            else if (po.PropertyType == typeof(Int32) || po.PropertyType == typeof(Int16))
                                po.SetValue(file, Helper.getInt(tx.Value) == null ? 0 : Helper.getInt(tx.Value));
                            else if (po.PropertyType == typeof(string))
                                po.SetValue(file, tx.Value);
                            else if (po.PropertyType == typeof(Nullable<DateTime>))
                                po.SetValue(file, Helper.getDate(tx.Value));
                            else if (po.PropertyType == typeof(DateTime))
                                po.SetValue(file, Helper.getDate(tx.Value) == null ? DateTime.MinValue : Helper.getDate(tx.Value));
                        }

                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            return file;
        }

        protected void btnInputFileID_Click(object sender, EventArgs e)
        {
            if (Helper.IsValidFileID(txtFileID.Value))
                fillFileData(txtFileID.Value);
            else
            {
                //error message
            }
        }

        protected void btnCloseFile_Click(object sender, EventArgs e)
        {
            //txtFileID.Value = string.Empty;
            //enableFileFields(true, txtFileID);
            //enableFileFields(true, cbAutoFileNum);
            //if (cbAutoFileNum.Checked == true)
            //    setAutoFileID();
            txtBoxNum.Text = string.Empty;
            LoadBoxData();
            txtBoxNum.Focus();

        }

        protected void cbAutoFileNum_CheckedChanged(object sender, EventArgs e)
        {
            if(cbAutoFileNum.Checked==true)
            {
                    setAutoFileID();
            }
            else
            {
                txtFileID.Value = string.Empty;
            }
        }

        private void setAutoFileID(int departmentID=-1)
        {
        Int64 lastFileNum = 0;
        Int64.TryParse(ParametersController.GetParameterValue(Consts.Parameters.PARAM_LAST_FILE_ID), out lastFileNum);
            if (lastFileNum > 0)
            {
                lastFileNum += 1;
                txtFileID.Value = lastFileNum.ToString();
                ParametersController.UpdateParameterValue(Consts.Parameters.PARAM_LAST_FILE_ID, lastFileNum.ToString());
                if (departmentID!=0)
                    fillFileData(txtFileID.Value);
            }
        }

    }

}