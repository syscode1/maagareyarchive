﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="MaagareyArchive.Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <table style="padding:20px;margin:20px;border-spacing:30px">
        <tr>
            <td> <asp:Button Text="יצוא נתונים לאקסל" ID="btnExportDataToExcell" class="button1" runat="server" OnClick="btnExportDataToExcell_Click"/></td>
        </tr>
         <tr>
            <td> <asp:Button Text="שליפות" ID="btnShipping" class="button1" runat="server" OnClick="btnShipping_Click"  /></td>
        </tr>
         <tr>
            <td> <asp:Button Text="גריסות" ID="btnExterminate" class="button1"  runat="server" OnClick="btnExterminate_Click" /></td>
        </tr>
         <tr>
            <td> <asp:Button Text="הזמנות" ID="btnOrders" class="button1"  runat="server" OnClick="btnOrders_Click"  /></td>
        </tr>
          <tr>
            <td> <asp:Button Text="אישורי מסירה" ID="btnReceivedShippings" class="button1"  runat="server" OnClick="btnReceivedShippings_Click"  /></td>
        </tr>
          <tr>
            <td>
                <asp:Label Text="כמות תיבות ללקוח" runat="server" />
                <asp:TextBox ID="txtNomCustBoxes" Enabled="false" BackColor="Transparent" runat="server" />
           </td>
            <td>
                <asp:Label Text="כמות תיבות מושאלות ללקוח" runat="server" />
                <asp:TextBox ID="txtNomLendBoxes" Enabled="false" BackColor="Transparent" runat="server" />
           </td>
        </tr>
    </table>
   
</asp:Content>
