﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CitiesTable.aspx.cs" Inherits="MaagareyArchive.CitiesTable" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }

    </script>
    <h1 id="header" runat="server">טבלת ערים</h1>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
            <table style="width: 400px; display: inline;">
                <tr>
                    <td>
                        <asp:Label Text="שם העיר" runat="server" />
                        <asp:TextBox ID="txtItemName" Width="200px" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnAddItem" Text="הוסף לטבלה" OnClick="btnAddItem_Click" Width="100px" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataGrid ID="dgResults" runat="server"
                            AutoGenerateColumns="false" CssClass="grid" Width="400px">
                            <%--DataKeyField="FileID"--%>

                            <HeaderStyle CssClass="header" />

                            <AlternatingItemStyle CssClass="alt" />
                            <ItemStyle CssClass="row" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="מס'" ItemStyle-CssClass="ltr">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate><%# rownum++  %></ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="פריט" ItemStyle-CssClass="ltr">
                                    <HeaderStyle CssClass="t-center" />
                                    <ItemStyle CssClass="t-natural va-middle" />
                                    <ItemTemplate>
                                        <span id="itemName" runat="server"><%#(DataBinder.Eval(Container.DataItem, "CityName")).ToString()%> </span>
                                         </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>

                    </td>
                </tr>
            </table>

            <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 462px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exit()" runat="server" />
                    </td>
               </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>