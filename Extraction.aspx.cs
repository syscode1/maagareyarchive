﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class Extraction : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }


        public const string href = "OrderDetailes.aspx?order_id=";
        private OrderedDictionary Workers
        {
            get
            {
                if (Session["extraction_workers"] == null)
                    Session["extraction_workers"] = UserController.getUsersByWorkers(Consts.enJobs.Extractor);
                return Session["extraction_workers"] as OrderedDictionary;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            txtFileBoxID.Focus();

            if (!Page.IsPostBack)
            {
                List<OrdersUI> orders = OrdersController.getAllActiveOders();
                List<OrdersUI> exterm = OrdersToExterminateController.getAllActiveExtermOders();
                List<OrdersUI> unionData = orders.Union(exterm).ToList();
                dgResults.DataSource = unionData;
                dgResults.DataBind();
                fillCustomersDdl(unionData);
                sumOfBoxesToExterm.Text = exterm.Count().ToString();
                fillWarehouseCount();


            }

        }

        private void fillWarehouseCount()
        {
            int sum = 0;
            Dictionary<string, int[]> dict = OrdersController.calcOrdersByWarehous();
            foreach (var item in dict)
            {
                HtmlTableCell tc1 = new HtmlTableCell();
                tc1.InnerText = item.Key;

                HtmlTableCell tc2 = new HtmlTableCell();
                tc2.InnerText = item.Value[0].ToString();

                HtmlTableCell tc3 = new HtmlTableCell();
                tc3.InnerText = item.Value[1].ToString();

                tblWarehouse.Rows[0].Cells.Add(tc1);
                tblWarehouse.Rows[1].Cells.Add(tc2);
                tblWarehouse.Rows[2].Cells.Add(tc3);

                sum += item.Value[0]+item.Value[1];
            }

            sumOfFilesToRetrieve.Text = sum.ToString();

            List<string> list = dict.Keys.ToList();
            list.Insert(0, "הפק דוח לפי מחסן");
            ddlWarehouse.DataSource = list;
            ddlWarehouse.DataBind();

            Dictionary<string, int> siteDict = fillSiteDict();
            ddlSite.DataSource = siteDict;
            ddlSite.DataTextField = "Key";
            ddlSite.DataValueField = "Value";
            ddlSite.DataBind();
        }

        private Dictionary<string, int> fillSiteDict()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            dict.Add("הפק דוח לפי אתר", 0);
            dict.Add("בית אלעזרי", 1);
            dict.Add("ראם", 2);
            dict.Add("אחר", 3);
            return dict;
        }

        protected void btnInputFileBoxID_Click(object sender, EventArgs e)
        {
            string orderID = OrdersController.getOrderByBoxFile(txtFileBoxID.Value, txtFileBoxID.Value);
            if (string.IsNullOrEmpty(orderID))
            {
                dvErrorMessage.InnerText = "תיק/ארגז לא קיים בהזמנה";
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "playSound('"+ParametersController.GetParameterValue(Consts.Parameters.PARAM_SOUND_ALARM_PATH_TEMPLATE)+"')", true);
            }
            else
            {
                OrdersController.scanOrder(orderID, txtFileBoxID.Value, txtFileBoxID.Value, user.UserID);
                dvErrorMessage.InnerText = "";
            }
            txtFileBoxID.Value = string.Empty;
        }
        private void fillCustomersDdl(List<OrdersUI> extractionData)
        {
            int[] customers = extractionData.GroupBy(d => d.CustomerID).Select(c => c.FirstOrDefault().CustomerID).ToArray();
             
            OrderedDictionary data=CustomersController.getAllActiveCustomersOrdered(customers);
            if (!data.Contains(0))
                data.Insert(0, 0, Consts.SELECT_CUSTOMER);
            fillDdlOrderDict(data, ddlCustomer);
        }
        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    OrdersUI order = (OrdersUI)e.Item.DataItem;
                    if (order.Urgent == true)
                    {
                        e.Item.BackColor = Color.Red;
                        hfIsUrgent.Value= ParametersController.GetParameterValue(Consts.Parameters.PARAM_SOUND_ALARM_PATH_TEMPLATE);

                    }
                    if (!string.IsNullOrEmpty(order.EmployID))
                    {
                        (e.Item.FindControl("cbCheck") as HtmlInputCheckBox).Disabled = true;
                        e.Item.BackColor = Color.Gray;

                    }
                              else if (order.isExterm == true)
                        e.Item.Style["background-color"] = "rgba(230, 60, 230, 0.92)";

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            StringBuilder ids = new StringBuilder("");

            foreach (DataGridItem item in dgResults.Items)
            {
                if ((item.FindControl("cbCheck") as HtmlInputCheckBox).Checked)
                {
                    if (ids.ToString()!=string.Empty)
                        ids.Append(",");
                    ids.Append((item.FindControl("spnOrderID") as HtmlGenericControl).InnerText);
                    
                }
            }
            Response.Redirect(href + ids.ToString());
        }

        protected void ddlWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            int? selectedWarehouse = Helper.getInt(ddlWarehouse.SelectedValue);
            if (selectedWarehouse != null)
                Response.Redirect(string.Format("OrderDetailes.aspx?fromW={0}&toW={0}", ddlWarehouse.SelectedValue));
        }

        protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            int? selectedSite = Helper.getInt(ddlSite.SelectedValue);
            if (selectedSite != null && selectedSite>0)
                switch (selectedSite.Value)
                {
                    case 1://בית אלעזרי
                        {
                            Response.Redirect(string.Format("OrderDetailes.aspx?fromW={0}&toW={1}","1","15" ));
                            break;
                        }
                    case 2://ראם
                        {
                            Response.Redirect(string.Format("OrderDetailes.aspx?fromW={0}&toW={1}","16", "199"));
                            break;
                        }
                    case 3://אחר
                        {
                            Response.Redirect(string.Format("OrderDetailes.aspx?fromW={0}&toW={1}","200","250" ));
                            break;
                        }
                    default:
                        break;
                }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("OrderDetailes.aspx?fromW={0}&toW={1}", "0", "0"));
        }
    }
}