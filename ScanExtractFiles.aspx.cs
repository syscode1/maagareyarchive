﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ScanExtractFiles : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            hfSoundPath.Value = ParametersController.GetParameterValue(Consts.Parameters.PARAM_SOUND_ALARM_PATH_TEMPLATE);
            txtFileBoxID.Focus();
        }

        protected void btnInputFileBoxID_Click(object sender, EventArgs e)
        {
            string orderID = OrdersController.getOrderByBoxFile(txtFileBoxID.Value,txtFileBoxID.Value);
            if(string.IsNullOrEmpty(orderID))
            {
                dvErrorMessage.InnerText = "תיק/ארגז לא קיים בהזמנה";
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "playSound()", true);
            }
            else
            {
                OrdersController.scanOrder(orderID, txtFileBoxID.Value, txtFileBoxID.Value, user.UserID);
                dvErrorMessage.InnerText = "";
            }
            txtFileBoxID.Value = string.Empty;
        }
    }
}