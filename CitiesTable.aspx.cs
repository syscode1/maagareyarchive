﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class CitiesTable :BasePage
    {
        public int rownum = 1;
        

        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                bindGridData();
        }

        private void bindGridData()
        {
            dgResults.DataSource = CitiesController.getAllCities();
            dgResults.DataBind();
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtItemName.Text))
            {
                CitiesController.insert(txtItemName.Text);
                txtItemName.Text = "";
                MyCache cities = new MyCache();
                cities.ClearCitiesList();
                bindGridData();
            }
        }

    }
}