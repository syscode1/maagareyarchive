﻿using dg.Utilities;
using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class LoadTextFiles : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private List<string> errorsList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string path = ParametersController.GetParameterValue(Consts.Parameters.FILES_TO_UPLOAD_PATH);
                lblPath.InnerText = path;
            }
        }

       protected void btnLoad_Click(object sender, EventArgs e)
        {
            string dir = ParametersController.GetParameterValue(Consts.Parameters.FILES_TO_UPLOAD_PATH);
            string[] files = System.IO.Directory.GetFiles(dir);

            foreach (string filePath in files)
            {
                try
                {


                    string fileName = filePath.Substring(filePath.LastIndexOf(@"\") + 1, filePath.Length - filePath.LastIndexOf(@"\") - 1);
                    string[] lines = System.IO.File.ReadAllLines(@filePath);
                    if (!Directory.Exists(dir + "\\old"))
                    {
                        Directory.CreateDirectory(dir + "\\old");
                    }
                    switch (fileName.Substring(0, 2))
                    {
                        case "SB":
                            {
                                loadShelfBox(lines);
                                File.Move(filePath, getFileNameNotExist(dir + "\\old\\",fileName.Replace(".txt", "")));
                                break;
                            }
                        case "BB":
                            {
                                loadBoxBuffer(lines,dir,fileName);
                                File.Move(filePath, getFileNameNotExist(dir + "\\old\\", fileName.Replace(".txt", "")));
                                break;
                            }
                        case "BF":
                            {
                                loadBoxFile(lines);
                                File.Move(filePath, getFileNameNotExist(dir + "\\old\\", fileName.Replace(".txt", "")));
                                break;
                            }
                        default:
                            break;
                    }

                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message), ex); 
                }

               

            }
            try
            {

            }
            catch (Exception ex)
            {

                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message), ex);
            }
            fileOfBoxBuffer(dir);

        }


        protected void btnSavePath_Click(object sender, EventArgs e)
        {
          if(!string.IsNullOrEmpty(   txtPath.Value))
            {
                ParametersController.UpdateParameterValue(Consts.Parameters.FILES_TO_UPLOAD_PATH,txtPath.Value);
                 lblPath.InnerText = ParametersController.GetParameterValue(Consts.Parameters.FILES_TO_UPLOAD_PATH);
                
            }
        }


        private void loadBoxBuffer(string[] lines, string filePath, string fileName)
        {
            string shelfID = string.Empty;
            errorsList = new List<string>();
            foreach (string line in lines)
            {
                try
                {
                    if (!string.IsNullOrEmpty(line))
                        if (line.Length == 9)
                        {
                            shelfID = line;
                        }
                        else {
                            string[] split = line.Split(';');
                            if (split.Length == 3)
                            {
                                string boxID = split[2];
                                int customerID = Helper.getIntNotNull(split[1]);
                                string custBoxNum = split[0];
                                int? customerBoxNum = !string.IsNullOrEmpty(custBoxNum) ? Helper.getInt(custBoxNum) : BoxesController.getLastCustomerBoxNum(customerID);

                                if (boxID.Length == 13 && boxID.Substring(0, 3) == "261" && customerID > 0 && !string.IsNullOrEmpty(custBoxNum))
                                {
                                    Customers customer = CustomersController.getCustomerByID(customerID);
                                    if (customer != null)
                                    {
                                        bool isValid = validateBoxBuffer(boxID, customerID, customerBoxNum, customer, line);
                                        if (isValid)
                                        {
    
                                            BoxesController.insert(
                                                new Boxes()
                                                {
                                                    BoxID = boxID,
                                                    CustomerID = customerID,
                                                    CustomerBoxNum = customerBoxNum,
                                                    DepartmentID = 1,
                                                    ExterminateYear = 2199,
                                                    BoxDescription = "",
                                                    InsertDate = DateTime.Now,
                                                    LoginID = user.UserID,
                                                    Location = false,
                                                    BoxType = 1,
                                                    Exterminate = false,
                                                    InsertData = customer.InsertData == true ? 1 : 0,
                                                    ShelfID = shelfID,
                                                });

                                            bool isSecceed = BoxBufferController.insert(new BoxesBuffer()
                                            {
                                                BoxID = boxID,
                                                CustomerBoxNum = customerBoxNum,
                                                CustomerID = customerID,
                                                InsertDate = DateTime.Now,
                                                Print = false,
                                                DepartmentID = 1,
                                                ExterminateYear = 2199,
                                                BoxDescription = "",
                                                isPlaced = true,
                                            }, user.UserID);
                                            if (!isSecceed)
                                                addErrorToErrorsList(line, "שמירת השורה נכשלה");
                                        }
                                    }
                                    else
                                    {
                                        addErrorToErrorsList(line, "מס' לקוח לא קיים במערכת");
                                    }
                                }
                                else
                                {
                                    if (boxID.Length != 13 || boxID.Substring(0, 3) != "261")
                                        addErrorToErrorsList(line, "מס' ארגז לא תקין");
                                    if (customerID <= 0)
                                        addErrorToErrorsList(line, "מס' לקוח לא תקין");
                                    if (string.IsNullOrEmpty(custBoxNum))
                                        addErrorToErrorsList(line, "מס' ארגז לקוח לא תקין");
                                }

                            }
                            else
                            {
                                addErrorToErrorsList(line, "חסרים נתונים");
                            }
                        }
                }

                catch (Exception ex)
                {
                    addErrorToErrorsList(line, "שמירת השורה נכשלה");
                    logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message),ex);

                }
            }
            if (errorsList.Count > 0)
            {
                using (StreamWriter outputFile = new StreamWriter(getFileNameNotExist(filePath + @"\old\", fileName.Replace(".txt",  "_error") ) ))
                {
                    foreach (string line in errorsList)
                        {
                            outputFile.WriteLine(line);
                        }
            }
        }
    }

        private void loadShelfBox(string[] lines)
        {
            string shelfID = string.Empty;
            
            foreach (string line in lines)
            {
                try
                {
                    if (line.Trim().Length == 9)
                    {

                        shelfID = line.Trim();

                    }
                    else if (line.Trim().Substring(0, 3) == "261" && line.Trim().Length == 13 && !string.IsNullOrEmpty(shelfID))
                    {
                        BoxesController.updateBoxShelfLocation(false, line.Trim(), shelfID, user.UserID);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message), ex);
                }
                
            }
        }
        private void loadBoxFile(string[] lines)
        {
            string boxID = string.Empty;
            foreach (string line in lines)
            {
                try
                {

                
                switch (line.Trim().Substring(0, 3))
                {
                    case "261":
                        {
                            if (line.Trim().Length == 13)
                            {
                                boxID = line.Trim();
                            }
                            else
                            {
                                boxID = string.Empty;
                            }

                            break;
                        }
                    case "262":
                        {
                            if (line.Trim().Length == 13)
                            {
                                if (!string.IsNullOrEmpty(boxID))
                                {
                                    FilesController.updateFileBox(line.Trim(), boxID, user.UserID);
                                }
                            }
                            break;
                        }
                    default:
                        break;
                }
}
                catch (Exception ex)
                {
                    logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message), ex);
                }

            }
        }
        private void fileOfBoxBuffer(string dir)
        {

            List<BoxesBuffer> bb = BoxBufferController.getBoxesBuffer();
            try
            {

         
            using (StreamWriter outputFile = new StreamWriter(dir + @"\boxescus" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt"))
            {
                foreach (BoxesBuffer b in bb)
                {
                    string line = string.Format("{0}\t{1}\t{2}", b.BoxID, b.CustomerID, b.CustomerBoxNum);
                    outputFile.WriteLine(line);
                }
            }
            }
            catch (Exception ex)
            {

                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message), ex); ;
            }

        }
        private bool validateBoxBuffer(string boxID, int customerID, int? custBoxNum,Customers customer,string line)
        {
            //bool isValid = true;
            Boxes ExistBox = BoxesController.validateIfBoxIDExist(boxID);
            if (ExistBox != null)
            {
                addErrorToErrorsList(line, Consts.BOX_BUFFER_ID_EXIST.Replace("XXX", customerID.ToString()).Replace("YYY", custBoxNum!=null?custBoxNum.ToString():""));
                /*isValid=*/ return  false;
            }
            ExistBox = BoxesController.validateIfBoxNumExist(custBoxNum, customerID);
            if (ExistBox != null && custBoxNum!=null)
            {
                addErrorToErrorsList(line, Consts.BOX_BUFFER_CUSTOMER_ID_EXIST.Replace("XXX", boxID));
                /*isValid=*/ return false;
            }
             return true /*isValid*/ ;
        }

 
        private void addErrorToErrorsList(string line,string error)
        {
            errorsList.Add(string.Format("{0}\t{1}",line,error));
        }
        private string getFileNameNotExist(string dir, string file)
        {
            string fileName = dir + file + ".txt";
            if (File.Exists(fileName))
            {
                int index = 1;
                fileName = dir + file + "(" + index + ")" + ".txt";
                while (File.Exists(fileName))
                    fileName = dir + file + "(" + ++index + ")" + ".txt";
            }
            return fileName;
        }
    }
}