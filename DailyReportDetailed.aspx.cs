﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class DailyReportDetailed :BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            fillDdl(ddlCustomer);
        }

        private void fillDdl(DropDownList ddl)
        {
            ddl.DataSource = (new MyCache()).CustomersNames;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();

        }

    }
}