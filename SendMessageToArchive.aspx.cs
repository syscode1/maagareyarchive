﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class SendMessageToArchive :BasePage
    {

        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            txtTo.Text =Consts.PARAM_EMAIL_MESSAGE_TO_TEXT;
            txtTo.Enabled = false;

        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                string to = ParametersController.GetParameterValue(Consts.Parameters.PARAM_EMAIL_MESSAGE_TO_ADDRESS);
                string from = ParametersController.GetParameterValue(Consts.Parameters.PARAM_EMAIL_MESSAGE_FROM_ADDRESS);
                EventController.insert(new Events()
                {
                    CustomerID = user.CustomerID,
                    Date = DateTime.Now,
                    DepartmentID =0,// user.DepartmentID,
                    EmailSubject = txtSubject.Text,
                    EmailBody = txtBody.Text,
                    EventType = (int)Consts.enEventTypes.userComplaint,
                    UserID = user.UserID,
                    UserName = user.FullName,
                    EmailTo = txtTo.Text == Consts.PARAM_EMAIL_MESSAGE_TO_TEXT ? to : txtTo.Text,
                    SendTo=Consts.OFFICE_VALUE,
                });
                Customers cust = CustomersController.getCustomerByID(user.CustomerID);
                string subject = Consts.EMAIL_SUBJECT.Replace("XXX", user.FullName) + " " + txtSubject.Text;
                string body = ParametersController.GetParameterValue(Consts.Parameters.PARAM_EMAIL_MESSAGE_BODY_TEMPLATE).
                    Replace("#USER_NAME", user.FullName).
                    Replace("#USER_ID", user.UserID.ToString()).
                    Replace("#CUSTOMER_NAME", cust.CustomerName).
                    Replace("#BODY", txtBody.Text);
                string strFileName = "";
                if (AttachedFile.PostedFile != null)
                {
                    /* Get a reference to PostedFile object */
                    HttpPostedFile attFile = AttachedFile.PostedFile;
                    /* Get size of the file */
                    int attachFileLength = attFile.ContentLength;
                    /* Make sure the size of the file is > 0  */
                    if (attachFileLength > 0)
                    {
                        /* Get the file name */
                        strFileName = Path.GetFileName(AttachedFile.PostedFile.FileName);
                        /* Save the file on the server */
                        AttachedFile.PostedFile.SaveAs(Server.MapPath(strFileName));
                    }
                }
                if (Helper.sendEmail(from,
                                     to,
                                     subject,
                                     body,
                                     ConfigurationManager.AppSettings["SmtpServerUserName"],
                                     ConfigurationManager.AppSettings["SmtpServerPassword"],
                                     strFileName
                                     ))


                    showHideSucceedMessage(true, Consts.CONST_SUCCEED_MESSAGE);

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

        }
        private void showHideSucceedMessage(bool toShow, string text = "")
        {
            if (toShow)
            {
                dvErrorMessage.InnerText = text;
                dvErrorMessage.Style["display"] = "block";
                dvErrorMessage.Style["visibility"] = "visible";
                dvErrorMessage.Style["background-color"] = "rgba(182, 255, 0, 0.27)";
            }
            else
            {
                dvErrorMessage.Style["display"] = "none";
                dvErrorMessage.Style["visibility"] = "hidden";
                dvErrorMessage.Style["background-color"] = "rgba(255, 255, 255, 0.16)";
            }

        }
    }
}