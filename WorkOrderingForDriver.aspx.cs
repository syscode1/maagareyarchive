﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class WorkOrderingForDriver : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        public List<ShippingsUI> shippings
        {
            get
            {
                if (Session["WorkOrderingForDriver_Shippings"] != null)
                    return Session["WorkOrderingForDriver_Shippings"] as List<ShippingsUI>;
                return null;
            }
            set
            {
                Session["WorkOrderingForDriver_Shippings"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindData();
            }

        }

        private void bindData()
        {
            string shippingId = Request.QueryString["id"];
            shippings = ShippingController.getSippings(shippingId);
            if (shippings != null && shippings.Count > 0)
            {
                dgResultsR.DataSource = shippings;
                dgResultsR.DataBind();

                hDate.InnerText = shippings[0].ShippingDate.ToString();
                hDriverID.InnerText = Request.QueryString["driver"];
                hShippingID.InnerText = shippingId;
                dgResultsR.Focus();
            }
        }

        protected void dgResultsR_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                ShippingsUI dataItem = e.Row.DataItem as ShippingsUI;
                (e.Row.FindControl("cbMove") as CheckBox).Attributes["onclick"] = "return popup('ShippingReplaceDriver.aspx?','rowID','" + dataItem.rowID + "','600')";
                //e.Row.Attributes["onclick"] = "popup(" + dataItem.CustomerID + ")";
                if (dataItem.Urgent == true)
                    e.Row.BackColor = Color.Pink;
                if (dataItem.DoneStatus == (int)Consts.enDoneStatus.Done || dataItem.DoneStatus == (int)Consts.enDoneStatus.PartlyDone)
                    if (Request.QueryString["isHistory"] == "1")
                    {
                        e.Row.Enabled = false;
                        e.Row.BackColor = Color.Gray;
                        if (dataItem.DoneStatus == (int)Consts.enDoneStatus.Done)
                            (e.Row.FindControl("cbDone") as CheckBox).Checked = true;
                        else
                            (e.Row.FindControl("cbPartlyDone") as CheckBox).Checked = true;
                    }
                    else
                        e.Row.Visible = false;
                else if ((dataItem.SumExterminateBoxes == 0 || dataItem.SumExterminateBoxes == null) &&
                            (dataItem.SumFullBoxes == 0 || dataItem.SumFullBoxes == null) &&
                            (dataItem.SumNewBoxOrder == 0 || dataItem.SumNewBoxOrder == null) &&
                            (dataItem.SumRetriveBoxes == 0 || dataItem.SumRetriveBoxes == null))
                    (e.Row.FindControl("cbPartlyDone") as CheckBox).Enabled = false;
                if (dataItem.Color == true)
                    (e.Row.FindControl("txtComment") as TextBox).BackColor=Color.Yellow;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                (e.Row.FindControl("lblSumFiles") as Label).Text = shippings.Sum(x => x.SumFiles).ToString();
                (e.Row.FindControl("lblSumBoxes") as Label).Text = shippings.Sum(x => x.SumBoxes).ToString();
                (e.Row.FindControl("lblSumNewBoxOrder") as Label).Text = shippings.Sum(x => x.SumNewBoxOrder).ToString();
                (e.Row.FindControl("lblSumFullBoxes") as Label).Text = shippings.Sum(x => x.SumFullBoxes).ToString();
                (e.Row.FindControl("lblSumRetriveBoxes") as Label).Text = shippings.Sum(x => x.SumRetriveBoxes).ToString();
                (e.Row.FindControl("lblSumExterminate") as Label).Text = shippings.Sum(x => x.SumExterminateBoxes).ToString();

            }
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ArchiveEntities context = new ArchiveEntities();
            int numInShipping = 1;
            List<ShippingsUI> NewOrder = new List<ShippingsUI>();
            List<OrderIDs> orderIDs = new List<OrderIDs>();
            string[] items = hfIndex.Value.Split(',');
            List<int> orderIds = shippings.Select(x => x.NumInShipping).ToList();
            int index = 0;
                foreach (var i in items)
                {
                string[] item = i.Split(';');
                int rowID = Helper.getInt( item[1]).Value;
                ShippingsUI ship = new ShippingsUI();
                ship.rowID = rowID;
                ship.Color = item[3] == "1" ? true : false;

                Consts.enDoneStatus status = (Consts.enDoneStatus)System.Enum.Parse(typeof(Consts.enDoneStatus), item[2]);
                if (status == Consts.enDoneStatus.NotCommited)
                {
                    OrdersController.removeShippingRowID(rowID);
                    ship.Driver = "-1";
                }
                else if (status == Consts.enDoneStatus.Done)
                {
                    ship.DoneStatus = (int)Consts.enDoneStatus.Done;
                    ship.ReceivedBy = "";//= ddlUser.SelectedValue;
                }
                ship.NumInShipping = shippings[index].NumInShipping;
                ship.DoneStatus =(int)status;
                ship.Comment = ship.Urgent == true ? item[4] + Consts.URGENT : item[4];
                NewOrder.Add(ship);
                index++;
                }


 
            //    switch (status)
            //{
            //        case Consts.enDoneStatus.NotCommited:
            //            {
            //                OrdersController.getOrderByShipping(shippings[0;
            //                break;
            //            }
            //        case Consts.enDoneStatus.:
            //            {
            //                break;
            //            }
            //        case Consts.enDoneStatus.:
            //            {
            //                break;
            //            }

            //        default:
            //        break;
            
            ShippingController.update(NewOrder);

            bindData();
        }
 
        
        
    }
}
