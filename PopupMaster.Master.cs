﻿using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class PopupMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void LimitToPermissions(Users user, params string[] permissions)
        {
            if (permissions == null || permissions.Length == 0 || Permissions.UserHasAnyPermissionIn(user, permissions)) return;
            Response.Redirect("404.aspx");
        }
    }
}