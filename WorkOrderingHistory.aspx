﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="WorkOrderingHistory.aspx.cs" Inherits="MaagareyArchive.WorkOrderingHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script>
        function saveOrder() {
            dvErrorMessage = $('#<%= dvErrorMessage.ClientID%>')[0];
            dvErrorMessage.style.display = "none";
            var ddlDrivers = $("[id*='ddlDrivers']");
            if (ddlDrivers[0].value != "" && ddlDrivers[0].value != "0") {
                var ids = $("#<%=dgResultsR.ClientID%> tr:has(td)");
                var strId = "";
                for (i = 0; i < ids.length; i++) {
                    var cbCheck = $("[id*='cbCheck']", ids[i])[0];
                    if (cbCheck != undefined && cbCheck.checked == true) {
                        if (strId != "")
                            strId += ","
                        strId += $("span[id*='spnIndex']", ids[i])[0].innerText + ";"

                        //     strId += $("span[id*='spnComment']", ids[i])[0].style.backgroundColor != "" ? "1" : "0";

                    }
                }
                $("[id*='hfIndex']")[0].value = strId.trim();
                return true;
            }
            else {
                dvErrorMessage.innerText = "נא לבחור נהג";
                dvErrorMessage.style.display = "";
                return false;
            }
        }
        function highlightText(event) {
            var color = ($("span[id*='spnComment']", $(event).parents("tr"))[0]).style.backgroundColor;
            if (color == "#FFFF00" || color == "rgb(255, 255, 0)")
                ($("span[id*='spnComment']", $(event).parents("tr"))[0]).style.backgroundColor = "";
            else
                ($("span[id*='spnComment']", $(event).parents("tr"))[0]).style.backgroundColor = "#FFFF00";
        }
    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <h1>
                סידור עבודה היסטורי
            </h1>
            <h2 id="header2" runat="server"></h2>            
    <asp:HiddenField ID="hfIndex" Value="" runat="server" />
    <asp:GridView ID="dgResultsR" runat="server" CssClass="grid"
        EnableViewState="true" Width="1038px"
        AutoGenerateColumns="false"
        SelectedRowStyle-BackColor="Red" 
        EnablePersistedSelection="true"
        DataKeyNames="ShippingID" 
        OnRowDataBound="dgResultsR_RowDataBound">
        <HeaderStyle CssClass="header"/>

        <AlternatingRowStyle CssClass="alt" />
        <RowStyle CssClass="row" />
        <Columns>
            <asp:TemplateField HeaderText="מספר סידור עבודה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                   <a href="WorkOrderingForDriver.aspx?id=<%#(Eval("ShippingID"))%>&driver=<%#(Eval( "DriverName"))%>&isHistory=1"  >
                                <span><%#(Eval("ShippingID"))%> </span></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="תאריך הפקת סידור עבודה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval("ShippingDate"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="נהג" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "DriverName"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ תיקים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumFiles"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumBoxes"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים ריקים" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumNewBoxOrder"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים לפינוי" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumFullBoxes"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים להחזרה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumRetriveBoxes"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים לגריסה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumExterminateBoxes"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
   
                <table style="float: left;width:100%">
                    <tr>
                        <td colspan="2">
                <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display: none"></span><span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display: none"></span></br/>

                        </td>
                    </tr>
                <tr>
                     <td style="width: 662px;text-align: right;">
                    <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />
                </td>
                    <td  style="float:left">
                     
                    </td>
                    <%--OnClientClick="return getSelectedFiles()"--%>
                </tr>
                    <tr>
                     <td  style="text-align: right;">
                        <input id="btnBackCustomer" type="button" value="חזרה למסך לקוח" onclick="exitCustomerScreen()" runat="server" style="display:none" />
                     </td>

                    </tr>
                    

            </table>
            </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
