﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MA_DAL.PermissionServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="PermissionServiceReference.ICheckArchiveClientStatus")]
    public interface ICheckArchiveClientStatus {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICheckArchiveClientStatus/GetClientStatus", ReplyAction="http://tempuri.org/ICheckArchiveClientStatus/GetClientStatusResponse")]
        bool GetClientStatus(string ClientCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICheckArchiveClientStatus/GetClientStatus", ReplyAction="http://tempuri.org/ICheckArchiveClientStatus/GetClientStatusResponse")]
        System.Threading.Tasks.Task<bool> GetClientStatusAsync(string ClientCode);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICheckArchiveClientStatusChannel : MA_DAL.PermissionServiceReference.ICheckArchiveClientStatus, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CheckArchiveClientStatusClient : System.ServiceModel.ClientBase<MA_DAL.PermissionServiceReference.ICheckArchiveClientStatus>, MA_DAL.PermissionServiceReference.ICheckArchiveClientStatus {
        
        public CheckArchiveClientStatusClient() {
        }
        
        public CheckArchiveClientStatusClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CheckArchiveClientStatusClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CheckArchiveClientStatusClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CheckArchiveClientStatusClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool GetClientStatus(string ClientCode) {
            return base.Channel.GetClientStatus(ClientCode);
        }
        
        public System.Threading.Tasks.Task<bool> GetClientStatusAsync(string ClientCode) {
            return base.Channel.GetClientStatusAsync(ClientCode);
        }
    }
}
