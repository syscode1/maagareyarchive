//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MA_DAL.MA_DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Users
    {
        public string Password { get; set; }
        public string FullName { get; set; }
        public Nullable<System.DateTime> PasswordLastDate { get; set; }
        public Nullable<bool> ArchiveWorker { get; set; }
        public Nullable<bool> Administrator { get; set; }
        public Nullable<bool> PermDep1 { get; set; }
        public Nullable<bool> PermDep2 { get; set; }
        public Nullable<bool> PermDep3 { get; set; }
        public Nullable<bool> PermDep4 { get; set; }
        public Nullable<bool> PermDep5 { get; set; }
        public Nullable<bool> PermDep6 { get; set; }
        public Nullable<bool> PermDep7 { get; set; }
        public Nullable<bool> PermDep8 { get; set; }
        public Nullable<bool> PermDep9 { get; set; }
        public Nullable<bool> PermDep10 { get; set; }
        public Nullable<bool> PermForReport { get; set; }
        public Nullable<bool> PermForOrderSpecial { get; set; }
        public Nullable<bool> PermForSearch { get; set; }
        public Nullable<bool> PermForTyping { get; set; }
        public Nullable<bool> PermForCollection { get; set; }
        public Nullable<bool> PermForCustomerScreen { get; set; }
        public Nullable<bool> PermForRetrive { get; set; }
        public Nullable<bool> PermForExterminate { get; set; }
        public Nullable<bool> PermForLocation { get; set; }
        public Nullable<bool> PermForAction { get; set; }
        public Nullable<bool> PermForWorksForDrivers { get; set; }
        public Nullable<bool> PermForMaintence { get; set; }
        public Nullable<bool> PermFormSecondRetrive { get; set; }
        public Nullable<bool> PermForOrderItems { get; set; }
        public Nullable<bool> PermForUrgentDelivery { get; set; }
        public string ID { get; set; }
        public string Phone { get; set; }
        public int Job { get; set; }
        public int CustomerID { get; set; }
        public bool Active { get; set; }
        public int UserID { get; set; }
        public string CityAddress { get; set; }
        public string FullAddress { get; set; }
        public string DescriptionAddress { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public int SubjectID { get; set; }
    
        public virtual Jobs Jobs { get; set; }
    }
}
