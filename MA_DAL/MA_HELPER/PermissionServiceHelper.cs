﻿using MA_DAL.PermissionServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_HELPER
{
    public static class PermissionServiceHelper
    {
        public static bool getPermissions()
        {
            PermissionServiceReference.CheckArchiveClientStatusClient  srv = 
                new PermissionServiceReference.CheckArchiveClientStatusClient();
            return srv.GetClientStatus("1");
        }
    }
}
