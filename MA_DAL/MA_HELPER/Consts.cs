﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_HELPER
{
  public  class Consts
    {
        public const string PARAM_EMAIL_MESSAGE_TO_TEXT = "מאגרי ארגיב וגניזה בע''מ";
        public const string PARAM_ARCHIVE_ID = "9999";
        public const int BOXES_TO_PLACE = 10;
        public const int DAYS_OF_SAVE_SHELF_ID_TO_BOX = 30;
        public const string PARAM_EMPTY_SHELF_ID = "999999999";
        public const string BOX_OUT_ARCHIVE_ID = "2619999999999";
        public const string SHELF_OUT_ARCHIVE_ID = "911111111";
        public const string SHELF_OFFICE = "977777777";
        public const string TEMP_BOX_IN_OFFICE = "2619999999997";
        public const string TEMP_BOX_IN_ARCHIVE_ID = "2619999999999";
        public const string TEMP_SHELF_IN_ARCHIVE_ID = "922222222";
        public const string OFFICE = "משרד";
        public const int OFFICE_VALUE = -1;
        public const int OFFICE_JOB_VALUE = 4;
        public const string ALL_DEPARTMENTS = "כל המחלקות";
        public const string ALL_CUSTOMERS = "כל הלקוחות";
        public const string ALL_USERS = "כל העובדים";
        public const string SELECT = "--בחר--";
        public const string SELECT_CUSTOMER = "--בחר לקוח--";
        public const string SELECT_USER = "--בחר משתמש--";
        public const string SELECT_JOB = "--בחר תפקיד--";
        public const string SELECT_DRIVER = "--בחר נהג--";
        public const string ALL_SUBJECTS = "--כל הנושאים--";
        public const string SELECT_SUPPLIER = "בחר ספק";
        public const string SELECT_ITEM = "בחר פריט";
        public const string SUM_ITEM = "סה\"כ : XXX";
        public const string URGENT = " דחוף! ";
        public const string USER_NUM = " מס' משתמש: XXX ";
        public const string USER_PASSWORD = ", סיסמא: YYY";
        public const string FROZEN_CHAR = "(מוקפא)";// ! if you change this value you must change it also in main.js!
        public const string ORDER_WAREHOSE = "מחסן ";
        public const string ORDER_WAREHOSES = "מחסנים XXX-YYY";
        public const string EXIST_ORDER = "קיימת הזמנה בעריכה עבור לקוח XXX ";

        //Messages
        public const string CUSTOMER_STATUS_FROZEN = "הלקוח מוקפא נא לפנות לטלפון 08-9349491";
        public const string NO_DATA_TO_DISPLAY = "לא נימצאו נתונים התואמים את החיפוש";
        public const string CONST_SAVE_ERROR_MESSAGE_NO_DATA = "לא נבחרו תיקים להזמנה לגריסה/ תיקים לא תקינים";
        public const string CONST_SAVE_ERROR_MESSAGE_FAIL = "ארעה שגיאה לא צפויה במהלך שמירת הנתונים";
        public const string SAVED_SUCCEED = "הנתונים נשמרו בהצלחה";
        public const string SAVED_FAIL = "שמירת הנתונים נכשלה";
        public const string SAVED_BOX_FAIL = "שמירת ארגז נכשלה";
        public const string FILE_EXIST_IN_BOX = "תיק זה קיים בארגז מס : XXX האם להמשיך?";
        public const string MISSING_PAIED_BOXES = "ללקוח זה אין מספיק ארגזים ששולמו על מנת לבצע את ההזמנה, \n כמות תיקים אפשרית להזמנה: XXX";
        public const string MISSING_SUPPLIER_NAME = "יש להזין/ לבחור שם ספק";
        public const string MISSING_USER_NAME = "יש להזין/ לבחור שם משתמש";
        public const string CONST_REQUIERD_FIELDS_ERROR_MESSAGE = "יש להזין את שדות הכתובת: עיר, כתובת, תאור ואיש קשר";
        public const string CONST_SAVE_ERROR_MESSAGE = "ארעה שגיאה לא צפויה, ההזמנה לא נקלטה במערכת.";
        public const string CONST_DUPLICATE_EMPTY_ADDRESS_ERROR_MESSAGE = "ניתן להוסיף רק שורה אחת עם כתובת 'אחר'";
        public const string CONST_SUCCEED_MESSAGE = "המייל נשלח בהצלחה!";
        public const string CONST_EVENT_SUCCEED_MESSAGE = "המשימה נוספה בהצלחה!";
        public const string CONST_SUPPLIER_ATTACHED_FILE_NAME = "המייל נשלח בהצלחה!";
        public const string CONST_MISSING_MAIL_ADDRESS = "לספק זה לא קיים כתובת מייל במערכת, ניתן לנסות שוב לאחר עדכון פרטי הספק";
        public const string CONST_ELSE_COMBO_ITEM = "אחר - יש למלא שדות כתובת למטה";
        public const string FILE_NOT_FOUND_MESSAGE = "קובץ לא נימצא";
        public const string SUPPLIER_MAIL_SUBJECT = "הזמנה חדשה , מס הזמנה: XXX";
        public const string FILE_EXIST_IN_OTHER_BOX = "מספר תיק בשימוש אחר";
        public const string INVALID_BOX_NUM = "מספר ארגז לא תקין";
        public const string INVALID_FILE_NUM = "מספר תיק לא תקין";
        public const string BOX_NOT_EXIST = "ארגז לא קיים בארכיון";
        public const string FILE_NOT_EXIST = "תיק לא קיים בארכיון";
        public const string INVALID_NUM = "מספר לא תקין";
        public const string INVALID_CUSTOMER = "לקוח לא קיים במערכת";
        public const string BOX_BUFFER_ID_EXIST = "ארגז זה כבר משויך ללקוח XXX לארגז לקוח מספר YYY";
        public const string BOX_BUFFER_CUSTOMER_ID_EXIST = "מספר ארגז לקוח זה כבר משויך לארגז XXX";
        public const string BOX_EXIST = "ארגז זה כבר קיים ומשוייך ללקוח";
        public const string REQUIERED_FIELD = "שדה XXX הינו שדה חובה";
        public const string INVALID_COMPARE_FIELDS = "\"שדה \"מ-\" צריך להיות גדול משדה \"עד";
        public const string VALIDATE_EXCEL_FILE = "שורות שלא נטענו והמקומות הדורשים תיקון:";
        public const string VALIDATE_NUMERIC_FIELD = "שורה: XXX שדה: YYY - שדה חובה, ערך מספרי עד ZZZ תוים";
        public const string VALIDATE_NUMERIC = "שורה: XXX שדה: YYY - שדה חובה, ערך מספרי ";
        public const string INVALID_FIELD= "שורה: XXX שדה: YYY - ערך לא תקין";
        public const string INVALID_BOXID_FIELD= "שורה: XXX שדה: מספר ארגז - ערך תקין מתחיל בספרות 261";
        public const string SUM_OF_BOXES = "מלאי כולל של תיבות";
        public const string INVOICES_CREATED_MESSAGE = "חשבוניות כבר הופקו לחודש XXX שנה YYY";
        public const string NOT_ENOUGH_SPACE_IN_MIGNAZA = "לא נמצאו מספיק מקומות אכסון במחסנים ובשורות שנבחרו עבור מס' הארגזים שסומנו";

        public const string EMAIL_SUBJECT = "[מאת: XXX]";

        public const string PARTLY_DONE_COMMENT = " - השלמה";
        public const int EMPTY_BOXES_ITEM_ID = 1;

        public static string[] warehouseList = new string[] { "099", "100", "101", "102", "103" };
        public class Parameters
        {
            public const string PARAM_KEY_PASSWORD_EXPIRY_DAYS = "passwordExpiryDays";
            public const string PARAM_KEY_LAST_ORDER = "lastOrder";
            public const string PARAM_KEY_LAST_SHIPPING = "lastShipping";
            public const string PARAM_KEY_LAST_DELIVERY = "lastDelivery";
            public const string PARAM_KEY_LAST_SUPPLIER_ORDER = "lastSupplierOrder";
            public const string PARAM_EMAIL_MESSAGE_TO_ADDRESS = "emailMessageTo";
            public const string PARAM_EMAIL_MESSAGE_FROM_ADDRESS = "emailMessageFrom";
            public const string PARAM_EMAIL_MESSAGE_BODY_TEMPLATE = "emailBodyTemplate";
            public const string PARAM_SOUND_ALARM_PATH_TEMPLATE = "soundAlarmPath";
            public const string PARAM_LAST_FILE_ID = "lastFileID";
            public const string PARAM_LAST_INVOICE_MONTH = "lastInvoiceMonth";
            public const string FILES_TO_UPLOAD_PATH = "filesToUploadPath";
        }

        public enum enFileStatus 
        {
            created=0,
            inArchive =1,
            waitingForShelf=2,
            inOrder=3,
            sentToCustomer=4,
            recivedInCustomer=5,
            notFound =6,
            deactivated = 7,//נגרע
            exterminated=8,// נגרס
            extracted=9,// נשלף
        }
        public enum enFieldType 
        {
            text = 1,
            number = 2,
            date = 3,
            boolean=4,
           
        }
        public enum enDepartment
        {
          general=1,
          account=2,
          claim=3,
          HR=4,
          eng=5,
          collect=6,
          law=7,
          science=8,
          customers=9,
          carClaims=10,
        }
        public enum enCustomerStatus 
        {
            inactive=0,
            active = 1,
            frozen = 2
        }
        public enum enEventTypes 
        {
            userComplaint = 1,
            repeatedRequest = 2,
            collection = 3,
            orderFromSupplier=4
        }
        public enum enDoneStatus
        {
            NotCommited = 0,
            InProgress=1,
            PartlyDone = 2,
            Done = 3,
        }
        public enum enJobs
        {
            General=1,
            Extractor = 2,
            Driver=3
        }
        public enum enPOP
        {
            Monthly=1,
            Quater = 2,
            HalfOfYear=3,
            Yearly=4,
        }
        public enum enMOP
        {
            Next=1,
            Back = 2,
            Osem=3,
            Britman=4,
        }
    }
}
