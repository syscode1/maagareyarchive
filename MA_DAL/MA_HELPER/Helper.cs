﻿using log4net;
using MA_DAL.MA_BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MA_DAL.MA_HELPER
{
    public static class  Helper
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }

        public static string getOrderID(int loginID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Today.Day < 10 ? "0" + DateTime.Today.Day.ToString() : DateTime.Today.Day.ToString());
            sb.Append(DateTime.Today.Month < 10 ? "0" + DateTime.Today.Month.ToString() : DateTime.Today.Month.ToString());
            sb.Append(DateTime.Today.Year);
            Int64 lastOrder= Convert.ToInt64(ParametersController.GetParameterValue(Consts.Parameters.PARAM_KEY_LAST_ORDER));
            lastOrder = lastOrder == 99999 ? 1 : lastOrder + 1;
            ParametersController.UpdateParameterValue(Consts.Parameters.PARAM_KEY_LAST_ORDER, lastOrder.ToString());
            string orderNum = "00000" + lastOrder.ToString();  
            sb.Append(orderNum.Substring(orderNum.Length - 5, 5));
            string userNum = "0000" + loginID.ToString();
            sb.Append(userNum.Substring(userNum.Length - 4, 4));
            return sb.ToString();
        }

        public static string getShippingID(int loginID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Today.Day < 10 ? "0" + DateTime.Today.Day.ToString() : DateTime.Today.Day.ToString());
            sb.Append(DateTime.Today.Month < 10 ? "0" + DateTime.Today.Month.ToString() : DateTime.Today.Month.ToString());
            sb.Append(DateTime.Today.Year);
            Int64 lastShipping = Convert.ToInt64(ParametersController.GetParameterValue(Consts.Parameters.PARAM_KEY_LAST_SHIPPING));
            lastShipping = lastShipping == 999999 ? 1 : lastShipping + 1;
            ParametersController.UpdateParameterValue(Consts.Parameters.PARAM_KEY_LAST_SHIPPING, lastShipping.ToString());
            string orderNum = "000000" + lastShipping.ToString();
            sb.Append(orderNum.Substring(orderNum.Length - 6, 6));
            string userNum = "0000" + loginID.ToString();
            sb.Append(userNum.Substring(userNum.Length - 4, 4));
            return sb.ToString();
        }

        public static string getDeliveryID(int loginID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Today.Day < 10 ? "0" + DateTime.Today.Day.ToString() : DateTime.Today.Day.ToString());
            sb.Append(DateTime.Today.Month < 10 ? "0" + DateTime.Today.Month.ToString() : DateTime.Today.Month.ToString());
            sb.Append(DateTime.Today.Year);
            Int64 lastDelivery = Convert.ToInt64(ParametersController.GetParameterValue(Consts.Parameters.PARAM_KEY_LAST_DELIVERY));
            lastDelivery= lastDelivery== 999999?  1: lastDelivery+1;
            ParametersController.UpdateParameterValue(Consts.Parameters.PARAM_KEY_LAST_DELIVERY, lastDelivery.ToString());
            string deliveryNum = "000000" + lastDelivery.ToString();
            sb.Append(deliveryNum.Substring(deliveryNum.Length - 6, 6));
            string userNum = "0000" + loginID.ToString();
            sb.Append(userNum.Substring(userNum.Length - 4, 4));
            return sb.ToString();
        }

        public static string getSupplierOrderID()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Today.Day < 10 ? "0" + DateTime.Today.Day.ToString() : DateTime.Today.Day.ToString());
            sb.Append(DateTime.Today.Month < 10 ? "0" + DateTime.Today.Month.ToString() : DateTime.Today.Month.ToString());
            sb.Append(DateTime.Today.Year);
            Int64 lastOrder = Convert.ToInt64(ParametersController.GetParameterValue(Consts.Parameters.PARAM_KEY_LAST_SUPPLIER_ORDER)) ;
            lastOrder = lastOrder == 9999 ? 1 : lastOrder + 1;
            ParametersController.UpdateParameterValue(Consts.Parameters.PARAM_KEY_LAST_SUPPLIER_ORDER, lastOrder.ToString());
            string orderNum = "0000" + lastOrder.ToString();
            sb.Append(orderNum.Substring(orderNum.Length - 4, 4));
            return sb.ToString();
        }
        public static bool sendEmail(string from,string to,string subject,string body,string smtpUserName,string smtpPassword ,string attachFileName="",string logoPath="")
        {
            try
            {


                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress(from);
                mail.To.Add(to);
                mail.Subject = subject;
                if (!string.IsNullOrEmpty(logoPath))
                {
                    mail.AlternateViews.Add(getEmbeddedImage(logoPath,body));
                }
                else
                    mail.Body = body;
               
                mail.IsBodyHtml = true;

                if (!string.IsNullOrEmpty(attachFileName))
                {
                    Attachment attach = new Attachment(HttpContext.Current.Server.MapPath(attachFileName));
                    mail.Attachments.Add(attach);
                }
                    

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(smtpUserName, smtpPassword);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);

                if (!string.IsNullOrEmpty( attachFileName) && File.Exists(attachFileName))
                    File.Delete(HttpContext.Current.Server.MapPath(attachFileName));
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

                return false;
            }
        }

        private static AlternateView getEmbeddedImage(String filePath,string body)
        {
            LinkedResource res = new LinkedResource(filePath);
            res.ContentId = Guid.NewGuid().ToString();
            string htmlBody = body.Replace("logo", @"<img  src='cid:" + res.ContentId + @"'/>");
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(res);
            return alternateView;
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type ColumnType = pi.PropertyType;
                if ((ColumnType.IsGenericType))
                {
                    ColumnType = ColumnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, ColumnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static DateTime? getDate(string value)
        {
            DateTime tempDateTime;
            bool isParseSucceed = DateTime.TryParse(value, out (tempDateTime));
            if(isParseSucceed)
            {
                tempDateTime= tempDateTime.AddYears(tempDateTime.Year < 1000 ? 2000 :0);
                return tempDateTime;
            }
            return  null;
        }
        public static int? getInt(string value)
        {
            int tempInt;
            bool isParseSucceed = int.TryParse(value, out (tempInt));
            return isParseSucceed ? (int?)tempInt : null;
        }
        public static long? getLong(string value)
        {
            long tempLong;
            bool isParseSucceed = long.TryParse(value, out (tempLong));
            return isParseSucceed ? (long?)tempLong : null;
        }
        public static int getIntNotNull(string value)
        {
            int tempInt;
            bool isParseSucceed = int.TryParse(value, out (tempInt));
            return isParseSucceed ? tempInt : 0;
        }
        public static decimal? getDecimal(string value)
        {
            decimal tempInt;
            bool isParseSucceed = decimal.TryParse(value, out (tempInt));
            return isParseSucceed ? (decimal?)tempInt : null;
        }

        public static DataTable ImportExcellFileToGrid(string FilePath, string Extension, string isHDR)
        {
            OleDbConnection connExcel=null;
            try
            {

            
            string conStr = "";
            switch (Extension)
            {
                case ".xls": //Excel 97-03
                    conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"]
                             .ConnectionString;
                    break;
                case ".xlsx": //Excel 07
                    conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"]
                              .ConnectionString;
                    break;
            }
            conStr = String.Format(conStr, FilePath, isHDR);
            connExcel = new OleDbConnection(conStr);
            OleDbCommand cmdExcel = new OleDbCommand();
            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            cmdExcel.Connection = connExcel;

            //Get the name of First Sheet
            connExcel.Open();
            DataTable dtExcelSchema;
            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
            connExcel.Close();

            //Read Data from First Sheet
            connExcel.Open();
            cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
            oda.SelectCommand = cmdExcel;
            oda.Fill(dt);
            connExcel.Close();

            //Bind Data to GridView
           // GridView1.Caption = Path.GetFileName(FilePath);
           return dt;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                if (connExcel != null)
                    connExcel.Close();
                return null;

            }
        }

        public static string GridViewToHtml(DataGrid gv)
        {
            gv.Style["direction"] = "rtl";
            gv.Style["float"] = "right";
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gv.RenderControl(hw);
            return sb.ToString();
        }

        public static string GridViewToHtml(GridView gv)
        {
            gv.Style["direction"] = "rtl";
            gv.Style["float"] = "right";
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gv.RenderControl(hw);
            return sb.ToString();
        }
        public static bool IsValidFileID(string fileID)
        {
            if (fileID.Length != 13)
                return false;
            if (!fileID.StartsWith("262"))
                return false;
            return true;
        }
        public static bool IsValidBoxID(string boxID)
        {
            if (boxID.Length != 13)
                return false;
            if (!boxID.StartsWith("261"))
                return false;
            return true;
        }
    }

}
