﻿using MA_DAL.MA_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_HELPER
{
    public class Permissions
    {
        public static bool UserHasAnyPermissionIn(Users user, params string[] permissionKeys)
        {
            if (permissionKeys.Length == 0) return false;
            bool permission = true;
            foreach (string perm in permissionKeys)
            {
                permission= permission & checkPermission(user, perm);
            }
            return permission;
        }

        private static bool  checkPermission(Users user, string perm)
        {
            switch (perm)
            {
                case PermissionKeys.administrator:
                    {
                        if (user.Administrator == true)
                            return true;
                        break;
                    }
                case PermissionKeys.archivWorker:
                    {
                        if (user.ArchiveWorker == true)
                            return true;
                        break;
                    }
                case PermissionKeys.anyUser:
                    {
                        return true;
                    }

                default:
                    {
                        foreach (PropertyInfo property in user.GetType().GetProperties())
                        {
                            if (perm == property.Name)
                            {
                                bool? value = (property.GetValue(user) as bool?);
                                if (value != null && value == true)
                                    return true;

                            }
                        }
                    }
                    break;
            }
        
            return false;
        }

        public struct PermissionKeys
        {
            public   const string administrator = @"administrator";
            public  const string archivWorker = @"archivWorker";
            public  const string anyUser = @"anyUser";

        }
    }
}
