﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
   public class SuppliersController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static OrderedDictionary getSuppliersList()
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                List < Suppliers > supp= context.Suppliers.ToList();
                OrderedDictionary suppOrderDict = new OrderedDictionary();
                foreach (var item in supp)
                {
                    suppOrderDict.Add(item.SupplierID, item.SupplierName);
                }
                return suppOrderDict;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new OrderedDictionary();

            }
        }

        public static Suppliers getSupplierById(int supplierID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                
                return context.Suppliers.Where(s=>s.SupplierID== supplierID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Suppliers();

            }
        }

        public static bool Update(Suppliers supplier)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                Suppliers uSupplier= context.Suppliers.Where(s => s.SupplierID ==supplier.SupplierID).FirstOrDefault();
                uSupplier.SupplierName = supplier.SupplierName;
                uSupplier.Telephone1 = supplier.Telephone1;
                uSupplier.Telephone2 = supplier.Telephone2;
                uSupplier.TermOfPayment = supplier.TermOfPayment;
                uSupplier.FullAddress = supplier.FullAddress;
                uSupplier.Email = supplier.Email;
                uSupplier.Fax = supplier.Fax;
                uSupplier.Description = supplier.Description;
                uSupplier.DateOfContract = supplier.DateOfContract;
                uSupplier.ContactMan = supplier.ContactMan;
                uSupplier.CityAddress = supplier.CityAddress;
                uSupplier.DescriptionAddress = supplier.DescriptionAddress;

                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
        public static int Insert(Suppliers supplier)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                context.Suppliers.Add(supplier);
                context.SaveChanges();
                return supplier.SupplierID;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }
        }

    }
}
