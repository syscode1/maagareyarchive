﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MA_DAL.MA_DAL;
using System.Data.Entity;
using MA_DAL.MA_BL;
using System.Collections.Specialized;
using log4net;
using MA_DAL.MA_HELPER;

namespace MA_CORE.MA_BL
{
    public class UserController : BaseController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static Users Login(int? userName, string password)
        {
            ArchiveEntities contex = new ArchiveEntities();
            if (!PermissionServiceHelper.getPermissions())
                return null;
            var users = contex.Users.Where(u => userName!=null && u.UserID == userName && u.Password == password && u.Active==true).FirstOrDefault();
            return users;
        }

        public static Users insertUpdate(Users user)
        {
            ArchiveEntities contex = new ArchiveEntities();
            bool isSucceed = false;
            try
            {
                var userForUpdate = contex.Users.Where(u => u.UserID == user.UserID).FirstOrDefault();

                if (userForUpdate != null)//update
                {
                    if (!string.IsNullOrEmpty(user.Password) && userForUpdate.Password != user.Password)
                    {
                        PasswordHistory ph = new PasswordHistory()
                        {
                            PasswordH = userForUpdate.Password,
                            UserID = userForUpdate.UserID,
                        };
                        userForUpdate.Password = user.Password;
                        userForUpdate.PasswordLastDate = DateTime.Now;
                    }
                }
                else
                {
                    userForUpdate = new Users();
                    userForUpdate.Password = user.Password;
                    userForUpdate.PasswordLastDate = DateTime.Now;
                    userForUpdate.Active = true;
                    userForUpdate.CustomerID = user.CustomerID;
                    userForUpdate.FullName = user.FullName;
                    contex.Users.Add(userForUpdate);
                }
                userForUpdate.Administrator = user.Administrator;
                userForUpdate.ArchiveWorker = user.ArchiveWorker;
                userForUpdate.Job = user.Job;
                userForUpdate.PermDep1 = user.PermDep1;
                userForUpdate.PermDep2 = user.PermDep2;
                userForUpdate.PermDep3 = user.PermDep3;
                userForUpdate.PermDep4 = user.PermDep4;
                userForUpdate.PermDep5 = user.PermDep5;
                userForUpdate.PermDep6 = user.PermDep6;
                userForUpdate.PermDep7 = user.PermDep7;
                userForUpdate.PermDep8 = user.PermDep8;
                userForUpdate.PermDep9 = user.PermDep9;
                userForUpdate.PermDep10 = user.PermDep10;
                userForUpdate.PermForAction = user.PermForAction;
                userForUpdate.PermForCollection = user.PermForCollection;
                userForUpdate.PermForCustomerScreen = user.PermForCustomerScreen;
                userForUpdate.PermForExterminate = user.PermForExterminate;
                userForUpdate.PermForLocation = user.PermForLocation;
                userForUpdate.PermForMaintence = user.PermForMaintence;
                userForUpdate.PermFormSecondRetrive = user.PermFormSecondRetrive;
                userForUpdate.PermForOrderItems = user.PermForOrderItems;
                userForUpdate.PermForOrderSpecial = user.PermForOrderSpecial;
                userForUpdate.PermForReport = user.PermForReport;
                userForUpdate.PermForRetrive = user.PermForRetrive;
                userForUpdate.PermForSearch = user.PermForSearch;
                userForUpdate.PermForTyping = user.PermForTyping;
                userForUpdate.PermForUrgentDelivery = user.PermForUrgentDelivery;
                userForUpdate.PermForWorksForDrivers = user.PermForWorksForDrivers;
                userForUpdate.SubjectID = user.SubjectID;
                userForUpdate.CityAddress = user.CityAddress;
                userForUpdate.FullAddress = user.FullAddress;
                userForUpdate.DescriptionAddress = user.DescriptionAddress;
                userForUpdate.Email = user.Email;
                userForUpdate.Fax = user.Fax;
                userForUpdate.Telephone1 = user.Telephone1;
                userForUpdate.Telephone2 = user.Telephone2;

                contex.SaveChanges();

                return userForUpdate;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return null;

        }
        public static bool ChangePassword(int userID, string NewPassword)
        {
            if (PasswordHistoryController.isPasswordExist(userID, NewPassword))
                return false;
            else
            {
                ArchiveEntities contex = new ArchiveEntities();
                Users user = contex.Users.Where(u => u.UserID == userID).FirstOrDefault();
                if( contex.PasswordHistory.Where(x=>x.UserID== userID&& x.PasswordH== user.Password).FirstOrDefault()== null)
                    contex.PasswordHistory.Add(new PasswordHistory() { UserID = userID, PasswordH = user.Password });
                user.Password = NewPassword;
                user.PasswordLastDate = DateTime.Now;
                contex.SaveChanges();
            }
            return true;
        }
        public static bool DeactivateUser(int userID)
        {

                ArchiveEntities contex = new ArchiveEntities();
                Users user = contex.Users.Where(u => u.UserID == userID).FirstOrDefault();
                user.Active = false;
                contex.SaveChanges();
            
            return true;
        }
        public static bool DeactivateUserByCustomer(int customerID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            List<int> users = contex.Users.Where(u => u.CustomerID == customerID).Select(u => u.UserID).ToList();
            foreach (int userID in users)
            {
                DeactivateUser(userID);
            }


            return true;
        }
        public static OrderedDictionary getUsersByCustomer(int customerID )
        {
            ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDetiels = new OrderedDictionary();
            try
            {
            var users = contex.Users.Where(u => u.CustomerID==customerID);
            foreach (var item in users)
            {
                UserDetiels.Add(item.UserID, item.FullName);
            }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return UserDetiels;
        }

        public static List<Users> getUsersListCustomers(int customerID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                var users = contex.Users.Where(u => u.CustomerID == customerID
                && u.PermForOrderSpecial==true).ToList();

                return users;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Users>();
            }
        }


        public static Users getUserByUserID(int userID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                var user = contex.Users.Where(u => u.UserID == userID).FirstOrDefault();
                return user;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Users();
            }

        }

        public static List<Users> getAllUsersList(int customerID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDetiels = new OrderedDictionary();
            try
            {

               return contex.Users.Where(u => u.Active == true && (customerID == 0 || u.CustomerID == customerID)).OrderBy(x => x.FullName).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return new List<Users>();
        }
        public static OrderedDictionary getAllUsers(int customerID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDetiels = new OrderedDictionary();
            try
            {
                
                var users = contex.Users.Where(u => u.Active == true && u.CustomerID== customerID).OrderBy(x=>x.FullName);
                foreach (var item in users)
                {
                    UserDetiels.Add(item.UserID, item.FullName);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return UserDetiels;
        }
                public static OrderedDictionary getAllArchiveUsers()
        {
            ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDetiels = new OrderedDictionary();
            try
            {
                int archive = Helper.getIntNotNull(Consts.PARAM_ARCHIVE_ID);
                var users = contex.Users.Where(u => u.Active == true && u.ArchiveWorker==true && u.CustomerID== archive);
                foreach (var item in users)
                {
                    UserDetiels.Add(item.UserID, item.FullName);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return UserDetiels;
        }
public static OrderedDictionary getUsersByWorkers(Consts.enJobs job )
        {
            ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDetiels = new OrderedDictionary();
            try
            {
                var users = contex.Users.Where(u => u.ArchiveWorker==true && u.Job==(int)job);
                foreach (var item in users)
                {
                    UserDetiels.Add(item.UserID, item.FullName);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return UserDetiels;
        }

    }
}
