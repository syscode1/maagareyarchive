﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class FileStatusController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void UpdateInsert(FileStatus fileStatus)
        {
            try
            {

            ArchiveEntities context = new ArchiveEntities();
            var id = fileStatus.FieldID;
            context.FileStatus.AddOrUpdate(fileStatus);
            context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        public static void Update(string fileID, Consts.enFileStatus status =Consts.enFileStatus.inArchive,string boxID="")
        {
            try
            {
            ArchiveEntities context = new ArchiveEntities();

            FileStatus fs = context.FileStatus.Where(f => f.FieldID == fileID).FirstOrDefault();
            fs.StatusID =(int) status;
            fs.StatusDate=DateTime.Now;
                fs.boxID = boxID;
            context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static void Update(List<string> fileID, Consts.enFileStatus status = Consts.enFileStatus.inArchive,string boxID="")
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                foreach (var item in fileID)
                {
                    Update(item, status,boxID);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static int sumNotFoundFiles(DateTime fromDate, DateTime toDate)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.FileStatus.Where(f => f.StatusID==(int)Consts.enFileStatus.notFound &&
                f.StatusDate >= fromDate && f.StatusDate <= toDate).Count();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }
        }
       public static List<DaylyReportData> sumNotFoundFilesGroupByCust(DateTime fromDate, DateTime toDate)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return (from files in context.Files
                        join fs in context.FileStatus  on files.FileID equals fs.FieldID
                        where fs.StatusID == (int)Consts.enFileStatus.notFound &&
                 fs.StatusDate >= fromDate && fs.StatusDate <= toDate
                        group new { files } by new { files.CustomerID } into bGroup
                        select new DaylyReportData()
                        {
                            CustomerID= bGroup.Key.CustomerID.Value,
                            Sum = bGroup.Count()
                        }).ToList();


            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<DaylyReportData> ();
            }
        }
        public static FileStatus Get(string fileID)
        {
            try
            {
            ArchiveEntities context = new ArchiveEntities();
            return context.FileStatus.Where(f => f.FieldID == fileID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;
            }
        }
        public static List<FileStatusUI> GetByCustomer(int customerID,Consts.enFileStatus status)
        {
            try
            {
            ArchiveEntities context = new ArchiveEntities();

                return (from fs in context.FileStatus
                        join file in context.Files on fs.FieldID equals file.FileID
                        where fs.StatusID == (int)status && file.CustomerID == customerID
                        select new FileStatusUI()
                        {
                            DepartmentID = file.DepartmentID==null?0:file.DepartmentID.Value,
                            FieldID = fs.FieldID,
                            LoginID=fs.LoginID,
                            StatusDate=fs.StatusDate,
                        }).OrderByDescending(x=>x.StatusDate).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;
            }
        }
        public static Dictionary<Files,FileStatus> GetFilesByCustomer(int customerID,Consts.enFileStatus status,DateTime fromDate,DateTime toDate)
        {
            try
            {
            ArchiveEntities context = new ArchiveEntities();

                return (from fs in context.FileStatus
                        join file in context.Files on fs.FieldID equals file.FileID
                        where fs.StatusID == (int)status && file.CustomerID == customerID &&
                        fs.StatusDate>=fromDate && fs.StatusDate<=toDate 
                        select new  { file,fs }).OrderByDescending(x=>x.fs.StatusDate).ToDictionary(kvp => kvp.file, kvp => kvp.fs);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;
            }
        }


    }
    public partial class FileStatusUI
    {
        public string FieldID { get; set; }
        public int StatusID { get; set; }
        public int DepartmentID { get; set; }
        public System.DateTime StatusDate { get; set; }
        public int? LoginID { get; set; }

    }

    public class DaylyReportData
    {
        public int CustomerID { get; set; }
        public int Sum { get; set; }

    }
}
