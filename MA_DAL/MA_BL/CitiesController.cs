﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
   public class CitiesController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static List<Cities> getAllCities()
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                return contex.Cities.OrderBy(x=>x.CityName).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Cities>();

            }

        }
        public static bool insert(string city)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                contex.Cities.Add(new Cities() { CityName = city });
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }
    }
}
