﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class SuppliersOrderController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static  List<SuppliersOrderUI> getByOrderID(string orderId)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                List<SuppliersOrderUI> suppliersOrder = (from so in contex.SuppliersOrder
                                                         join items in contex.ItemsAll on so.ItemID equals items.ItemID
                                                         where so.SuppliersOrderID == orderId
                                                         select new SuppliersOrderUI() {
                                                             SuppliersOrderID = so.SuppliersOrderID,
                                                             ItemID = so.ItemID,
                                                             ItemName = items.ItemName,
                                                             Amount = so.Amount,
                                                             AmountProvided = so.AmountProvided,
                                                             Date = so.Date,
                                                             Remark = so.Remark,
                                                             SupplierID = so.SupplierID,
                                                             SupplierPrice = so.SupplierPrice,
                                                             NumInOrder = so.numInOrder

                                                         }).ToList();
                
                return suppliersOrder;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<SuppliersOrderUI> ();
            }
        }
        public static  SuppliersOrderUI getOrder(string orderId)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                SuppliersOrderUI suppliersOrder = (from so in contex.SuppliersOrder
                                                         join supplier in contex.Suppliers on so.SupplierID equals supplier.SupplierID
                                                         where so.SuppliersOrderID == orderId
                                                         select new SuppliersOrderUI() {
                                                             SuppliersOrderID = so.SuppliersOrderID,
                                                             Amount = so.Amount,
                                                             AmountProvided = so.AmountProvided,
                                                             Date = so.Date,
                                                             Remark = so.Remark,
                                                             SupplierID = so.SupplierID,
                                                             SupplierPrice = so.SupplierPrice,
                                                             NumInOrder = so.numInOrder,
                                                             SupplierName=supplier.SupplierName,
                                                             Telephon1=supplier.Telephone1,
                                                             Fax=supplier.Fax,
                                                         }).ToList().FirstOrDefault();
                
                return suppliersOrder;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new SuppliersOrderUI();
            }
        }

        public static List<SuppliersOrderUI> getNotSuppliedOrders()
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                List<SuppliersOrderUI> suppliersOrder = (from so in contex.SuppliersOrder
                                                         join items in contex.ItemsAll on so.ItemID equals items.ItemID
                                                         join supp in contex.Suppliers on so.SupplierID equals supp.SupplierID
                                                         where so.AmountProvided < so.Amount
                                                         group so by new SuppliersOrderUI()
                                                         {
                                                             SuppliersOrderID = so.SuppliersOrderID,
                                                             Date = so.Date,
                                                             SupplierID = so.SupplierID,
                                                             SupplierName = supp.SupplierName
                                                         } into s
                                                         select new SuppliersOrderUI()
                                                         {
                                                             SuppliersOrderID = s.Key.SuppliersOrderID,
                                                             Date = s.Key.Date,
                                                             SupplierID = s.Key.SupplierID,
                                                             SupplierName= s.Key.SupplierName
                                                         }).ToList();

                return suppliersOrder;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<SuppliersOrderUI>();
            }
        }
        public static bool insert(SuppliersOrder order)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                contex.SuppliersOrder.Add(order);
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
        public static bool updateAmountProvided(List<SuppliersOrder> provided)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                foreach (var item in provided)
                {
                    SuppliersOrder so= contex.SuppliersOrder.Where(s=> s.SuppliersOrderID==item.SuppliersOrderID && s.numInOrder==item.numInOrder).FirstOrDefault();
                    so.AmountProvided = item.AmountProvided;
                }

                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
    }

    public class SuppliersOrderUI
    {

        public string SuppliersOrderID { get; set; }
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string Telephon1 { get; set; }
        public string Fax { get; set; }
        public int NumInOrder { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public Nullable<decimal> SupplierPrice { get; set; }
        public Nullable<int> Amount { get; set; }
        public string Remark { get; set; }
        public Nullable<int> AmountProvided { get; set; }

    }
}
