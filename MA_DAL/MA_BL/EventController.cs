﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class EventController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool insert(Events usewrEvent)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                context.Events.Add(usewrEvent);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }

        public static bool updateTo(int rowId,int sendTo,string eventDesc)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                Events evt=context.Events.Where(x=>x.RowID==rowId).FirstOrDefault();
                evt.SendTo = sendTo;
                evt.IsRead = false;
                evt.EventDescription += eventDesc;
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }

        public static bool updateIsRead(int rowId)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                Events evt=context.Events.Where(x=>x.RowID==rowId).FirstOrDefault();
                evt.IsRead = true;
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
        public static int getNumOfUnreadEvents(int userId,int job)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                return context.Events.Where(e => (e.SendTo == userId || (job== Consts.OFFICE_JOB_VALUE &&  e.SendTo ==Consts.OFFICE_VALUE)) && e.IsRead==false).Count();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }
        }
        public static List<Events> getEventsByCustomer(int customer)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                return context.Events.Where(e => e.CustomerID == customer).OrderByDescending(x => x.Date).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Events>();
            }
        }
        public static List<EventsUI> getAllEvents(int userID,int job)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                return (from events in context.Events
                        join cust in context.Customers on events.CustomerID equals cust.CustomerID
                        join users in context.Users on events.SendTo equals users.UserID into u
                        from subU in u.DefaultIfEmpty()
                        join eventTypes in context.EventTypes on events.EventType equals eventTypes.EventTypeID
                        where events.SendTo == userID || (job == Consts.OFFICE_JOB_VALUE && events.SendTo == Consts.OFFICE_VALUE)
                        select new EventsUI()
                        {
                            CustomerID = events.CustomerID,
                            CustomerName = cust.CustomerName,
                            Date = events.Date,
                            DepartmentID = events.DepartmentID,
                            EmailBody = events.EmailBody,
                            EmailSubject = events.EmailSubject,
                            EventDescription = events.EventDescription,
                            EventType = events.EventType,
                            EventTypeName = eventTypes.EventTypeName,
                            FileID = events.FileID,
                            FullName = subU.FullName,
                            RowID = events.RowID,
                            SendTo = events.SendTo,
                            UserID = events.UserID,
                            UserName = events.UserName,
                            IsRead=events.IsRead,
                        } ).OrderBy(x => x.IsRead).ThenByDescending(x=>x.Date).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<EventsUI>();
            }
        }
        public static EventsUI getEventByID(int rowID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                return (from events in context.Events 
                        join cust in context.Customers on events.CustomerID equals cust.CustomerID 
                        join users in context.Users on events.UserID equals users.UserID into u
                        from subU in u.DefaultIfEmpty()
                        join eventTypes in context.EventTypes on events.EventType equals eventTypes.EventTypeID
                        where events.RowID==rowID
                        select new EventsUI()
                        {
                            CustomerID= events.CustomerID,
                            CustomerName=cust.CustomerName,
                            Date=events.Date,
                            DepartmentID=events.DepartmentID,
                            EmailBody=events.EmailBody,
                            EmailSubject=events.EmailSubject,
                            EventDescription=events.EventDescription,
                            EventType=events.EventType,
                            EventTypeName=eventTypes.EventTypeName,
                            FileID=events.FileID,
                            FullName=subU.FullName,
                            RowID=events.RowID,
                            SendTo=events.SendTo,
                            UserID=events.UserID,
                            UserName=events.UserName,
                        })  .FirstOrDefault();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new EventsUI();
            }
        }
        public static List<EventTypes> getEventTypes()
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                return context.EventTypes.ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<EventTypes>();
            }
        }

    }  
        public partial class EventsUI
        {
            public string CustomerName { get; set; }
            public int CustomerID { get; set; }
            public int DepartmentID { get; set; }
            public int? UserID { get; set; }
            public System.DateTime Date { get; set; }
            public string EventDescription { get; set; }
            public int?   SendTo { get; set; }
            public string FileID { get; set; }
            public int EventType { get; set; }
            public string EventTypeName { get; set; }
            public string EmailSubject { get; set; }
            public string EmailBody { get; set; }
            public string UserName { get; set; }
            public string FullName { get; set; }
            public int RowID { get; set; }
        public bool IsRead { get; set; }
        public virtual EventTypes EventTypes { get; set; }
            public virtual Users Users { get; set; }
        
    }
}
