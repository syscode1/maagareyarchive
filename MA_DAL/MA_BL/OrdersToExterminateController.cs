﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class OrdersToExterminateController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool Insert(List<OrdersToExterminate> orders, int loginID)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                foreach (OrdersToExterminate item in orders)
                {
                    // save to orderToExterm
                    contex.OrdersToExterminate.Add(item);

                    //change location of box to 1
                    BoxesController.updateBoxLocation(true, item.BoxID);

                    //change files status
                    List<string> files = contex.Files.Where(f => f.BoxID == item.BoxID).Select(s => s.FileID).ToList();
                    if (files != null && files.Count > 0)
                    {
                        foreach (string fileId in files)
                        {
                            //Add line to status archive
                            FileStatusArchiveController.Insert(new FileStatusArchive()
                            {
                                FieldID = fileId,
                                LoginID = loginID,
                                StatusDate = DateTime.Now,
                                StatusID = (int)Consts.enFileStatus.exterminated
                            });
                            //Add if not exist /change file status
                            FileStatusController.UpdateInsert(new FileStatus()
                            {
                                FieldID = fileId,
                                LoginID = loginID,
                                StatusDate = DateTime.Now,
                                StatusID = (int)Consts.enFileStatus.exterminated
                            });
                        }
                    }

                }
                contex.SaveChanges();


                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }

        public static List<OrdersUI> getAllActiveExtermOders()
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<OrdersUI> listExterm = (from order in contex.OrdersToExterminate
                                             where order.BoxID != null
                                             group new { order } by new { order.OrderID } into ordGroup
                                             select new OrdersUI()
                                             {
                                                 OrderID = ordGroup.Key.OrderID,
                                                 CustomerID = (from row2 in ordGroup select row2.order.CustomerID).FirstOrDefault(),
                                                 count = ordGroup.Count(),
                                                 Date = (from row2 in ordGroup select row2.order.OrderDate).FirstOrDefault(),
                                                 Urgent = false,
                                                 DescriptionAddress = (from row2 in ordGroup select row2.order.DescriptionAddress).FirstOrDefault(),
                                                 Print = false,
                                                 isExterm = true,
                                             }
                                       ).OrderBy(r => r.Date).ToList();
                return listExterm;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<OrdersUI>();

            }

        }
    }
}
