﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class ItemsController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<ItemsAll> getAllItems()
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.ItemsAll.Where(i=>i.IsActive==true).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<ItemsAll>();

            }
        }
        public static OrderedDictionary getAllItemsBySupplierID(int supplierId)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                List<ItemUI> itemsList = (from supItem in context.SuppliersItems
                                          join items in context.ItemsAll on supItem.ItemID equals items.ItemID
                                          where supItem.SupplierId == supplierId &&
                                          items.IsActive==true &&
                                          supItem.IsActive==true
                                          select new ItemUI() { ItemID = supItem.ItemID, ItemName = items.ItemName }).ToList();
                OrderedDictionary suppOrderDict = new OrderedDictionary();
                foreach (var item in itemsList)
                {
                    suppOrderDict.Add(item.ItemID, item.ItemName);
                }
                return suppOrderDict;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new OrderedDictionary();

            }
        }

        public static void insertNewItem(ItemsAll item)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                context.ItemsAll.Add(item);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }
        public static void removeItemByID(int itemID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                ItemsAll item=(context.ItemsAll.Where(i => i.ItemID == itemID).FirstOrDefault());
                if(item!=null)
                {
                    item.IsActive = false;
                    item.UpdatedDate = DateTime.Now;
                    context.SaveChanges();
                }
               
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        internal class ItemUI
        {
            public ItemUI()
            {
            }

            public int ItemID { get; set; }
            public string ItemName { get; set; }
        }

    }
}
