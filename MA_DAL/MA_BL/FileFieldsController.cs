﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class FileFieldsController
    {
    private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static List<FileFieldsUI> getFileFieldsByDepartment(int departmentID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            List<FileFieldsUI> files = new List<FileFieldsUI>();
            try
            {
                files = (from depF in contex.FieldsByDepartment
                         join fields in contex.AllFields on depF.FieldID equals fields.ID
                         where depF.DepartmentID == departmentID
                         orderby depF.TabIndex
                         select new FileFieldsUI()
                         {
                             FieldID = depF.FieldID,
                             IsRequiered = depF.IsRequiered,
                             Name = fields.Name,
                             Type = fields.Type,
                             TabIndex = depF.TabIndex,
                             Title = fields.Title,
                             MaxChars=fields.MaxChars
                         }).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
            return files;
        }
 
       public static List<AllFields> geAllFields()
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.AllFields.ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<AllFields>();
            }
        }
    }

    public class FileFieldsUI

    {
        public int DepartmentID { get; set; }
        public bool IsRequiered { get; set; }
        public Nullable<int> TabIndex { get; set; }
        public Nullable<int> FieldID { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int? MaxChars { get; set; }        
        public string Title { get; set; }
    }
}
