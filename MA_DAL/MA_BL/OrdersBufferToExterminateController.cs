﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class OrdersBufferToExterminateController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool Insert(List<OrderBufferToExterm> orders,int loginID)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();

                List<OrderBufferToExterm> orderBuffer = contex.OrderBufferToExterm.Where(ob => ob.LoginID == loginID /*&& ob.DepartmentID == departmentID*/).ToList();
                int numInOrder = 1;
                if (orderBuffer.Count > 0)
                {
                    numInOrder = orderBuffer.Max(o => o.NumInOrder) + 1;
                }

                foreach (OrderBufferToExterm item in orders)
                {
                    contex.OrderBufferToExterm.Add(
                        new OrderBufferToExterm()
                        {
                            BoxID = item.BoxID,
                            CustomerID = item.CustomerID,
                            DepartmentID = item.DepartmentID,
                            FileID = item.FileID,
                            LoginID = loginID,
                            NumInOrder = numInOrder,
                            OrderDate = DateTime.Now

                        });

                   
                }
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
            logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            return false;
            }

        }

        public static List<OrderBufferToExtermUI>  getBufferByUser(int userID,int customerID)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();

                List<OrderBufferToExtermUI> orders = (from orderBufferToExterm in contex.OrderBufferToExterm
                          join box in contex.Boxes on orderBufferToExterm.BoxID equals box.BoxID
                          join dep in contex.Department on orderBufferToExterm.DepartmentID equals dep.DepartmentID
                          where orderBufferToExterm.LoginID == userID  && dep.CustomerID== customerID
                          select new OrderBufferToExtermUI()
                          {
                              LoginID = orderBufferToExterm.LoginID,
                              BoxID = orderBufferToExterm.BoxID,
                              CustomerID = orderBufferToExterm.CustomerID,
                              DepartmentID = orderBufferToExterm.DepartmentID,
                              DepartmentName="",//dep.DepartmentName,
                              NumInOrder = orderBufferToExterm.NumInOrder,
                              BoxDescription= box.BoxDescription,
                              CustomerBoxNum= box.CustomerBoxNum,
                              ExterminateYear=box.ExterminateYear,
                              OrderDate= orderBufferToExterm.OrderDate,
                              RowId= orderBufferToExterm.RowId,
                              IsSelect=orderBufferToExterm.isSelect,

                          }).ToList();
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<OrderBufferToExtermUI>();
            }

        }

        public static void updateIsSelect(int userID, string boxID, bool isSelect)
        {
            {
                try
                {
                    ArchiveEntities contex = new ArchiveEntities();
                    OrderBufferToExterm orderb = contex.OrderBufferToExterm.Where(ob => ob.BoxID == boxID && ob.LoginID == userID).FirstOrDefault();
                    orderb.isSelect = isSelect;
                    contex.SaveChanges();
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                }

            }
        }

        public static bool RemoveAll(int loginID)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                List<OrderBufferToExterm> orderBuffer = contex.OrderBufferToExterm.Where(ob => ob.LoginID == loginID).ToList();
                foreach (OrderBufferToExterm item in orderBuffer)
                {
                contex.OrderBufferToExterm.Remove(item);

                }
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }

        public static bool Remove(List<string> boxes, int loginID)
        {
            try
            {
                bool isSucceed = true;
                ArchiveEntities contex = new ArchiveEntities();
                foreach (string boxID in boxes)
                    isSucceed= isSucceed & Remove(boxID, loginID);
                return isSucceed;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }

        public static bool Remove(string boxID,int loginID)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();

                OrderBufferToExterm orderBuffer = contex.OrderBufferToExterm.Where(ob => ob.BoxID == boxID).FirstOrDefault();
                contex.OrderBufferToExterm.Remove(orderBuffer);
                contex.SaveChanges();

                List<string> files = contex.Files.Where(f => f.BoxID == boxID).Select(s => s.FileID).ToList();
                    if (files != null && files.Count > 0)
                    {
                        foreach (string fileId in files)
                        {
                            //Add line to status archive
                            FileStatusArchiveController.Insert(new FileStatusArchive()
                            {
                                FieldID = fileId,
                                LoginID = loginID,
                                StatusDate = DateTime.Now,
                                StatusID = (int)Consts.enFileStatus.inArchive,
                                boxID=boxID,
                            });
                            //Add if not exist /change file status
                            FileStatusController.UpdateInsert(new FileStatus()
                            {
                                FieldID = fileId,
                                LoginID = loginID,
                                StatusDate = DateTime.Now,
                                StatusID = (int)Consts.enFileStatus.inArchive,
                                boxID=boxID,
                            });
                        }
                    }

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }
    }

    public class OrderBufferToExtermUI
    {
        public Nullable<System.DateTime> OrderDate { get; set; }
        public int LoginID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int NumInOrder { get; set; }
        public string BoxID { get; set; }
        public string FileID { get; set; }
        public int RowId { get; set; }
        public int CustomerID { get; set; }

        public bool IsSelect { get; set; }
        public Nullable<int> ExterminateYear { get; set; }

        public string BoxDescription { get; set; }
        public int? CustomerBoxNum { get; set; }
    }
}
