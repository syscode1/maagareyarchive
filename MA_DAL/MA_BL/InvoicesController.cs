﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class InvoicesController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static int getNumOfPaiedBoxes(int customerID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                int? sum = context.Invoices.Where(b => b.CustomerID == customerID && b.ItemID == (int?)Consts.EMPTY_BOXES_ITEM_ID && b.Paied == true).Sum(emp => emp.Amount);
                return sum == null || sum <= 0 ? 0 : (int)sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        }


    }
}
  
