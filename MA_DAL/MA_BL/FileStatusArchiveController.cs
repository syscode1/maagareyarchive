﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class FileStatusArchiveController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<FileStatusArchiveUI> GetFileStatusArchive(string fileID)
        {
            ArchiveEntities contex = new ArchiveEntities();

            List<FileStatusArchiveUI> fileStatusArchiveUI = new List<FileStatusArchiveUI>();
            try
            {
               fileStatusArchiveUI= (from file in contex.FileStatusArchive
                         join status in contex.StatusNames on file.StatusID equals status.StatusID
                         join user in contex.Users on file.LoginID equals user.UserID
                         join order in contex.Orders on file.FieldID equals order.FileID into ord
                         from subOrd in ord.DefaultIfEmpty()
                         join shipping in contex.Shippings on subOrd.ShippingRowID equals shipping.rowID into ship
                         from subShip in ship.DefaultIfEmpty()
                         where file.FieldID == fileID
                         select new FileStatusArchiveUI() { FieldID= file.FieldID, LoginID= file.LoginID,
                                                            StatusID = file.StatusID, StatusName= status.StatusName,
                                                            StatusDate = file.StatusDate, FullName= user.FullName,
                                                            Comment=file.Comment,
                                                            ShippingID =file.StatusID==(int)Consts.enFileStatus.sentToCustomer && 
                                                            file.StatusDate.Year== subShip.ShippingDate.Value.Year &&
                                                            file.StatusDate.Month == subShip.ShippingDate.Value.Month &&
                                                            file.StatusDate.Day == subShip.ShippingDate.Value.Day 
                                                            ? subOrd.ShippingRowID.Value.ToString():""}).Distinct().OrderByDescending(f=>f.StatusDate).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return fileStatusArchiveUI;               
        } 
        
 public static string getLastFileBoxID(string fileID)
        {
            try
            {
            ArchiveEntities contex = new ArchiveEntities();
                FileStatusArchive fileS = contex.FileStatusArchive.Where(x => x.FieldID == fileID && x.StatusID == (int)Consts.enFileStatus.inArchive).OrderByDescending(x => x.StatusDate).FirstOrDefault();
            return fileS!=null?fileS.boxID:"";
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return "";
            }


        }

        public static void Insert(FileStatusArchive fileStatusArchiv)
        {
            try
            {
            ArchiveEntities contex = new ArchiveEntities();
            contex.FileStatusArchive.Add(fileStatusArchiv);
            contex.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }


        }

        public static void Insert(string fileId, Consts.enFileStatus status,int? userID)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                contex.FileStatusArchive.Add(new FileStatusArchive() {
                    FieldID=fileId,
                    LoginID=userID,
                    StatusDate=DateTime.Now,
                    StatusID=(int)status,
                });
                contex.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }

        }

        public static void Insert(List<string> fileId, Consts.enFileStatus status, int? userID)
        {
            try
            {
                foreach (var item in fileId)
                {
                    Insert(item, status, userID);
                }

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }

        }
    }                                       
    public class FileStatusArchiveUI
    {
        public string FieldID { get; set; }
        public int StatusID { get; set; }
        public System.DateTime StatusDate { get; set; }
        public int? LoginID { get; set; }
        public string StatusName { get; set; }
        public string FullName { get; set; }
        public string Comment { get; set; }
        public string ShippingID { get; set; }

    }
}
