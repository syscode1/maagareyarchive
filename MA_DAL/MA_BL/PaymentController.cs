﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class PaymentController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static Dictionary<int,string> getPeriodOfPaymentList()
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();
               

               return context.PeriodOfPayment.Select(p => new { p.POPID, p.MOPName })
                                            .AsEnumerable()
                                            .ToDictionary(kvp => kvp.POPID, kvp => kvp.MOPName);

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Dictionary<int, string>();

            }
        }

        public static Dictionary<int,string> getMethodOfPaymentList()
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();
               return context.MethodOfPayment.Select(p => new { p.MOPID, p.MOPName })
                                            .AsEnumerable()
                                            .ToDictionary(kvp => kvp.MOPID, kvp => kvp.MOPName);

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Dictionary<int, string>();

            }
        }
         public static List<LinkToPayment> getLinkToPaymentList()
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();
               

               return context.LinkToPayment.ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<LinkToPayment>();

            }
        }

        public static Dictionary<int, string> getCreditTerms()
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();

                return context.CreditTerms.Select(p => new { p.ID, p.Name })
                                          .AsEnumerable()
                                          .ToDictionary(kvp => kvp.ID, kvp => kvp.Name);

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Dictionary<int, string>();

            }
        }
    }
}
