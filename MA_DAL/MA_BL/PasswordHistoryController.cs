﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
   public class PasswordHistoryController:BaseController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool isPasswordExist(int userID, string password)
        {
            PasswordHistory p = null;
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                p = contex.PasswordHistory.Where(u => u.UserID == userID && u.PasswordH == password).FirstOrDefault();
                return (p != null);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return true;
            }

        }
        
    }
}
