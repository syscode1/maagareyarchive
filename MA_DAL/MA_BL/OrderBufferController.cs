﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class OrderBufferController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<OrderBufferUI> GetOrderBuffer(int loginID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            // List<OrderBufferUI> orderBuffer = contex.OrderBuffer.Where(ob => ob.LoginID == loginID /*&& ob.DepartmentID == departmentID*/).ToList();


            List<OrderBufferUI> orders = new List<OrderBufferUI>();
            try
            {
                orders=(from orderBuffer in contex.OrderBuffer
                                          join box in contex.Boxes on orderBuffer.BoxID equals box.BoxID
                                          join department in contex.Department on box.CustomerID equals department.CustomerID
                                          join file in contex.Files on orderBuffer.FileID equals file.FileID into f
                                          from subFile in f.DefaultIfEmpty()
                                          where orderBuffer.LoginID == loginID && department.DepartmentID == box.DepartmentID
                                          select new OrderBufferUI()
                                          {
                                              FileID = orderBuffer.FileID,
                                              LoginID = orderBuffer.LoginID,
                                              BoxID = orderBuffer.BoxID,
                                              CustomerID = orderBuffer.CustomerID,
                                              DepartmentID = orderBuffer.DepartmentID,
                                              Urgent = orderBuffer.Urgent,
                                              StartYear = subFile.StartYear,
                                              EndYear = subFile.EndYear,
                                              ScanNumStart = subFile.StartScanNum,
                                              ScanNumEnd = subFile.EndScanNum,
                                              Description1 = (subFile.FileID != null ? subFile.Description1 : box.BoxDescription),
                                              DepartmentName = department.DepartmentName,
                                              Email=orderBuffer.Email,
                                              Fax=orderBuffer.Fax,
                                              NumInOrder=orderBuffer.NumInOrder,
                                              Comment=orderBuffer.Comment,
                                          }).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

            return orders;

        }
       public static List<OrderBufferSearch> GetOrderBufferFiles(int loginID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                var orders = (from orderBuffer in contex.OrderBuffer
                              join box in contex.Boxes on orderBuffer.BoxID equals box.BoxID
                              join department in contex.Department on box.CustomerID equals department.CustomerID
                              join file in contex.Files on orderBuffer.FileID equals file.FileID into f
                              from subFile in f.DefaultIfEmpty()
                              where orderBuffer.LoginID == loginID && department.DepartmentID == box.DepartmentID
                              select new OrderBufferSearch() { BoxID = orderBuffer.BoxID,FileID=orderBuffer.FileID }).ToList();//.ToArray();
             return orders;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<OrderBufferSearch>();
            }

            

        }
        public static bool Insert(List<OrderBufferUI> orderBufferUI, int loginID)
        {
            bool isSucceed = false;
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                List<OrderBuffer> orderBuffer = contex.OrderBuffer.Where(ob => ob.LoginID == loginID /*&& ob.DepartmentID == departmentID*/).ToList();
                int numInOrder = 1;
                if (orderBuffer.Count > 0)
                {
                    numInOrder = orderBuffer.Max(o => o.NumInOrder) + 1;
                }

                //orderID = (long.Parse(ParametersController.GetParameterValue(Consts.Parameters.PARAM_KEY_LAST_ORDER)) + 1).ToString();
                //Add detailes to orderBuffer
                foreach (OrderBufferUI item in orderBufferUI)
                {
                    if (string.IsNullOrEmpty(item.FileID) || orderBufferUI.Where(x => x.BoxID == item.BoxID && string.IsNullOrEmpty(x.FileID)).ToList().Count <= 0)
                        contex.OrderBuffer.Add(new OrderBuffer()
                        {
                            FileID = item.FileID,
                            BoxID = item.BoxID,
                            DepartmentID = (int)item.DepartmentID,
                            CustomerID = (int)item.CustomerID,
                            LoginID = loginID,
                            OrderDate = DateTime.Now,
                            Urgent = item.Urgent,
                            NumInOrder = ++numInOrder,
                            Email = item.Email,
                            Fax = item.Fax,
                            Comment=item.Comment,
                        });
                }
                contex.SaveChanges();
                isSucceed = true;
            }

            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

            return isSucceed;
        }

        public static void Remove(int loginID, string fileID, string boxID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                contex.OrderBuffer.Remove(contex.OrderBuffer.Where(o => o.LoginID == loginID && o.FileID == fileID && o.BoxID == boxID).FirstOrDefault());
                contex.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }


        }
             public static void UpdateUrgent(int loginID, string fileID, string boxID,bool urgent)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                OrderBuffer ob= contex.OrderBuffer.Where(o => o.LoginID == loginID && o.FileID == fileID && o.BoxID == boxID).FirstOrDefault();
                if (ob != null)
                {
                    ob.Urgent = urgent;
                    contex.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }


        }                          
    }

    public class OrderBufferUI
    {
//        public string OrderID { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public int LoginID { get; set; }
        public int CustomerID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int NumInOrder { get; set; }
        public string BoxID { get; set; }
        public string FileID { get; set; }
        public Nullable<bool> Urgent { get; set; }
        public Nullable<int> StartYear { get; set; }
        public Nullable<int> EndYear { get; set; }
        public string Description1 { get; set; }
        public string ScanNumStart { get; set; }
        public string ScanNumEnd { get; set; }
        public Nullable<bool> Fax { get; set; }
        public Nullable<bool> Email { get; set; }
        public string Comment { get; set; }
    }
    public class OrderBufferSearch
    {
//        public string OrderID { get; set; }

        public string BoxID { get; set; }
        public string FileID { get; set; }

    }
}
