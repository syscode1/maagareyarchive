﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class OrdersController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool Insert(List<Orders> orders)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                foreach (Orders item in orders)
                {
                    contex.Orders.Add(item);
                    //Add line to status archive
                    List<string> files=new List<string>();
                    if (string.IsNullOrEmpty(item.FileID))
                    {
                        if (!string.IsNullOrEmpty(item.BoxID))
                            BoxesController.updateBoxLocation(true, item.BoxID);
                        files = FilesController.getFilesListByBox(item.BoxID).Select(x => x.FileID).ToList();
                    }
                    else
                        files.Add(item.FileID);
                    if (files.Count > 0)
                        foreach (string fileID in files)
                        {
                            string comment = item.OrderFax == true ? " פקס " : "";
                            comment += item.OrderEmail == true ? " מייל " : "";
                            comment += item.Office == true ? " משרד " : "";
                            comment += item.Filing == true ? " תיוקים " : "";
                            FileStatusArchiveController.Insert(new FileStatusArchive()
                            {
                                FieldID = fileID,
                                LoginID = item.LoginID,
                                StatusDate = DateTime.Now,
                                StatusID = (int)Consts.enFileStatus.inOrder,
                                Comment=comment,
                            });
                            //Add if not exist /change file status
                            FileStatusController.UpdateInsert(new FileStatus()
                            {
                                FieldID = fileID,
                                LoginID = item.LoginID,
                                StatusDate = DateTime.Now,
                                StatusID = (int)Consts.enFileStatus.inOrder
                            });
                        }
                    
                }

                contex.SaveChanges();
                
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

                return false;
            }

        }

        public static bool Insert(Orders order)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                contex.Orders.Add(order);
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

                return false;
            }
        }

        public static List<Orders> getOrderByID(string orderID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Orders.Where(b => b.OrderID == orderID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Orders>();

            }
        }
        public static List<Orders> getOrderByCust(int customerID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Orders.Where(b => b.CustomerID == customerID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Orders>();

            }
        }
        public static bool updateOrderStatusDone(int rowID,int userID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<Orders> orders = contex.Orders.Where(b => b.ShippingRowID== rowID).ToList();
                foreach (var item in orders)
                {
                    item.Done = true;
                }
                contex.SaveChanges();
                List<string> files = new List<string>();
                foreach (var order in orders)
                {

                    if (string.IsNullOrEmpty(order.FileID))
                    {
                        if (!string.IsNullOrEmpty(order.BoxID))
                            BoxesController.updateBoxLocation(true, order.BoxID);
                        files.AddRange( FilesController.getFilesListByBox(order.BoxID).Select(x => x.FileID).ToList());
                    }
                    else
                        files.Add(order.FileID);
                }
                
                FileStatusController.Update(files, Consts.enFileStatus.recivedInCustomer);
                FileStatusArchiveController.Insert(files, Consts.enFileStatus.recivedInCustomer, userID);
                return true;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }

        public static List<string> getOrderByShipping(List<int> shippingRowID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Orders.Where(b => shippingRowID.Contains( b.ShippingRowID.Value)).Select(x=>x.FileID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<string>();

            }
        }
        public static List<Orders> getOrderByShipping(int shippingRowID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Orders.Where(b => b.ShippingRowID==shippingRowID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Orders>();

            }
        }
        public static string getOrderByBoxFile(string boxID,string fileID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Orders ord = contex.Orders.Where(b => (b.BoxID == boxID || b.FileID == fileID) && b.Done == false).OrderByDescending(x=>x.Date).FirstOrDefault();
                return ord != null ? ord.OrderID : "";
                   
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return "";

            }
        }
        public static void scanOrder(string orderID, string boxID,string fileID,int userId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<Orders> list = contex.Orders.Where(b => (b.BoxID == boxID || b.FileID == fileID) && b.OrderID == orderID).ToList();
                foreach (var item in list)
                {
                    List<string> files = new List<string>();
                    if (string.IsNullOrEmpty(item.FileID))
                    {
                        if (!string.IsNullOrEmpty(item.BoxID))
                            if(item.Office==true || item.Filing==true || item.OrderEmail==true || item.OrderFax==true)
                                BoxesController.updateShelfID(item.BoxID,Consts.SHELF_OFFICE,userId);
                            else
                            BoxesController.updateShelfID(item.BoxID,userId);
                        files = FilesController.getFilesListByBox(item.BoxID).Select(x => x.FileID).ToList();
                    }
                    else
                        files.Add(item.FileID);
                    if (files.Count > 0)
                        foreach (string f in files)
                        {
                            {
                                FileStatusController.Update(f, Consts.enFileStatus.extracted);
                                FileStatusArchiveController.Insert(new FileStatusArchive()
                                {
                                    FieldID = f,
                                    StatusDate = DateTime.Now,
                                    StatusID = (int)Consts.enFileStatus.extracted,
                                    LoginID = userId,
                                });
                                if (!string.IsNullOrEmpty(item.FileID))
                                {
                                    Files file = contex.Files.Where(x => x.FileID == f).FirstOrDefault();
                                    file.BoxID =item.Office == true || item.Filing  == true || item.OrderEmail == true || item.OrderFax == true ? Consts.TEMP_BOX_IN_OFFICE : Consts.BOX_OUT_ARCHIVE_ID;
                                    contex.SaveChanges();
                                }
                            }
                        }
                        
                    
                }
                contex.SaveChanges();
               
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }
        public static List<Orders> exportOrders(int customerID, int? department,
                 DateTime? fromOrderDate, DateTime? toOrderDate, string numOfOrder)
        {
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                return contex.Orders.Where(b => b.CustomerID == customerID &&
                                           (department == null || ((int)(b.DepartmentID)) == department)
                                           && (fromOrderDate == null || fromOrderDate <= b.Date)
                                           && (toOrderDate == null || toOrderDate >= b.Date)
                                           && (string.IsNullOrEmpty(numOfOrder) || numOfOrder == b.OrderID)).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Orders>();

            }
        }

        public static object exportGroupedOrders(int customerID, int? department,
                 DateTime? fromOrderDate, DateTime? toOrderDate, string numOfOrder)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                var fileStatusArchiveUI = (from order in contex.Orders
                                           join user in contex.Users on order.LoginID equals user.UserID
                                           where order.CustomerID == customerID &&
                                              (department == null || ((int)(order.DepartmentID)) == department)
                                              && (fromOrderDate == null || fromOrderDate <= order.Date)
                                              && (toOrderDate == null || toOrderDate >= order.Date)
                                              && (string.IsNullOrEmpty(numOfOrder) || numOfOrder == order.OrderID)
                                           group new { order, user } by new { order.OrderID } into ordGroup
                                           select new
                                           {
                                               OrderID = ordGroup.Key.OrderID,
                                               Date = (from row2 in ordGroup select row2.order.Date).FirstOrDefault(),
                                               LoginID = (from row2 in ordGroup select row2.order.LoginID).FirstOrDefault(),
                                               FullName = (from row2 in ordGroup select row2.user.FullName).FirstOrDefault(),
                                               count = ordGroup.Count()
                                           }).OrderByDescending(f => f.Date).ToList();
                return fileStatusArchiveUI;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;

            }
        }

        public static int getNumOfNewBoxesOrders(int customerId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                 int? sum= contex.Orders.Where(b => b.CustomerID == customerId).Sum(emp=>emp.NumNewBoxOrder);
                return sum == null || sum <= 0 ? 0 : (int)sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        }
        public static int getNumOfNewBoxesOrdersForPeriod(int customerId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {    //return contex.Shippings.Where(b => b.CustomerID == customerId && b.ReceivedDate != null && b.ReceivedDate < DateTime.Now.AddMonths(-3)).Select(emp => emp.SumNewBoxOrder==null?0:emp.SumNewBoxOrder.Value).ToList();
                int? sum= contex.Orders.Where(b => b.CustomerID == customerId && b.ShippingRowID!=null && b.Date<DateTime.Now.AddMonths(-3)).Sum(emp=>emp.NumNewBoxOrder);
                return sum == null || sum <= 0 ? 0 : (int)sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        }
      public static int getNumOfSuppliedEmptyBoxes(int customerId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                 int? sum= contex.Orders.Where(b => b.CustomerID == customerId && b.ShippingRowID!=null).Sum(emp=>emp.NumNewBoxOrder);
                return sum == null || sum <= 0 ? 0 : (int)sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        }
        public static List<OrdersUI> getAllActiveOders()
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<OrdersUI> list = (from order in contex.Orders

                                       join worker in contex.Users on order.Employ equals worker.UserID.ToString() into w
                                       from subWorker in w.DefaultIfEmpty()
                                       join status in contex.FileStatus on order.FileID equals status.FieldID into st
                                       from subst in st.DefaultIfEmpty()
                                       join boxes in contex.Boxes on order.BoxID equals boxes.BoxID 
                                       where order.BoxID != null && 
                                       (subst.StatusID == (int)Consts.enFileStatus.inOrder || subst.StatusID==null && string.IsNullOrEmpty(order.FileID) && boxes.ShelfID!=Consts.SHELF_OUT_ARCHIVE_ID && boxes.ShelfID!=Consts.SHELF_OFFICE)
                                        && order.Done!=true
                                       group new { order,subWorker } by new { order.OrderID } into ordGroup
                                       
                                       select new OrdersUI()
                                       {
                                           OrderID= ordGroup.Key.OrderID,
                                           CustomerID = (from row2 in ordGroup select row2.order.CustomerID).FirstOrDefault(),
                                           count = ordGroup.Count(),
                                           Date = (from row2 in ordGroup select row2.order.Date).FirstOrDefault(),
                                           Urgent = (from row2 in ordGroup where row2.order.Urgent==true select row2.order.Urgent).Count()>0? true :false,
                                           DescriptionAddress = (from row2 in ordGroup select row2.order.DescriptionAddress).FirstOrDefault(),
                                           Print= (from row2 in ordGroup where row2.order.Print==true select row2.order.Print).Count() > 0 ? true : false,
                                           EmployID = (from row2 in ordGroup where row2.order.Print != true select row2.order.Print).Count() > 0?"":(from row2 in ordGroup select new Employee() {EmployeeID= row2.order.Employ }).OrderBy(x=>x.EmployeeID).FirstOrDefault().EmployeeID,
                                           EmployName = (from row2 in ordGroup select row2.subWorker.FullName).FirstOrDefault(),

                                       }
                                       
                                       
                                       ).OrderBy(o=>o.EmployID).ThenByDescending(r=>r.Urgent).ThenBy(r=>r.Date).ToList();

                return list;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<OrdersUI>();

            }

        }

        public static Dictionary<string,int[]> calcOrdersByWarehous()
        {
            //Dictionary<string, int> count = orders.GroupBy(d => d.CustomerID).Select(c => c.FirstOrDefault().CustomerID).ToArray();
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                var shelvesWithEmp = (from order in contex.Orders
                                      join box in contex.Boxes on order.BoxID equals box.BoxID
                                      join status in contex.FileStatus on order.FileID equals status.FieldID into st
                                      from subst in st.DefaultIfEmpty()
                                      where order.BoxID != null &&
                                      (subst.StatusID == (int)Consts.enFileStatus.inOrder || subst.StatusID == null && string.IsNullOrEmpty(order.FileID))
                                       && order.Done != true && !string.IsNullOrEmpty(order.Employ)
                                      select box.ShelfID.Substring(0, 3)).ToArray();
                var shelvesWithoutEmp = (from order in contex.Orders
                                         join box in contex.Boxes on order.BoxID equals box.BoxID
                                         join status in contex.FileStatus on order.FileID equals status.FieldID into st
                                         from subst in st.DefaultIfEmpty()
                                         where order.BoxID != null &&
                             (subst.StatusID == (int)Consts.enFileStatus.inOrder || subst.StatusID == null && string.IsNullOrEmpty(order.FileID))
                              && order.Done != true && string.IsNullOrEmpty(order.Employ)
                                select box.ShelfID.Substring(0, 3)).ToArray();
                Dictionary<string, int> dictWithEmp = shelvesWithEmp.GroupBy(s => s).Select(sh => new {Key= sh.Key,Value= sh.Count() }).ToDictionary(kvp=>kvp.Key,kvp=>kvp.Value);
                Dictionary<string, int> dictWithoutEmp = shelvesWithoutEmp.GroupBy(s => s).Select(sh => new {Key= sh.Key,Value= sh.Count() }).ToDictionary(kvp=>kvp.Key,kvp=>kvp.Value);
                List<string> warehouses = dictWithEmp.Keys.ToList();
                warehouses=warehouses.Union(dictWithoutEmp.Keys.ToList()).ToList();
                warehouses.Sort();
                Dictionary<string, int[]> dict = new Dictionary<string, int[]>();
                foreach (string item in warehouses)
                {
                    dict.Add(item, new int[] { dictWithEmp.Keys.Contains(item) ? dictWithEmp[item] : 0 , dictWithoutEmp.Keys.Contains(item) ? dictWithoutEmp[item] : 0});
                }

                    return dict;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Dictionary<string, int[]>();

            }

        }

        public static List<OrdersUI> getOrdersByIDs(string[] orderID,int fromW=-1,int toW=-1)
        {
            ArchiveEntities contex = new ArchiveEntities();
            int temp = 0;
            try
            {
                var list = (from order in contex.Orders
                            join users in contex.Users on order.LoginID equals users.UserID
                            join employee in contex.Users on order.Employ.Trim() equals employee.UserID.ToString().Trim() into emp
                            from subEmp in emp.DefaultIfEmpty()
                            join files in contex.Files on order.FileID equals files.FileID into file
                            from subFile in file.DefaultIfEmpty()

                            join boxes in contex.Boxes on order.BoxID equals boxes.BoxID
                            join status in contex.FileStatus on order.FileID equals status.FieldID into st
                            from subst in st.DefaultIfEmpty()

                            where (subst.StatusID == (int)Consts.enFileStatus.inOrder || subst.StatusID == null && string.IsNullOrEmpty(order.FileID) && boxes.ShelfID != Consts.SHELF_OUT_ARCHIVE_ID && boxes.ShelfID != Consts.SHELF_OFFICE)
                                        && order.Done != true
                                        //&& string.IsNullOrEmpty(order.Employ)
                          select new OrdersUI()
                         {
                             OrderID = order.OrderID,
                             NumInOrder=order.NumInOrder,
                             CustomerID = order.CustomerID,
                             DepartmentID = order.DepartmentID,
                             Identity=users.ID,
                             ShelfID=boxes.ShelfID,
                             FileID=order.FileID,
                             BoxID=order.BoxID,
                             CustomerBoxNum=boxes.CustomerBoxNum,
                             Description= subFile.FileID!=null? subFile.Description1: boxes.BoxDescription,
                             StartYear= subFile.FileID!=null? subFile.StartYear: null,
                             CustomFileNum=subFile.FileID!=null? subFile.CustomFileNum: "",
                             Date = order.Date,
                             Print = order.Print,
                             FaxEmailComment=order.FaxMailComment,
                              OrderFax = order.OrderFax==true? true :false,
                              OrderEmail = order.OrderEmail==true? true :false,
                             EmployID=order.Employ,
                             EmployName=subEmp.FullName,
                             Urgent=order.Urgent,
                              // LablePrint
                          }).OrderBy(x=>x.EmployID).ThenBy(x=>x.ShelfID).ToList().Where(x=>(orderID.Contains(x.OrderID) || (!string.IsNullOrEmpty(x.ShelfID.Trim()) && int.TryParse(x.ShelfID.Substring(0, 3),out temp)==true && ((fromW>=-1 && (fromW == 0 || Convert.ToInt16(x.ShelfID.Substring(0, 3)) >= fromW)) && (toW>=-1 && (toW == 0 || Convert.ToInt16(x.ShelfID.Substring(0, 3)) <= toW))) )&& (!string.IsNullOrEmpty(x.EmployID)||  x.Urgent!=true))).ToList();
                return list;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<OrdersUI>();

            }
        }

        public static List<WorkOrderingUI> getWorkOrders(int customerID=0)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                int i = 0;
                var list = (from order in contex.Orders
                                //   join status in contex.FileStatus on order.FileID equals status.FieldID into st
                                //   from subst in st.DefaultIfEmpty()
                            join department in contex.Department on order.DepartmentID equals department.DepartmentID into dep
                            from subDep in dep.DefaultIfEmpty()
                            join shipping in contex.Shippings on order.ShippingRowID equals (int?)shipping.rowID into ship
                            from subShip in ship.DefaultIfEmpty()
                            join users in contex.Users on subShip.Driver.Trim() equals users.UserID.ToString() into user
                            from subUser in user.DefaultIfEmpty()
                            join files in contex.Files on order.FileID equals files.FileID into file
                            from subFiles in file.DefaultIfEmpty()
                            join boxes in contex.Boxes on order.BoxID equals boxes.BoxID into box
                            from subBoxes in box.DefaultIfEmpty()
                            where (order.Done == false || order.Done == null) && (customerID == 0 || order.CustomerID == customerID) && (order.DepartmentID == 0 || order.CustomerID == subDep.CustomerID)
                            && (subFiles.BoxID == Consts.BOX_OUT_ARCHIVE_ID || subBoxes.ShelfID== Consts.SHELF_OUT_ARCHIVE_ID)
                            group new { order,subDep,subUser} by new { order.CustomerID,order.DepartmentID,order.CityAddress,order.FullAddress,order.Urgent,order.ShippingRowID } into ordGroup
                            select new WorkOrderingUI()
                            {
                                //OrderID = (from row2 in ordGroup select row2.order.OrderID).FirstOrDefault(),
                                CustomerID = ordGroup.Key.CustomerID,
                                DepartmentID = ordGroup.Key.DepartmentID,
                                DepartmentName= (from row2 in ordGroup select row2.subDep.DepartmentName).FirstOrDefault(),
                                DriverName= (from row2 in ordGroup select row2.subUser.FullName).FirstOrDefault(),
                                CityAddress =ordGroup.Key.CityAddress,
                                FullAddress= ordGroup.Key.FullAddress,
                                Comment = (from row2 in ordGroup select row2.order.Comment).FirstOrDefault(),
                                Date= (from row2 in ordGroup select row2.order.Date).FirstOrDefault(),
                                SumExterminateBoxes= (from row2 in ordGroup select row2.order.NumExterminateBoxes).Sum(),
                                SumRetriveBoxes= (from row2 in ordGroup select row2.order.NumRetriveBoxes).Sum(),
                                SumFullBoxes= (from row2 in ordGroup select row2.order.NumFullBoxes).Sum(),
                                SumNewBoxOrder= (from row2 in ordGroup select row2.order.NumNewBoxOrder).Sum(),
                                SumBoxes= (from row2 in ordGroup where string.IsNullOrEmpty( row2.order.FileID) && !string.IsNullOrEmpty( row2.order.BoxID) select row2.order.BoxID).Count(),
                                SumFiles=(from row2 in ordGroup where !string.IsNullOrEmpty( row2.order.FileID) select row2.order.FileID).Count(),
                                Urgent= (from row2 in ordGroup select row2.order.Urgent).FirstOrDefault()  ,                             // LablePrint
                                ShippingRowID= (from row2 in ordGroup select row2.order.ShippingRowID).FirstOrDefault(),
                                OrderIDs = ( from row2 in ordGroup select new OrderIDs() {OrderID= row2.order.OrderID,NumInOrder= row2.order.NumInOrder }).ToList(),
                            }).OrderBy(x=>x.ShippingRowID).
                            ThenByDescending(x => x.Urgent).
                            ThenBy(x=>x.CityAddress).
                            ThenBy(x=>x.FullAddress).
                            ThenBy(x => x.CustomerID).
                            ThenBy(x => x.DepartmentID).                        
                            ToList();
                foreach (var item in list)
                {
                    item.index = i++;
                }
                return list;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<WorkOrderingUI>();

            }
        }
        public static void updatePrintOrder(string orderID,int numInOrder,string employee,bool print)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Orders order= contex.Orders.Where(b =>b.OrderID== orderID && b.NumInOrder==numInOrder).FirstOrDefault();
                if (order != null)
                {
                    order.Print = print;
                    order.Employ = employee.Trim();
                }
                contex.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static void removeShippingRowID(int shippingRowId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                   List< Orders> orders = contex.Orders.Where(o => o.ShippingRowID == shippingRowId).ToList();
                foreach (var item in orders)
                {
                        item.ShippingRowID = null;
                        contex.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static void updateShippingRowID(List<OrderIDs> orderIDs, List<Shippings> shippings)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                foreach (var item in orderIDs)
                {
                    Orders order = contex.Orders.Where(o => o.OrderID == item.OrderID && o.NumInOrder == item.NumInOrder).FirstOrDefault();
                   
                    if (order != null)
                    {
                        order.ShippingRowID = shippings.Where(s=>s.NumInShipping==item.NumInShiping).FirstOrDefault().rowID;
                        contex.SaveChanges();
                    }  
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }
    }
    public class OrderSuppliedUI
    {
        public Nullable<int> UserID { get; set; }
        public Nullable<int> EmptyBoxes { get; set; }

        public Nullable<int> FullBoxes { get; set; }
        public Nullable<int> RetriveBoxes { get; set; }

        public Nullable<int> ExterminateBoxes { get; set; }


    }


    public partial class OrdersUI
    {
        public string OrderID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public int LoginID { get; set; }
        public string Identity { get; set; }
        public string Description { get; set; }
        public string LoginName { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public int? CustomerBoxNum  { get; set; }
        public string ShelfID { get; set; }
        public int DepartmentID { get; set; }
        public int NumInOrder { get; set; }
        public string BoxID { get; set; }
        public string FileID { get; set; }
        public Nullable<bool> Urgent { get; set; }
        public string DescriptionAddress { get; set; }
        public Nullable<bool> Print { get; set; }
        public Nullable<bool> LablePrint { get; set; }
        public string EmployID { get; set; }
        public string EmployName { get; set; }
        public Nullable<bool> Done { get; set; }
        public Nullable<int> count { get; set; } 
        public Nullable<bool> isExterm { get; set; }   
        public string FaxEmailComment { get; set; }
        public Nullable<int> StartYear { get; set; }
        public string CustomFileNum { get; set; }
        public bool OrderFax { get; set; } 
        public bool OrderEmail { get; set; } 
    }
    public partial class OrderIDs
    {
        public string OrderID { get; set; }
        public int NumInOrder { get; set; }

        public int? NumInShiping { get; set; }        
    }
   public partial class Employee
    {
        public string EmployeeID { get; set; }
    }

    public class WorkOrderingUI
    {
        public int index { get; set; }
        public List<OrderIDs> OrderIDs { get; set; }
        public System.DateTime Date { get; set; }
        public string LoginID { get; set; }
        public string DriverID { get; set; }
        public string DriverName { get; set; }
        public int CustomerID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int? ShippingRowID { get; set; }
        public int NumInOrder { get; set; }
        public string BoxID { get; set; }
        public string FileID { get; set; }
        public Nullable<bool> Urgent { get; set; }
        public Nullable<bool> OrderFax { get; set; }
        public Nullable<bool> OrderEmail { get; set; }
        public Nullable<int> SumNewBoxOrder { get; set; }
        public Nullable<int> SumFullBoxes { get; set; }
        public Nullable<int> SumRetriveBoxes { get; set; }
        public Nullable<int> SumExterminateBoxes { get; set; }
        public Nullable<int> SumBoxes { get; set; }
        public Nullable<int> SumFiles { get; set; }
        public string CityAddress { get; set; }
        public string FullAddress { get; set; }
        public string DescriptionAddress { get; set; }
        public string ContactMan { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public Nullable<bool> Print { get; set; }
        public string Employ { get; set; }
        public Nullable<bool> Done { get; set; }
        public Nullable<int> NumPaidBoxes { get; set; }
        public Nullable<bool> Office { get; set; }
        public Nullable<bool> Filing { get; set; }
        public string Comment { get; set; }
        
        public string Telephon1 { get; set; }
        public string Telephon2 { get; set; }
    }
}
