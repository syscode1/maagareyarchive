﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class FilesController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<FileUI> SearchFiles(int customerID, int department,
                                             int? customerBoxNum, string fileID,
                                             int[] subjectID,string boxID,string boxDescription,string additionalFileFields,bool isArchive)


        {
            ArchiveEntities contex = new ArchiveEntities();
            List<FileUI> files = new List<FileUI>();
            try
            {

                files = contex.Database.SqlQuery<FileUI>(" SELECT f.*,b.BoxID as bBoxID,b.Location as bLocation,b.CustomerID as BoxCustomerID,b.BoxDescription," +
                                                        " fs.StatusID,b.DepartmentID as BoxDepartmentID,b.ShelfID,b.CustomerBoxNum,f.SubjectID,cs.SubjectName" +

                                                        " FROM dbo.Files as f" +
                                                        " LEFT JOIN FileStatus as fs on f.FileID = fs.FieldID" +
                                                        " LEFT JOIN CustomerSubjects as cs on f.SubjectID = cs.SubjectID" +
                                                        " RIGHT JOIN Boxes as b on b.BoxID = f.BoxID" +
                                                        " WHERE (@CustomerID=0 OR b.CustomerID = @CustomerID OR (f.CustomerID=@CustomerID AND b.BoxID in('2619999999999','2619999999998','2618888888888','2619999999997') ))" +
                                                        // (isArchive==true?(" AND ((b.Exterminate=0 OR b.Exterminate is null) OR (f.Exterminate=0 OR f.Exterminate is null) AND fs.StatusID!=" + (int)Consts.enFileStatus.exterminated+")"):"") +
                                                        " AND ( @Department=0 OR b.DepartmentID = @Department OR (b.DepartmentID=0 AND b.BoxID in('2619999999999','2619999999998','2618888888888','2619999999997') ))" +
                                                        " AND ("+ (subjectID.Count()==1 && subjectID[0]==0?"@Subject=0 OR":"")+" f.SubjectID "+buildSubjectList(subjectID )+ ")" +
                                                        " AND (@CustomerBoxNum=-1 OR b.CustomerBoxNum = @CustomerBoxNum)" +
                                                        " AND (@FileID = '' OR f.FileID = @FileID)" +
                                                        " AND (@BoxID = '' OR f.BoxID = @BoxID)" +
                                                        " AND (@BoxDescription = '' OR b.BoxDescription Like '%'+@BoxDescription+'%')" +
                                                        " AND (f.SubjectID IS NULL OR cs.SubjectID IS NULL OR f.SubjectID =0 OR cs.CustomerID = f.CustomerID)" +
                                                        " AND (fs.StatusID IS NULL OR fs.StatusID != @FileStatusID)" + additionalFileFields,
                                                        new SqlParameter("CustomerID", customerID),
                                                        new SqlParameter("Department", department),
                                                        new SqlParameter("Subject", string.Join(",",subjectID)),
                                                        new SqlParameter("CustomerBoxNum", customerBoxNum==null?-1: customerBoxNum),
                                                        new SqlParameter("FileID", fileID),
                                                        new SqlParameter("BoxID", boxID),
                                                        new SqlParameter("BoxDescription", boxDescription),
                                                        new SqlParameter("FileStatusID", (int)Consts.enFileStatus.deactivated)).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

            return files;
        }

        private static string buildSubjectList(int[] subjectID)
        {
            StringBuilder sb = new StringBuilder(" in(");

            for (int i = 0; i < subjectID.Count(); i++)
            {
                if (i != 0)
                    sb.Append(",");
                sb.Append(subjectID[i]);
            }

            sb.Append(") ");
            return sb.ToString();

        }

        public static List<FileUI> getFilesUIByBox(string boxID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            List<FileUI> files = new List<FileUI>();
            try
            {
                files = (from file in contex.Files
                         join status in contex.FileStatus on file.FileID equals status.FieldID
                         where file.BoxID == boxID /*&& (status.StatusID==(int)Consts.FileStatus.created ||
                                                                                            status.StatusID == (int)Consts.FileStatus.inArchive ||
                                                                                            status.StatusID == (int)Consts.FileStatus.waitingForShelf
                                                                                           )*/
                         select new FileUI()
                         {
                             BoxID = file.BoxID,
                             CustomerID = file.CustomerID,
                             DepartmentID = file.DepartmentID,
                             FileID = file.FileID,
                             StatusID = status.StatusID
                         }).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

            return files;
        }

        public static object getFilesByBox(string boxID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                var files = (from file in contex.Files
                             join boxes in contex.Boxes on file.BoxID equals boxes.BoxID
                             where file.BoxID.Trim() == boxID.Trim()
                             select new
                             {
                                 FileID = file.FileID,
                                 ExterminateYear = file.ExterminateYear,
                                 Description1 = file.Description1,
                                 CustomerBoxNum = boxes.CustomerBoxNum
                             }).ToList();
                return files;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;
            }
        }

        public static List<Files> getFilesListByBox(string boxID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Files.Where(f => f.BoxID == boxID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;
            }
        }

        public static List<Files> getFilesOutOfBoxList()
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Files.Where(f => f.BoxID == Consts.TEMP_BOX_IN_ARCHIVE_ID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;
            }
        }
        public static Files getFileByID(string fileID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Files file = contex.Files.Where(f => f.FileID == fileID).FirstOrDefault();
                return file;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;
            }
        }

        public static List<FileUI> getFilesByID(List<string> filesID,List<string> boxesID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<FileUI> file = (from b in contex.Boxes
                                     where boxesID.Contains(b.BoxID) 
                                      select new FileUI()
                                      {
                                          Description1 = b.BoxDescription,
                                          Identity = "",
                                          FullName = "ארגז שלם",
                                          InsertDate =b.InsertDate,
                                          StatusID = null,
                                          FileID=b.BoxID,
                                      }).ToList();
                List<FileUI> boxes=(from f in contex.Files
                                      join status in contex.FileStatus on f.FileID equals status.FieldID
                                     where filesID.Contains(f.FileID) 
                                      select new FileUI()
                                      {
                                          Description1 = f.Description1,
                                          Identity = f.Identity,
                                          FullName = f.FullName,
                                          InsertDate = f.InsertDate,
                                          StatusID = status.StatusID,
                                          FileID=f.FileID,
                                      }).ToList();
                return file.Union(boxes).ToList() ;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<FileUI>();
            }
        }


        public static List<Files> exportFiles(int customerID, int? department, DateTime? fromStartDate,
                                              DateTime? toStartDate, DateTime? fromInsertDate, DateTime? toInsertDate,
                                              int? fromYear, int? toYear, int? loginID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            List<Files> files = new List<Files>();
            try
            {

                files = contex.Files.Where(f => f.CustomerID == customerID &&
                                          (department == null || ((int)(f.DepartmentID)) == department)
                                          && (fromStartDate == null || fromStartDate <= f.StartDate)
                                          && (toStartDate == null || toStartDate >= f.EndDate)
                                          && (fromInsertDate == null || fromInsertDate <= f.InsertDate)
                                          && (toInsertDate == null || toInsertDate >= f.InsertDate)
                                          && (fromYear == null || fromYear <= f.StartYear)
                                          && (toYear == null || toYear >= f.EndYear)
                                          && (loginID==null || f.LoginID == loginID)).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
            return files;
        }



        public static bool updateLocation(string file, bool location)
        {
            ArchiveEntities contex = new ArchiveEntities();

            try
            {

                Files fileToUpdate = contex.Files.Where(f => f.FileID == file).FirstOrDefault();
                if (fileToUpdate != null)
                {
                    fileToUpdate.Location = location;
                    contex.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }
        public static bool updatePrintRetrives(string file)
        {
            ArchiveEntities contex = new ArchiveEntities();

            try
            {

                Files fileToUpdate = contex.Files.Where(f => f.FileID == file).FirstOrDefault();
                if (fileToUpdate != null)
                {
                    fileToUpdate.PrintRetrivesDate = DateTime.Now;
                    contex.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }

        public static bool updateFileBox(string file,string boxID,int loginID)
        {
            ArchiveEntities contex = new ArchiveEntities();

            try
            {

                Files fileToUpdate = contex.Files.Where(f => f.FileID == file).FirstOrDefault();
                if (fileToUpdate != null)
                {
                    fileToUpdate.BoxID = boxID;
                    fileToUpdate.Location = false;
                    FileStatusController.UpdateInsert(new FileStatus()
                    {
                        FieldID = file,
                        boxID = boxID,
                        LoginID = loginID,
                        StatusDate = DateTime.Now,
                        StatusID = (int)Consts.enFileStatus.inArchive
                    });
                    FileStatusArchiveController.Insert(new FileStatusArchive()
                    {
                        FieldID = file,
                        boxID = boxID,
                        LoginID = loginID,
                        StatusDate = DateTime.Now,
                        StatusID = (int)Consts.enFileStatus.inArchive
                    });
                    contex.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }
        public static bool updateLocation(List<string> files, bool location)
        {
            ArchiveEntities contex = new ArchiveEntities();
            bool isSucceed = true;
            try
            {
                foreach (string item in files)
                {
                    isSucceed = isSucceed & updateLocation(item, location);
                }
                return isSucceed;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }
        public static bool updatePrintLable(string boxID,string fileID="")
        {
            ArchiveEntities contex = new ArchiveEntities();
            bool isSucceed = true;
            try
            {

                if (string.IsNullOrEmpty(fileID))
                    contex.Boxes.Where(b => b.BoxID == boxID).FirstOrDefault().LablePrint = true;
                else
                    contex.Files.Where(f => f.FileID == fileID).FirstOrDefault().LabelPrint = true;
                return true;
                
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }

        public static bool deactivateFiles(int customerID, int loginID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            List<Files> files = contex.Files.Where(f => f.CustomerID == customerID).ToList();

            bool isSucceed = true;
            try
            {
                foreach (Files file in files)
                {
                    file.Location = true;
                    file.Exterminate = true;
                    file.ExterminateDate = DateTime.Now;
                    FileStatusController.UpdateInsert(new FileStatus()
                    {
                        FieldID = file.FileID,
                        LoginID = loginID,
                        StatusDate = DateTime.Now,
                        StatusID = (int)Consts.enFileStatus.deactivated
                    });
                    FileStatusArchiveController.Insert(new FileStatusArchive()
                    {
                        FieldID = file.FileID,
                        LoginID = loginID,
                        StatusDate = DateTime.Now,
                        StatusID = (int)Consts.enFileStatus.deactivated
                    });
                }
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }

        public static bool insertUpdate(Files file,Users user)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Files existFile = contex.Files.Where(f => f.FileID == file.FileID).FirstOrDefault();
                if (existFile != null)
                {
                    existFile.BornYear = file.BornYear;
                    existFile.BoxID = file.BoxID;
                    existFile.CarNumber = file.CarNumber;
                    existFile.ClaimEnd = file.ClaimEnd;
                    existFile.ClaimStart = file.ClaimStart;
                    existFile.CustomFileNum = file.CustomFileNum;
                    existFile.CustomFileNum2 = file.CustomFileNum2;
                    existFile.CustomFileNum3 = file.CustomFileNum3;
                    existFile.DemandNum = file.DemandNum;
                    existFile.DemandNum2 = file.DemandNum2;
                    existFile.DemandNum3 = file.DemandNum3;
                    existFile.DemandNum4 = file.DemandNum4;
                    existFile.DemandNum5 = file.DemandNum5;
                    existFile.DemandNum6 = file.DemandNum6;
                    existFile.DemandNum7 = file.DemandNum7;
                    existFile.DemandNum8 = file.DemandNum8;
                    existFile.DemandNum9 = file.DemandNum9;
                    existFile.DemandNum10 = file.DemandNum10;
                    existFile.Description1 = file.Description1;
                    existFile.Description2 = file.Description2;
                    existFile.EndCommand = file.EndCommand;
                    existFile.EndDate = file.EndDate;
                    existFile.EndYear = file.EndYear;
                    existFile.EndNum = file.EndNum;
                    existFile.EventDate = file.EventDate;
                    existFile.Exterminate = file.Exterminate;
                    existFile.ExterminateDate = file.ExterminateDate;
                    existFile.ExterminateYear = file.ExterminateYear;
                    existFile.FileType = file.FileType;
                    existFile.FullName = file.FullName;
                    existFile.Identity = file.Identity;
                    existFile.LabelPrint = file.LabelPrint;
                    existFile.OldFileId = file.OldFileId;
                    existFile.ReferenceEnd = file.ReferenceEnd;
                    existFile.ReferenceStart = file.ReferenceStart;
                    existFile.EndScanNum = file.EndScanNum;
                    existFile.StartScanNum = file.StartScanNum;
                    existFile.ScientistName = file.ScientistName;
                    existFile.StartCommand = file.StartCommand;
                    existFile.StartDate = file.StartDate;
                    existFile.StartNum = file.StartNum;
                    existFile.StartYear = file.StartYear;
                    existFile.SubFile = file.SubFile;
                    existFile.SubjectID = file.SubjectID;
                    contex.SaveChanges();
                }
                else
                {
                    existFile = new Files()
                    {
                        CustomerID = file.CustomerID,
                        DepartmentID = file.DepartmentID,
                        FileID = file.FileID,
                        InsertDate = DateTime.Now,
                        Location = false,
                        LoginID = user.UserID,
                        Scan = false,
                        BornYear = file.BornYear,
                        BoxID = file.BoxID,
                        CarNumber = file.CarNumber,
                        ClaimEnd = file.ClaimEnd,
                        ClaimStart = file.ClaimStart,
                        CustomFileNum = file.CustomFileNum,
                        CustomFileNum2 = file.CustomFileNum2,
                        CustomFileNum3 = file.CustomFileNum3,
                        DemandNum = file.DemandNum,
                        DemandNum2 = file.DemandNum2,
                        DemandNum3 = file.DemandNum3,
                        DemandNum4 = file.DemandNum4,
                        DemandNum5 = file.DemandNum5,
                        DemandNum6 = file.DemandNum6,
                        DemandNum7 = file.DemandNum7,
                        DemandNum8 = file.DemandNum8,
                        DemandNum9 = file.DemandNum9,
                        DemandNum10 = file.DemandNum10,
                        Description1 = file.Description1,
                        Description2 = file.Description2,
                        EndCommand = file.EndCommand,
                        EndDate = file.EndDate,
                        EndYear = file.EndYear,
                        EndNum = file.EndNum,
                        EventDate = file.EventDate,
                        Exterminate = false,
                        ExterminateDate = file.ExterminateDate,
                        ExterminateYear = file.ExterminateYear,
                        FileType = file.FileType,
                        FullName = file.FullName,
                        Identity = file.Identity,
                        LabelPrint =false,
                        OldFileId = file.OldFileId,
                        ReferenceEnd = file.ReferenceEnd,
                        ReferenceStart = file.ReferenceStart,
                        EndScanNum = file.EndScanNum,
                        StartScanNum = file.StartScanNum,
                        ScientistName = file.ScientistName,
                        StartCommand = file.StartCommand,
                        StartDate = file.StartDate,
                        StartNum = file.StartNum,
                        StartYear = file.StartYear,
                        SubFile = file.SubFile,
                        SubjectID = file.SubjectID,
                    };
                    contex.Files.Add(existFile);
                    contex.SaveChanges();

                }
                FileStatus fs = FileStatusController.Get(file.FileID);
                if (user.ArchiveWorker == true)
                {

                    if (fs == null )
                    {
                        FileStatusController.UpdateInsert(new FileStatus()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.waitingForShelf,

                        });
                        FileStatusArchiveController.Insert(new FileStatusArchive()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.created ,
                        });
                        FileStatusArchiveController.Insert(new FileStatusArchive()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.waitingForShelf,
                        });
                    }
                    else if( fs.StatusID == (int)Consts.enFileStatus.created)
                    {
                        FileStatusController.Update(file.FileID, Consts.enFileStatus.waitingForShelf);
                        FileStatusArchiveController.Insert(new FileStatusArchive()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.waitingForShelf,
                        });
                    }
                }
                else
                {
                    if (fs == null)
                    {
                        FileStatusController.UpdateInsert(new FileStatus()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.created,
                        });
                        FileStatusArchiveController.Insert(new FileStatusArchive()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.created,
                        });

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }
    }

    
        public class FileUI
        {
        public string FileID { get; set; }
        public string BoxID { get; set; }
        public string bBoxID { get; set; }
        public string ShelfID { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> BoxDepartmentID { get; set; }
        public Nullable<int> BoxCustomerID { get; set; }
        public int? CustomerBoxNum { get; set; }
        public int? LoginID { get; set; }
        public Nullable<int> SubjectID { get; set; }
        public string SubjectName { get; set; }
        public Nullable<System.DateTime> InsertDate { get; set; }
        public string Identity { get; set; }
        public string FullName { get; set; }
        public Nullable<System.DateTime> BornYear { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> StartNum { get; set; }
        public Nullable<int> EndNum { get; set; }
        public Nullable<int> StartYear { get; set; }
        public Nullable<int> EndYear { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public Nullable<bool> Location { get; set; }
        public Nullable<bool> Scan { get; set; }
        public Nullable<bool> Exterminate { get; set; }
        public Nullable<int> ExterminateYear { get; set; }
        public string DemandNum { get; set; }
        public string CustomFileNum { get; set; }
        public string CustomFileNum2 { get; set; }
        public string CustomFileNum3 { get; set; }
        public string ScientistName { get; set; }
        public Nullable<bool> LabelPrint { get; set; }
        public Nullable<System.DateTime> ExterminateDate { get; set; }
        public string OldFileId { get; set; }
        public string DemandNum2 { get; set; }
        public string DemandNum3 { get; set; }
        public string DemandNum4 { get; set; }
        public string DemandNum5 { get; set; }
        public string DemandNum6 { get; set; }
        public string DemandNum7 { get; set; }
        public string DemandNum8 { get; set; }
        public string DemandNum9 { get; set; }
        public string DemandNum10 { get; set; }
        public string ClaimStart { get; set; }
        public string ClaimEnd { get; set; }
        public string ReferenceStart { get; set; }
        public string ReferenceEnd { get; set; }
        public string CarNumber { get; set; }
        public Nullable<System.DateTime> EventDate { get; set; }
        public string FileType { get; set; }
        public Nullable<int> StartCommand { get; set; }
        public Nullable<int> EndCommand { get; set; }
        public string StartScanNum { get; set; }
        public string EndScanNum { get; set; }

        public string BoxDescription { get; set; }

        public Nullable<bool> bLocation { get; set; }
       public Nullable<int> StatusID { get; set; }

        }
        public class SearchFileParams
        {
            public string FileID { get; set; }
            public string BoxID { get; set; }
            public int? CustomerBoxNum { get; set; }
            public string CustomerFileNum { get; set; }
            public string BoxDescription { get; set; }
            public int CustomerID { get; set; }
            public string CustomerName { get; set; }
            public int[] SubjectID { get; set; }
            public string FromInsertDate { get; set; }
            public string ToInsertDate { get; set; }
            public string Year { get; set; }
            public string Description1 { get; set; }
            public string Description2 { get; set; }
            public string ScanNum { get; set; }
            public int DepartmentID { get; set; }
            public string DepartmentName { get; set; }
            public string SenderUserID { get; set; }
            public string AdditionalDepartmentFields { get; set; }


        }
        public class FilesExport
        {
            public string FileID { get; set; }
            public string BoxID { get; set; }
            public Nullable<int> DepartmentID { get; set; }
            public Nullable<int> CustomerID { get; set; }
            public String DepartmentName { get; set; }
            public String CustomerName { get; set; }
            public string LoginID { get; set; }
            public string LoginName { get; set; }
            public int SubjectID { get; set; }
            public String SubjectName { get; set; }
            public Nullable<System.DateTime> InsertDate { get; set; }
            public string ID { get; set; }
            public string FullName { get; set; }
            public Nullable<System.DateTime> BornYear { get; set; }
            public Nullable<System.DateTime> StartDate { get; set; }
            public Nullable<System.DateTime> EndDate { get; set; }
            public Nullable<int> StartNum { get; set; }
            public Nullable<int> EndNum { get; set; }
            public Nullable<int> StartYear { get; set; }
            public Nullable<int> EndYear { get; set; }
            public string Description1 { get; set; }
            public string Description2 { get; set; }
            public string StartScanNum { get; set; }
            public string EndScanNum { get; set; }
            public Nullable<bool> Location { get; set; }
            public Nullable<bool> Scan { get; set; }
            public Nullable<bool> Exterminate { get; set; }
            public Nullable<int> ExterminateYear { get; set; }
            public string DemandNum { get; set; }
            public string CustomFileNum { get; set; }
            public string CustomFileNum2 { get; set; }
            public string CustomFileNum3 { get; set; }
            public string ScientistName { get; set; }
            public Nullable<bool> LabelPrint { get; set; }
            public Nullable<System.DateTime> ExterminateDate { get; set; }

        }
    public enum FieldType
    {
        String = 1,
        Number = 2,
        Date = 3,
        Bool = 4,
    }
    
}
