﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class JobController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static OrderedDictionary getAllJobs()
        {
            ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDetiels = new OrderedDictionary();
            try
            {
                var jobs = contex.Jobs.ToList();
                foreach (var item in jobs)
                {
                    UserDetiels.Add(item.JobID, item.JobName);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return UserDetiels;
        }

    }
}
