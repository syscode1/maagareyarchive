﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class AccountController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<Account> get()
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.Account.ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Account>();

            }
        }

       public static bool insert(Account account )
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();

                context.Account.Add(account);

                context.SaveChanges();

                return true;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }
    }
}

