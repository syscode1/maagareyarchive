﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class MignazaController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static void insertUpdate(Mignaza mignaza)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                Mignaza mExist = context.Mignaza.Where(m => m.ShelfID == mignaza.ShelfID).FirstOrDefault();
                if (mExist != null)
                {
                    mExist.NumBoxCapacity = mignaza.NumBoxCapacity;
                }
                else
                {

                    mignaza.NumBoxesOut = 0;
                    context.Mignaza.Add(mignaza);
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static void decreaseFromShelfID(string shelfID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                Mignaza mOld = context.Mignaza.Where(m => m.ShelfID == shelfID).FirstOrDefault();
                if (mOld != null)
                {
                    mOld.NumBoxesOut -= 1;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static int getCellLength(string cellName)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.MignazaCell.Where(mc => mc.CellName == cellName).FirstOrDefault().CellNum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }
        }
        public static List<Cells> getCellsList()
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.Mignaza.Select(x => new Cells() { cellName = x.ShelfID.Substring(5, 2) }).Distinct().ToList().OrderBy(x => x.cellName).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Cells>();
            }
        }

        public static List<ShelfBoxCalc> getMignazaMax(int fromWarehouse, int toWarehouse, int fromRow, int toRow)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.Mignaza.ToList().Where(x => int.Parse(x.ShelfID.Substring(0, 3)) >= fromWarehouse &&
                                                  int.Parse(x.ShelfID.Substring(0, 3)) <= toWarehouse &&
                                                  int.Parse(x.ShelfID.Substring(3, 2)) >= fromRow &&
                                                  (toRow == 0 || int.Parse(x.ShelfID.Substring(3, 2)) <= toRow))
                                                  .Select(x => new ShelfBoxCalc() {
                                                      ShelfID = x.ShelfID,
                                                      Value = x.NumBoxCapacity })
               .OrderBy(x => x.ShelfID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<ShelfBoxCalc>();
            }
        }
        public static List<ShelfBoxCalc> getMignazaPopulated(int fromWarehouse, int toWarehouse, int fromRow, int toRow)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                string[] nums = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                Dictionary<string, int> populated = (from boxes in context.Boxes
                                                     where boxes.Exterminate != true && !nums.Contains(boxes.ShelfID.Substring(5, 1))
                                                     group new { boxes } by new { boxes.ShelfID } into gBox
                                                     select new
                                                     {
                                                         x = gBox.Key.ShelfID,
                                                         y = gBox.Count()
                                                     }).ToList().Where(x => int.Parse(x.x.Substring(0, 3)) >= fromWarehouse &&
                                                 int.Parse(x.x.Substring(0, 3)) <= toWarehouse &&
                                                 int.Parse(x.x.Substring(3, 2)) >= fromRow &&
                                                 (toRow == 0 || int.Parse(x.x.Substring(3, 2)) <= toRow))
.ToDictionary(kvp => kvp.x, kvp => kvp.y);
                return context.Mignaza.ToList()
                    .Where(x => int.Parse(x.ShelfID.Substring(0, 3)) >= fromWarehouse &&
                                                  int.Parse(x.ShelfID.Substring(0, 3)) <= toWarehouse &&
                                                  int.Parse(x.ShelfID.Substring(3, 2)) >= fromRow &&
                                                  (toRow == 0 || int.Parse(x.ShelfID.Substring(3, 2)) <= toRow))

                    .Select(x => new ShelfBoxCalc()
                    {
                        ShelfID = x.ShelfID,
                        Value = populated.ContainsKey(x.ShelfID) ? populated[x.ShelfID] : 0
                    }).OrderBy(x => x.ShelfID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<ShelfBoxCalc>();
            }
        }
        public static List<ShelfBoxCalc> getMignazaEmpty(int fromWarehouse, int toWarehouse, int fromRow, int toRow, int fromShelf = 0, int toShelf = 0)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                string[] nums = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                Dictionary<string, int> populated = (from boxes in context.Boxes
                                                     where boxes.Exterminate != true && !nums.Contains(boxes.ShelfID.Substring(5, 1))
                                                     group new { boxes } by new { boxes.ShelfID } into gBox
                                                     select new
                                                     {
                                                         x = gBox.Key.ShelfID,
                                                         y = gBox.Count()
                                                     }).ToList().Where(x => int.Parse(x.x.Substring(0, 3)) >= fromWarehouse &&
                                                 int.Parse(x.x.Substring(0, 3)) <= toWarehouse &&
                                                 int.Parse(x.x.Substring(3, 2)) >= fromRow &&
                                                 (toRow == 0 || int.Parse(x.x.Substring(3, 2)) <= toRow) &&
                                                  int.Parse(x.x.Substring(7, 2)) >= fromShelf &&
                                                 (toShelf == 0 || int.Parse(x.x.Substring(7, 2)) <= toShelf))
.ToDictionary(kvp => kvp.x, kvp => kvp.y);
                return context.Mignaza.ToList()
                    .Where(x => int.Parse(x.ShelfID.Substring(0, 3)) >= fromWarehouse &&
                                                  int.Parse(x.ShelfID.Substring(0, 3)) <= toWarehouse &&
                                                  int.Parse(x.ShelfID.Substring(3, 2)) >= fromRow &&
                                                  (toRow == 0 || int.Parse(x.ShelfID.Substring(3, 2)) <= toRow) &&
                                                  (x.SavedShelfDate == null || x.SavedShelfDate.Value.AddDays(Consts.DAYS_OF_SAVE_SHELF_ID_TO_BOX) < DateTime.Now)
                                                  )

                    .Select(x => new ShelfBoxCalc() {
                        ShelfID = x.ShelfID,
                        Value = populated.ContainsKey(x.ShelfID) ? (x.NumBoxCapacity - populated[x.ShelfID] < 0 ? 0 : x.NumBoxCapacity - populated[x.ShelfID]) : x.NumBoxCapacity
                    }).OrderBy(x => x.ShelfID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<ShelfBoxCalc>();
            }
        }

        public static void saveShelfs(ShelfBoxCalc maxRow, int sumBoxes)
        {
            ArchiveEntities context = new ArchiveEntities();
            List<Mignaza> shelfs = context.Mignaza.Where(x => x.ShelfID.StartsWith(maxRow.ShelfID)).ToList().Where( x=>
            x.SavedShelfDate == null || x.SavedShelfDate.Value.AddDays(Consts.DAYS_OF_SAVE_SHELF_ID_TO_BOX)>DateTime.Now).Take(sumBoxes).ToList();
            foreach (Mignaza item in shelfs)
            {
                item.SavedShelfDate = DateTime.Now;
            }
            context.SaveChanges();
        }

        public static List<MignazaUI> calcSumByWarehous()
        {
            //Dictionary<string, int> count = orders.GroupBy(d => d.CustomerID).Select(c => c.FirstOrDefault().CustomerID).ToArray();
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<MignazaUI> shelves = (from mig in contex.Mignaza

                               select new MignazaUI()
                               {
                                   ShelfID = mig.ShelfID.Substring(0, 3),
                                   NumBoxCapacity = mig.NumBoxCapacity,
                                   NumBoxesOut = mig.NumBoxesOut,
                               }).ToList();
                          List<MignazaUI> groupShelves = (from mignaza in shelves
                                                        group new { mignaza } by new { mignaza.ShelfID } into shGroup

                                                        select new MignazaUI()
                               {
                                   ShelfID =shGroup.Key.ShelfID,
                                   NumBoxCapacity = (from row2 in shGroup select row2.mignaza.NumBoxCapacity).Sum(),
                                   NumBoxesOut = (from row2 in shGroup select row2.mignaza.NumBoxesOut).Sum(),
                               }).ToList();
                return groupShelves;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<MignazaUI>();

            }

        }

    }
    public partial class MignazaUI
    {
        public string ShelfID { get; set; }
        public Nullable<int> NumBoxCapacity { get; set; }
        public Nullable<int> NumBoxesOut { get; set; }
    }
    public partial class Retrives
    {
        public string ShelfID { get; set; }
        public string BoxID { get; set; }
        public string FileID { get; set; }
        public string Warehouse { get; set; }
        public string Row { get; set; }
        public string Cell { get; set; }
        public string Shelf { get; set; }
        public int? CustomerBoxNum { get; set; }
        public int CustomerID { get; set; }
        public DateTime? PrintRetrivesDate { get; set; }
    }
    public partial class ShelfBoxCalc
    {
        public string ShelfID { get; set; }
        public Nullable<int> Value { get; set; }
    }
   public  class Cells
    {
        public string cellName { get; set; }
    }

    public class ShelfFileID
    {
        public string shelfID { get; set; }
        public List<string> files{ get; set; }
    }
}
