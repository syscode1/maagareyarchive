﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class SubjectsController
    {

            private static readonly ILog logger = LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //public static List<CustomerSubject> getAllActiveSubjects()
        //{
        //    try
        //    {
        //        ArchiveEntities context = new ArchiveEntities();
        //        return context.CustomerSubjects.Where(s => s.IsActive).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
        //        return new List<CustomerSubject>();

        //    }
        //}
        public static List<CustomerSubjects> getAllSubjectsByCustomerID(int customerID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                List<CustomerSubjects> itemsList = context.CustomerSubjects.Where(cs=>cs.CustomerID == customerID && cs.IsActive == true).OrderBy(x=>x.SubjectID).ToList();
                return itemsList;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<CustomerSubjects>();

            }
        }
       public static OrderedDictionary getAllSubjectsByCustomerID_OrderDict(int customerID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                List<CustomerSubjects> itemsList = context.CustomerSubjects.Where(cs=>cs.CustomerID == customerID && cs.IsActive == true).ToList();
                OrderedDictionary data = new OrderedDictionary();
                foreach (var item in itemsList)
                {
                    if(item.SubjectID!=9001)
                        data.Add(item.SubjectID, item.SubjectName);
                }
                return data;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new OrderedDictionary();

            }
        }
       public static OrderedDictionary getAllSubjectsByCustomerID_OrderDictForUserPermission(int customerID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                List<CustomerSubjects> itemsList = context.CustomerSubjects.Where(cs=>cs.CustomerID == customerID && cs.IsActive == true).ToList();
                OrderedDictionary data = new OrderedDictionary();
                foreach (var item in itemsList)
                {
                        data.Add(item.SubjectID, item.SubjectName);
                }
                return data;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new OrderedDictionary();

            }
        }
       public static OrderedDictionary getAllPermitedSubjectsByCustomerID_OrderDict(int customerID,int userID,int subjectID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                List<CustomerSubjects> itemsList = new List<CustomerSubjects>();
                if (subjectID == 0)
                {
                    return getAllSubjectsByCustomerID_OrderDict(customerID);
                }
                else if (subjectID == 9001)
                {
                    int[] ids = new int[] { 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047 };
                    itemsList = context.CustomerSubjects.Where(cs => cs.CustomerID == customerID && cs.IsActive == true && ids.Contains(cs.SubjectID)).ToList();
                }
                else
                {
                    itemsList = (from customerSubject in context.CustomerSubjects
                                 where customerSubject.CustomerID == customerID && customerSubject.IsActive == true && customerSubject.SubjectID == subjectID
                                 select new
                                 {
                                     CustomerID = customerSubject.CustomerID,
                                     SubjectID = customerSubject.SubjectID,
                                     SubjectName = customerSubject.SubjectName,
                                 }).ToList().Select(x => new CustomerSubjects()
                                 {
                                     CustomerID = x.CustomerID,
                                     SubjectID = x.SubjectID,
                                     SubjectName = x.SubjectName
                                 }).ToList();
                }
                OrderedDictionary data = new OrderedDictionary();
                foreach (var item in itemsList)
                {
                    data.Add(item.SubjectID, item.SubjectName);
                }
                return data;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new OrderedDictionary();

            }
        }
        public static bool insertNewSubject(CustomerSubjects sub)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                context.CustomerSubjects.Add(sub);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }

        public static bool insertUpdateSubject(List<CustomerSubjects> sub)
        {
            try
            {
                bool isSucceed = true;
                foreach (CustomerSubjects item in sub)
                {
                    if (item.SubjectID <= 0)
                    {
                        ArchiveEntities context = new ArchiveEntities();
                        int subjectID=  context.CustomerSubjects.Select(x => x.SubjectID).Max();
                        item.SubjectID = subjectID + 1;
                        isSucceed &= insertNewSubject(item);
                    }
                    else
                        isSucceed &= updateSubjectByID(item);
                }
                return isSucceed;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
        public static void deactivateSubjectByID(int subjectID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                CustomerSubjects sub =context.CustomerSubjects.Where(i => i.SubjectID == subjectID).FirstOrDefault();
                if (sub != null)
                    sub.IsActive = false;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }
        public static bool updateSubjectByID(CustomerSubjects custSub)
        {

            try
            {
                ArchiveEntities context = new ArchiveEntities();
                CustomerSubjects sub =context.CustomerSubjects.Where(i => i.SubjectID == custSub.SubjectID).FirstOrDefault();
                if (sub != null)
                {
                    sub.SubjectName = custSub.SubjectName;
                    sub.UpdatedDate = DateTime.Now;
                }
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
 

    }
    
}
