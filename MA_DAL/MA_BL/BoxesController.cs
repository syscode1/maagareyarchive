﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class BoxesController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<Boxes> exportBoxes(int customerID, int? department, DateTime?
                                            fromInsertDate, DateTime? toInsertDate)
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => b.CustomerID == customerID &&
                                           (department == null || ((int)(b.DepartmentID)) == department)
                                           && (fromInsertDate == null || fromInsertDate <= b.InsertDate)
                                           && (toInsertDate == null || toInsertDate >= b.InsertDate)).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }

        public static int getSumPlacedBoxes(DateTime fromDate, DateTime toDate)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                string[] nums = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                int sum = (from boxes in contex.Boxes
                           where boxes.Exterminate != true && boxes.InsertDate.Value >= fromDate && boxes.InsertDate <= toDate
                            && (!string.IsNullOrEmpty(boxes.ShelfID) && !nums.Contains(boxes.ShelfID.Substring(5, 1)))
                           select boxes.BoxID).Count();
                return sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }


        }
       public static List<DaylyReportData> getSumPlacedBoxesByCustomer(DateTime fromDate, DateTime toDate)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                string[] nums = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                List<DaylyReportData> sum = (from boxes in contex.Boxes
                           where boxes.Exterminate != true && boxes.InsertDate.Value >= fromDate && boxes.InsertDate <= toDate
                            && (!string.IsNullOrEmpty(boxes.ShelfID) && !nums.Contains(boxes.ShelfID.Substring(5, 1)))
                            group new {boxes} by boxes.CustomerID into gBox
                           select new DaylyReportData() {CustomerID= gBox.Key,Sum=gBox.Count() }).ToList();
                return sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<DaylyReportData>();

            }


        }
        public static List<DaylyReportData> getSumNotPlacedBoxesByCustomer(DateTime fromDate, DateTime toDate)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                string[] nums = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

                List<DaylyReportData> sumAll = (from boxes in contex.Boxes
                                                where boxes.Exterminate != true && boxes.InsertDate.Value >= fromDate && boxes.InsertDate <= toDate
                                                group new { boxes } by new { boxes.CustomerID } into gBox
                                                select new DaylyReportData() { CustomerID = gBox.Key.CustomerID, Sum = gBox.Count() }).ToList();


                List<DaylyReportData> sumPlaced = (from boxes in contex.Boxes
                           where boxes.Exterminate != true && boxes.InsertDate.Value >= fromDate && boxes.InsertDate <= toDate
                            && (!string.IsNullOrEmpty(boxes.ShelfID) && !nums.Contains(boxes.ShelfID.Substring(5, 1)))
                            group new {boxes} by boxes.CustomerID into gBox
                           select new DaylyReportData() {CustomerID= gBox.Key,Sum=gBox.Count() }).ToList();

                List<DaylyReportData> sumNotPlaced = new List<DaylyReportData>();
                foreach (DaylyReportData item in sumAll)
                {
                    DaylyReportData d = sumPlaced.Where(x => x.CustomerID == item.CustomerID).FirstOrDefault();
                    if (d != null)
                    {
                        if (item.Sum - d.Sum > 0)
                            sumNotPlaced.Add(new DaylyReportData() { CustomerID = item.CustomerID, Sum = item.Sum - d.Sum });
                    }
                    else
                    {
                        sumNotPlaced.Add(item);
                    }

                }
                return sumNotPlaced;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<DaylyReportData>();

            }


        }
        public static int sumBoxesForExterminate()
        {           try
            {

                ArchiveEntities contex = new ArchiveEntities();
                string[] nums = new string[] {"0","1","2","3","4","5","6","7","8","9" };
                int sum = (from boxes in contex.Boxes
                           where boxes.Exterminate == true
                            &&(!string.IsNullOrEmpty(boxes.ShelfID) && !nums.Contains(boxes.ShelfID.Substring(5, 1)))
                           select boxes.BoxID).Count();
                return sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        

            throw new NotImplementedException();
        }
   public static List<DaylyReportData> sumBoxesForExterminateByCust()
        {           try
            {

                ArchiveEntities contex = new ArchiveEntities();
                string[] nums = new string[] {"0","1","2","3","4","5","6","7","8","9" };
                List<DaylyReportData> sum = (from boxes in contex.Boxes
                           where boxes.Exterminate == true
                            &&(!string.IsNullOrEmpty(boxes.ShelfID) && !nums.Contains(boxes.ShelfID.Substring(5, 1)))
                            group new { boxes} by new {boxes.CustomerID } into gBox
                           select new DaylyReportData() {CustomerID= gBox.Key.CustomerID,Sum= gBox.Count() } ).ToList();
                return sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<DaylyReportData>();

            }
        

            throw new NotImplementedException();
        }
        public static int getSumFullNewBoxes(DateTime  fromDate, DateTime  toDate)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                int sum = (from boxes in contex.Boxes
                           where boxes.InsertData==2 && boxes.Exterminate != true && boxes.InsertDate.Value >=fromDate && boxes.InsertDate <= toDate
                           select boxes
                           ).Count();
                return sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }


        }
        public static List<DaylyReportData> getSumFullNewBoxesByCust(DateTime  fromDate, DateTime  toDate)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                List<DaylyReportData> sum = (from boxes in contex.Boxes
                                             join files in contex.Files on boxes.BoxID equals files.BoxID
                                             where boxes.Exterminate != true && boxes.InsertDate.Value >= fromDate && boxes.InsertDate <= toDate
                                             group new { boxes } by new { boxes.BoxID } into bGroup
                                             select new { customerId = (from row2 in bGroup select row2.boxes.CustomerID).FirstOrDefault() }).GroupBy(x => new { x.customerId }).Select(x => new DaylyReportData() { CustomerID = x.Key.customerId, Sum = x.Count() }).ToList();
                                             
                return sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<DaylyReportData>();

            }


        }
       public static List<DaylyReportData> getSumEmptyNewBoxesByCust(DateTime  fromDate, DateTime  toDate)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                List<DaylyReportData> sumAll = (from boxes in contex.Boxes
                           where boxes.Exterminate != true && boxes.InsertDate.Value >=fromDate && boxes.InsertDate <= toDate
                           group new { boxes } by new { boxes.CustomerID }  into gBox
                           select new DaylyReportData() {CustomerID=gBox.Key.CustomerID, Sum=gBox.Count() }).ToList();

                List<DaylyReportData> sumFull= getSumFullNewBoxesByCust(fromDate, toDate);
                List<DaylyReportData> sumEmpyty = new List<DaylyReportData>();
                foreach (DaylyReportData item in sumAll)
                {
                    DaylyReportData d = sumFull.Where(x => x.CustomerID == item.CustomerID).FirstOrDefault();
                    if(d!=null)
                    {
                        if (item.Sum - d.Sum > 0)
                            sumEmpyty.Add(new DaylyReportData() { CustomerID = item.CustomerID, Sum = item.Sum - d.Sum });
                    }
                    else
                    {
                        sumEmpyty.Add(item);
                    }
                        
                }

                return sumEmpyty;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<DaylyReportData>();

            }


        }
         public static List<DaylyReportData> getSumNewBoxes(DateTime  fromDate, DateTime  toDate)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                List<DaylyReportData> sumAll = (from boxes in contex.Boxes
                           where boxes.Exterminate != true && boxes.InsertDate.Value >=fromDate && boxes.InsertDate <= toDate
                           group new { boxes } by new { boxes.CustomerID }  into gBox
                           select new DaylyReportData() {CustomerID=gBox.Key.CustomerID, Sum=gBox.Count() }).ToList();
                return sumAll;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<DaylyReportData>();

            }


        }
      public static int getSumEmptyNewBoxes(DateTime  fromDate, DateTime  toDate,int sumFull)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                int sum = (from boxes in contex.Boxes
                           where boxes.Exterminate != true && boxes.InsertDate.Value >=fromDate && boxes.InsertDate <= toDate
                           select boxes.BoxID).Count()-sumFull;
                return sum;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }


        }
        public static List<Boxes> exportExterminatedBoxes(int customerID, int? department, DateTime?
                                             fromExterminatedDate, DateTime? toExterminatedDate, int? loginID)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => b.CustomerID == customerID &&
                                           (department == null || ((int)(b.DepartmentID)) == department)
                                           && (fromExterminatedDate == null || fromExterminatedDate <= b.ExterminateDate)
                                           && (toExterminatedDate == null || toExterminatedDate >= b.ExterminateDate)
                                           && (loginID==null || loginID == b.LoginID)
                                           && b.Exterminate == true).OrderBy(o => o.DepartmentID).ThenBy(r => r.LoginID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }
        public static List<Boxes> exportBoxesToExtrminate(int customerID, int? department
                                             /*   ,DateTime? fromExterminatedDate, DateTime? toExterminatedDate*/
                                             , DateTime? fromInsertDate, DateTime? toInsertDate, int? toYear)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => b.CustomerID == customerID &&
                                           (department == null || ((int)(b.DepartmentID)) == department)
                                           //   && (fromExterminatedDate == null || fromExterminatedDate <= b.ExterminateDate)
                                           //   && (toExterminatedDate == null || toExterminatedDate >= b.ExterminateDate)
                                           && (fromInsertDate == null || fromInsertDate <= b.ExterminateDate)
                                           && (toInsertDate == null || toInsertDate >= b.ExterminateDate)
                                           && (toYear == null || b.ExterminateYear == null || toYear >= b.ExterminateYear)
                                           && b.Exterminate != true
                                           && b.Location == false).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }

        public static Boxes getBoxByID(string boxeId)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => boxeId==b.BoxID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Boxes();

            }
        }
       public static List<Boxes> getBoxesByIDs(List<string> boxesIds)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => boxesIds.Contains(b.BoxID)).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }
        public static List<Boxes> getBoxesRetrives()
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => b.ShelfID== Consts.TEMP_SHELF_IN_ARCHIVE_ID).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }
        public static Int64 getBoxesCount(int? customerId, List<int> departments)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => (customerId == 0 || b.CustomerID == customerId) && b.Exterminate != true && (departments.Count==0 ||departments.Contains(b.DepartmentID.Value))).Count();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        }
        public static List<Boxes> getExistBoxes(int? customerId, List<int> departments)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => (customerId == 0 || b.CustomerID == customerId) && b.Exterminate != true && (departments.Count==0 ||departments.Contains(b.DepartmentID.Value))).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }

        public static List<Boxes> getExtermBoxes(int? customerId, List<int> departments)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => (customerId == 0 || b.CustomerID == customerId) && b.Exterminate == false && (departments.Count == 0 || departments.Contains(b.DepartmentID.Value))).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }
        public static Int64 getLendBoxesCount(int? customerId, List<int> departments)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                int NumNewBoxOrder = context.Orders.Where(b => b.CustomerID == customerId && departments.Contains(b.DepartmentID)).Sum(s => (s.NumNewBoxOrder == null ? 0 : s.NumNewBoxOrder)).Value;
                int NumFullBoxes = context.Orders.Where(b => b.CustomerID == customerId && departments.Contains(b.DepartmentID)).Sum(s => (s.NumFullBoxes == null ? 0 : s.NumFullBoxes)).Value;
                int NumPaidBoxes = context.Orders.Where(b => b.CustomerID == customerId && departments.Contains(b.DepartmentID)).Sum(s => (s.NumPaidBoxes == null ? 0 : s.NumPaidBoxes)).Value;
                return NumNewBoxOrder - NumFullBoxes - NumPaidBoxes;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }
        }

        internal static bool deactivateBoxes(int customerID, int loginID)
        {
            {
                ArchiveEntities contex = new ArchiveEntities();
                List<Boxes> boxes = contex.Boxes.Where(f => f.CustomerID == customerID).ToList();

                try
                {
                    foreach (Boxes box in boxes)
                    {
                        box.Location = true;
                        box.Exterminate = true;
                        box.ExterminateDate = DateTime.Now;
                        //FileStatusController.UpdateInsert(new FileStatus()
                        //{
                        //    FieldID = box.BoxID,
                        //    LoginID = loginID,
                        //    StatusDate = DateTime.Now,
                        //    StatusID = (int)Consts.FileStatus.deactivated
                        //});
                        //FileStatusArchiveController.Insert(new FileStatusArchive()
                        //{
                        //    FieldID = box.BoxID,
                        //    LoginID = loginID,
                        //    StatusDate = DateTime.Now,
                        //    StatusID = (int)Consts.FileStatus.deactivated
                        //});
                    }
                    contex.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                    return false;

                }



            }
        }

        internal static void updateShelfID(string boxID,int userID)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                Boxes box = context.Boxes.Where(b => b.BoxID == boxID).FirstOrDefault();
                if (box != null)
                {
                    box.ShelfID = Consts.SHELF_OUT_ARCHIVE_ID;
                    context.SaveChanges();
                    BoxHistoryController.insert(new BoxHistory() {BoxID= boxID, Date=DateTime.Now,UserID=userID,ShelfID = Consts.SHELF_OUT_ARCHIVE_ID });
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static void updateBoxLocation(bool location, string boxID)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                Boxes box = context.Boxes.Where(b => b.BoxID == boxID).FirstOrDefault();
                if (box != null)
                {
                    box.Location = location;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        public static bool updatePrintRetrives(string box)
        {
            ArchiveEntities contex = new ArchiveEntities();

            try
            {

                Boxes boxToUpdate = contex.Boxes.Where(b => b.BoxID == box).FirstOrDefault();
                if (boxToUpdate != null)
                {
                    boxToUpdate.PrintRetrivesDate = DateTime.Now;
                    contex.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }

        }
        public static void updateBoxShelfLocation(bool location, string boxID,string shelfID,int loginID)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                Boxes box = context.Boxes.Where(b => b.BoxID == boxID).FirstOrDefault();
                if (box != null)
                {
                    box.ShelfID = shelfID;
                    box.Location = location;
                    context.SaveChanges();
                    BoxHistoryController.insert(new BoxHistory() { BoxID = boxID, Date = DateTime.Now, UserID = loginID, ShelfID = shelfID });
                    List<string> files = FilesController.getFilesListByBox(boxID).Select(x=>x.FileID).ToList();
                    foreach (var file in files)
                    {
                        FilesController.updateFileBox(file, boxID, loginID);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }
     public static bool updateShelfID( string boxID,string shelfID,int userID)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                Boxes box = context.Boxes.Where(b => b.BoxID == boxID).FirstOrDefault();
                if (box != null)
                {
                    box.ShelfID = shelfID;
                    BoxHistoryController.insert(new BoxHistory() { BoxID = boxID, Date = DateTime.Now, UserID = userID, ShelfID = shelfID });
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }

        public static int calcNumOfPaiedBoxes(Customers customer)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                int numOfNewBoxesOrders = OrdersController.getNumOfNewBoxesOrders(customer.CustomerID);
                int somOfBoxes = getNumOfBoxes(customer.CustomerID);
                int somOfPaiedBoxes = InvoicesController.getNumOfPaiedBoxes(customer.CustomerID);
                 int creditOfBoxes = customer.EmptyBoxMaxEmount==null?0:customer.EmptyBoxMaxEmount.Value;
                int calc = creditOfBoxes+ somOfBoxes+ somOfPaiedBoxes- numOfNewBoxesOrders ;
                return calc < 0 ? 0 : calc;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }

        }
         public static int calcNumOfEmptyBoxes(Customers customer)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                int numOfNewBoxesOrders = OrdersController.getNumOfNewBoxesOrdersForPeriod(customer.CustomerID);
                int somOfBoxes = getNumOfBoxes(customer.CustomerID);
                int boxesAccount = AccountController.get().Where(x => x.CustomerID == customer.CustomerID).Select(x => x.EmptyBoxAmount.Value).Sum();
                int calc =  numOfNewBoxesOrders- somOfBoxes - boxesAccount ;
                return calc < 0 ? 0 : calc;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }

        }
        public static int calcNumOfEmptyBoxes(int customerID,int somOfBoxes)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                int numOfNewBoxesOrders = OrdersController.getNumOfSuppliedEmptyBoxes(customerID);
                int somOfPaiedBoxes = InvoicesController.getNumOfPaiedBoxes(customerID);
                int calc = numOfNewBoxesOrders-somOfBoxes-somOfPaiedBoxes ;
                return calc < 0 ? 0 : calc;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;
            }

        }
        public static int getNumOfBoxes(int customerId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Boxes.Where(b => b.CustomerID == customerId && b.Exterminate!=true).Count();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        }
        public static List<Boxes> getAllBoxes(int customerId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                return contex.Boxes.Where(b => b.CustomerID == customerId ).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Boxes>();

            }
        }

        public static List<BoxFileUI> getBoxFiles(string boxID,int customerID)
        {
            try
            {

                ArchiveEntities context = new ArchiveEntities();
                List<BoxFileUI> boxFileUI = (from box in context.Boxes
                                             join file in context.Files on box.BoxID equals file.BoxID into f
                                             from subF in f.DefaultIfEmpty()
                                             where box.BoxID == boxID && (box.CustomerID==customerID || customerID==9999 || new string[]{ "2619999999999","2619999999998","2618888888888", "2619999999997" }.Contains(box.BoxID))
                                             select new BoxFileUI()
                                             {
                                                 BoxID = box.BoxID,
                                                 BoxDescription = box.BoxDescription,
                                                 ExterminateYear = box.ExterminateYear,
                                                 FileID = subF.FileID,
                                                 CustomerID=box.CustomerID,
                                                 DepartmentID=box.DepartmentID.Value,
                                                 CustomerBoxNum= box.CustomerBoxNum,
                                                 boxLoginID=box.LoginID,
                                             }).ToList();
                //if(boxFileUI.Count==0)
                //{
                //    boxFileUI = (from box in context.BoxesBuffer
                //                                 where box.BoxID == boxID
                //                                 select new BoxFileUI()
                //                                 {
                //                                     BoxID = box.BoxID,
                //                                     BoxDescription = box.BoxDescription,
                //                                     ExterminateYear = box.ExterminateYear,
                //                                     FileID = "",
                //                                     CustomerID = box.CustomerID.Value,
                //                                     DepartmentID = box.DepartmentID!=null? box.DepartmentID.Value:0,
                //                                     CustomerBoxNum = box.CustomerBoxNum,
                //                                 }).ToList();
                //}
                return boxFileUI;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<BoxFileUI>();

            }
        }
       public static bool insert(Boxes box)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {

                Boxes existBox = new Boxes()
                {
                    BoxDescription = box.BoxDescription,
                    BoxID = box.BoxID,
                    CustomerID = box.CustomerID,
                    DepartmentID = box.DepartmentID,
                    ExterminateYear = box.ExterminateYear,
                    CustomerBoxNum = box.CustomerBoxNum,
                    InsertDate = DateTime.Now,
                    LoginID = box.LoginID,
                    Location = false,
                    BoxType = 1,
                    Exterminate = false,
                    ShelfID =string.IsNullOrEmpty(box.ShelfID)? Consts.PARAM_EMPTY_SHELF_ID:box.ShelfID,
                };
                contex.Boxes.Add(existBox);
                contex.SaveChanges();
                BoxHistoryController.insert(new BoxHistory() { BoxID = box.BoxID, Date = DateTime.Now, UserID = box.LoginID, ShelfID = string.IsNullOrEmpty(box.ShelfID) ? Consts.PARAM_EMPTY_SHELF_ID : box.ShelfID });

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }
        public static bool insertUpdate(Boxes box)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Boxes existBox= contex.Boxes.Where(b => b.BoxID== box.BoxID).FirstOrDefault();
                if (existBox != null)
                {
                    existBox.ExterminateYear = box.ExterminateYear;
                    existBox.BoxDescription = box.BoxDescription;
                    existBox.CustomerBoxNum = box.CustomerBoxNum;
                    existBox.DepartmentID = box.DepartmentID;
                    existBox.InsertData = 2;
                    contex.SaveChanges();
                }
                else
                {
                    existBox = new Boxes()
                    {
                        BoxDescription = box.BoxDescription,
                        BoxID = box.BoxID,
                        CustomerID = box.CustomerID,
                        DepartmentID = box.DepartmentID,
                        ExterminateYear = box.ExterminateYear,
                        CustomerBoxNum=box.CustomerBoxNum,
                        InsertDate = DateTime.Now,
                        LoginID = box.LoginID,
                        Location = false,
                        BoxType=1,
                        Exterminate = false,
                        ShelfID = Consts.PARAM_EMPTY_SHELF_ID,
                        InsertData=2,
                    };
                    contex.Boxes.Add(existBox);
                    contex.SaveChanges();
                    BoxHistoryController.insert(new BoxHistory() { BoxID = box.BoxID, Date = DateTime.Now, UserID = box.LoginID, ShelfID = Consts.PARAM_EMPTY_SHELF_ID });

                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }

        public static int getLastCustomerBoxNum(int customerId)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                int? maxNum= contex.Boxes.Where(b => b.CustomerID == customerId).Select(x=>x.CustomerBoxNum).Max();
                int num = 0;
                if (maxNum!=null && maxNum > num)
                    num = maxNum.Value;
                return num+1;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return 0;

            }
        }

        public static Boxes validateIfBoxIDExist(string boxID)//,string customerBoxNam,int customerID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => b.BoxID == boxID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Boxes();
            }
        }

        public static Boxes validateIfBoxNumExist(int? customerBoxNam, int customerID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.Boxes.Where(b => b.CustomerBoxNum == customerBoxNam && b.CustomerID == customerID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Boxes();
            }
        }
    }
    public class BoxFileUI
        {
            public string BoxID { get; set; }
            public string BoxDescription { get; set; }
            public Nullable<int> ExterminateYear { get; set; }
            public string FileID { get; set; }
        public int CustomerID { get; set; }
        public int DepartmentID { get; set; }
        public int? CustomerBoxNum { get; set; }
        public Nullable<int> boxLoginID { get; set; }
    }
}
