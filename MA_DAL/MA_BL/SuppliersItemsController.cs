﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class SuppliersItemsController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static List<SuppliersItems> Get(int supplierId)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                var item = context.SuppliersItems.Where(si => si.SupplierId == supplierId 
                && (si.IsActive==true || si.IsActive == null)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<SuppliersItems>();
            }

        }


 
        public static void InsertUpdate(SuppliersItems entity)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                var item = context.SuppliersItems.Where(si => si.SupplierId == entity.SupplierId && si.ItemID == entity.ItemID).FirstOrDefault(); ;
                if (item != null)//update
                {
                    item.SupplierPrice = entity.SupplierPrice;
                    item.UpdatedDate = DateTime.Now;
                }
                else
                    context.SuppliersItems.Add(entity);
                
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));


            }
        }

        public static void InsertUpdate(List<SuppliersItems> entity)
        {
            try
            {
                foreach (var item in entity)
                {
                    InsertUpdate(item);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));


            }
        }

        public static void Delete(SuppliersItems entity)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                SuppliersItems itemToDelete = context.SuppliersItems.Where(si => si.SupplierId == entity.SupplierId && si.ItemID == entity.ItemID).FirstOrDefault();
                itemToDelete.IsActive = false;
                itemToDelete.UpdatedDate = DateTime.Now;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        public static void Delete(List<SuppliersItems> entity)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                foreach (var item in entity)
                {
                    if(item != null && context.SuppliersItems.Where(si => si.SupplierId == item.SupplierId && si.ItemID == item.ItemID).FirstOrDefault()!=null)
                    Delete(item);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));


            }
        }

        public static void RemoveSupplierItemsByItemId(int itemId)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                    List<SuppliersItems> suppliersItems = context.SuppliersItems.Where(si =>  si.ItemID == itemId).ToList();
                    if (suppliersItems.Count > 0)
                        Delete(suppliersItems);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));


            }
        }

    }

   
}
