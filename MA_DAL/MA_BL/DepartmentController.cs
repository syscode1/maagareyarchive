﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class DepartmentController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static OrderedDictionary GetUserDepartmentsDictionary(Users user)
        {
           
          ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDeparnments = new OrderedDictionary();
            try
            {
            List<Department> customerDep= contex.Department.Where(d=>d.CustomerID == user.CustomerID).ToList();
            foreach (var item in customerDep)
            {
                bool permittion = (bool)(user.GetType().GetProperty("PermDep" + item.DepartmentID)).GetValue(user);
                if (permittion)
                    UserDeparnments.Add(item.DepartmentID, item.DepartmentName);
            }

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
            return UserDeparnments;
        }
        public static OrderedDictionary GetCustomerDepartmentsDictionary(int customerID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            OrderedDictionary UserDeparnments = new OrderedDictionary();
            try
            { 
            List<Department> customerDep = contex.Department.Where(d => d.CustomerID == customerID).ToList();
            foreach (var item in customerDep)
            {
                    UserDeparnments.Add(item.DepartmentID, item.DepartmentName);
            }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return UserDeparnments;
        }
        public static List<Department> GetCustomerDepartments(int customerID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            List<Department> customerDep = new List<Department>();
            try
            { 
            customerDep = contex.Department.Where(d => d.CustomerID == customerID).ToList();
            
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return customerDep;
        }
        public static Dictionary<Customers,int> GetCustomerDepartmentsForInvoice()
        {
            ArchiveEntities contex = new ArchiveEntities();
            Dictionary<Customers, int> customerDep = new Dictionary<Customers, int>();
            try
            { 
            customerDep = (from dep in contex.Department
                           join cust in  contex.Customers on dep.CustomerID equals cust.CustomerID 
                           where dep.SelfPayment==false
                           group new {dep,cust } by dep.CustomerID into gDep
                           select  new { x= (from row2 in gDep select row2.cust).FirstOrDefault(),
                                         y = 0 })
                .ToDictionary(kvp => kvp.x, kvp => kvp.y);

                customerDep.Union ((from dep in contex.Department
                                    join cust in contex.Customers on dep.CustomerID equals cust.CustomerID
                                    where dep.SelfPayment==true
                           select  new { x= cust, y= dep.DepartmentID })
                .ToDictionary(kvp => kvp.x, kvp => kvp.y));
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
            return customerDep;
        }
        public static Department GetDepartment(int customerID , int departmentID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            Department Dep = contex.Department.Where(d => d.CustomerID == customerID && d.DepartmentID== departmentID).FirstOrDefault();
            return Dep;
        }

        public static List<Department> GetUserDepartments(Users user)
        {
            ArchiveEntities contex = new ArchiveEntities();
            List<Department> UserDeparnments = new List<Department>();
            try
            {

            List<Department> customerDep = contex.Department.Where(d => d.CustomerID == user.CustomerID).ToList();
            foreach (var item in customerDep)
            {
                bool permittion = (bool)(user.GetType().GetProperty("PermDep" + item.DepartmentID)).GetValue(user);
                if (permittion)
                    UserDeparnments.Add(item);
            }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
            return UserDeparnments;
        }

        public static bool insertUpdate(List<Department> department)
        {
            try {
                ArchiveEntities contex = new ArchiveEntities();
                foreach (var item in department)
                {
                   Department existDep= contex.Department.Where(d => d.DepartmentID == item.DepartmentID && d.CustomerID==item.CustomerID).FirstOrDefault();
if (existDep != null)
                    {
               //     existDep.CityAddress = item.CityAddress;
                        existDep.CollectionName = item.CollectionName;
                        existDep.CollectionPhone = item.CollectionPhone;
              //          existDep.ContactMan = item.ContactMan;
              //          existDep.DescriptionAddress = item.DescriptionAddress;
              //          existDep.Email = item.Email;
              //          existDep.Fax = item.Fax;
              //          existDep.FullAddress = item.FullAddress;
                        existDep.ID = item.ID;
                        existDep.SelfPayment = item.SelfPayment;
              //          existDep.Telephone1 = item.Telephone1;
              //          existDep.Telephone2 = item.Telephone2;
                        contex.SaveChanges();
                    }
                    else {
                        existDep = new Department();
              //          existDep.CityAddress = item.CityAddress;
                        existDep.CollectionName = item.CollectionName;
                        existDep.CollectionPhone = item.CollectionPhone;
              //          existDep.ContactMan = item.ContactMan;
              //          existDep.DescriptionAddress = item.DescriptionAddress;
              //          existDep.Email = item.Email;
              //          existDep.Fax = item.Fax;
              //          existDep.FullAddress = item.FullAddress;
                        existDep.ID = item.ID;
                        existDep.SelfPayment = item.SelfPayment;
              //          existDep.Telephone1 = item.Telephone1;
              //          existDep.Telephone2 = item.Telephone2;
                        existDep.CustomerID = item.CustomerID;
                        existDep.CustomerName = item.CustomerName;
                        existDep.DepartmentID = item.DepartmentID;
                        existDep.DepartmentName = item.DepartmentName;
                        contex.Department.Add(existDep);
                        contex.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }          
        }
    }
    public class DepartmentUI
    {
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
    }
}
