﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
   public class ParametersController:BaseController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string GetParameterValue(string key)
        {

            ArchiveEntities contex = new ArchiveEntities();
            Parameters param = null;
            try
            { 
               param= contex.Parameters.Where(p => p.ParameterKey == key).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
            return param!=null? param.ParameterValue:"";
        }
        public static void UpdateParameterValue(string key, string value)
        {
            try
            {
            ArchiveEntities contex = new ArchiveEntities();
            Parameters param = contex.Parameters.Where(p => p.ParameterKey == key).FirstOrDefault();
            param.ParameterValue = value;
            contex.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }
    }
}
