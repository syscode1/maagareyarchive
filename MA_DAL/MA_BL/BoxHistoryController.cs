﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class BoxHistoryController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<BoxHistory> getHistoryByBoxID(string boxID)
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();
                return context.BoxHistory.Where(b =>b.BoxID==boxID).OrderByDescending(x=>x.Date).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<BoxHistory>();

            }
        }
        public static bool insert(BoxHistory boxHistory)
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();
                context.BoxHistory.Add(boxHistory);
                context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }

    }
}
