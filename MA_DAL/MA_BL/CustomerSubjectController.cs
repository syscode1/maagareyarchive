﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class CustomerSubjectController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static List<CustomerSubjects> Get(int customerID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                var item = context.CustomerSubjects.Where(si => si.CustomerID == customerID).ToList();
                return item;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<CustomerSubjects>();
            }

        }



        public static bool Insert(CustomerSubjects entity)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                var item = context.CustomerSubjects.Where(si => si.CustomerID == entity.CustomerID && si.SubjectID == entity.SubjectID).FirstOrDefault(); ;
                if (item == null)
                    context.CustomerSubjects.Add(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }

        public static bool Insert(List<CustomerSubjects> entity)
        {
            bool isSucceeded = true;
            try
            {
                foreach (var item in entity)
                {
                    isSucceeded= isSucceeded & Insert(item);
                }
                return isSucceeded;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }

        public static bool Delete(CustomerSubjects entity)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                CustomerSubjects itemToDelete = context.CustomerSubjects.Where(si => si.CustomerID == entity.CustomerID && si.SubjectID == entity.SubjectID).FirstOrDefault();
                context.CustomerSubjects.Remove(itemToDelete);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }

        public static bool Delete(List<CustomerSubjects> entity)
        {
            try
            {
                bool isSucceeded = true;
                ArchiveEntities context = new ArchiveEntities();
                foreach (var item in entity)
                {
                    if (item != null && context.CustomerSubjects.Where(si => si.CustomerID == item.CustomerID && si.SubjectID == item.SubjectID).FirstOrDefault() != null)
                        isSucceeded=isSucceeded & Delete(item);
                }
                return isSucceeded;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

                return false;
            }
        }

        public static void DeleteSupplierSubjectsBySubjectId(int subjectId)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                List<CustomerSubjects> suppliersItems = context.CustomerSubjects.Where(si => si.SubjectID == subjectId).ToList();
                if (suppliersItems.Count > 0)
                    Delete(suppliersItems);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));


            }
        }
    }
}
