﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class BoxBufferController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool insert(BoxesBuffer box, int loginID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                context.BoxesBuffer.Add(box);
                context.SaveChanges();

                BoxesController.updateBoxLocation(false, box.BoxID);

                List<Files> files = FilesController.getFilesListByBox(box.BoxID);

                foreach (Files item in files)
                {
                    FilesController.updateLocation(item.FileID, false);
                    FileStatusController.Update(item.FileID, Consts.enFileStatus.waitingForShelf);
                    FileStatusArchiveController.Insert(new FileStatusArchive()
                    {

                        FieldID = item.FileID,
                        LoginID = loginID,
                        StatusDate = DateTime.Now,
                        StatusID = (int)Consts.enFileStatus.created,

                    });
                    FileStatusArchiveController.Insert(new FileStatusArchive()
                    {

                        FieldID = item.FileID,
                        LoginID = loginID,
                        StatusDate = DateTime.Now,
                        StatusID = (int)Consts.enFileStatus.waitingForShelf,
                    });
                }


                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
        public static bool remove(string boxID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                BoxesBuffer boxB = context.BoxesBuffer.Where(x => x.BoxID.Trim() == boxID.Trim()).FirstOrDefault();
                if (boxB != null)
                {
                    context.BoxesBuffer.Remove(boxB);

                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
        public static bool updatePlaced(string boxID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                BoxesBuffer boxB = context.BoxesBuffer.Where(x => x.BoxID.Trim() == boxID.Trim()).FirstOrDefault();
                if (boxB != null)
                {
                    boxB.isPlaced = true;
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
 
        public static List<BoxesBufferUI> getAllBoxesBuffer(int customerID,bool isArchive)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return (from bb in context.BoxesBuffer
                        join cust in context.Customers on bb.CustomerID equals cust.CustomerID
                        join box in context.Boxes on bb.BoxID equals box.BoxID into b
                        from subBox in b.DefaultIfEmpty()
                        where bb.Print == false && bb.isPlaced == false && (customerID.ToString()==Consts.PARAM_ARCHIVE_ID || isArchive==true || bb.CustomerID== customerID)
                        select new BoxesBufferUI()
                        {
                            BoxID = bb.BoxID,
                            CustomerID = bb.CustomerID,
                            CustomerBoxNum = bb.CustomerBoxNum,
                            CustomerName = cust.CustomerName,
                            InsertDate = bb.InsertDate,
                            Print = bb.Print,
                            BoxDescription = bb.BoxDescription,
                            ExterminateYear = bb.ExterminateYear,
                            DepartmentID = bb.DepartmentID,
                            InsertData=subBox == null?0:subBox.InsertData
                        }).OrderByDescending(x => x.InsertDate).ToList();


            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<BoxesBufferUI>();
            }
        }

       public static List<BoxesBuffer> getBoxesBuffer()
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.BoxesBuffer.ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1} ; innerException: {2}", Helper.GetCurrentMethod(), ex.Message,ex.InnerException));
                return new List<BoxesBuffer>();
            }
        }


        public static List<BoxesBufferUI> getAllNewBoxesBuffer()
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return (from bb in context.BoxesBuffer
                        join cust in context.Customers on bb.CustomerID equals cust.CustomerID
                        join box in context.Boxes on bb.BoxID equals box.BoxID into b
                        from subBox in b.DefaultIfEmpty()
                        where subBox.InsertData==1
                        select new BoxesBufferUI()
                        {
                            BoxID = bb.BoxID,
                            CustomerID = bb.CustomerID,
                            CustomerBoxNum = bb.CustomerBoxNum,
                            CustomerName = cust.CustomerName,
                            InsertDate = bb.InsertDate,
                            Print = bb.Print,
                            BoxDescription = bb.BoxDescription,
                            ExterminateYear = bb.ExterminateYear,
                            DepartmentID = bb.DepartmentID,
                        }).OrderBy(x => x.CustomerName).ThenBy(x=>x.BoxID).ToList();


            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<BoxesBufferUI>();
            }
        }


    }

    public class BoxesBufferUI
    {
        public string BoxID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string CustomerName { get; set; }
        public int? CustomerBoxNum { get; set; }
        public Nullable<System.DateTime> InsertDate { get; set; }
        public Nullable<bool> Print { get; set; }
        public string BoxDescription { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<int> ExterminateYear { get; set; }
        public int InsertData { get; set; }

    }
}
