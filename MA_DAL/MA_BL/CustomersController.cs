﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class CustomersController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //public static Dictionary<int, string> getAllCustomers()
        //{
        //    try
        //    {

        //        ArchiveEntities contex = new ArchiveEntities();
        //        var dictionary = contex.Customers.Select(p => new { p.CustomerID, p.CustomerName })
        //                                         .AsEnumerable()
        //                                         .ToDictionary(kvp => kvp.CustomerID, kvp => kvp.CustomerName);
        //        return dictionary;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
        //        return new Dictionary<int, string>();

        //    }

        //}


        public static Dictionary<int, string> getAllActiveCustomers()
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                var dictionary = contex.Customers.Where(c=>c.CustomerStatusID!=(int)Consts.enCustomerStatus.inactive)
                                                 .Select(p => new { p.CustomerID, p.CustomerName,p.CustomerStatusID })
                                                 .AsEnumerable()
                                                 .ToDictionary(kvp => kvp.CustomerID, kvp => kvp.CustomerStatusID == (int)Consts.enCustomerStatus.frozen?kvp.CustomerName + Consts.FROZEN_CHAR : kvp.CustomerName);
                return dictionary;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Dictionary<int, string>();

            }

        }

        public static Customers getCustomerByID(int customerID)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                var customer = contex.Customers.Where(c => c.CustomerID == customerID).FirstOrDefault();
                return customer;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;

            }

        }
        public static int? getCustomerStatus(int customerID)
        {
            try
            {

                ArchiveEntities contex = new ArchiveEntities();
                return contex.Customers.Where(c => c.CustomerID == customerID).FirstOrDefault().CustomerStatusID ;
                 
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return null;

            }

        }
        public static OrderedDictionary getAllActiveCustomersOrdered(int[] custIds)
        {
            try
            {
                OrderedDictionary od = new OrderedDictionary();
                ArchiveEntities contex = new ArchiveEntities();
                List<Customers> dictionary = contex.Customers.Where(c => custIds.Contains(c.CustomerID)).ToList();

                foreach (var item in dictionary)
                {
                    od.Add(item.CustomerID, item.CustomerStatusID == (int)Consts.enCustomerStatus.frozen ? item.CustomerName + Consts.FROZEN_CHAR : item.CustomerName);
                }
                return od;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new OrderedDictionary();

            }

        }
        public static OrderedDictionary getAllActiveCustomersOrdered()
        {
            try
            {
                OrderedDictionary od = new OrderedDictionary();
                ArchiveEntities contex = new ArchiveEntities();
                List<Customers> dictionary = contex.Customers.Where(c => c.CustomerStatusID != (int)Consts.enCustomerStatus.inactive).OrderBy(x=>x.CustomerName).ToList();

                foreach (var item in dictionary)
                {
                    od.Add(item.CustomerID, item.CustomerStatusID == (int)Consts.enCustomerStatus.frozen ? item.CustomerName + Consts.FROZEN_CHAR : item.CustomerName);
                }
                return od;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new OrderedDictionary();

            }

        }
        public static Dictionary<int, string> getCustomersStatusList()
        {
            try
            {


                ArchiveEntities context = new ArchiveEntities();


                return context.CustomerStetus.Select(p => new { p.CustomerStatusID, p.CustomerStatusName })
                                             .AsEnumerable()
                                             .ToDictionary(kvp => kvp.CustomerStatusID, kvp => kvp.CustomerStatusName);

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Dictionary<int, string>();

            }
        }



        public static bool insert(Customers customer)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                context.Customers.Add(customer);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }
       public static Customers updateBoxAmountOsem(Customers customer,int boxAmountOsem1,int boxAmountOsem2,int boxAmountOsem3,int boxAmountOsem4)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                Customers cust = context.Customers.Where(x => x.CustomerID == customer.CustomerID).FirstOrDefault();
                cust.BoxAmountOsem1=boxAmountOsem1;
                cust.BoxAmountOsem2 = boxAmountOsem2;
                cust.BoxAmountOsem3 = boxAmountOsem3;
                cust.BoxAmountOsem4 = boxAmountOsem4;
                context.SaveChanges();
                return cust;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new Customers();
            }
        }
        public static bool update(Customers customer)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                Customers custToUpdate = context.Customers.Where(c => c.CustomerID == customer.CustomerID).FirstOrDefault();
                custToUpdate.BoxPrice1 = customer.BoxPrice1;
                custToUpdate.BoxPrice2 = customer.BoxPrice2;
                custToUpdate.BoxPrice3 = customer.BoxPrice3;
                custToUpdate.BoxPrice4 = customer.BoxPrice4;
                custToUpdate.CityAddress = customer.CityAddress;
                custToUpdate.ContactMan = customer.ContactMan;
                custToUpdate.Credit = customer.Credit;
                custToUpdate.Crushing = customer.Crushing;
                custToUpdate.CrushingAndTransport = customer.CrushingAndTransport;
                custToUpdate.CustomerStatusID = customer.CustomerStatusID;
                custToUpdate.CustomerName = customer.CustomerName;
                custToUpdate.DateOfContract = customer.DateOfContract;
                custToUpdate.DeliveryApproval = customer.DeliveryApproval;
                custToUpdate.Description = customer.Description;
                custToUpdate.DescriptionAddress = customer.DescriptionAddress;
                custToUpdate.Email = customer.Email;
                custToUpdate.EmptyBox = customer.EmptyBox;
                custToUpdate.EmptyBoxMaxEmount = customer.EmptyBoxMaxEmount;
                custToUpdate.Fax = customer.Fax;
                custToUpdate.FixedPayment = customer.FixedPayment;
                custToUpdate.FullAddress = customer.FullAddress;
                custToUpdate.LinkTo = customer.LinkTo;
                custToUpdate.MaxRetrive = customer.MaxRetrive;
                custToUpdate.maxUrgentRetrive = customer.maxUrgentRetrive;
                custToUpdate.PaidBy = customer.PaidBy;
                custToUpdate.MethodOfPayment = customer.MethodOfPayment;
                custToUpdate.PeriodOfPayment = customer.PeriodOfPayment;
                custToUpdate.PO = customer.PO;
                custToUpdate.Registration = customer.Registration;
                custToUpdate.Retrive = customer.Retrive;
                custToUpdate.Scan = customer.Scan;
                custToUpdate.Telephon1 = customer.Telephon1;
                custToUpdate.Telephon2 = customer.Telephon2;
                custToUpdate.Transport = customer.Transport;
                custToUpdate.TransportAndContract = customer.TransportAndContract;
                custToUpdate.TransportAndIntake = customer.TransportAndIntake;
                custToUpdate.UrgentRetrive = customer.UrgentRetrive;
                custToUpdate.IsBoxDescRequiered = customer.IsBoxDescRequiered;
                custToUpdate.IsExtermYearRequiered = customer.IsExtermYearRequiered;
                custToUpdate.InsertData = customer.InsertData;
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }

       public static bool deactivate(int customerID,int loginID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                Customers custToUpdate = context.Customers.Where(c => c.CustomerID == customerID).FirstOrDefault();
                custToUpdate.CustomerStatusID = (int)Consts.enCustomerStatus.inactive;
                FilesController.deactivateFiles(customerID, loginID);
                BoxesController.deactivateBoxes(customerID, loginID);
                UserController.DeactivateUserByCustomer(customerID);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
        }  

    }
}