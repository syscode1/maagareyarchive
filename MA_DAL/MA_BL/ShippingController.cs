﻿using log4net;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DAL.MA_BL
{
    public class ShippingController
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static List<Shippings> exportShipping(int customerID, int? department, DateTime?
                                      fromShippingDate, DateTime? toShippingDate, int? loginID)
        {
            try
            {
                ArchiveEntities context = new ArchiveEntities();
                return context.Shippings.Where(b => b.CustomerID == customerID &&
                                           (department == null || ((int)(b.DepartmentID)) == department)
                                           && (fromShippingDate == null || fromShippingDate <= b.ShippingDate)
                                           && (toShippingDate == null || toShippingDate >= b.ShippingDate)
                                           && (loginID==null || loginID == b.LoginID)).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Shippings>();

            }
        }


        public static List<Shippings> exportShippingReceived(int customerID, int? department,
         DateTime? fromReceivedDate, DateTime? toReceivedDate, string ShippingID)
        {
            ArchiveEntities context = new ArchiveEntities();
            try
            {

                return context.Shippings.Where(b => b.CustomerID == customerID &&
                                           (department == null || ((int)(b.DepartmentID)) == department)
                                           && (fromReceivedDate == null || fromReceivedDate <= b.ReceivedDate)
                                           && (toReceivedDate == null || toReceivedDate >= b.ReceivedDate)
                                           && (string.IsNullOrEmpty(ShippingID) || ShippingID == b.ShippingID)).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Shippings>();

            }
        }


        public static object exportGroupedShippings(int customerID, int? department,
         DateTime? fromReceivedDate, DateTime? toReceivedDate, string ShippingID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                var fileStatusArchiveUI = (from shipping in contex.Shippings
                                           where shipping.CustomerID == customerID &&
                                              (department == null || ((int)(shipping.DepartmentID)) == department)
                                              && (fromReceivedDate == null || fromReceivedDate <= shipping.ReceivedDate)
                                              && (toReceivedDate == null || toReceivedDate >= shipping.ReceivedDate)
                                              && (string.IsNullOrEmpty(ShippingID) || ShippingID == shipping.ShippingID)
                                           group new { shipping } by new { shipping.ShippingID } into ordGroup
                                           select new
                                           {
                                               ShippingID = ordGroup.Key.ShippingID,
                                               ReceivedDate = (from row2 in ordGroup select row2.shipping.ReceivedDate).FirstOrDefault(),
                                               ReceivedBy = (from row2 in ordGroup select row2.shipping.ReceivedBy).FirstOrDefault(),
                                               count = ordGroup.Count()
                                           }).OrderByDescending(f => f.ReceivedDate).ToList();
                return fileStatusArchiveUI;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<Shippings>();

            }
        }

        public static bool insert(List<Shippings> shipping)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                foreach (var item in shipping)
                {
                    contex.Shippings.Add(item);
                }
                contex.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }


        public static bool insert(Shippings shipping)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                contex.Shippings.Add(shipping);
                contex.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;

            }
        }
        public static List<ShippingsUI> getSippings(string shippingID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<ShippingsUI> data = new List<ShippingsUI>();
                data = (from shippings in contex.Shippings
                        join customers in contex.Customers on shippings.CustomerID equals customers.CustomerID
                        join department in contex.Department on shippings.CustomerID equals department.CustomerID into dep
                        //                        join user in contex.Users on shippings.Driver equals user.UserID into u
                        from subDep in dep.DefaultIfEmpty()
                        where shippings.rowID.ToString() == shippingID.Trim() && (shippings.DepartmentID==0 || subDep.DepartmentID == shippings.DepartmentID) &&
                        shippings.Driver!=string.Empty 
                        select new ShippingsUI()
                        {
                            rowID=shippings.rowID,
                            CustomerName = customers.CustomerName,
                            DepartmentName = shippings.DepartmentID == 0 ? "": subDep.DepartmentName,
                            CustomerID = shippings.CustomerID,
                            DepartmentID = shippings.DepartmentID,
                            CityAddress = shippings.CityAddress,
                            Comment = shippings.Comment,
                            Color=shippings.Color,
                            ContactMan = shippings.ContactMan,
                            DoneStatus = shippings.DoneStatus,
                            DriverName = "",//subu.FullName,
                            DeliveryID=shippings.DeliveryID,
                            ShippingDate = shippings.ShippingDate,
                            ShippingID = shippings.ShippingID,
                            FullAddress = shippings.FullAddress,
                            NumInShipping = shippings.NumInShipping,
                            Telephone1 = shippings.Telephone1,
                            Telephone2 = shippings.Telephone2,
                            SumBoxes = shippings.SumBoxes,
                            SumExterminateBoxes = shippings.SumExterminateBoxes,
                            SumFiles = shippings.SumFiles,
                            SumFullBoxes = shippings.SumFullBoxes,
                            SumNewBoxOrder = shippings.SumNewBoxOrder,
                            SumRetriveBoxes = shippings.SumRetriveBoxes,
                            Urgent = shippings.Urgent,
                        }).Distinct().OrderBy(x => x.DoneStatus).ThenBy(x=>x.NumInShipping).ToList();
                return data;


            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<ShippingsUI>();

            }
        }
        public static ShippingsUI getSipping(int rowID)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                ShippingsUI data = new ShippingsUI();
                data = (from shippings in contex.Shippings
                        join customers in contex.Customers on shippings.CustomerID equals customers.CustomerID
                        join department in contex.Department on shippings.CustomerID equals department.CustomerID
                        //                        join user in contex.Users on shippings.Driver equals user.UserID into u
                        //                       from subu in u.DefaultIfEmpty()
                        where shippings.rowID == rowID
                        select new ShippingsUI()
                        {
                            rowID = shippings.rowID,
                            CustomerName = customers.CustomerName,
                            DepartmentName = department.DepartmentName,
                            CustomerID = shippings.CustomerID,
                            DepartmentID = shippings.DepartmentID,
                            CityAddress = shippings.CityAddress,
                            Comment = shippings.Comment,
                            Color = shippings.Color,
                            ContactMan = shippings.ContactMan,
                            DoneStatus = shippings.DoneStatus,
                            Driver = shippings.Driver,
                            DeliveryID=shippings.DeliveryID,
                            ShippingDate = shippings.ShippingDate,
                            ShippingID = shippings.ShippingID,
                            FullAddress = shippings.FullAddress,
                            NumInShipping = shippings.NumInShipping,
                            Telephone1 = shippings.Telephone1,
                            Telephone2 = shippings.Telephone2,
                            SumBoxes = shippings.SumBoxes,
                            SumExterminateBoxes = shippings.SumExterminateBoxes,
                            SumFiles = shippings.SumFiles,
                            SumFullBoxes = shippings.SumFullBoxes,
                            SumNewBoxOrder = shippings.SumNewBoxOrder,
                            SumRetriveBoxes = shippings.SumRetriveBoxes,
                            Urgent = shippings.Urgent,
                            ReceivedBy=shippings.ReceivedBy,
                            ReceivedDate=shippings.ReceivedDate,
                        }).FirstOrDefault();
                return data;


            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new ShippingsUI();

            }
        }

        public static List<ShippingsUI> getSippings(int customerID=0,int departmentID=0,int driver=0)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<ShippingsUI> data = new List<ShippingsUI>();
                data = (from shippings in contex.Shippings
                        join customers in contex.Customers on shippings.CustomerID equals customers.CustomerID
                        join department in contex.Department on shippings.DepartmentID equals department.DepartmentID into d
                                          from subDep in d.DefaultIfEmpty()

                        //                        join user in contex.Users on shippings.Driver equals user.UserID into u
                        //                       from subu in u.DefaultIfEmpty()
                where (shippings.CustomerID == customerID || customerID == 0) &&
                (shippings.CustomerID==subDep.CustomerID || subDep==null ) &&
                       (shippings.DepartmentID == 0 && subDep==null || subDep.DepartmentID == shippings.DepartmentID) &&
                       (departmentID == 0  || shippings.DepartmentID == departmentID ) &&
                       (driver == 0 || shippings.Driver.Trim() == driver.ToString()) &&
                        // (shippings.DoneStatus == (int)Consts.enDoneStatus.NotCommited || 
                        // shippings.DoneStatus == (int)Consts.enDoneStatus.InProgress|| 
                        // shippings.DoneStatus == (int)Consts.enDoneStatus.Done) &&
                        shippings.Driver != string.Empty 
                        select new ShippingsUI()
                        {
                            rowID=shippings.rowID,
                            CustomerName = customers.CustomerName,
                            DepartmentName = shippings.DepartmentID ==0? "": subDep.DepartmentName,
                            CustomerID = shippings.CustomerID,
                            DepartmentID = shippings.DepartmentID,
                            CityAddress = shippings.CityAddress,
                            Comment = shippings.Comment,
                            Color=shippings.Color,
                            ContactMan = shippings.ContactMan,
                            DoneStatus = shippings.DoneStatus,
                            DriverName = "",//subu.FullName,
                            DeliveryID=shippings.DeliveryID,
                            ShippingDate = shippings.ShippingDate,
                            ShippingID = shippings.ShippingID,
                            FullAddress = shippings.FullAddress,
                            NumInShipping = shippings.NumInShipping,
                            Telephone1 = shippings.Telephone1,
                            Telephone2 = shippings.Telephone2,
                            SumBoxes = shippings.SumBoxes,
                            SumExterminateBoxes = shippings.SumExterminateBoxes,
                            SumFiles = shippings.SumFiles,
                            SumFullBoxes = shippings.SumFullBoxes,
                            SumNewBoxOrder = shippings.SumNewBoxOrder,
                            SumRetriveBoxes = shippings.SumRetriveBoxes,
                            Urgent = shippings.Urgent,
                            ReceivedDate=shippings.ReceivedDate==null?DateTime.MaxValue:shippings.ReceivedDate,
                        }).OrderByDescending(x=>x.ReceivedDate).ThenByDescending(x=>x.rowID).ToList();
                return data;


            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<ShippingsUI>();

            }
        }
        public static List<ShippingsUI> getGroupShippings(DateTime fromDate, DateTime toDate, string shippingID,int customerID=0)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                List<ShippingsUI> data = new List<ShippingsUI>();
                data = (from shippings in contex.Shippings
                        join user in contex.Users on shippings.Driver equals user.UserID.ToString()
                        where (string.IsNullOrEmpty(shippingID) || shippings.ShippingID == shippingID) &&
                        shippings.ShippingDate >= fromDate &&
                        shippings.ShippingDate <= toDate &&
                        (customerID == 0 || shippings.CustomerID == customerID)
                        group new { shippings,user } by new { shippings.ShippingID } into shipGroup
                        select new ShippingsUI()
                        {
                            DriverName = (from row2 in shipGroup select row2.user.FullName).FirstOrDefault(),
                            Driver = (from row2 in shipGroup select row2.user.UserID.ToString()).FirstOrDefault(),
                            ShippingDate = (from row2 in shipGroup select row2.shippings.ShippingDate).FirstOrDefault(),
                            ShippingID =  shipGroup.Key.ShippingID,
                            DeliveryID= (from row2 in shipGroup select row2.shippings.DeliveryID).Min(),
                            SumBoxes = (from row2 in shipGroup select row2.shippings.SumBoxes).Sum() ,
                            SumExterminateBoxes = (from row2 in shipGroup select row2.shippings.SumExterminateBoxes).Sum(),
                            SumFiles = (from row2 in shipGroup select row2.shippings.SumFiles).Sum(),
                            SumFullBoxes = (from row2 in shipGroup select row2.shippings.SumFullBoxes).Sum(),
                            SumNewBoxOrder = (from row2 in shipGroup select row2.shippings.SumNewBoxOrder).Sum(),
                            SumRetriveBoxes = (from row2 in shipGroup select row2.shippings.SumRetriveBoxes).Sum(),
                        }).OrderBy(x => x.DeliveryID).ThenByDescending(x => x.ShippingDate).ToList();
                return data;


            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return new List<ShippingsUI>();

            }
        }
        public static bool update(List<Shippings> shippings)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                bool isSucceed = true;
                foreach (Shippings shipping in shippings)
                {
                    isSucceed &= update(shipping);
                }
                return isSucceed;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }
        public static bool update(List<ShippingsUI> shippings)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                bool isSucceed = true;
                foreach (ShippingsUI shipping in shippings)
                {
                    isSucceed &= update(shipping);
                }
                //if(isSucceed)
                //{
                //    foreach (ShippingsUI shipping in shippings)
                //    {
                //        Shippings ship = contex.Shippings.Where(x => x.rowID == shipping.rowID).FirstOrDefault();
                //        ship.NumInShipping = shipping.NumInShipping;
                //        contex.SaveChanges();
                //    }
                //}
                return isSucceed;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }
        public static bool update(Shippings shipping)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Shippings ship = contex.Shippings.Where(x => x.ShippingID == shipping.ShippingID).FirstOrDefault();
                ship.Color = shipping.Color;
                ship.NumInShipping = shipping.NumInShipping;
                ship.Comment = shipping.Comment;
                ship.DoneStatus = shipping.DoneStatus;
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }
    public static bool updateShippingDriver(int rowID,string newShippingID,int numInShipping, string driver)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Shippings ship = contex.Shippings.Where(x => x.rowID == rowID).FirstOrDefault();
                ship.ShippingID = newShippingID;
                ship.NumInShipping = numInShipping;
                ship.Driver = driver;
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }
        public static bool update(ShippingsUI shipping)
        {
            ArchiveEntities contex = new ArchiveEntities();
            try
            {
                Shippings ship = contex.Shippings.Where(x => x.rowID == shipping.rowID ).FirstOrDefault();
                ship.Color = shipping.Color;
                ship.NumInShipping = shipping.NumInShipping;
                ship.Comment = shipping.Comment;
                if(shipping.DoneStatus==(int)Consts.enDoneStatus.Done)
                   ship.DoneStatus = shipping.DoneStatus;
                if (shipping.Driver == "-1")
                    ship.Driver = string.Empty;
                contex.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

        }
    }
}
    public partial class ShippingsUI
    {
        public string ShippingID { get; set; }
        public int NumInShipping { get; set; }
        public int CustomerID { get; set; }
        public int DepartmentID { get; set; }
        public string CustomerName { get; set; }
        public string DepartmentName { get; set; }
        public string CityAddress { get; set; }
        public string FullAddress { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public Nullable<System.DateTime> ShippingDate { get; set; }
        public int LoginID { get; set; }
        public Nullable<int> SumExterminateBoxes { get; set; }
        public Nullable<int> SumRetriveBoxes { get; set; }
        public Nullable<int> SumFullBoxes { get; set; }
        public Nullable<int> SumNewBoxOrder { get; set; }
        public Nullable<int> SumBoxes { get; set; }
        public Nullable<int> SumFiles { get; set; }
        public Nullable<bool> Urgent { get; set; }
        public string ReceivedBy { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public bool Color { get; set; }
        public int DoneStatus { get; set; }
        public string ContactMan { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Driver { get; set; }
        public string DriverName { get; set; }
        public int rowID { get; set; }

    public string DeliveryID { get; set; }

    }


