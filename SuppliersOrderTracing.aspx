﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SuppliersOrderTracing.aspx.cs" Inherits="MaagareyArchive.SuppliersOrderTracing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="resources/js/jquery.tablesorter.min.js" type="text/javascript"></script>

      <script>
        function popup(mylink, windowname, suppliersOrderID, supplierID) {
            if (!window.focus)
                return true;
            var href;
            var w =600;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            if (typeof (mylink) == 'string')
                href = mylink;
            else href = mylink.href;
            window.open(href + "?suppliersOrderID=" + suppliersOrderID + "&supplierID=" + supplierID, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }
        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }

            $(document).ready(function () {
            setSortedTable();
            });

        function setSortedTable() {
            $("#<%=dgResults.ClientID%>").tablesorter({ dateFormat: "uk" });
            SetDefaultSortOrder();
        }
        function Sort(cell, sortOrder) {
            debugger
              var sorting = [[cell.cellIndex, sortOrder]];
              $("#<%=dgResults.ClientID%>").trigger("sorton", [sorting]);
        if (sortOrder == 0) {
            sortOrder = 1;
            cell.className = "sortDesc";
        }
        else {
            sortOrder = 0;
            cell.className = "sortAsc";
        }
        cell.setAttribute("onclick", "Sort(this, " + sortOrder + ")");
        cell.onclick = function () { Sort(this, sortOrder); };
    }

        function SetDefaultSortOrder() {
            
        var gvHeader = document.getElementById("<%=dgResults.ClientID%>");
            var dgResults = document.getElementById("<%=dgResults.ClientID%>");

            var headers = gvHeader.getElementsByTagName("TH");
            for (var i = 0; i < headers.length; i++) {
                headers[i].setAttribute("onclick", "Sort(this, 1)");
                headers[i].onclick = function () { Sort(this, 1); };
                headers[i].className = "sortDesc";
                //dgResults.getElementsByTagName("TH")[i].style.width = headers[i].style.width;
            }
       }
      
    </script>
    <h1>ניהול ומעקב הזמנות</h1>
         <asp:GridView ID="dgResults" runat="server" Width="650px"
                            AutoGenerateColumns="false" CssClass="grid"
                            EnableViewState="true">
                            <%--DataKeyField="FileID"--%>

                            <HeaderStyle CssClass="header" />

                            <AlternatingRowStyle CssClass="alt" />
                            <RowStyle CssClass="row" />
                            <Columns>
                                <asp:TemplateField HeaderText="תאריך"   ItemStyle-CssClass="ltr">
                                    <ItemTemplate >
                                        <%#(DataBinder.Eval(Container.DataItem, "Date")!=null?Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "Date")).ToShortDateString():"").ToString()%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="מספר הזמנה" ItemStyle-CssClass="ltr">
                                    <ItemTemplate>
                                         <a href="SuppliersOrderTracing_popup.aspx" onclick="return popup(this, 'notes','<%#(DataBinder.Eval(Container.DataItem, "SuppliersOrderID"))%>','<%#(DataBinder.Eval(Container.DataItem, "SupplierID"))%>')"> 
                                        <%#(DataBinder.Eval(Container.DataItem, "SuppliersOrderID")).ToString()%></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ספק" ItemStyle-CssClass="ltr">
                                    <ItemTemplate>
                                        <%#(DataBinder.Eval(Container.DataItem, "SupplierName")).ToString()%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
        <table style="margin-top: 10px; border-spacing: 0px; float: left; width: 100%">
        <tr>
            <td style="width: 240px; text-align: right;">
                <input type="button" value="יציאה" onclick="exit()" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
