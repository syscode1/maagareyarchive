﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" Inherits="MaagareyArchive.SearchOrderMaterial" CodeBehind="SearchOrderMaterial.aspx.cs" AutoEventWireup="True" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $(document).ready(function () {
            boxNum = document.getElementById('<%= btnSearch.ClientID%>').focus();
        });

        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                        window.location = "MainPageArchive.aspx";
                    else
                        window.location = "MainPage.aspx";
                }

    </script>
    <h1>חיפוש והזמנת חומר
    </h1>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <table style="border-spacing: 10px; display: inline">
                <asp:Label style="display:none;color:red;float:right" Font-Bold="true" ID="lblSearchExist" runat="server" />
                <tr id="trCustomer" runat="server" style="display: none">
                    <td>
                        <span>לקוח</span>
                        <br />

                        <asp:DropDownList ID="ddlCustomerName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <span>מספר לקוח</span>
                        <br />
                        <asp:DropDownList ID="ddlCustomerID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerID_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
                <tr>
                    <td style="width:200px">
                        <span>מחלקה</span>
                        <br />

                        <asp:DropDownList ID="ddlDepartment" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td style="width:200px">
                        <label>נושא</label>
                        <br />
                        <asp:DropDownList runat="server" ID="ddlSubject">
                        </asp:DropDownList>
                    </td>
                    <td style="width:100%"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="6">
                        <asp:Panel ID="dvFileFields" style="width: 958px;" EnableViewState="true" runat="server">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>מספר תיק</span>
                        <br />
                        <asp:TextBox ID="txtFileNum" runat="server" />
                    </td>
                    <td>
                        <span>מספר ארגז</span>
                        <br />
                        <asp:TextBox ID="txtBoxNum" runat="server" />
                    </td>
                    <td>
                        <span>מספר ארגז לקוח</span>
                        <br />
                        <asp:TextBox ID="txtCustomerBoxNum" runat="server" maxlength="9" onkeypress="return isNumberKey(event)" />
                    </td>
                    <td>
                        <span>תיאור ארגז</span>
                        <br />
                        <asp:TextBox ID="txtBoxDescription" runat="server" />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td  colspan="3" >
                        <input type="button" value="יציאה" onclick="exit()"  style="margin-left:10px;width:130px;float: right;" runat="server" />
                        <asp:Button ID="btnAdd" Text="להזמנה" runat="server" Width="85px" style="float: right !important;" OnClick="btnAdd_Click" />
                    </td>
                    <td colspan="3">
                        <asp:Button Text="חפש" ID="btnSearch" Style="float: left" Width="85px" OnClick="btnSearch_Click" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
