﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class CustomerSubjects :BasePage
    {
        public int rownum = 1;
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }


        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<MA_DAL.MA_DAL.CustomerSubjects> Data
        {
            get
            {
                if (Session["CustomerSubject_Data"] == null)
                    return null;
                return Session["CustomerSubject_Data"] as List<MA_DAL.MA_DAL.CustomerSubjects>;
            }
            set
            {
                Session["CustomerSubject_Data"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            clearMessages();
            if (!Page.IsPostBack)
            {
                fillDdl();
                string customerID = Request.QueryString["custID"];
                if (!string.IsNullOrEmpty(customerID))
                {
                    ddlCustomerID.SelectedValue = customerID;
                    ddlCustomerID_SelectedIndexChanged(null, null);
                }
            }
        }

        private void fillDdl()
        {
            ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
            ddlCustomerID.DataSource = (new MyCache()).CustomersIds;

            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();

            ddlCustomerID.DataTextField = "Value";
            ddlCustomerID.DataValueField = "Key";
            ddlCustomerID.DataBind();
        }

        private void bindGridData()
        {
            int? customerId = Helper.getInt(ddlCustomerID.SelectedValue);
            if (customerId != null)
            {
                Data  = SubjectsController.getAllSubjectsByCustomerID((int)customerId);
                //addEmptyRow();
                dgResults.DataSource = Data;
                dgResults.DataBind();
            }
        }

        private void addEmptyRow()
        { int? customerId = Helper.getInt(ddlCustomerID.SelectedValue);
            if (customerId == null || customerId <= 0)
                return;
           Data.Add(new MA_DAL.MA_DAL.CustomerSubjects()
            {
                CustomerID = (int)customerId,
                IsActive = true,
                SubjectName = string.Empty,
            });
        }

        protected void ddlCustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerName.SelectedValue = ddlCustomerID.SelectedValue;
            customerChanged();
        }

        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlCustomerID.SelectedValue = ddlCustomerName.SelectedValue;
            customerChanged();
        }
        private void customerChanged()
        {
            if (ddlCustomerID.SelectedValue.Trim() != "")
                btnAddRow.Style["display"] = "";
            bindGridData();
        }
    


        protected void btnSave_Click(object sender, EventArgs e)
        {
            int? CustomerId = Helper.getInt(ddlCustomerID.SelectedValue);

            bool isSucceed = false;

            fillItemsToAdd();
            isSucceed= SubjectsController.insertUpdateSubject(Data);
            string message = isSucceed ? Consts.SAVED_SUCCEED : Consts.SAVED_FAIL;
            showIndicationMessage(isSucceed, message);
        }

        private void fillItemsToAdd()
        {
            Data = new List<MA_DAL.MA_DAL.CustomerSubjects>();
            foreach (DataGridItem item in dgResults.Items)
            {
                int subjectID = Convert.ToInt32((item.FindControl("subjectID") as HtmlGenericControl).InnerText);
                if (string.IsNullOrEmpty((item.FindControl("subjectName") as TextBox).Text.Trim()))
                {
                    if (subjectID > 0)
                        SubjectsController.deactivateSubjectByID(subjectID);
                    continue;
                }
                Data.Add(new MA_DAL.MA_DAL.CustomerSubjects()
                {

                    SubjectID = subjectID,
                    SubjectName = (item.FindControl("subjectName") as TextBox).Text,
                    IsActive = true,
                    CustomerID = Convert.ToInt16(ddlCustomerID.SelectedValue),
                    UpdatedDate = DateTime.Now,
                });
            }

        }


        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }

        protected void btnAddRow_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool isEmptyAddress = false;
                fillItemsToAdd();
                addEmptyRow();
                dgResults.DataSource = Data;
                dgResults.DataBind();
                if (Data.Count > 0)
                    dgResults.Items[Data.Count - 1].FindControl("subjectName").Focus();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem && e.Item.ItemIndex==0)
                {
                    (e.Item.FindControl("subjectName") as TextBox).Focus();

                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }
        private void clearMessages()
        {
            dvSucceedMessage.InnerText = "";
            dvSucceedMessage.Style["display"] = "none";
            dvErrorMessage.InnerText = "";
            dvErrorMessage.Style["display"] = "none";
        }

    }
}
