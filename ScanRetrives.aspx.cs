﻿
using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ScanRetrives : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        string lastFile = "";
        string lastBox = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                bindData();
        }
        private void bindData()
        {
            List<Files> files = FilesController.getFilesOutOfBoxList();
            List<Retrives> shelfs = new List<Retrives>();
            foreach (var file in files)
            {
                string boxID =FileStatusArchiveController.getLastFileBoxID(file.FileID);
                if(!string.IsNullOrEmpty(boxID))
                {
                    Boxes box = BoxesController.getBoxByID(boxID);
                    shelfs.Add(new Retrives()
                    {
                        BoxID = boxID,
                        FileID = file.FileID,
                        ShelfID = box.ShelfID,
                        Warehouse = box.ShelfID.Substring(0, 3),
                        Cell = box.ShelfID.Substring(5, 2),
                        Row = box.ShelfID.Substring(3, 2),
                        Shelf = box.ShelfID.Substring(7, 2),
                        CustomerID=box.CustomerID,
                        CustomerBoxNum=box.CustomerBoxNum,
                        PrintRetrivesDate = file.PrintRetrivesDate,
                    });
                }
            }
            List<Boxes> boxes = BoxesController.getBoxesRetrives();
            foreach (var item in boxes)
            {
                shelfs.Add(new Retrives()
                {
                    BoxID = item.BoxID,
                    FileID = "",
                    ShelfID = item.ShelfID,
                    Warehouse ="000",// item.ShelfID.Substring(0, 3),
                    Cell ="00",// item.ShelfID.Substring(5, 2),
                    Row ="00",// item.ShelfID.Substring(3, 2),
                    Shelf ="00",// item.ShelfID.Substring(7, 2),
                    CustomerID = item.CustomerID,
                    CustomerBoxNum = item.CustomerBoxNum,
                    PrintRetrivesDate = item.PrintRetrivesDate,
                });
            }
            dgResults.DataSource = shelfs.OrderBy(x=>x.Warehouse).ThenBy(x=>x.PrintRetrivesDate);
            dgResults.DataBind();

            if(!string.IsNullOrEmpty(lastBox))
            {
                dgLastResult.DataSource = shelfs.Where(x => x.BoxID== lastBox);
                dgLastResult.DataBind();
            }
            else if(!string.IsNullOrEmpty(lastFile))
            {
                dgLastResult.DataSource = shelfs.Where(x => x.FileID == lastFile);
                dgLastResult.DataBind();
            }
        }
        protected void btnInputFileBoxID_Click(object sender, EventArgs e)
        {
            lastBox = lastFile = "";
            try
            {
                ArchiveEntities contex = new ArchiveEntities();
                if (txtFileBoxID.Value.StartsWith("261"))
                {
                    Boxes box = contex.Boxes.Where(b => b.BoxID == txtFileBoxID.Value).FirstOrDefault();
                    if (box != null)
                    {
                        box.Location = false;
                        box.PrintRetrivesDate = null;
                        box.ShelfID =Consts.TEMP_SHELF_IN_ARCHIVE_ID;
                        contex.SaveChanges();
                        BoxHistoryController.insert(new BoxHistory() { BoxID = box.BoxID, Date = DateTime.Now, UserID = user.UserID, ShelfID = Consts.TEMP_SHELF_IN_ARCHIVE_ID });

                        List<string> files = FilesController.getFilesListByBox(txtFileBoxID.Value).Select(f => f.FileID).ToList();
                        FilesController.updateLocation(files, true);
                        FileStatusController.Update(files, Consts.enFileStatus.inArchive);
                        FileStatusArchiveController.Insert(files, Consts.enFileStatus.inArchive, user.UserID);
                        lastBox = box.BoxID;
                        bindData();
                    }
                    else
                    {
                        dvErrorMessage.InnerText = Consts.BOX_NOT_EXIST;
                    }

                }
                else if (txtFileBoxID.Value.StartsWith("262"))
                {

                    Files file = contex.Files.Where(f => f.FileID == txtFileBoxID.Value).FirstOrDefault();
                    if (file != null)
                    {
                        file.BoxID = Consts.TEMP_BOX_IN_ARCHIVE_ID;
                        file.Location = false;
                        file.PrintRetrivesDate = null;
                        contex.SaveChanges();
                        FileStatusArchiveController.Insert(new FileStatusArchive()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.waitingForShelf
                        });
                        //Add if not exist /change file status
                        FileStatusController.UpdateInsert(new FileStatus()
                        {
                            FieldID = file.FileID,
                            LoginID = user.UserID,
                            StatusDate = DateTime.Now,
                            StatusID = (int)Consts.enFileStatus.waitingForShelf
                        });
                        lastFile = file.FileID;
                        bindData();
                    }
                    else
                    {
                        dvErrorMessage.InnerText = Consts.FILE_NOT_EXIST;
                    }

                }
                else
                {
                    dvErrorMessage.InnerText = Consts.INVALID_NUM;
                }
                txtFileBoxID.Value = string.Empty;
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));

            }
        }

        protected void dgResults_DataBound(object sender, EventArgs e)
        {
            for (int i = dgResults.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = dgResults.Rows[i];
                GridViewRow previousRow = dgResults.Rows[i - 1];

                    if (row.Cells[0].Text == previousRow.Cells[0].Text)
                    {
                        if (previousRow.Cells[0].RowSpan == 0)
                        {
                            if (row.Cells[0].RowSpan == 0)
                            {
                                previousRow.Cells[0].RowSpan += 2;
                            }
                            else
                            {
                                previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
                            }
                            row.Cells[0].Visible = false;
                        }

                    }
                else
                {
                    row.Style["border-top-style"] = "double";
                    row.Style["border-top-width"] = "8px";
                    row.Style["border-top-color"] = "#080808";
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
           foreach(GridViewRow item in  dgResults.Rows)
            {
                string printRetrivesDate = (item.FindControl("spnPrintRetrivesDate") as HtmlGenericControl).InnerText;
                if(string.IsNullOrEmpty(printRetrivesDate) )
                    
                {
                    string file = (item.FindControl("spnFileID") as HtmlGenericControl).InnerText;

                    if(!string.IsNullOrEmpty(file) )
                    {
                        FilesController.updatePrintRetrives(file);
                    }
                    else
                    {
                        string box = (item.FindControl("spnBoxID") as HtmlGenericControl).InnerText;
                        if (!string.IsNullOrEmpty(box))
                        {
                            BoxesController.updatePrintRetrives(box);
                        }
                    }
                }
            }
            bindData();
        }
    }
}