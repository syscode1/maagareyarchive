﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="OrderDetailesPopup.aspx.cs" Inherits="MaagareyArchive.OrderDetailesPopup" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 id="header" runat="server" > </h1>
    <div>
        <asp:DataGrid  ID="dgResults" runat="server"
             AutoGenerateColumns="false"  CssClass="gridPopup"
             EnableViewState="true" style="width: auto;"
            OnItemDataBound="dgResults_ItemDataBound">
            <HeaderStyle CssClass="header"/>
            <AlternatingItemStyle CssClass="alt" />
            <ItemStyle CssClass="row" />
            <Columns>
            </Columns>
        </asp:DataGrid>
    </div>
</asp:Content>
