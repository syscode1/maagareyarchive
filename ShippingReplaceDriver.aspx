﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="ShippingReplaceDriver.aspx.cs" Inherits="MaagareyArchive.ShippingReplaceDriver" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function moveShipping(cb) {
            if(cb.checked==true)
            {
                debugger
                $("#<%=hfShippngChecked.ClientID%>")[0].value = ($("[id*='spnShippingID']", $(cb).parents("tr"))[0]).innerText;
                $("#<%=hfDriverChecked.ClientID%>")[0].value = ($("[id*='spnDriverID']", $(cb).parents("tr"))[0]).innerText;
                $("#<%=btnMove.ClientID%>").click();
                return true;

            }
            return false;
        }
    </script>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
                        <asp:HiddenField ID="hfShippngChecked" runat="server"/>
                        <asp:HiddenField ID="hfDriverChecked" runat="server"/>
      <asp:Button ID="btnMove" style="display:none" OnClick="cbMove_CheckedChanged" runat="server" />
       <asp:GridView ID="dgResultsR" runat="server" CssClass="grid"
        EnableViewState="true" Width="500px"
        AutoGenerateColumns="false"
        EnablePersistedSelection="true"
        DataKeyNames="NumInShipping"
        >
        <HeaderStyle CssClass="header"/>
        <AlternatingRowStyle CssClass="alt" />
        <RowStyle CssClass="row" />
        <Columns>
            <asp:TemplateField HeaderText="בחר" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>

                    <asp:CheckBox ID="cbMove"  runat="server" onclick="return moveShipping(this)"/>
                  
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="מס משלוח" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                        <span id="spnShippingID" runat="server"><%#(Eval("ShippingID"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="נהג" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span id="spnDriverName" runat="server"><%#(Eval("DriverName"))%> </span>
                    <span id="spnDriverID" runat="server" style="display:none"><%#(Eval("Driver"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
         </Columns>
    </asp:GridView>

        </ContentTemplate>
    </asp:UpdatePanel>
              <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
