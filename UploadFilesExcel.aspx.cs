﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class UploadFilesExcel :BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public const int CONST_boxID =0 ;
        public const int CONST_boxDesc =1 ;
        public const int CONST_customerBoxNum =2 ;
        public const int CONST_extermBoxYear =3 ;
        public const int CONST_fileID = 4;
        public const int CONST_customerID =5 ;
        public const int CONST_departmentID =6 ;
        public const int CONST_subjectID =7 ;
        public const int CONST_fileDescription1 =8 ;
        public const int CONST_fileNum =9 ;
        public const int CONST_fromYear =10 ;
        public const int CONST_toYear =11 ;
        public const int CONST_fromDate = 12;
        public const int CONST_toDate = 13;
        public const int CONST_fromNum =14 ;
        public const int CONST_toNum =15 ;
        public const int CONST_oldNum =16;
        public const int CONST_extermYear =17 ;
        public const int CONST_fullName = 18;
        public const int CONST_fromScan = 19;
        public const int CONST_toScan =20 ;
        public const int CONST_fromCommand =21 ;
        public const int CONST_toCommand =22 ;
        public const int CONST_fromClaim =23 ;
        public const int CONST_toClaim =24 ;
        public const int CONST_identity =25 ;
        public const int CONST_bornYear =26 ;
        public const int CONST_nameBuildfile =27 ;
        public const int CONST_numBuildfile = 28;
        public const int CONST_demandNum =29 ;
        public const int CONST_demandNum1 =30 ;
        public const int CONST_demandNum2 = 31;
        public const int CONST_demandNum3 =32 ;
        public const int CONST_demandNum4 = 33;
        public const int CONST_demandNum5 =34 ;
        public const int CONST_demandNum6 =35 ;
        public const int CONST_demandNum7 =36 ;
        public const int CONST_demandNum8 = 37;
        public const int CONST_demandNum9 =38 ;
        public const int CONST_fromReference = 39;
        public const int CONST_toReference =40;
        public const int CONST_projectName =41 ;
        public const int CONST_fileNum2 = 42;
        public const int CONST_fileNum3 =43 ;
        public const int CONST_scientistName=44 ;
        public const int CONST_carNum =45 ;
        public const int CONST_eventDate =46;
        public const int CONST_custDep_customerName =47 ;
        public const int CONST_custDep_customerNum =48;

        public const string CONST_ROW_FIELD = "שורה: XXX  - ";

        int customerBoxNum;

        string existBoxID;
        private DataTable dtData
        {
            get
            {
                if(Session["UploadFilesExcel_dtData"]!=null)
                return Session["UploadFilesExcel_dtData"] as DataTable;
                return new DataTable();
            }
            set
            {
                Session["UploadFilesExcel_dtData"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                dtData = null;
            dvErrorMessage.InnerText = "";
            dvSucceedMessage.InnerText = "";
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if ((FileUpload1 as FileUpload).PostedFile!=null &&
                !string.IsNullOrEmpty((FileUpload1 as FileUpload).PostedFile.FileName))
            {
                string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string Extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                string FolderPath = ConfigurationManager.AppSettings["FolderPath"];

                string FilePath = Server.MapPath(FolderPath + FileName);
                if (!Directory.Exists(Server.MapPath(FolderPath)))
                {
                    Directory.CreateDirectory(Server.MapPath(FolderPath));
                }
                (FileUpload1 as FileUpload).PostedFile.SaveAs(FilePath);
                //               Import_To_Grid(FilePath, Extension, rbHDR.SelectedItem.Text);

                dtData = Helper.ImportExcellFileToGrid(FilePath, Extension, rbHDR.SelectedValue);
                GridView1.Caption = Path.GetFileName(FilePath);
                GridView1.DataSource = dtData;
                GridView1.DataBind();
            }
        }


        protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.DataSource = dtData;
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected void Download_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/vnd.ms-excel";

            Response.AppendHeader("Content-Disposition", "attachment; filename=MyFile.xlsx");

            Response.TransmitFile(Server.MapPath("~/resources/filesFormat/DataFileFormat.xlsx"));

            Response.End();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string errorMessage = string.Empty;
            int index = 0;
            customerBoxNum = 0;
            foreach (DataRow dr in dtData.Rows)
            {
                index++;
                try
                {
                string message= validateRow(dr,index);
                if (!string.IsNullOrEmpty(message))
                {
                    errorMessage += message;

                }

                else {
                    message= saveBox(dr,index);
                    if (!string.IsNullOrEmpty(message))
                    {
                         errorMessage += message;
                    }
                    else {
                        Files file = getFile(dr);
                        if (file != null)
                            if (!FilesController.insertUpdate(file, user))
                            {
                                message = Server.HtmlEncode(CONST_ROW_FIELD.Replace("XXX", index.ToString()) + Consts.SAVED_FAIL) + "<br/>";
                                errorMessage += message;
                            }
                    }
                }

                }
                catch (Exception ex)
                {
                    errorMessage += Server.HtmlEncode(CONST_ROW_FIELD.Replace("XXX", index.ToString()) + Consts.SAVED_FAIL) + "<br/>";
                                logger.Error(string.Format("method: {0} ; message: {1} ; innerExceptin: {2}", Helper.GetCurrentMethod(), ex.Message,ex.InnerException));

                }
            }
            if (!string.IsNullOrEmpty(errorMessage))
                dvErrorMessage.InnerHtml = Server.HtmlEncode(Consts.VALIDATE_EXCEL_FILE) + "<br/>" + errorMessage;
            else
                dvSucceedMessage.InnerHtml = Server.HtmlEncode(Consts.SAVED_SUCCEED) + "<br/>";
        }

        private string validateRow(DataRow dr, int index)
        {
            string errorMessage = string.Empty;
            long? boxID = isNomericLong(13, dr[CONST_boxID].ToString());

            if (boxID == null && !string.IsNullOrEmpty(dr[CONST_boxID].ToString()))
            {
                errorMessage += Server.HtmlEncode(Consts.VALIDATE_NUMERIC_FIELD.Replace("XXX", index.ToString()).Replace("YYY", "מספר ארגז").Replace("ZZZ", "13")) + "<br/>";
            }
            if (!string.IsNullOrEmpty(dr[CONST_boxID].ToString()) && !dr[CONST_boxID].ToString().StartsWith("261"))
            {
                errorMessage += Server.HtmlEncode(Consts.INVALID_BOXID_FIELD.Replace("XXX", index.ToString())) + "<br/>";
            }
            if (string.IsNullOrEmpty(dr[CONST_customerBoxNum].ToString()))
            {
                errorMessage += CONST_ROW_FIELD.Replace("XXX", index.ToString()) + Server.HtmlEncode(Consts.REQUIERED_FIELD).Replace("XXX","מספר ארגז לקוח")+ "<br/>";
            }
            if (dr[CONST_fileID] != null && !string.IsNullOrEmpty(dr[CONST_fileID].ToString()) && (!dr[CONST_fileID].ToString().StartsWith("262") || dr[CONST_fileID].ToString().Length != 13))
            {
                errorMessage += Server.HtmlEncode(Consts.INVALID_FIELD.Replace("XXX", index.ToString()).Replace("YYY", "מספר תיק")) + "<br/>";
            }
            int? customerID = isNomeric(12, dr[CONST_customerID].ToString());
            if (customerID == null)
            {
                errorMessage += Server.HtmlEncode(Consts.VALIDATE_NUMERIC_FIELD.Replace("XXX", index.ToString()).Replace("YYY", "מספר לקוח").Replace("ZZZ", "13")) + "<br/>";
            }
            else
            {
                Customers customer = CustomersController.getCustomerByID(customerID.Value);
                if(customer==null)
                    errorMessage += CONST_ROW_FIELD.Replace("XXX", index.ToString()) + Server.HtmlEncode(Consts.INVALID_CUSTOMER) + "<br/>";

            }

            //int? extermYear=  isNomeric(4, dr[2].ToString());

            //if(extermYear == null && dr[2]!=null && !string.IsNullOrEmpty(dr[2].ToString()))
            //{
            //    errorMessage += Server.HtmlEncode(Consts.VALIDATE_NUMERIC_FIELD.Replace("XXX", index.ToString()).Replace("YYY", "שנת ביעור").Replace("ZZZ", "4")) + "<br/>";
            //}
            return errorMessage;
        }

        private string saveBox(DataRow dr,int index)
        {
            Boxes ExistBox = null;
            existBoxID = string.Empty;
            if (!string.IsNullOrEmpty(dr[CONST_boxID].ToString()))
                ExistBox=BoxesController.validateIfBoxIDExist(dr[CONST_boxID].ToString());
            int? custBoxNum = Helper.getInt(dr[CONST_customerBoxNum].ToString());
            if (ExistBox == null)
            {
                ExistBox = BoxesController.validateIfBoxNumExist(custBoxNum, Helper.getInt(dr[CONST_customerID].ToString()).Value);
                if (ExistBox == null && string.IsNullOrEmpty(dr[CONST_boxID].ToString()))
                {
                    return Server.HtmlEncode(CONST_ROW_FIELD.Replace("XXX",index.ToString())+ Consts.BOX_NOT_EXIST) + "<br/>";    
                }
                else if (ExistBox!=null && !string.IsNullOrEmpty(dr[CONST_boxID].ToString()) && ExistBox.BoxID != dr[CONST_boxID].ToString())
                {
                    return Server.HtmlEncode(CONST_ROW_FIELD.Replace("XXX", index.ToString()) + Consts.BOX_BUFFER_ID_EXIST.Replace("XXX", dr[CONST_customerID].ToString()).Replace("YYY", ExistBox.BoxID)) + "<br/>";
                }

            }
            else
            {
                if ((ExistBox.CustomerBoxNum != custBoxNum) || ExistBox.CustomerID != Helper.getInt(dr[CONST_customerID].ToString()))
                {
                    return Server.HtmlEncode(CONST_ROW_FIELD.Replace("XXX", index.ToString()) + Consts.BOX_BUFFER_ID_EXIST.Replace("XXX", dr[CONST_customerID].ToString()).Replace("YYY", ExistBox.BoxID)) + "<br/>";
                }
            }

            int customerID = Convert.ToInt32(dr[CONST_customerID].ToString());
            int? customerBoxNum = custBoxNum!=null ? Helper.getInt( dr[CONST_customerBoxNum].ToString()) : getLastCustomerBoxNum(customerID);
            if (ExistBox == null)
                
                BoxBufferController.insert(new BoxesBuffer()
            {
                BoxID = ExistBox != null ? ExistBox.BoxID : dr[CONST_boxID].ToString(),
                CustomerBoxNum = customerBoxNum,
                CustomerID = customerID,
                InsertDate = DateTime.Now,
                Print = false,
                    DepartmentID = string.IsNullOrEmpty(dr[CONST_departmentID].ToString()) ? 1 : Helper.getInt(dr[CONST_departmentID].ToString()),
                    ExterminateYear = !string.IsNullOrEmpty(dr[CONST_extermBoxYear].ToString()) ? Convert.ToInt16(dr[CONST_extermBoxYear].ToString()) : 0,
                    BoxDescription = dr[CONST_boxDesc].ToString(),
                }, user.UserID);

            Boxes b = new Boxes()
            {
                BoxID= ExistBox!=null?ExistBox.BoxID: dr[CONST_boxID].ToString(),
                BoxDescription = dr[CONST_boxDesc].ToString(),
                CustomerID =customerID,
                DepartmentID =string.IsNullOrEmpty( dr[CONST_departmentID].ToString())?1: Helper.getInt(dr[CONST_departmentID].ToString()),
                ExterminateYear = !string.IsNullOrEmpty(dr[CONST_extermBoxYear].ToString()) ? Convert.ToInt16(dr[CONST_extermBoxYear].ToString()) : 0,
                CustomerBoxNum =customerBoxNum,
                LoginID = user.UserID,
                InsertData = 2,
            };
            if (ExistBox != null)
                existBoxID = ExistBox.BoxID;
            bool isSucceed = BoxesController.insertUpdate(b);
            return isSucceed ? "" : Server.HtmlEncode(CONST_ROW_FIELD.Replace("XXX", index.ToString()) +Consts.SAVED_BOX_FAIL) + "<br/>";

        }

        private int? getLastCustomerBoxNum(int customerID)
        {
            if (customerBoxNum > 0)
            {
                do
                {
                    customerBoxNum++;
                }
                while (BoxesController.validateIfBoxNumExist(customerBoxNum, customerID) != null);
            }
            else
                customerBoxNum = BoxesController.getLastCustomerBoxNum(customerID);

            return customerBoxNum;
        }

        private Files getFile(DataRow dr)
        {
            Files file = new Files();
            if(dr[CONST_fileID] ==null || string.IsNullOrEmpty( dr[CONST_fileID].ToString()))
            {
                file.FileID = setAutoFileID();
            }
            else
                file.FileID = dr[CONST_fileID].ToString() ;

            int? departmentID = dr[CONST_departmentID] ==null ?1:Helper.getInt( dr[CONST_departmentID].ToString());
            file.DepartmentID = departmentID==null?1:departmentID;

            file.CustomerID = Helper.getInt(dr[CONST_customerID].ToString());

            file.BoxID =!string.IsNullOrEmpty( dr[CONST_boxID].ToString())?dr[CONST_boxID].ToString(): existBoxID;

            int? subjectID = Helper.getInt(dr[CONST_subjectID].ToString());
            file.SubjectID = subjectID != null ? subjectID.Value : 0;

            file.BornYear = Helper.getDate(dr[CONST_bornYear].ToString());
            file.CarNumber = dr[CONST_carNum].ToString();
            file.ClaimEnd= dr[CONST_toClaim].ToString();
            file.ClaimStart= dr[CONST_fromClaim].ToString();
            file.CustomFileNum = departmentID == 5 ? dr[CONST_numBuildfile].ToString() : (departmentID == 8 ? dr[CONST_custDep_customerNum].ToString() : dr[CONST_fileNum].ToString());
            file.CustomFileNum2 = dr[CONST_fileNum2].ToString();
            file.CustomFileNum3 = dr[CONST_fileNum3].ToString();
            file.DemandNum = dr[CONST_demandNum].ToString();
            file.DemandNum2 = dr[CONST_demandNum1].ToString();
            file.DemandNum3 = dr[CONST_demandNum2].ToString();
            file.DemandNum4 = dr[CONST_demandNum3].ToString();
            file.DemandNum5 = dr[CONST_demandNum4].ToString();
            file.DemandNum6 = dr[CONST_demandNum5].ToString();
            file.DemandNum7 = dr[CONST_demandNum6].ToString();
            file.DemandNum8 = dr[CONST_demandNum7].ToString();
            file.DemandNum9 = dr[CONST_demandNum8].ToString();
            file.DemandNum10 = dr[CONST_demandNum9].ToString();
            file.Description1 = departmentID == 5 ? dr[CONST_nameBuildfile].ToString() : (departmentID == 8 ? dr[CONST_custDep_customerName].ToString() : (departmentID==7? dr[CONST_projectName].ToString(): dr[CONST_fileDescription1].ToString()));
            file.EndCommand=Helper.getInt( dr[CONST_toCommand].ToString());
            file.EndDate = Helper.getDate(dr[CONST_toDate].ToString());
            file.EndNum=Helper.getInt( dr[CONST_toNum].ToString());
            file.EndScanNum= dr[CONST_toScan].ToString();
            file.EndYear=Helper.getInt( dr[CONST_toYear].ToString());
            file.EventDate = Helper.getDate(dr[CONST_eventDate].ToString());
            file.ExterminateYear = Helper.getInt(dr[CONST_extermYear].ToString());
            file.FullName= dr[CONST_fullName].ToString();
            file.Identity= dr[CONST_identity].ToString();
            file.OldFileId=dr[CONST_oldNum].ToString();
            file.ReferenceEnd= dr[CONST_toReference].ToString();
            file.ReferenceStart= dr[CONST_fromReference].ToString();
            file.ScientistName=dr[CONST_scientistName].ToString();
            file.StartCommand= Helper.getInt(dr[CONST_fromCommand].ToString());
            file.StartDate=  Helper.getDate(dr[CONST_fromDate].ToString());
            file.StartNum=Helper.getInt(dr[CONST_fromNum].ToString());
            file.StartScanNum= dr[CONST_fromScan].ToString();
            file.StartYear=Helper.getInt(dr[CONST_fromYear].ToString());
            return file;
        }
        private int? isNomeric(int maxLength,string value)
        {
            if (value.Length > maxLength)
                return null;

            return Helper.getInt(value);
        }
        private long? isNomericLong(int maxLength,string value)
        {
            if (value.Length > maxLength)
                return null;

            return Helper.getLong(value);
        }
        private string setAutoFileID()
        {
            string fileID = "";
            Int64 lastFileNum = 0;
            Int64.TryParse(ParametersController.GetParameterValue(Consts.Parameters.PARAM_LAST_FILE_ID), out lastFileNum);
            if (lastFileNum > 0)
            {
                lastFileNum += 1;
                fileID = lastFileNum.ToString();
                ParametersController.UpdateParameterValue(Consts.Parameters.PARAM_LAST_FILE_ID, lastFileNum.ToString());

            }
            return fileID;
        }

    }
}