﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="InputData.aspx.cs" Inherits="MaagareyArchive.InputData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        //Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        <%--        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            boxId = $('#<%=txtBoxNum.ClientID %>')[0].value;

            if (boxId != "" && boxId != undefined)
                fillBoxData(boxId);
        }--%>

        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }

        function validateBox() {
            isValid = true;
            viewErrorMesageBox("");
            var txtBoxNum = document.getElementById('<%= txtBoxNum.ClientID%>');
            if (!document.getElementById('<%= txtBoxNum.ClientID%>').value.startsWith("261")) {
                drowField(true, txtBoxNum);
                viewErrorMesageBox(" מספר ארגז צריך להתחיל ב 261 ולהכיל 13 ספרות");

                isValid = false;

            }
            if (isValid)
                document.getElementById('<%=txtBoxDescription.ClientID%>').focus();

            return isValid == 0 ? false : true;
        }



        function viewErrorMesageBox(message) {
            document.getElementById('<%= dvBoxErrorMessageBottom.ClientID%>').innerText += message;
        }
        function viewErrorMesage(message) {
            document.getElementById('<%= dvErrorMessage.ClientID%>').innerText = message;
            document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "";
        }

        function validate() {
            isValidBox = true;
            isValid = true;
            viewErrorMesageBox("");
            viewErrorMesage("");
            isValidBox = isValidBox & validateBox();

            isValid = isValid & isCompleteField(document.getElementById('<%= txtFileID.ClientID%>'));
            var inputs = $("#" + '<%= dvFileFields.ClientID%>' + " :input");
            for (i = 0; i < inputs.length; i++) {
                if (inputs[i].attributes["isrequiered"].nodeValue == "True")
                    isValid = isValid & isCompleteField(inputs[i]);
                if (inputs[i].attributes["fieldName"].nodeValue.toLowerCase().indexOf("from") > -1 ||
                    inputs[i].attributes["fieldName"].nodeValue.toLowerCase().indexOf("start") > -1) {
                    var value = (inputs[i].attributes["type"].nodeValue == "date" ?
                        new Date(inputs[i].value) : (inputs[i].attributes["type"].nodeValue == "number" ?
                        Number(inputs[i].value) : inputs[i].value));
                    var nextValue = (inputs[i + 1].attributes["type"].nodeValue == "date" ?
                        new Date(inputs[i + 1].value) : (inputs[i + 1].attributes["type"].nodeValue == "number" ?
                        Number(inputs[i + 1].value) : inputs[i + 1].value));
                    if(nextValue!="" && nextValue!=null)
                    if (inputs[i].attributes["type"].nodeValue == "date") {
                        if (value.getTime() > nextValue.getTime()) {

                            viewErrorMesage('<%= MA_DAL.MA_HELPER.Consts.INVALID_COMPARE_FIELDS %>');
                            drowField(true, inputs[i])
                            drowField(true, inputs[i + 1])
                            return false;
                        }
                        else {
                            drowField(false, inputs[i])
                            drowField(false, inputs[i + 1])
                        }

                    }
                    else if (inputs[i].attributes["type"].nodeValue == "number") {
                        if (Number(value) > Number(nextValue)) {
                            viewErrorMesage('<%= MA_DAL.MA_HELPER.Consts.INVALID_COMPARE_FIELDS %>');
                            drowField(true, inputs[i])
                            drowField(true, inputs[i + 1])
                            return false;

                        }
                        else {
                            drowField(false, inputs[i])
                            drowField(false, inputs[i + 1])
                        }
                    }
                    else {
                        if (value > nextValue) {
                            viewErrorMesage('<%= MA_DAL.MA_HELPER.Consts.INVALID_COMPARE_FIELDS %>');
                            drowField(true, inputs[i])
                            drowField(true, inputs[i + 1])
                            return false;

                        }
                        else {
                            drowField(false, inputs[i])
                            drowField(false, inputs[i + 1])
                        }
                    }
            }
        }




        if (isValid == false) {
            document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "";
            document.getElementById('<%= dvErrorMessage.ClientID%>').innerText = "יש למלאות את כל שדות החובה";
        }
            <%--      if (document.getElementById('<%= .ClientID%>').checked && document.getElementById('<%= ddlCustomerName.ClientID%>').value.trim() == "0") {
                document.getElementById('<%= dvSucceedMessage.ClientID%>').style.display = "none";
                document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "";
                document.getElementById('<%= dvErrorMessage.ClientID%>').innerText += "\nיש לבחור לקוח";
                isValid = false;
            }--%>
            return (isValid & isValidBox) == 0 ? false : true;

        }
        var newCustID;
        function inputBox(evt) {
            newCustID = "";
            document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "none";
            if (evt.currentTarget.value.length > 13) {
                {
                    enableDidableControl(true, document.getElementById('<%= dvInputFiles.ClientID%>'));
                    enableDidableControl(true, document.getElementById('<%= btnSaveBox.ClientID%>'));
                }

            }
            if (evt.currentTarget.value.length == 13) {
                if (evt.currentTarget.value.indexOf("261") == 0) {
                    debugger
                    var selectedCustIdValue=document.getElementById('<%= ddlCustomerID.ClientID%>').value;
                    getCustByBox(evt.currentTarget.value);
                    if (selectedCustIdValue != "0" && selectedCustIdValue!=newCustID)
                    {
                        var toEdit = confirm(`אתה עובד על לקוח ${selectedCustIdValue}, האם אתה מעונין לעבור ללקוח ${newCustID} ?`); 
                        if (!toEdit)
                            evt.currentTarget.value = '';
                    }
                    if (toEdit || selectedCustIdValue == "0" || selectedCustIdValue == newCustID)
                    {
                            document.getElementById('<%= hfFillFields.ClientID%>').value = evt.currentTarget.value;
                            enableDidableControl(false, document.getElementById('<%= btnCloseFile.ClientID%>'));
                            document.getElementById('<%= btnLoadBoxData.ClientID%>').click();
                        }
                }
                else {
                    document.getElementById('<%= dvBoxErrorMessageBottom.ClientID%>').style.display = "";
                    document.getElementById('<%= dvBoxErrorMessageBottom.ClientID%>').innerText = "מספר ארגז תקין מתחיל ב 261";

                }
            }
            return false;
        }

        function inputCustBoxNum() {
            document.getElementById('<%= hfIsAutoCustBoxNum.ClientID%>').value = "0";
        }

        function inputFile(evt) {
            document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "none";
            if (evt.currentTarget.value.length > 13 || evt.currentTarget.value.length < 13) {
                {
                    enableDidableControl(true, document.getElementById('<%= btnSaveFile.ClientID%>'));
                    // enableDidableControl(true, document.getElementById('<%= btnCloseFile.ClientID%>'));
                    return false;
                }

            }
            if (evt.currentTarget.value.length == 13) {
                var selectedCustomer = document.getElementById('<%= ddlCustomerID.ClientID%>').value;
                var selectedDepartment = document.getElementById('<%= ddlDepartment.ClientID%>').value;
                return checkFileExist(evt.currentTarget.value, selectedCustomer, selectedDepartment);
            }

            return true;
        }
        function enableDidableControl(disable, control) {
            if (disable == true) {
                control.style.pointerEvents = "none";
                control.style.opacity = "0.4";
            }
            else
                control.removeAttribute("style")
        }
        function checkFileExist(fileID, customerID, departmentID) {
            return jQuery.ajax({
                type: "POST",
                url: 'InputData.aspx/checkFileExist',
                data: "{fileID : '" + fileID + "',customerID : '" + customerID + "',departmentID : '" + departmentID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }
        function getCustByBox(boxID) {
            return jQuery.ajax({
                type: "POST",
                async: false,
                url: 'InputData.aspx/getCustByBox',
                data: "{boxID : '" + boxID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccessCustByBox,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }
        function OnSuccessCustByBox(response) {
            newCustID = response.d;
            return;
    }
        function OnSuccess(response) {
            if (response.d == "") {
                document.getElementById('<%= btnInputFileID.ClientID%>').click();
                enableDidableControl(false, document.getElementById('<%= btnSaveFile.ClientID%>'));
                // enableDidableControl(false, document.getElementById('<%= btnCloseFile.ClientID%>'));
            }
            else if (response.d == null) {
                document.getElementById('<%= txtFileID.ClientID%>').value = "";
                document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "";
                document.getElementById('<%= dvErrorMessage.ClientID%>').innerText = '<%= MA_DAL.MA_HELPER.Consts.FILE_EXIST_IN_OTHER_BOX %>';
            }
            else {

                var con = confirm('<%= MA_DAL.MA_HELPER.Consts.FILE_EXIST_IN_BOX %>'.replace("XXX", response.d));// file exist in box
                if (con == true) {
                    document.getElementById('<%= btnInputFileID.ClientID%>').click();
                    enableDidableControl(false, document.getElementById('<%= btnSaveFile.ClientID%>'));
                    //  enableDidableControl(false, document.getElementById('<%= btnCloseFile.ClientID%>'));
                }
                else
                    document.getElementById('<%= txtFileID.ClientID%>').value = "";
            }
        return false;
    }
    function newBox() {
        document.getElementById('<%= txtBoxNum.ClientID%>').value = "";
        document.getElementById('<%= btnLoadBoxData.ClientID%>').click();
        return false;
    }
    function boxBufferClick(event) {
        document.getElementById('<%= txtBoxNum.ClientID%>').value = event.attributes["name"].value;
        document.getElementById('<%= btnLoadBoxData.ClientID%>').click();
    }

    function popup(href) {
        if (!window.focus)
            return true;
        var href;
        var w = 1200;
        var h = 600;
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        window.open(href, 'upload data', 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
        return false;
    }

    </script>
    <h1>רישום תוכן
    </h1>
   
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfIsAutoCustBoxNum" runat="server" />
            <table style="width: 100%">
                <tr>
                    <td>
                        <div style="width: 100%">
                            <table>
                                <tr>
                                    <td><span style="padding-top: 15px; padding-left: 10px;"><b>הכנס לקוח</b></span></td>
                                    <td>
                                        <label>שם לקוח</label>
                                        <br />
                                        <asp:DropDownList runat="server" Style="width: 140px" ID="ddlCustomerName" TabIndex="5" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged">
                                        </asp:DropDownList>

                                    </td>

                                    <td>
                                        <asp:Label Text="מספר לקוח" runat="server" />
                                        <br />
                                        <asp:DropDownList runat="server" Style="width: 140px" ID="ddlCustomerID" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerID_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 359px"></td>
                                    <td>
                                        <asp:Button Text="קליטה מאקסל" runat="server" Width="230px" OnClientClick="popup('UploadFilesExcel.aspx')" />

                                    </td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>
                       <div class="h1_sub"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <table style="width: 100%">
                                <tr>
                                    <td><span style="padding-top: 15px; padding-left: 10px;"><b>סרוק ברקוד תיבה</b></span></td>
                                    <td>
                                        <asp:TextBox ID="txtBoxNum" TabIndex="1" oninput="return inputBox(event)" Width="123px" onkeypress="return isNumberKey(event)" MaxLength="13" runat="server" />
                                        <%--onchange="return inputBox(event)" --%>
                                        <asp:ImageButton ID="btnAddRow" Style="vertical-align: bottom" OnClientClick="return newBox()" ImageUrl="resources/images/add.png" Height="31px" runat="server" />

                                        <asp:Button Style="display: none" ID="btnLoadBoxData" runat="server" UseSubmitBehavior="true" OnClick="btnLoadBoxData_Click" />

                                    </td>
                                    <td>
                                        <input type="text" placeholder="מספר ארגז לקוח" tabindex="3" oninput="return inputCustBoxNum(event)"  maxlength="9" onkeypress="return isNumberKey(event)"  id="txtCustomerBoxNum" runat="server" />

                                    </td>
                                    <td>
                                        <input type="text" placeholder="טקסט לארגז" tabindex="2" onkeypress="return maxCharLength(event)" maxlength="256" id="txtBoxDescription" runat="server" />

                                    </td>
                                    <td>
                                        <input type="text" placeholder="שנת ביעור" tabindex="4" onkeypress="return isNumberKey(event)" maxlength="4" style="width: 73px" id="txtExterminatedDate" runat="server" />

                                    </td>
                                    <td>
                                        <span style="padding-top: 15px; padding-left: 10px;">שם מקליד:</span>
                                    </td>
                                    <td>
                                        <input type="text"  id="txtTypingName" disabled="" tabindex="4" maxlength="25" style="width: 73px" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="h1_sub"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <span style="padding-top: 15px; padding-left: 10px;"><b>בחר מחלקה / נושא</b></span>
                                    </td>
                                    <td>
                                        <label>מחלקה</label>
                                        <br />
                                        <asp:DropDownList runat="server" Style="width: 140px" ID="ddlDepartment" TabIndex="6" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <label>נושא</label>
                                        <br />
                                        <asp:DropDownList runat="server" Style="width: 140px" ID="ddlSubject">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                            </table>


                        </div>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="h1_sub"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hfFillFields" runat="server" Value="0" />
                        <div style="clear: both">

                            <div id="dvInputFiles" style="float: right" runat="server">
                                <div id="dvHeaderFileFields" runat="server">
                                    <table>
                                        <tr>
                                            <td>

                                                <asp:CheckBox ID="cbAutoFileNum" runat="server" Style="float: right" OnCheckedChanged="cbAutoFileNum_CheckedChanged" AutoPostBack="true" />
                                                <label style="float: right">מספור תיקים אוטומטי</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>מספר תיק</label>
                                                <br />
                                                <input type="text" id="txtFileID" style="width: 140px" oninput="return inputFile(event)" maxlength="13" onkeypress="return isNumberKey(event)" runat="server" />
                                                <asp:Button Style="display: none" ID="btnInputFileID" runat="server" UseSubmitBehavior="true" OnClick="btnInputFileID_Click" />

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="dvFiles" style="padding-top: 5px; height: 303px; width: 173px; background-color: rgba(226, 251, 202, 0.95); height: 300px; border-radius: 8px; border-right: 3px solid #5f9827 !important; overflow: auto">
                                                    <asp:Repeater runat="server" ID="rptBoxFiles">
                                                        <ItemTemplate>
                                                            <table class="tblFiles" cellpadding="10" cellspacing="0" style="text-align: center; width: 100%;">
                                                                <tr>
                                                                    <td style="padding-top: 3px; padding-bottom: 3px">
                                                                        <asp:LinkButton ID="lbInputDataFileID" CssClass="lbInputDataFileID" Text='<%# DataBinder.Eval(Container.DataItem, "FileID") %>' OnClick="fileID_Click" runat="server" />
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="float: left">
                                <table style="width: 100%">
                                    <tr>
                                        <td>
                                            <div class="dvInputFiles">
                                                <asp:Panel ID="dvFileFields" EnableViewState="true" runat="server">
                                                </asp:Panel>
                                            </div>
                                        </td>
                                    </tr>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="float: right">
                                                <asp:Button ID="btnSaveFile" runat="server" OnClick="btnSaveFile_Click" OnClientClick="return validate();" Style="pointer-events: none; opacity: 0.4;" Text="סיים תיק" Width="80px" />
                                            </td>
                                            <td><span id="dvErrorMessage" runat="server" class="dvErrorMessage"></span><span id="dvFileErrorMessageBottom" runat="server" class="dvErrorMessage" style="display: none"></span><span id="dvBoxErrorMessageBottom" runat="server" class="dvErrorMessage"></span></td>
                                            <td style="float: left">
                                                <asp:Button ID="btnSaveBox" runat="server" OnClick="btnSaveBox_Click" OnClientClick="return validateBox();" Style="pointer-events: none; opacity: 0.4" Text="סיים תיבה" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </table>

                            </div>
                        </div>
                    </td>
                </tr>
             
            </table>



            </div>
            </div>
            </td>
            </tr>
            </table>



            <div style="float: right;display:none;">
                <asp:Button Text="סגור" ID="btnCloseFile" Width="80px" runat="server" Style="pointer-events: none; opacity: 0.4;" OnClick="btnCloseFile_Click" />

            </div>

      <%--       <input type="button" value="יציאה" onclick="exit()" runat="server" style="float: right; width: 80px" />

--%>
     <%--      <div style="float: left; height: 600px; width: 214px; margin-top: 10px; margin-right: 11px; position: relative; border-style: solid">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Repeater runat="server" ID="rptBoxesBuffer">
                            <ItemTemplate>
                                <table class="tblFiles" cellpadding="10" cellspacing="0" style="text-align: center; width: 100%;">
                                    <tr>
                                        <td style="padding-top: 3px; padding-bottom: 3px">
                                            <label id="lbInputDataFileID" class="lbSelectBoxBuffer" name='<%#DataBinder.Eval(Container.DataItem, "BoxID") %>' onclick="boxBufferClick(this)" runat="server">
                                                <%#DataBinder.Eval(Container.DataItem, "CustomerName")+"-"+ DataBinder.Eval(Container.DataItem, "BoxID") %>
                                            </label>
                                            <br />

                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>--%>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
