﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MaagareyArchive
{

    public partial class OrderFromSupplier : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        public int rownum = 1;
        private string orderId
        {
            get
            {
                return Session["OrderFromSupplier_orderId"] != null ? Session["OrderFromSupplier_orderId"].ToString() : "";
            }
            set
            {
                Session["OrderFromSupplier_orderId"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                orderId = "";
                fillDdl();
            }
        }

        private void fillDdl()
        {

            OrderedDictionary data;
            data = SuppliersController.getSuppliersList();
            data.Insert(0, 0, Consts.SELECT_SUPPLIER);
            ddlSuppliers.DataSource = data;
            ddlSuppliers.DataTextField = "Value";
            ddlSuppliers.DataValueField = "Key";
            ddlSuppliers.DataBind();
        }



        protected void ddlSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            orderId = "";
            OrderedDictionary data;
            int? supplierId = Helper.getInt(ddlSuppliers.SelectedValue);
            if (supplierId != null)
            {
                data = ItemsController.getAllItemsBySupplierID((int)supplierId);
                data.Insert(0, 0, Consts.SELECT_ITEM);
                ddlItems.DataSource = data;
                ddlItems.DataTextField = "Value";
                ddlItems.DataValueField = "Key";
                ddlItems.DataBind();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int? supplierId = Helper.getInt(ddlSuppliers.SelectedValue);
            int? itemId = Helper.getInt(ddlItems.SelectedValue);
            int? amount = Helper.getInt(txtAmount.Value);
            string remark = txtRemark.Value;
            if (string.IsNullOrEmpty(orderId))
                orderId = Helper.getSupplierOrderID();
            if (supplierId != null && supplierId > 0 && itemId != null && itemId > 0 && amount != null && amount > 0)
            {
                SuppliersOrder so = new SuppliersOrder()
                {
                    Amount = (int)amount,
                    AmountProvided = 0,
                    Date = DateTime.Now,
                    ItemID = (int)itemId,
                    Remark = remark,
                    SupplierID = (int)supplierId,
                    SupplierPrice = 0,
                    SuppliersOrderID = orderId,
                    numInOrder = dgResults.Rows != null ? dgResults.Rows.Count + 1 : 1
                };
                bool isSucceed = SuppliersOrderController.insert(so);
                if (isSucceed)
                {
                    List<SuppliersOrderUI> list = SuppliersOrderController.getByOrderID(orderId);
                    dgResults.DataSource = list;
                    dgResults.DataBind();
                    lblSupplier.InnerText = ddlSuppliers.SelectedItem.Text;
                    lblOrderNum.InnerText = orderId;
                    tblResults.Style["display"] = "";
                }

            }

        }

        protected void btnSaveToFile_Click(object sender, EventArgs e)
        {
            try
            {
                List<SuppliersOrderUI> list = SuppliersOrderController.getByOrderID(lblOrderNum.InnerText);
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add(ExcelHeaders.ResourceManager.GetString("Supplier"), lblSupplier.InnerText);
                param.Add(ExcelHeaders.ResourceManager.GetString("OrderNum"), lblOrderNum.InnerText);

                exportToExcell(list, "export_supplier_order", param);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                int? supplierId = Helper.getInt(ddlSuppliers.SelectedValue);
                if (supplierId == null || supplierId <= 0)
                {
                    showHideSucceedMessage(false, Consts.MISSING_SUPPLIER_NAME);
                    return;
                }
                string to = SuppliersController.getSupplierById((int)supplierId).Email;
                if (string.IsNullOrEmpty(to.Trim()))
                {
                    showHideSucceedMessage(false, Consts.CONST_MISSING_MAIL_ADDRESS);
                    return;
                }
                string from = ParametersController.GetParameterValue(Consts.Parameters.PARAM_EMAIL_MESSAGE_FROM_ADDRESS);
                string subject = Consts.SUPPLIER_MAIL_SUBJECT.Replace("XXX", orderId);
                string gridHtml = string.Empty;
                string body = gridToHTMLReport(out gridHtml);
                EventController.insert(new Events()
                {
                    CustomerID = user.CustomerID,
                    Date = DateTime.Now,
                    DepartmentID = 0,// user.DepartmentID,
                    EmailSubject = subject,
                    EmailBody = gridHtml,
                    EventType = (int)Consts.enEventTypes.orderFromSupplier,
                    UserID = user.UserID,
                    UserName = user.FullName,
                    EmailTo = to,
                    SendTo=Consts.OFFICE_VALUE,
                });
                if (Helper.sendEmail(from,
                                     to,
                                     subject,
                                     body,
                                     ConfigurationManager.AppSettings["SmtpServerUserName"],
                                     ConfigurationManager.AppSettings["SmtpServerPassword"],
                                     "",
                                    Server.MapPath("~/resources/images/Logo.png")))


                    showHideSucceedMessage(true, Consts.CONST_SUCCEED_MESSAGE);




            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }


        }
        private void showHideSucceedMessage(bool toShow, string text = "")
        {
            if (toShow)
            {
                dvErrorMessage.InnerText = text;
                dvErrorMessage.Style["display"] = "block";
                dvErrorMessage.Style["visibility"] = "visible";
                dvErrorMessage.Style["background-color"] = "rgba(182, 255, 0, 0.27)";
            }
            else
            {
                dvErrorMessage.Style["display"] = "none";
                dvErrorMessage.Style["visibility"] = "hidden";
                dvErrorMessage.Style["background-color"] = "rgba(255, 255, 255, 0.16)";
            }

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            StringWriter swLogo = new StringWriter();
            HtmlTextWriter hwLogo = new HtmlTextWriter(swLogo);
            imgLogo.RenderControl(hwLogo);
            string logoHTML = swLogo.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            string gridHtml = string.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            string style = "<style type = 'text/css'>thead {display:table-header-group;} tfoot{display:table-footer-group;}</style>";
            sb.Append(style + gridToHTMLReport(out gridHtml).Replace("logo", logoHTML));
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();");
            sb.Append("};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint",sb.ToString());
            //List<SuppliersOrderUI> list = SuppliersOrderController.getByOrderID(orderId);
            //dgResults.DataSource = list;
            //dgResults.DataBind();

        }


        private string gridToHTMLReport(out string gridHTML)
        {

            dgResults.UseAccessibleHeader = true;
            if (dgResults.HeaderRow != null)
                dgResults.HeaderRow.TableSection = TableRowSection.TableHeader;
            if (dgResults.FooterRow != null)
                dgResults.FooterRow.TableSection = TableRowSection.TableFooter;
            dgResults.Attributes["style"] = "border-collapse:separate";
            foreach (GridViewRow row in dgResults.Rows)
            {
                if (row.RowIndex % 10 == 0 && row.RowIndex != 0)
                {
                    row.Attributes["style"] = "page-break-after:always;";
                }
            }
            if (dgResults.Rows.Count > 23)
                dvFooterButtom.Style["position"] = "inherit";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            dgResults.RenderControl(hw);
            gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");

            StringWriter swHeader = new StringWriter();
            HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
            dvHeader.RenderControl(hwHeader);
            SuppliersOrderUI order = SuppliersOrderController.getOrder(orderId);
            string headerHTML = swHeader.ToString().Replace("\"", "'")
                .Replace("spnOrderNum", order.SuppliersOrderID)
                .Replace("spnOrderDate", order.Date.ToString())
                .Replace("spnSupplier", order.SupplierName)
                .Replace("spnPhone", order.Telephon1)
                .Replace("spnFax", order.Fax)
                .Replace(System.Environment.NewLine, "");

            StringWriter swFooter = new StringWriter();
            HtmlTextWriter hwFooter = new HtmlTextWriter(swFooter);
            dvFooter.RenderControl(hwFooter);
            string footerHTML = swFooter.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            return "<div style='height:100%;width:840px;border-style:solid'>" + headerHTML + "<div style='direction: rtl;padding-right:30px' >" + gridHTML + "</div>" + footerHTML + "</div";
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /*Verifies that the control is rendered */
        }
    }



}