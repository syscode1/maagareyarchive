﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class OrderDetailesPopup : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            string orderID = Request.QueryString["orderID"];
            if (!string.IsNullOrEmpty(orderID))
            {
                header.InnerText = " פרטי הזמנה " + orderID;
            }
            if (!Page.IsPostBack)
            {
                dgResults.DataSource = OrdersController.getOrderByID(orderID);
                dgResults.AutoGenerateColumns = true; 
                dgResults.DataBind();
            }
        }

        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Header)
                {
                    TableCell cell = e.Item.Cells[0];
                    cell.Width = 95;
                    foreach (TableCell item in e.Item.Cells)
                    {
                        string header = ExcelHeaders.ResourceManager.GetString(item.Text);
                        item.Text = string.IsNullOrEmpty(header) ? item.Text : header;
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }
    }
}