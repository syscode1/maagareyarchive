﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="LoadTextFiles.aspx.cs" Inherits="MaagareyArchive.LoadTextFiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function lnkbtnChangePath_Click() {
            document.getElementById('dvChangePath').style.display = "";
            return false;
        }
    </script>
    <h1>
        טעינת קבצים ממסופון
    </h1>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>
    <table style="width:100%">
        <tr>
            <td>
                <asp:Button Text="טען קבצים" style="float: none !important;" runat="server" ID="btnLoad" OnClick="btnLoad_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <span><b>מתקיה:</b></span>
                <label id="lblPath" runat="server"> </label>
                &nbsp
                <asp:LinkButton ID="lnkbtnChangePath"  OnClientClick="return lnkbtnChangePath_Click()" runat="server">שנה </asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <div id="dvChangePath" style="display:none">
                    <asp:Label Text="הכנס נתיב לתקיה" runat="server" /> 
                    <input type="text" id="txtPath" runat="server" name="name" value=" " style="width:300px" />
                    <asp:Button Text="שמור" ID="btnSavePath" OnClick="btnSavePath_Click" runat="server" style="float:none !important" />
                </div>
            </td>
        </tr>
    </table>
            </ContentTemplate>
    </asp:UpdatePanel>

            <asp:UpdateProgress ID="UpdateProgress1"
        AssociatedUpdatePanelID="up"
        runat="server">
            <ProgressTemplate>
                 <div class="divWaiting">            
	<asp:Label ID="lblWait2" runat="server" 

	Text=" Please wait... " style="font-size: 16px; font-weight: bolder;vertical-align:bottom" />
	<asp:Image ID="imgWait2" runat="server" 

	ImageAlign="Middle" style="vertical-align:sub" ImageUrl="resources/images/16.gif" />           
       
               
            </ProgressTemplate>
        </asp:UpdateProgress>
</asp:Content>
