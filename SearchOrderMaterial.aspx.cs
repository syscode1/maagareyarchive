﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class SearchOrderMaterial : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        

        HtmlInputGenericControl[] TextBoxList;
        public DateTime date;
        
        private List<AllFields> allFields
        {
            get
            {
                if (Session["InputData_allFields"] == null)
                    Session["InputData_allFields"] = FileFieldsController.geAllFields();
                return Session["InputData_allFields"] as List<AllFields>;
            }
            set
            {
                Session["InputData_allFields"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if(user.ArchiveWorker==true)
                {
                    fillCustomerDdl();
                    trCustomer.Style["display"] = "";
                }
                else
                {
                    OrderedDictionary department = DepartmentController.GetUserDepartmentsDictionary(user);
                    fillDdlOrderDict(department, ddlDepartment);
                    ddlDepartment_SelectedIndexChanged(null, null);
                }
                fillDdlSubject();
            }
        }

        #region generic fields

        protected void Page_Init(object sender, EventArgs e)
        {
            dvFileFields.Controls.Clear();
            Files file = new Files();
            TextBoxList = new HtmlInputGenericControl[allFields.Count()];
            int index = 0;
            foreach (AllFields field in allFields)
            {
                TextBoxList[index] = new HtmlInputGenericControl();
                TextBoxList[index].ID = field.ID.ToString();
                TextBoxList[index].Style["display"] = "none";
                TextBoxList[index].Style["width"] = "180px";
                TextBoxList[index].Attributes.Add("fieldName", field.Name);
                dvFileFields.Controls.Add(TextBoxList[index++]);
            }

        }

        private void drowFields(int departmentID, Files file = null)
        {
            var fields = FileFieldsController.getFileFieldsByDepartment(departmentID);
            if (fields.Count > 0)
            {
                HtmlTable table = new HtmlTable();

                table.CellPadding = 10;
                HtmlGenericControl gc = new HtmlGenericControl();
                table.Width = "100%";
                table.Height = "100%";
                table.ID = "tblFields";
                HtmlTableRow tr = new HtmlTableRow();
                int index = 0;
                HtmlInputGenericControl tx;


                foreach (var field in fields)
                {
                    index++;
                    HtmlTableCell td = new HtmlTableCell();
                    tx = TextBoxList.Where(t => t.ID == field.FieldID.ToString()).FirstOrDefault();
                    tx.Style["display"] = "";
                    Consts.enFieldType type = (Consts.enFieldType)Enum.Parse(typeof(Consts.enFieldType), field.Type.ToString());
                    addValidationToField(tx, type, field.MaxChars);
                    if (file != null)
                    {
                        fillFieldData(field.Name, file, tx);
                    }
                    else
                    {
                        tx.Value = string.Empty;
                    }
                    tx.Attributes.Add("isRequiered", field.IsRequiered.ToString());
                    if (field.Type==((int)Consts.enFieldType.date))
                    {
                        tx.Attributes["type"] = "date";
                    }
                    HtmlGenericControl lb = new HtmlGenericControl();
                    lb.InnerText = field.Title;
                    lb.InnerHtml += "<br>";
                    if (index == 4)
                    {
                        index = 0;
                        table.Rows.Add(tr);
                        tr = new HtmlTableRow();
                    }
                    td.Controls.Add(lb);
                    td.Controls.Add(tx);
                    // td.Controls.Add(tryTextBox);
                    tr.Cells.Add(td);
                }
                if (index != 4)
                    table.Rows.Add(tr);
                dvFileFields.Controls.Clear();
                if (dvFileFields.FindControl(table.ClientID) != null)
                    dvFileFields.Controls.Remove(table);
                dvFileFields.DataBind();
                dvFileFields.Controls.Add(table);
            }
        }
        private void addValidationToField(HtmlInputGenericControl tx, Consts.enFieldType type, int? maxChars)
        {
            if (type == Consts.enFieldType.number)
            {
                tx.Attributes["type"] = "text";
                tx.Attributes["onkeypress"] = "return isNumberKey(event," + maxChars + ");";
            }
            else
            {
                tx.Attributes["type"] = Enum.GetName(typeof(Consts.enFieldType), type);

            }
            if (type == Consts.enFieldType.date)
                tx.Attributes["max"] = "9999-12-31";
            else if (maxChars != null)
            {
                tx.Attributes["maxlength"] = maxChars.Value.ToString();
            }

        }
        private void fillFieldData(string name, Files file, HtmlInputGenericControl tx)
        {
            //tx = new TextBox();
            try
            {

                var value = (file.GetType().GetProperty(name.Trim()).GetValue(file));
                if ((file.GetType().GetProperty(name.Trim()).PropertyType == typeof(DateTime?) ||
                    file.GetType().GetProperty(name.Trim()).PropertyType == typeof(DateTime)) && value != null)
                    tx.Value = Helper.getDate(value.ToString()).Value.ToString("yyyy-MM-dd");
                else
                    tx.Value = value != null ? value.ToString() : "";
            }
            catch (Exception)
            {
            }
        }

        private void enableFileFields(bool enable, HtmlControl control)
        {
            if (enable)
            {
                control.Style.Clear();
            }
            else
            {
                control.Style["pointer-events"] = "none";
                control.Style["opacity"] = "0.4";
            }
        }

        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Session["SearchOrderMaterial_selectedDepartment"] = ddlDepartment.SelectedValue;
            Session["SearchOrderMaterial_selectedCustomer"] = ddlCustomerName.SelectedValue;
            try
            {

                Session["SearchOrderMaterial_Params"] = new SearchFileParams()
                {
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    DepartmentName = ddlDepartment.SelectedItem.Text,
                    FileID = txtFileNum.Text.Trim(),
                    BoxID = txtBoxNum.Text,
                    Year = "",// txtYear.Text.Trim(),
                    FromInsertDate = "",//!string.IsNullOrEmpty( txtFromInsertDate.Value)?Helper.getDate(txtFromInsertDate.Value).Value.ToString("yyyy-dd-MM"):"",
                    ToInsertDate = "",//!string.IsNullOrEmpty(txtToInsertDate.Value) ? Helper.getDate(txtToInsertDate.Value).Value.ToString("yyyy-dd-MM") : "",
                    ScanNum = "",// txtScanNum.Text.Trim(),
                    CustomerFileNum = "",// txtCustomerFileNum.Text.Trim(),
                    Description2 = "",//txtDescription2.Text,
                    Description1 = "",//txtFileName.Text,
                    CustomerBoxNum = Helper.getInt( txtCustomerBoxNum.Text.Trim()),
                    BoxDescription = txtBoxDescription.Text,
                    SenderUserID = "",//txtSenderNum.Text.Trim(),
                    CustomerID = user.ArchiveWorker == true ? Helper.getInt(ddlCustomerID.SelectedValue).Value : user.CustomerID,
                    CustomerName = user.ArchiveWorker == true ? ddlCustomerName.SelectedItem.Text :CustomersController.getCustomerByID( user.CustomerID).CustomerName,
                    SubjectID =getPermittedSubjectID(),
                    AdditionalDepartmentFields = buildDepartmentFields(),
                };
                

            Response.Redirect("SearchFilesResults.aspx",false);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        private int[] getPermittedSubjectID()
        {
            int[] subjects;
            if (!string.IsNullOrEmpty(ddlSubject.SelectedValue) && ddlSubject.SelectedValue != "0")
                subjects = new int[] { Helper.getIntNotNull(ddlSubject.SelectedValue) };
            else
            {
                if (user.ArchiveWorker == true)
                    subjects = new int[] { 0 };
                else
                {
                    OrderedDictionary subjectDict;
                    subjectDict = SubjectsController.getAllPermitedSubjectsByCustomerID_OrderDict(user.CustomerID, user.UserID,user.SubjectID);
                    subjects = new int[subjectDict.Count];
                    int i = 0;
                    foreach (DictionaryEntry item in subjectDict)
                    {
                        subjects[i++] = Helper.getIntNotNull(item.Key.ToString());
                    }
                }
            }
            return subjects;
        }

        private string buildDepartmentFields()
        {
            Files file = new Files();
            StringBuilder sb = new StringBuilder("");
            int? departmentID = Helper.getInt(ddlDepartment.SelectedValue);
            var fields = FileFieldsController.getFileFieldsByDepartment(departmentID.Value);
            for (int i = 0; i < dvFileFields.Controls.Count; i++)
            {
                if (dvFileFields.Controls[i].GetType() == typeof(HtmlInputGenericControl))
                {
                    HtmlInputGenericControl tx = dvFileFields.Controls[i] as HtmlInputGenericControl;
                    if (!string.IsNullOrEmpty(tx.Value))
                    {
                        if (fields.Where(f => f.FieldID.ToString() == tx.ID).Count() > 0)
                        {
                            PropertyInfo po = file.GetType().GetProperty(tx.Attributes["fieldName"].Trim());
                            try
                            {

                                if (po.Name.ToLower().StartsWith("from") || po.Name.ToLower().StartsWith("start") ||
                                    po.Name.ToLower().EndsWith("from") || po.Name.ToLower().EndsWith("start"))
                                {
                                    string nextValue = (dvFileFields.Controls[i + 1] as HtmlInputGenericControl).Value;
                                    nextValue = string.IsNullOrEmpty(nextValue) ? tx.Value : nextValue;
                                    string po2Name = po.Name.Replace("from", "to").Replace("From", "To").Replace("start", "end").Replace("Start", "End");
                                    if (po.PropertyType == typeof(DateTime) || po.PropertyType == typeof(Nullable<DateTime>))
                                    {
                                        sb.Append(string.Format(" AND  (convert(datetime, f.[{0}], 103)  <= '{1}' and  convert(datetime, f.[{2}], 103) >= '{3}')", po.Name, tx.Value, po2Name, nextValue));

                                    }
                                    else if ((po.PropertyType == typeof(Int32) || po.PropertyType == typeof(Int16)) ||
           po.PropertyType == typeof(Nullable<Int32>) || po.PropertyType == typeof(Nullable<Int16>))

                                    {
                                        sb.Append(string.Format(" AND   (f.[{0}]  <= {1} and f.[{2}] >= {3})", po.Name, tx.Value, po2Name, nextValue));
                                    }
                                    else
                                    {
                                        sb.Append(string.Format(" AND   (f.[{0}]  <= '{1}' and f.[{2}] >=  '{3}')", po.Name, tx.Value, po2Name, nextValue));

                                    }

                                }
                                else if (po.Name.ToLower().StartsWith("to") || po.Name.ToLower().StartsWith("end") ||
                                    po.Name.ToLower().EndsWith("to") || po.Name.ToLower().EndsWith("end"))
                                {
                                    sb.Append("");
                                }
                                else if (po.Name == "Description1")
                                    sb.Append(string.Format(" AND f.[{0}] LIKE '%{1}%' ", po.Name, tx.Value));
                                else {

                                    sb.Append(string.Format(" AND f.[{0}] = '{1}' ", po.Name, tx.Value));
                                }
                            }

                            catch (Exception ex)
                            {
                            }
                        }
                    }
                }
            }
            return sb.ToString();
        }

        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlCustomerID.SelectedValue = ddlCustomerName.SelectedValue;
            fillDepartmentByCustomer(Helper.getInt(ddlCustomerName.SelectedValue).Value);
            fillDdlSubject();
        }

        protected void ddlCustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
          ddlCustomerName.SelectedValue=ddlCustomerID.SelectedValue;
          fillDepartmentByCustomer(Helper.getInt(ddlCustomerID.SelectedValue).Value);
            fillDdlSubject();

        }
        private void fillDdlSubject()
        { OrderedDictionary data;
            if (user.ArchiveWorker == true)
                data = SubjectsController.getAllSubjectsByCustomerID_OrderDict(Convert.ToInt32(ddlCustomerID.SelectedItem.Value));
            else
                data = SubjectsController.getAllPermitedSubjectsByCustomerID_OrderDict(user.CustomerID,user.UserID,user.SubjectID);
            data.Insert(0, 0, Consts.ALL_SUBJECTS);
            fillDdlOrderDict(data, ddlSubject);
        }

        private void fillDepartmentByCustomer(int customerID)
        {
            OrderedDictionary department = DepartmentController.GetCustomerDepartmentsDictionary(customerID);
            department.Insert(0, 0, Consts.ALL_DEPARTMENTS);
            fillDdlOrderDict(department, ddlDepartment);
            if (Session["SearchOrderMaterial_selectedDepartment"] != null && ddlDepartment.Items.FindByValue(Session["SearchOrderMaterial_selectedDepartment"].ToString()) !=null)
            {
                ddlDepartment.SelectedValue = Session["SearchOrderMaterial_selectedDepartment"].ToString();
                ddlDepartment_SelectedIndexChanged(null, null);
            }
            else if (ddlDepartment.Items.FindByValue("1") != null)
            {
                ddlDepartment.SelectedValue = "1";
                ddlDepartment_SelectedIndexChanged(null, null);
            }
        }
        private void fillCustomerDdl()
        {
            lblSearchExist.Style["display"] = "none";

            ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
            ddlCustomerID.DataSource = (new MyCache()).CustomersIds;


            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();

            ddlCustomerID.DataTextField = "Value";
            ddlCustomerID.DataValueField = "Key";
            ddlCustomerID.DataBind();


            if (ddlCustomerID.Items.FindByValue("0") != null)
                ddlCustomerID.Items.Remove(ddlCustomerID.Items.FindByValue("0"));
            if (ddlCustomerName.Items.FindByValue("0") != null)
                ddlCustomerName.Items.Remove(ddlCustomerName.Items.FindByValue("0"));

            List<OrderBufferUI> ob = OrderBufferController.GetOrderBuffer(user.UserID);

            if (ob.Count > 0)
            {
                ddlCustomerID.SelectedValue = ob[0].CustomerID.ToString();
                ddlCustomerID.Enabled = false;
                ddlCustomerName.SelectedValue = ob[0].CustomerID.ToString();
                ddlCustomerName.Enabled = false;
                lblSearchExist.Text = Consts.EXIST_ORDER.Replace("XXX", ob[0].CustomerID.ToString());
                lblSearchExist.Style["display"] = "";
            }
            else
            {
                if (Session["SearchOrderMaterial_selectedCustomer"] != null)
                {
                    ddlCustomerID.SelectedValue = Session["SearchOrderMaterial_selectedCustomer"].ToString();
                    ddlCustomerName.SelectedValue = Session["SearchOrderMaterial_selectedCustomer"].ToString();
                }

            }
            int? selectedCustomer = Helper.getInt(ddlCustomerName.SelectedValue);
            if (selectedCustomer != null && selectedCustomer > 0)
                fillDepartmentByCustomer((int)selectedCustomer);

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(ddlDepartment.SelectedValue))
            drowFields(Helper.getInt(ddlDepartment.SelectedValue).Value);
            
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewOrder.aspx");
        }
    }
}