﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="MainPageArchive.aspx.cs" Inherits="MaagareyArchive.MainPageArchive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function popup(href) {
            if (!window.focus)
                return true;
            var href;
            var w = 1200;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            var custID = document.getElementById('<%=ddlCustomerID.ClientID%>').value;
            window.open(href + "?custID=" + custID, "CustomerMail", 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }
        function newCustomer() {
            window.location="CustomerAgreement.aspx"
        }
    </script>
    <h1 style="margin-bottom:0px">מסך לקוח</h1>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <div id="dvCustomerName" runat="server" style="float: right; margin-top: 3px;width:220px margin-right: 20px;">

                <label>בחר לקוח</label>
                <br />
                               <asp:ImageButton ID="btnAddRow" Style="vertical-align: bottom" OnClientClick="newCustomer()" ImageUrl="resources/images/add.png" Height="31px" runat="server" />

                <asp:DropDownList ID="ddlCustomerName" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged" runat="server"></asp:DropDownList>
            </div>
            <div id="dvCustomerID" style="float: right; margin-top: 3px; margin-right: 20px;" runat="server">
                <label>מספר לקוח</label>
                <asp:DropDownList ID="ddlCustomerID" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerID_SelectedIndexChanged" runat="server"></asp:DropDownList>
            </div>
            <div>
                <asp:Button Text="עדכן נתוני לקוח" ID="btnUpdateCust" OnClick="btnUpdateCust_Click" runat="server" />
            </div>


           

            <table style="line-height: 20px; width: 100%;">
                <tr>
                    <td colspan="4" >
                       
                         <div class="h1_sub"> <span>כתובת לקוח</span></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="עיר" />
                        <br />
                        <asp:DropDownList ID="ddlCities" runat="server" Enabled="false" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="כתובת" />
                        <br />
                        <input id="txtAddress" runat="server" onkeypress="return maxCharLength(event,30)" disabled="disabled" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="תיאור" />
                        <br />
                        <input id="txtDescriptionAddress" onkeypress="return maxCharLength(event,30)" runat="server" disabled="disabled" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="איש קשר" />
                        <br />
                        <input id="txtContactName" runat="server" onkeypress="return maxCharLength(event,20)" disabled="disabled" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="טלפון 1" />
                        <br />
                        <input id="txtPhone1" runat="server" disabled="disabled" onkeypress="return isNumberKey(event,10)" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="טלפון 2" />
                        <br />
                        <input id="txtPhone2" runat="server" disabled="disabled" onkeypress="return isNumberKey(event,10)" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="פקס" />
                        <br />
                        <input id="txtFax" runat="server" disabled="disabled" onkeypress="return isNumberKey(event,10)" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="מייל" />
                        <br />
                        <input id="txtMail" disabled="disabled" runat="server" />
                    </td>
                </tr>
<%--                <tr>
                    <td>
                        <asp:Label runat="server" Text="תאריך חתימת ההסכם" />
                        <br />
                        <input id="txtDateOfContract" disabled="disabled" runat="server" />
                    </td>
                    <td colspan="2">
                        <asp:Label runat="server" Text="PO" />
                        <br />
                        <input id="txtPO" disabled="disabled" runat="server" onkeypress="return maxCharLength(event,25)" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label runat="server" Text="הערות" />
                        <br />
                        <asp:TextBox ID="txtDescription" Enabled="false" runat="server" onkeypress="return maxCharLength(event,256)" />

                    </td>
                </tr>--%>

            </table>
            <div class="h1_sub">נתונים כלליים</div>
            <table style="line-height: 20px; width: 300px; ">
                <tr>
                    <td>
                        <asp:Label runat="server" Text="כמות תיבות" />
                        <br />
                        <input id="txtSumBoxes" runat="server" disabled="disabled" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="מלאי ריקות" />
                        <br />
                        <input id="txtSumEmpty" runat="server" disabled="disabled" />
                    </td>
                    </tr>
                </table>
                     <div class="h1_sub">חשבונאות</div>
             <table  style="line-height: 20px; width: 100%;">
                <tr>
                      <td>
                        <asp:Label runat="server" Text="תאריך חשבונית אחרונה" />
                        <br />
                        <input id="txtLastInvoice" runat="server" disabled="disabled" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="תאריך חיוב הבא" />
                        <br />
                        <input id="txtNextBill" runat="server" disabled="disabled" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="תאריך קבלה אחרונה" />
                        <br />
                        <input id="txtLastRecept" runat="server" disabled="disabled" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="יתרה בשח" />
                        <br />
                        <input id="txtBalance" runat="server" disabled="disabled" />
                    </td>
                    <td style="padding-top:20px">
                        <asp:Button Text="מחירון" ID="btnPrices" OnClick="btnPrices_Click" runat="server" />
                    </td>

                
                </tr>
            </table>
                           <div class="h1_sub">פעולות</div>
            <table>
                <tr>
                    <td>
                        <asp:Button Text="ניהול אנשי קשר" style="width: auto;" ID="btnUsersManager" OnClick="btnManegeUsers_Click" runat="server" />
                    </td>
                    <td>
                        <asp:Button Text="הרשאות" style="width: auto;" ID="btnManegeUsers" OnClick="btnManegeUsers_Click" runat="server" />

                    </td>
<%--                    <td>
                        <asp:Button Text="תקשורת לקוחות" ID="btnCustomerMail" OnClientClick="return popup('CustomerMail.aspx');" runat="server" />

                    </td>--%>
                    <td>
                        <asp:Button Text="הזמנות" style="width: auto;" ID="btnOrders" OnClick="btnOrders_Click" runat="server" />

                    </td>
                                       <td>
                        <asp:Button Text="גריסות" style="width: auto;" ID="btnExterminate" OnClick="btnExterminate_Click" runat="server" />

                    </td>

               
<%--                    <td>
                        <asp:Button Text="שליפות שלא נמצאו" ID="btnExtractionsNotFound" OnClientClick="return popup('ExtractionsNotFound.aspx');" runat="server" />

                    </td>--%>
 <%--                    <td>
                        <asp:Button Text="הסטורית ביעור" ID="btnExterminateReport" OnClick="btnExterminateReport_Click" runat="server" />

                    </td>--%>
     <td>
                        <asp:Button Text="סידור עבודה" style="width: auto;" OnClick="btnWorkOrdering_Click" ID="btnWorkOrdering" runat="server" />
                    </td>
                     
                    <td>
                        <asp:Button Text="אישורי מסירה" style="width: auto;" OnClick="btnDeliveryConfirmation_Click" ID="btnDeliveryConfirmation" runat="server" />
                    </td>
                    <td>
                        <asp:Button Text="מדבקות" style="width: auto;" ID="btnLables" runat="server" />

                    </td>

                                            <td>
                            <asp:Button Text="מכירת ארגזים ללקוח" style="width: auto;" ID="btnCustomerBoxesSale"  OnClick="btnCustomerBoxesSale_Click" class="button1" runat="server"  />

                        </td>
 <td>
                            <asp:Button Text="נושאים ללקוח" style="width: auto;" ID="btnCustomerSubject"  OnClick="btnCustomerSubject_Click" class="button1" runat="server"  />

                        </td>
                      <td>
                                                        <asp:Button Text="מחלקות ללקוח" style="width: auto;" ID="btnCustomerDepartment"  OnClick="btnCustomerDepartment_Click" class="button1" runat="server"  />

</td>
                </tr>
            </table>

            <div class="h1_sub"></div>

            <table style="line-height: 20px; width: 100%; ">
                <tr>

                 

                <asp:GridView ID="dgResultsR" runat="server" CssClass="grid"
                    OnRowDataBound="dgResultsR_RowDataBound"
                    EnableViewState="true" Width="1038px"
                    AutoGenerateColumns="false"
                    SelectedRowStyle-BackColor="Red"
                    EnablePersistedSelection="true"
                    DataKeyNames="index"
                    style="margin-top:0px">
                    <HeaderStyle CssClass="header" />

                    <AlternatingRowStyle CssClass="alt" />
                    <RowStyle CssClass="row" />
                    <Columns>
                        <asp:TemplateField HeaderText="דחיפות" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span>
                                    <asp:CheckBox Enabled="false" Checked='<%# Eval("Urgent")==null? false :(bool)Eval("Urgent")%>' runat="server" />
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="תאריך הזמנה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval("Date"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="מחלקה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval("DepartmentID"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="עיר" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval("CityAddress"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="כתובת" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "FullAddress"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="הערות" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span id="spnComment" runat="server">

                                    <%#(Eval( "Comment"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ תיקים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumFiles"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumBoxes"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים ריקים" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumNewBoxOrder"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים לפינוי" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumFullBoxes"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים להחזרה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumRetriveBoxes"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="סה''כ ארגזים לגריסה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(Eval( "SumExterminateBoxes"))%> </span>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </table>

  

        </ContentTemplate>
    </asp:UpdatePanel>
          <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>