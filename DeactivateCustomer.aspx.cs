﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class DeactivateCustomer : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillAllDdl();
            }
        }

        private void fillAllDdl()
        {
            ddlCustomers.DataSource = (new MyCache()).CustomersNames;
            ddlCustomers.DataTextField = "Value";
            ddlCustomers.DataValueField = "Key";
            ddlCustomers.DataBind();
        }

        protected void btnDeactivate_Click(object sender, EventArgs e)
        {
            bool isSucceed = false;
            int ? customerID = Helper.getInt(ddlCustomers.SelectedValue);
            if (customerID != null && customerID > 0)
            {
                isSucceed = CustomersController.deactivate(customerID.Value,user.UserID);
                if(isSucceed)
                {
                    (new MyCache()).ClearCustomersIdsList();
                    (new MyCache()).ClearCustomersNamesList();
                }
            }
            showIndicationMessage(isSucceed, isSucceed ? Consts.SAVED_SUCCEED : Consts.SAVED_FAIL);


        }
        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }
    }
}