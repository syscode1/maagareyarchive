﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="ExtractionsNotFound.aspx.cs" Inherits="MaagareyArchive.ExtractionsNotFound" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                   <asp:DataGrid ID="dgResults" runat="server"
                    AutoGenerateColumns="false" CssClass="grid"
                    EnableViewState="true" AllowSorting="true"
                     Width="200px" style="margin-top:10px">
                    <%--DataKeyField="FileID"--%>

                    <HeaderStyle CssClass="header" />

                    <AlternatingItemStyle CssClass="alt" />
                    <ItemStyle CssClass="row" />
                    <Columns>                       
                                                <asp:TemplateColumn HeaderText="מחלקה" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#DataBinder.Eval(Container.DataItem, "DepartmentID")%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="תיק" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#(DataBinder.Eval(Container.DataItem, "FieldID"))%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="תאריך" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#DataBinder.Eval(Container.DataItem, "StatusDate")%></span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="משתמש" ItemStyle-CssClass="ltr">
                            <HeaderStyle CssClass="t-center" />
                            <ItemStyle CssClass="t-natural va-middle" />
                            <ItemTemplate>
                                <span><%#DataBinder.Eval(Container.DataItem, "LoginID")%></span>
                            </ItemTemplate>
 </asp:TemplateColumn>
                    </Columns>

                </asp:DataGrid>

</asp:Content>
