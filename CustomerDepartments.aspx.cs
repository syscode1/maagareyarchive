﻿
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class CustomerDepartments : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            dvErrorMessage.Style["display"] =dvSucceedMessage.Style["display"] = "none";
            if (!Page.IsPostBack)
            {
                bindGridData();
                fillDdl();

                string customerID = Request.QueryString["custID"];
                if (!string.IsNullOrEmpty(customerID) && customerID.Trim()!="0")
                {
                    ddlCustomerName.SelectedValue = customerID;
                    btnBackCustomer.Style["display"] = "";
                    ddlCustomerName_SelectedIndexChanged(null, null);
                }
            }
        }

        private void fillDdl()
        {

            ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
            ddlCustomeID.DataSource = (new MyCache()).CustomersIds;

            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();

            ddlCustomeID.DataTextField = "Value";
            ddlCustomeID.DataValueField = "Key";
            ddlCustomeID.DataBind();

            //    fillDdl(PaymentController.getPeriodOfPaymentList(), ddlTemOfPayment);


        }

        private void bindGridData()
        {
            List<DepartmentUI> departments = new List<DepartmentUI>()
            {
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.general,DepartmentName="כללי" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.account,DepartmentName="הנהלת חשבונות" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.claim,DepartmentName="תביעות" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.HR,DepartmentName="כח אדם" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.eng,DepartmentName="הנדסה" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.collect,DepartmentName="גביה" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.law,DepartmentName="עריכת דין" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.science,DepartmentName="מדע" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.customers,DepartmentName="לקוחות" },
            new DepartmentUI() {DepartmentID=(int)Consts.enDepartment.carClaims,DepartmentName="תביעות רכב" },
            };



            dgResults.DataSource = departments;
            dgResults.DataBind();
        }


        protected void ddlCustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerName.SelectedValue = ddlCustomeID.SelectedValue;
            fillControlsData();
        }

        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlCustomeID.SelectedValue = ddlCustomerName.SelectedValue;
            clearControls();
            fillControlsData();
        }

        private void fillControlsData()
        {
            List<Department> departments = DepartmentController.GetCustomerDepartments(Convert.ToInt32(ddlCustomeID.SelectedValue));
            foreach (DataGridItem item in dgResults.Items)
            {

               int departmentID = Helper.getInt((item.FindControl("DepartmentID") as HtmlGenericControl).InnerText).Value;
                Department dep = departments.Where(x => x.DepartmentID == departmentID).FirstOrDefault();
                if(dep!=null)
                {
                   CheckBox cbCheck= item.FindControl("cbCheck") as CheckBox;
                    cbCheck.Checked = true;
                    cbCheck.Enabled = false;
                    DropDownList ddlCities = (item.FindControl("ddlCities") as DropDownList);
                    //int? index = ddlCities.Items.IndexOf(new ListItem(dep.CityAddress.Trim()));
                    //ddlCities.SelectedIndex = index != null && index >= 0 ? (int)index : 0;
                    
                    (item.FindControl("txtCollectionPhone") as HtmlInputControl).Value = dep.CollectionPhone;
                    (item.FindControl("txtCollectionName") as HtmlInputControl).Value = dep.CollectionName;

                    (item.FindControl("txtID") as HtmlInputControl).Value = dep.ID!=null?dep.ID.ToString():"";
                    (item.FindControl("cbSelfPayment") as CheckBox).Checked = dep.SelfPayment==true? true :false;

                }
                else
                { 
                    (item.FindControl("txtCollectionPhone") as HtmlInputControl).Value= string.Empty;
                    (item.FindControl("txtCollectionName") as HtmlInputControl).Value= string.Empty;

                    (item.FindControl("txtID") as HtmlInputControl).Value = string.Empty;
                    (item.FindControl("cbSelfPayment") as CheckBox).Checked = false;
                    (item.FindControl("cbCheck") as CheckBox).Checked = true;
                    (item.FindControl("cbCheck") as CheckBox).Enabled = true;
                }
            }
        }

        private void clearControls()
        {
            //Suppliers supplier = SuppliersController.getSupplierById(Convert.ToInt32(ddlSupplierID.SelectedValue));
            //txtAddress.Value = string.Empty;
            //txtComment.Value = string.Empty;
            //txtContactName.Value = string.Empty;
            //txtDateOfContract.Value = DateTime.Now.ToShortDateString();
            //txtDescription.Value = string.Empty;
            //txtFax.Value = string.Empty;
            //txtMail.Value = string.Empty;
            //txtPhone1.Value = string.Empty;
            //txtPhone2.Value = string.Empty;
            //txtSupplierName.Text = string.Empty;
            //ddlCities.ClearSelection();
            //ddlTemOfPayment.ClearSelection();
            //bindGridData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {


            int? customerID = Helper.getInt(ddlCustomeID.SelectedValue);

               List< Department> department = fillDepartments();

                bool isSucceed = false;



            isSucceed = DepartmentController.insertUpdate(department);
                string message = isSucceed ? Consts.SAVED_SUCCEED : Consts.SAVED_FAIL;
                showIndicationMessage(isSucceed, message);
            //    if (isSucceed && rbSupplierExist.Checked == false)
            //    {
            //        clearControls();
            //        fillDdl();
            //    }

            //}

        }


        private List<Department> fillDepartments()
        {
            List<Department> departments = new List<Department>();
            if(!string.IsNullOrEmpty(ddlCustomeID.SelectedValue))
            {
                int customerID = Helper.getInt(ddlCustomeID.SelectedValue).Value;
            foreach (DataGridItem item in dgResults.Items)
                {
                    CheckBox cbCheck = item.FindControl("cbCheck") as CheckBox;
                    int departmentID = Helper.getInt((item.FindControl("DepartmentID") as HtmlGenericControl).InnerText).Value;
                    if (cbCheck.Checked == true)
                    {
                        Department dep = new Department();
                        dep.CustomerID =customerID;
                        dep.CustomerName = ddlCustomerName.SelectedItem.Text;
                        dep.DepartmentID=Helper.getInt((item.FindControl("DepartmentID") as HtmlGenericControl).InnerText).Value;
                        dep.DepartmentName=(item.FindControl("DepartmentName") as HtmlGenericControl).InnerText;
                        dep.CollectionPhone = (item.FindControl("txtCollectionPhone") as HtmlInputControl).Value;
                        dep.CollectionName = (item.FindControl("txtCollectionName") as HtmlInputControl).Value;

                        dep.ID = Helper.getInt((item.FindControl("txtID") as HtmlInputControl).Value);
                        dep.SelfPayment = (item.FindControl("cbSelfPayment") as CheckBox).Checked;
                        departments.Add(dep);
                    }
                }
            }
            return departments;
        }

        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }

  
    }
}
