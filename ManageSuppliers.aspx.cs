﻿using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ManageSuppliers : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OrderFromSupplier_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderFromSupplier.aspx");
        }

        protected void NewSupplier_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewSupplier.aspx");
                }

        protected void Items_Click(object sender, EventArgs e)
        {
            Response.Redirect("ItemsTable.aspx");
        }

        protected void ManageOrders_Click(object sender, EventArgs e)
        {
            Response.Redirect("SuppliersOrderTracing.aspx");
        }
    }
}