﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class SuppliersOrderTracing : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            dgResults.DataSource = SuppliersOrderController.getNotSuppliedOrders();
            dgResults.DataBind();
            dgResults.UseAccessibleHeader = true;
            dgResults.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
}