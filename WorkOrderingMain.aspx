﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="WorkOrderingMain.aspx.cs" Inherits="MaagareyArchive.WorkOrderingMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>אישורי מסירה ממתינים למסירה</h1>
    <table>
        <tr>
            <td>
                <asp:Button Text="הפק סידור עבודה" ID="btnworkingReport" OnClick="btnworkingReport_Click" runat="server" />

            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Button Text="הפק סידור עבודה היסטורים" ID="btnworkingReportArchiv" runat="server" OnClick="btnworkingReportArchiv_Click" />

            </td>
            <td>
                <table>
                    <tr>
                        <td>
                            <span>מתאריך</span>
                            <input type="date" id="txtFromDate" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>עד תאריך</span>
                            <input type="date" id="txtToDate" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="txtShippingID" maxlength="25" type="text" placeholder="מספר סידור עבודה" name="name" runat="server" />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
