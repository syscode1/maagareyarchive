﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ExterminateRequiering : BasePage
    {
        private const string CONST_SAVE_ERROR_MESSAGE_NO_DATA ="לא נבחרו תיקים להזמנה לגריסה/ תיקים לא תקינים";
        private const string CONST_SAVE_ERROR_MESSAGE_FAIL ="ארעה שגיאה לא צפויה במהלך שמירת הנתונים";
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                OrderedDictionary data;
                if (user.ArchiveWorker == true)
                {

                    trCustomers.Visible = true;
                    fillCustomerDdl();
                    string customerID = Request.QueryString["custID"];
                    if (!string.IsNullOrEmpty(customerID))
                    {
                        ddlCustomer.SelectedValue = customerID;
                        btnBackCustomer.Style["display"] = "";
                        btnShow_Click(null, null);
                    }
                     if (!string.IsNullOrEmpty(ddlCustomer.SelectedItem.Value))
                    {
                        data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomer.SelectedItem.Value));
                        data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
                        fillDdlOrderDict(data, ddlDepartment);
                    }
                }
                else {
                    trCustomers.Visible = false;
                    data = DepartmentController.GetUserDepartmentsDictionary(user);
                    data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
                    fillDdlOrderDict(data, ddlDepartment);

                }
                getOrderBufferToExtermData();
            }
        }
        private void fillCustomerDdl()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
        }
        private void getOrderBufferToExtermData()
        {
          List< OrderBufferToExtermUI> list=  OrdersBufferToExterminateController.getBufferByUser(user.UserID,user.CustomerID);
            if(list.Count>0)
            {
                dgResults.DataSource = list;
                dgResults.DataBind();
                btnSendToExterm.Visible = true;
                dvClearTable.Style["display"] = "";
                showHideErrorMessage(false);
            }
            else
            {
                btnSendToExterm.Visible = false;
                dvClearTable.Style["display"] = "none";
               if(Page.IsPostBack) showHideErrorMessage(true, Consts.NO_DATA_TO_DISPLAY);
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderedDictionary data;
            data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomer.SelectedItem.Value));
            data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
            fillDdlOrderDict(data, ddlDepartment);
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                OrdersBufferToExterminateController.RemoveAll(user.UserID);
                List<Boxes> data = new List<Boxes>();
                if (FileUpload1.HasFile)
                {
                    data = getUploadData();

                }
                else
                {
                    data = getData();
                }
                getOrderBufferToExtermData();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        //private bool addDataToOrdersBuffer(List<Boxes> data)
        //{
        //    bool isSucceed = false;
        //    try
        //    {
        //        List<OrderBufferToExterm> orders = new List<OrderBufferToExterm>();
        //        int i = 1;
        //        string orderID = Helper.getOrderID(user.UserID);
        //        foreach (var item in data)
        //        {
        //            //int? customerID = item.CustomerID;
        //            //int department = item.DepartmentID;
        //            string boxID = item.BoxID;
        //           // if (customerID != null && department != null && !string.IsNullOrEmpty(boxID))
        //                orders.Add(new OrderBufferToExterm()
        //                {
        //                    LoginID = user.UserID,
        //                    DepartmentID = item.DepartmentID,
        //                    CustomerID = item.CustomerID!=null?(int) item.CustomerID:0,
        //                    NumInOrder = i++,
        //                    BoxID = boxID,
        //                    FileID = "",
        //                    OrderDate = DateTime.Now

        //                });
                    
        //        }
        //        if (orders.Count > 0)
        //            {
        //                OrdersBufferToExterminateController.Insert(orders,user.UserID);
        //            }
        //        isSucceed = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));


        //    }
        //    return isSucceed;
        //}

        private List<Boxes> getUploadData()
        {
            string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            string Extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
            string FolderPath = ConfigurationManager.AppSettings["FolderPath"];

            string FilePath = Server.MapPath(FolderPath + FileName);
            string dirPath = System.IO.Path.GetDirectoryName(FilePath);
            if (dirPath != null)
            {
                if (!System.IO.Directory.Exists(dirPath))
                {
                    dg.Utilities.Folders.CreateDirectory(dirPath, false);
                }
            }
            FileUpload1.SaveAs(FilePath);
            DataTable dtData = Helper.ImportExcellFileToGrid(FilePath, Extension, "Yes");
            int index= dtData.Columns.IndexOf("BoxID") > 0 ? dtData.Columns.IndexOf("BoxID") :
                dtData.Columns.IndexOf("boxId") > 0 ? dtData.Columns.IndexOf("boxId") :
                dtData.Columns.IndexOf("Box ID") > 0 ? dtData.Columns.IndexOf("Box ID") :
                dtData.Columns.IndexOf("boxID") > 0 ? dtData.Columns.IndexOf("boxID") : -1;

            if (index >-1)
            {
                List<string> boxesList = dtData.AsEnumerable().Select(x => x[index].ToString()).ToList();
                List<Boxes> boxes = BoxesController.getBoxesByIDs(boxesList);
                sendOrderToBufferExterm(boxes);
                return boxes;
            }
            return null;
        }

        private List<Boxes> getData()
        {
            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
            int? department = string.IsNullOrEmpty(ddlDepartment.SelectedValue) || ddlDepartment.SelectedValue == "0" ? null : (int?)Convert.ToInt16(ddlDepartment.SelectedValue);
            //   DateTime? fromExterminatedDate = Helper.getDate(txtFromExterminateDate.Value);
            //   DateTime? toExterminatedDate = Helper.getDate(txtToExterminateDate.Value);
            DateTime? fromInsertdDate = Helper.getDate(txtFromInsertDate.Value);
            DateTime? toInsertdDate = Helper.getDate(txtToInsertDate.Value);
            int? toYear = Helper.getInt(txtToYear.Text);
            List<Boxes> boxes= BoxesController.exportBoxesToExtrminate(customerID, department, fromInsertdDate, toInsertdDate, toYear);
            sendOrderToBufferExterm(boxes);
            return boxes;
        }

        private void sendOrderToBufferExterm(List<Boxes> boxes)
        {
            List<OrderBufferToExterm> orders = new List<OrderBufferToExterm>();
            int i = 1;
            string orderID = Helper.getOrderID(user.UserID);
            foreach (Boxes item in boxes)
            {
                orders.Add(new OrderBufferToExterm()
                {
                    DepartmentID = item.DepartmentID.Value,
                    CustomerID = item.CustomerID == null ? 0 : (int)item.CustomerID,
                    BoxID = item.BoxID,
                    FileID = "",
                    isSelect=false,
                });
            }
            if (orders.Count > 0)
                OrdersBufferToExterminateController.Insert(orders,user.UserID);

            
        }
        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DataGridItem item = e.Item;
                    int toYear = string.IsNullOrEmpty(txtToYear.Text) ? DateTime.Now.Year : (int)Helper.getInt(txtToYear.Text);
                    Files file =FilesController.getFilesListByBox((item.DataItem as Boxes).BoxID).Where(f => f.ExterminateYear > toYear).FirstOrDefault();
                    if (file != null)
                    {
                        (item.FindControl("cbExterminate") as CheckBox).Enabled = false;
                        item.BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        protected void btnSendToExterm_Click(object sender, EventArgs e)
        {

            try
            {
                
                List<OrdersToExterminate> orders = new List<OrdersToExterminate>();
                List<string> deletedOrders = new List<string>();
                int i = 1;
                string orderID = Helper.getOrderID(user.UserID);
                foreach (DataGridItem item in dgResults.Items)
                {
                    if ((item.FindControl("cbExterminate") as CheckBox).Checked == true && item.Visible == true && item.BackColor != Color.Red)
                    {
                        int? customerID = item.FindControl("customerID") as HtmlGenericControl != null ? Helper.getInt((item.FindControl("customerID") as HtmlGenericControl).InnerText) : null;
                        int? department = item.FindControl("departmentID") as HtmlGenericControl != null ? Helper.getInt((item.FindControl("departmentID") as HtmlGenericControl).InnerText) : null;
                        string boxID = item.FindControl("boxID") as HtmlGenericControl != null ? (item.FindControl("boxID") as HtmlGenericControl).InnerText : "";

                    if (customerID != null && department != null && !string.IsNullOrEmpty(boxID))
                       
                            orders.Add(new OrdersToExterminate()
                            {
                                LoginID = user.UserID,
                                DepartmentID = (int)department,
                                CustomerID = (int)customerID,
                                NumInOrder = i++,
                                BoxID = boxID,
                                FileID = "",
                                OrderDate = DateTime.Now,
                                OrderID=orderID,
                                


                            });
                        }
                }
                OrdersBufferToExterminateController.RemoveAll(user.UserID);
                if (orders.Count > 0)
                {
                    if (OrdersToExterminateController.Insert(orders,user.UserID))
                        Response.Redirect("OrderConfirmation.aspx?orderID=" + orderID);
                    else
                    {
                        showHideErrorMessage(true, CONST_SAVE_ERROR_MESSAGE_FAIL);
                    }
                }
                else
                    showHideErrorMessage(true, CONST_SAVE_ERROR_MESSAGE_NO_DATA);
  
            }

                

            catch (Exception ex)
            {

                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                showHideErrorMessage(true, CONST_SAVE_ERROR_MESSAGE_FAIL);
            }
        }
        

        private void showHideErrorMessage(bool toShow, string text = "")
        {
            if (toShow)
            {
                dvErrorMessage.InnerText = text;
                dvErrorMessage.Style["display"] = "block";
                dvErrorMessage.Style["visibility"] = "visible";
                dvErrorMessage.Style["background-color"] = "rgba(253, 157, 157, 0.16)";
            }
            else
            {
                dvErrorMessage.Style["display"] = "none";
                dvErrorMessage.Style["visibility"] = "hidden";
                dvErrorMessage.Style["background-color"] = "rgba(255, 255, 255, 0.16)";
            }

        }

        protected void btnClearData_ServerClick(object sender, EventArgs e)
        {
            OrdersBufferToExterminateController.RemoveAll(user.UserID);
            dvClearTable.Style["display"] = "none";
            dgResults.DataSource = null;
            dgResults.DataBind();
        }

        [System.Web.Services.WebMethod]
        public static string exterminateSelectChange(string boxID, string userID,string isSelect)
        {
            OrdersBufferToExterminateController.updateIsSelect(Helper.getIntNotNull( userID), boxID, isSelect.ToLower() == "true" ? true : false);
            
            return "";
        }
    }
}