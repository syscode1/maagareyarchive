﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class UsersManager : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.administrator }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillddl();
            }
        }

        private void fillddl()
        {
            fillddlJob();           
            fillddlSubject();

            fillddlCustomers();

            var listCache = new MyCache();
            ddlCities.DataSource = listCache.Cities;
            ddlCities.DataBind();
        }

        private void fillddlCustomers()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();

            string customerID = Request.QueryString["custID"];
            if (!string.IsNullOrEmpty(customerID) && Request.QueryString["userID"]==null && Request.QueryString["isNew"]==null)
            {
                rbCustomerExist.Checked = true;
                ddlCustomer.SelectedValue = customerID;
                ddlCustomerChangeSelection(ddlCustomer.SelectedValue==Consts.PARAM_ARCHIVE_ID);
                btnBackCustomer.Style["display"] = "";
            }
            
            
            else if (!string.IsNullOrEmpty(Request.QueryString["userID"]))
            {
                rbCustomerExist.Checked = true;
                string userID = Request.QueryString["userID"];
                ddlCustomer.SelectedValue = customerID;
                ddlCustomerChangeSelection(ddlCustomer.SelectedValue == Consts.PARAM_ARCHIVE_ID);
                string departmentID=Request.QueryString["depID"];
                fillddlUsers(Helper.getIntNotNull( customerID));
                ddlUsers.SelectedValue= ddlUsers.Items.FindByValue(userID) != null ? userID : null;
                ddlUsers_SelectedIndexChanged(null, null);
            }
            else
            {
                rbNew.Checked=true;
                rbCustomerExist_CheckedChanged(null, null);
                if(!string.IsNullOrEmpty(customerID))
                {
                    ddlCustomer.SelectedValue = customerID;
                    ddlCustomerChangeSelection(ddlCustomer.SelectedValue == Consts.PARAM_ARCHIVE_ID);
                }

            }
        }

        private void ddlCustomerChangeSelection(bool isArchive)
        {
            if (!isArchive)
            {
                dvJob.Visible = false;
                dvAdmin.Visible = false;
                dvMaintence.Visible = false;
                dvCustomerScreen.Visible = false;
                dvCollection.Visible = false;
                dvWorksForDrivers.Visible = false;
                dvRetrive.Visible = false;
                dvOrderItems.Visible = false;
                dvSecondRetrive.Visible = false;
                dvAllActions2.Visible = false;
            }
            else
            {
                dvJob.Visible = true;
                dvAdmin.Visible = true;
                dvMaintence.Visible = true;
                dvCustomerScreen.Visible = true;
                dvCollection.Visible = true;
                dvWorksForDrivers.Visible = true;
                dvRetrive.Visible = true;
                dvOrderItems.Visible = true;
                dvSecondRetrive.Visible = true;
                dvAllActions2.Visible = true;
                if (ddlJob.Items.FindByValue("-1") != null)
                    ddlJob.Items.Remove(ddlJob.Items.FindByValue("-1"));
            }
        }
        
        private void fillddlUsers(int customerID)
        {
            OrderedDictionary data;
            data = UserController.getAllUsers(customerID);
            data.Insert(0, 0, Consts.SELECT_USER);
            fillDdlOrderDict(data, ddlUsers);
        }
        private void fillddlJob()
        {
            OrderedDictionary data;
            data = JobController.getAllJobs();
            //if (ddlCustomer.SelectedValue == Consts.PARAM_ARCHIVE_ID && data.Contains(-1) ==true)
            //{
            //    data.Remove(-1);
            //}
            fillDdlOrderDict(data, ddlJob);
        }
       private void fillddlSubject()
        {
            OrderedDictionary data;
            data = SubjectsController.getAllSubjectsByCustomerID_OrderDictForUserPermission(Helper.getIntNotNull( ddlCustomer.SelectedValue));
            data.Insert(0, 0, Consts.ALL_SUBJECTS);
            fillDdlOrderDict(data, ddlSubject);
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            
            Users user = new Users();
            if (rbCustomerExist.Checked)
            {
                if (!string.IsNullOrEmpty(ddlUsers.SelectedValue) && ddlUsers.SelectedValue != "0")
                    user.UserID = Helper.getIntNotNull(ddlUsers.SelectedValue);
                else
                {
                    showIndicationMessage(false, Consts.MISSING_USER_NAME);
                    return;
                }
            }
            else
            {
                user.FullName = txtUserName.Text;
                user.CustomerID = Helper.getInt(ddlCustomer.SelectedValue).Value;
             }
            user.Password = txtPassword.Value;
            user.PasswordLastDate = DateTime.Now;
            user.Administrator = cbAdmin.Checked;
            user.ArchiveWorker = ddlCustomer.SelectedValue == Consts.PARAM_ARCHIVE_ID ? true : false;
            user.Job = ddlCustomer.SelectedValue == Consts.PARAM_ARCHIVE_ID? Helper.getInt(ddlJob.SelectedValue).Value:-1;
            user.PermDep1 = cbType1.Checked;
            user.PermDep2 = cbType2.Checked;
            user.PermDep3 = cbType3.Checked;
            user.PermDep4 = cbType4.Checked;
            user.PermDep5 = cbType5.Checked;
            user.PermDep6 = cbType6.Checked;
            user.PermDep7 = cbType7.Checked;
            user.PermDep8 = cbType8.Checked;
            user.PermDep9 = cbType9.Checked;
            user.PermDep10 = cbType10.Checked;
            user.PermForTyping = cbTyping.Checked;
            user.PermForUrgentDelivery = cbUrgent.Checked;
            user.PermForWorksForDrivers = cbWorksForDrivers.Checked;
            user.PermForCollection = cbCollection.Checked;
            user.PermForCustomerScreen = cbCustomerScreen.Checked;
            user.PermForExterminate = cbExterminate.Checked;
            user.PermForMaintence = cbMaintence.Checked;
            user.PermFormSecondRetrive = cbSecondRetrive.Checked;
            user.PermForOrderItems = cbOrderItems.Checked;
            user.PermForOrderSpecial = cbOrderSpecial.Checked;
            user.PermForReport = cbReport.Checked;
            user.PermForRetrive = cbRetrive.Checked;

            user.CityAddress = ddlCities.SelectedValue;
            user.DescriptionAddress = txtDescriptionAddress.Value;
            user.Email = txtMail.Value;
            user.Fax = txtFax.Value;
            user.FullAddress = txtAddress.Value;
            user.Telephone1 = txtPhone1.Value;
            user.Telephone2 = txtPhone2.Value;
            user.SubjectID = Helper.getIntNotNull(ddlSubject.SelectedValue);

            user = UserController.insertUpdate(user);
            showIndicationMessage(user!=null, user != null ? Consts.SAVED_SUCCEED : Consts.SAVED_FAIL);

       
            if (user != null && rbNew.Checked == true)
            {
                txtUserName.Text = string.Empty;
                dvSucceedMessage.InnerText+= Consts.USER_NUM.Replace("XXX", user.UserID.ToString())+ Consts.USER_PASSWORD.Replace("YYY",user.Password);
                Customers cust = CustomersController.getCustomerByID(Helper.getIntNotNull(ddlCustomer.SelectedValue));
                txtAddress.Value = cust.FullAddress;
                txtDescriptionAddress.Value = cust.DescriptionAddress;
                ddlCities.SelectedValue = ddlCities.Items.FindByValue(cust.CityAddress.TrimStart().TrimEnd()) != null ? cust.CityAddress.TrimStart().TrimEnd() : null;
            }

        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerID = Helper.getInt(ddlCustomer.SelectedValue).Value;
            fillddlUsers(customerID);
            fillddlSubject();
            if (rbNew.Checked==true)
            {
                Customers cust = CustomersController.getCustomerByID(customerID);
                txtAddress.Value = cust.FullAddress;
                txtDescriptionAddress.Value= cust.DescriptionAddress;
                ddlCities.SelectedValue = ddlCities.Items.FindByValue(cust.CityAddress.TrimStart().TrimEnd()) != null ? cust.CityAddress.TrimStart().TrimEnd() : null;
            }
            ddlCustomerChangeSelection(ddlCustomer.SelectedValue == Consts.PARAM_ARCHIVE_ID);

        }
        protected void rbCustomerExist_CheckedChanged(object sender, EventArgs e)
        {


            if (rbCustomerExist.Checked)
            {
                ddlUsers.Style["display"] = "";
                txtUserName.Style["display"] = "none";
                ddlCustomer.ClearSelection();
                txtPassword.Value = string.Empty;
            }
            else
            {
                txtUserName.Style["display"] = "";
                ddlUsers.Style["display"] = "none";
                userNum.InnerText = "";
                string newPassword = Membership.GeneratePassword(12, 0);
                Random rnd = new Random();
                newPassword = Regex.Replace(newPassword, @"[^a-zA-Z0-9]", m => rnd.Next(0, 10).ToString());
                txtPassword.Value = newPassword;
                //clearFields();

            }

        }

        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }

 
        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlUsers.SelectedValue) && ddlUsers.SelectedValue != "0")
            {
                Users user = UserController.getUserByUserID(Helper.getIntNotNull( ddlUsers.SelectedValue));
                txtUserName.Text=user.FullName ;
                ddlCustomer.SelectedValue =user.CustomerID.ToString();
                //ddlDepartment.SelectedValue= ddlDepartment.Items.FindByValue( user.DepartmentID.ToString())!=null?user.DepartmentID.ToString():null ;
                 txtPassword.Value=user.Password ;
                userNum.InnerText = Consts.USER_NUM.Replace("XXX", user.UserID.ToString());
                 cbAdmin.Checked=user.Administrator == true;
                 //if(user.ArchiveWorker==true) ddlCustomer.SelectedValue = Consts.PARAM_ARCHIVE_ID ;
                 ddlJob.SelectedValue = ddlCustomer.SelectedValue == Consts.PARAM_ARCHIVE_ID ? user.Job.ToString() : null;
                 ddlSubject.SelectedValue =ddlSubject.Items.FindByValue(user.SubjectID.ToString()) != null ? user.SubjectID.ToString() : null;
                 cbType1.Checked=user.PermDep1==true;
                 cbType2.Checked=user.PermDep2 == true;
                 cbType3.Checked=user.PermDep3 == true;
                 cbType4.Checked=user.PermDep4 == true;
                 cbType5.Checked=user.PermDep5 == true;
                 cbType6.Checked=user.PermDep6 == true;
                 cbType7.Checked=user.PermDep7 == true;
                 cbType8.Checked=user.PermDep8 == true;
                 cbType9.Checked=user.PermDep9 == true;
                cbType10.Checked=user.PermDep10 == true;
                cbTyping.Checked=user.PermForTyping == true;
                cbUrgent.Checked=user.PermForUrgentDelivery == true;
                cbWorksForDrivers.Checked=user.PermForWorksForDrivers == true;
                cbCollection.Checked=user.PermForCollection == true;
                cbCustomerScreen.Checked=user.PermForCustomerScreen == true;
                cbExterminate.Checked=user.PermForExterminate == true;
                cbMaintence.Checked=user.PermForMaintence == true;
                cbSecondRetrive.Checked=user.PermFormSecondRetrive == true;
                cbOrderItems.Checked=user.PermForOrderItems == true;
                cbOrderSpecial.Checked=user.PermForOrderSpecial == true;
                cbReport.Checked=user.PermForReport == true;
                cbRetrive.Checked=user.PermForRetrive == true;

                txtDescriptionAddress.Value = user.DescriptionAddress != null ? user.DescriptionAddress.ToString().TrimEnd() : string.Empty;
                txtMail.Value = user.Email != null ? user.Email.ToString().TrimEnd() : string.Empty;
                txtFax.Value = user.Fax != null ? user.Fax.ToString().TrimEnd() : string.Empty;
                txtAddress.Value = user.FullAddress != null ? user.FullAddress.ToString().TrimEnd() : string.Empty;
                txtPhone1.Value = user.Telephone1 != null ? user.Telephone1.ToString().TrimEnd() : string.Empty;
                txtPhone2.Value = user.Telephone2 != null ? user.Telephone2.ToString().TrimEnd() : string.Empty;
                ddlCities.SelectedValue = !string.IsNullOrEmpty(user.CityAddress) && ddlCities.Items.FindByText(user.CityAddress)!=null ? user.CityAddress : null;

            }
        }
    }
}
