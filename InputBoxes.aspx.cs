﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class InputBoxes : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.anyUser }; } }
        private string[] rowsList = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
        protected void Page_Load(object sender, EventArgs e)
        {
            List<BoxesBufferUI> boxBufferData = BoxBufferController.getAllBoxesBuffer(user.CustomerID, user.ArchiveWorker == true);
            dgResults.DataSource = boxBufferData;
            dgResults.DataBind();
            if (!Page.IsPostBack)
            {
                fillDdlData(boxBufferData);
                if (user.ArchiveWorker != true)
                {
                    tblPlace.Style["display"] = "none";
                    btnUpdate.Style["display"] = "none";
                    ddlPlaceCustomer.Style["display"] = "none";
                }
            }

        }

        private void fillDdlData(List<BoxesBufferUI> boxBufferData)
        {
            
            if (user.ArchiveWorker == true)
            {
                ddlCustomers.DataSource = (new MyCache()).CustomersNames;
                ddlCustomersID.DataSource = (new MyCache()).CustomersIds;
            }
            else
            {
                OrderedDictionary data;
                Customers cust = CustomersController.getCustomerByID(user.CustomerID);
                data = new OrderedDictionary();
                data.Insert(0, cust.CustomerID, cust.CustomerName);
                ddlCustomers.DataSource = data;
                data = new OrderedDictionary();
                data.Insert(0, cust.CustomerID, cust.CustomerID);
                ddlCustomersID.DataSource = data;
            }

            ddlCustomers.DataTextField = "Value";
            ddlCustomers.DataValueField = "Key";
            ddlCustomers.DataBind();

            
            ddlCustomersID.DataTextField = "Value";
            ddlCustomersID.DataValueField = "Key";
            ddlCustomersID.DataBind();

            fillPlaceCustomersDdl(boxBufferData);

            if (ddlCustomersID.SelectedValue != null && Helper.getInt(ddlCustomersID.SelectedValue) > 0)
                ddlCustomersID_SelectedIndexChanged(null, null);
            ddlFromWarehouse.DataSource = Consts.warehouseList;
            ddlFromWarehouse.DataBind();
            ddlToWarehouse.DataSource = Consts.warehouseList;
            ddlToWarehouse.DataBind();
            ddlToWarehouse.SelectedIndex = ddlToWarehouse.Items.Count > 0 ? ddlToWarehouse.Items.Count - 1 : 0;
            ddlFromRow.DataSource = rowsList;
            ddlFromRow.DataBind();
            ddlToRow.DataSource = rowsList;
            ddlToRow.DataBind();
            ddlToRow.SelectedIndex = ddlToRow.Items.Count > 0 ? ddlToRow.Items.Count - 1 : 0;

        }

        private void fillPlaceCustomersDdl(List<BoxesBufferUI> boxBufferData)
        {
            int[] customers = boxBufferData.GroupBy(d => d.CustomerID.Value).Select(c => c.FirstOrDefault().CustomerID.Value).ToArray();

            OrderedDictionary data = CustomersController.getAllActiveCustomersOrdered(customers);
            if (!data.Contains(0))
                data.Insert(0, 0, Consts.SELECT_CUSTOMER);
            fillDdlOrderDict(data, ddlPlaceCustomer);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            dvErrorMessage.InnerText = string.Empty;
            dvErrorMessage.Style["display"] = "none";
            Boxes ExistBox = BoxesController.validateIfBoxIDExist(txtBoxNum.Value);
            if (ExistBox != null)
            {
                if(user.ArchiveWorker==true)
                showIndicationMessage(false, Consts.BOX_BUFFER_ID_EXIST.Replace("XXX", ExistBox.CustomerID.ToString()).Replace("YYY", ExistBox.CustomerBoxNum!=null?ExistBox.CustomerBoxNum.ToString():""));
                else
                    showIndicationMessage(false, Consts.BOX_EXIST);

                txtBoxNum.Value = string.Empty;
                txtCustomerBoxNum.Value = string.Empty;
                txtBoxNum.Focus();
                return;
            }
            ExistBox = BoxesController.validateIfBoxNumExist(Helper.getInt( txtCustomerBoxNum.Value), Helper.getInt(ddlCustomers.SelectedValue).Value);
            if (ExistBox != null && !string.IsNullOrEmpty(txtCustomerBoxNum.Value))
            {
                showIndicationMessage(false, Consts.BOX_BUFFER_CUSTOMER_ID_EXIST.Replace("XXX", ExistBox.BoxID));
                txtCustomerBoxNum.Value = string.Empty;
                txtCustomerBoxNum.Focus();
                return;
            }
            int customerID = Helper.getInt(ddlCustomers.SelectedValue).Value;
            Customers customer = CustomersController.getCustomerByID(customerID);
            if (customer.IsBoxDescRequiered == true && string.IsNullOrEmpty(txtBoxDesc.Value))
            {
                showIndicationMessage(false, Consts.REQUIERED_FIELD.Replace("XXX", "טקסט לארגז"));
                txtBoxDesc.Focus();
                return;
            }
            if (customer.IsExtermYearRequiered == true && string.IsNullOrEmpty(txtExtermYear.Value))
            {
                showIndicationMessage(false, Consts.REQUIERED_FIELD.Replace("XXX", "שנת ביעור"));
                txtExtermYear.Focus();
                return;
            }
            int? customerBoxNum = !string.IsNullOrEmpty(txtCustomerBoxNum.Value) ? Helper.getInt(txtCustomerBoxNum.Value) : BoxesController.getLastCustomerBoxNum(customerID);

            if (ExistBox == null)
            {
                BoxesController.insert(
                    new Boxes()
                    {
                        BoxID = txtBoxNum.Value,
                        CustomerID = customerID,
                        CustomerBoxNum = customerBoxNum,
                        DepartmentID = Helper.getIntNotNull(ddlDepartment.SelectedValue),
                        ExterminateYear = !string.IsNullOrEmpty(txtExtermYear.Value) ? Helper.getInt(txtExtermYear.Value) : 2199,
                        BoxDescription = txtBoxDesc.Value,
                        InsertDate = DateTime.Now,
                        LoginID = user.UserID,
                        Location = false,
                        BoxType = 1,
                        Exterminate = false,
                        InsertData = customer.InsertData == true ? 1 : 0,
                    });
            }
            bool isSecceed = BoxBufferController.insert(new BoxesBuffer()
            {
                BoxID = txtBoxNum.Value,
                CustomerBoxNum = customerBoxNum,
                CustomerID = customerID,
                InsertDate = DateTime.Now,
                Print = false,
                DepartmentID = Helper.getIntNotNull(ddlDepartment.SelectedValue),
                ExterminateYear = !string.IsNullOrEmpty(txtExtermYear.Value) ? Helper.getInt(txtExtermYear.Value) : 2199,
                BoxDescription = txtBoxDesc.Value,
                isPlaced=false,
            }, user.UserID);
            txtBoxNum.Value = string.Empty;
            txtExtermYear.Value= string.Empty;
            txtBoxDesc.Value= string.Empty;
            txtBoxNum.Focus();
            txtCustomerBoxNum.Value = string.Empty;
            dgResults.DataSource = BoxBufferController.getAllBoxesBuffer(user.CustomerID,user.ArchiveWorker==true);
            dgResults.DataBind();
        }

        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int sumBoxes = 0;
            int sumHeight = 0;
            int fromWarehouse = 0;
            int toWarehouse = 0;
            int fromRow = 0;
            int toRow = 0;

            if (!string.IsNullOrEmpty(ddlFromWarehouse.SelectedValue))
                fromWarehouse = Helper.getInt(ddlFromWarehouse.SelectedValue).Value;

            if (!string.IsNullOrEmpty(ddlToWarehouse.SelectedValue))

                toWarehouse = Helper.getInt(ddlToWarehouse.SelectedValue).Value;


            if (!string.IsNullOrEmpty(ddlFromRow.SelectedValue))
                fromRow = Helper.getInt(ddlFromRow.SelectedValue).Value;

            if (!string.IsNullOrEmpty(ddlToRow.SelectedValue))
                toRow = Helper.getInt(ddlToRow.SelectedValue).Value;


            List<string> boxesList = new List<string>();
            foreach (DataGridItem item in dgResults.Items)
            {
                if ((item.FindControl("cbPlace") as HtmlInputCheckBox).Checked)
                {
                    boxesList.Add((item.FindControl("spnBoxID") as HtmlGenericControl).InnerText.Trim());
                }
            }
            sumBoxes = boxesList.Count();
            sumHeight = Helper.getIntNotNull(ddlToRow.SelectedValue) - Helper.getIntNotNull(ddlFromRow.SelectedValue) + 1;
            int maxBoxesToPlace = 9 * sumHeight <= Consts.BOXES_TO_PLACE ? 9 * sumHeight : Consts.BOXES_TO_PLACE;

            List<ShelfBoxCalc> list = MignazaController.getMignazaEmpty(fromWarehouse, toWarehouse, fromRow, toRow, 0, 0);
            List<ShelfBoxCalc> groupedlist = list.GroupBy(x => x.ShelfID.Substring(0, 6)).Select(x => new ShelfBoxCalc() { ShelfID = x.Key, Value = x.Select(y => y.Value).Sum() }).ToList();
            if (groupedlist.Count <= 0 || groupedlist.Select(x => x.Value).Sum() < sumBoxes)
            {
                dvPlaceErrorMessage.InnerText = Consts.NOT_ENOUGH_SPACE_IN_MIGNAZA;
}
            else
            {
                dvPlaceErrorMessage.InnerText = "";
                List<ShelfFileID> shelfBoxCalc = recGetPlace(boxesList, 0, sumBoxes, groupedlist, new List<ShelfFileID>(), maxBoxesToPlace);
                dvPlaces.Controls.Add(getTableByList(shelfBoxCalc));
                dvPlaces.Style["display"] = "";
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "PrintWindow()", true);
            }
        }



        private List<ShelfFileID> recGetPlace(List<string> boxesList, int index, int sumBoxes, List<ShelfBoxCalc> groupedlist, List<ShelfFileID> result, int maxBoxesToPlace)
        {
            if (sumBoxes <= 0)
                return result;
            int maxBoxes = sumBoxes < maxBoxesToPlace ? sumBoxes : maxBoxesToPlace;
            ShelfBoxCalc maxRow = groupedlist.Where(a => a.Value.Value >= maxBoxes)/*.OrderBy(a => a.Value.Value)*/.FirstOrDefault();
            if (maxRow == null)
                maxRow = groupedlist.Where(x => x.Value.Value == (groupedlist.Select(y => y.Value.Value).Max())).FirstOrDefault();
            MignazaController.saveShelfs(maxRow, maxBoxes);
            ShelfFileID shelfFileID = new ShelfFileID() { shelfID = maxRow.ShelfID, files = new List<string>() };
            for (int i = 0; i < Math.Min(maxRow.Value.Value, maxBoxes); i++)
            {
                if (boxesList.Count > index)
                {
                    shelfFileID.files.Add(boxesList[index]);
                    if (BoxesController.getBoxByID(boxesList[index]) != null)
                        BoxBufferController.remove(boxesList[index]);
                    else
                        BoxBufferController.updatePlaced(boxesList[index]);
                    index++;
                }
            }
            result.Add(shelfFileID);
            sumBoxes -= Math.Min(maxRow.Value.Value, maxBoxes);
            maxRow.Value -= Math.Min(maxRow.Value.Value, maxBoxes);

            recGetPlace(boxesList, index, sumBoxes, groupedlist, result, maxBoxesToPlace);

            return result;

        }



        private HtmlTable getTableByList(List<ShelfFileID> shelfBoxCalc)
        {
            HtmlTable table = new HtmlTable();
            table.Style["direction"] = "rtl";
            table.Style["float"] = "right";
            shelfBoxCalc = shelfBoxCalc.OrderBy(x => x.shelfID).ToList();
            foreach (ShelfFileID shelf in shelfBoxCalc)
            {
                HtmlTableCell tc1 = new HtmlTableCell();
                tc1.InnerText = shelf.shelfID;
                HtmlTableCell tc2 = new HtmlTableCell();
                tc2.InnerText = shelf.files.Count.ToString();
                HtmlTableRow tr = new HtmlTableRow();
                tr.Cells.Add(tc1);
                tr.Cells.Add(tc2);
                table.Rows.Add(tr);
                foreach (var file in shelf.files)
                {
                    HtmlTableCell tc3 = new HtmlTableCell();
                    tc3.InnerText = "";
                    HtmlTableCell tc4 = new HtmlTableCell();
                    tc4.InnerText = file;
                    HtmlTableRow tr1 = new HtmlTableRow();
                    tr1.Cells.Add(tc3);
                    tr1.Cells.Add(tc4);
                    table.Rows.Add(tr1);
                }
            }
            return table;



        }

        protected void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomersID.SelectedValue = ddlCustomers.SelectedValue;
            fillDdlDepartments(Helper.getIntNotNull(ddlCustomers.SelectedValue));
        }

        protected void ddlCustomersID_SelectedIndexChanged(object sender, EventArgs e)
        {
           ddlCustomers.SelectedValue= ddlCustomersID.SelectedValue  ;
            fillDdlDepartments(Helper.getIntNotNull(ddlCustomersID.SelectedValue));
        }

        private void fillDdlDepartments(int SelectedValue)
        {
            OrderedDictionary data = DepartmentController.GetCustomerDepartmentsDictionary(SelectedValue);
            data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
            fillDdlOrderDict(data, ddlDepartment);
        }
    }
}