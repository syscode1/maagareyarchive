﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="WorkOrderingForDriver.aspx.cs" Inherits="MaagareyArchive.WorkOrderingForDriver" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        var MoveRowUp = "table[id*=dgResultsR] input[id*='btnUp']"; //Instance of MoveUp btn
        var MoveRowDown = "table[id*=dgResultsR] input[id*='btnDown']";//Instance of MoveDown btn
        function up(event) {
            $(event).parents("tr").insertBefore($(event).parents("tr").prev())
            DisableRow();
            return false;
        }
        function down(event) {
            $(event).parents("tr").insertAfter($(event).parents("tr").next());
            DisableRow();
            return false;
        }
        $(document).ready(function () {
            DisableRow(); //This function disables first and last row
            //This function Moves up the GridView row
            //$(MoveRowUp).click(function () {
            //    $(this).parents("tr").insertBefore($(this).parents("tr").prev())            
            //     DisableRow();
            //    return false;
            //});

           
            //This function Moves down the Gridview row
            //$(MoveRowDown).click(function () {
            //    $(this).parents("tr").insertAfter($(this).parents("tr").next());               
            //      DisableRow();
            //    return false;
            //});
            //This function disables first and last row
           
        });       
             function DisableRow() {
                $("#<%=dgResultsR.ClientID%> tr:has(td) input[id*='btnUp']").attr("disabled", false);
                $("#<%=dgResultsR.ClientID%> tr:has(td):first input[id*='btnUp']").attr("disabled", true);
                $("#<%=dgResultsR.ClientID%> tr:has(td) input[id*='btnDown']").attr("disabled", false);
                $("#<%=dgResultsR.ClientID%> tr:last input[id*='btnDown']").attr("disabled", true);
            }     
        function btnUpdate_Click() {
                var ids = $("#<%=dgResultsR.ClientID%> tr:has(td)");
                var strId = "";
                for (i = 0; i < ids.length; i++) {

                    var cbRemove = $("[id*='cbRemove']", ids[i])[0];
                    var cbDone = $("[id*='cbDone']", ids[i])[0];
                    var cbPartlyDone = $("[id*='cbPartlyDone']", ids[i])[0];
                    if (cbRemove != undefined) {
                        if (strId != "")
                            strId += ",";
                        strId += $("span[id*='spnNumInShipping']", ids[i])[0].innerText + ";";
                        strId += $("span[id*='spnRowID']", ids[i])[0].innerText + ";";
                        strId += (cbRemove.checked == true ? "NotCommited" : (cbDone.checked == true ? "Done" : (cbPartlyDone.checked == true ? "PartlyDone" : "InProgress"))) + ";";
                        strId += ($("[id*='txtComment']", ids[i])[0].style.backgroundColor != "" ? "1" : "0") + ";";
                        strId += $("[id*='txtComment']", ids[i])[0].value;
                    }
                    }
                $("[id*='hfIndex']")[0].value = strId.trim();
                return true;

        }
        function highlightText(event) {
            var color = ($("[id*='txtComment']", $(event).parents("tr"))[0]).style.backgroundColor;
            if (color == "#FFFF00" || color == "rgb(255, 255, 0)")
                ($("[id*='txtComment']", $(event).parents("tr"))[0]).style.backgroundColor = "";
            else
                ($("[id*='txtComment']", $(event).parents("tr"))[0]).style.backgroundColor = "#FFFF00";

            return false;
        }

        function CallPrint() {
            debugger
            var prtContent = document.getElementById('dvPrint');
            var tableRightStyle = $('#<%= dgResultsR.ClientID %>', prtContent)[0].style.right;
            $('#<%= dgResultsR.ClientID %>', prtContent)[0].style.right = "0px";
            prtContent.border = 1; //set no border here

            var WinPrint = window.open('', '', 'left=0,top=100,width=1500,height=900,toolbar=0,scrollbars=1,status=0,resizable=1');
            WinPrint.document.write("<html><body style='-webkit-print-color-adjust:exact;'>"+prtContent.outerHTML+"</body></html>");


  
            var rows = WinPrint.document.getElementById('<%= dgResultsR.ClientID %>').rows;
            var originRows = document.getElementById('<%= dgResultsR.ClientID %>').rows;
            for (var i = 0; i < rows.length; i++) {
                rows[i].deleteCell(0);
                rows[i].deleteCell(0);
                if ($("[id*='txtComment']", rows[i]).length > 0)
                    $("[id*='txtComment']", rows[i])[0].value = $("[id*='txtComment']", originRows[i])[0].value;
            }


            var css = '@page { size: landscape; }',
head = WinPrint.document.head || document.getElementsByTagName('head')[0],
style = WinPrint.document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(WinPrint.document.createTextNode(css));
            }
            head.appendChild(style);

            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            $('#<%= dgResultsR.ClientID %>', prtContent)[0].style.right = tableRightStyle;
            return false;
          
        }


        function cbPartlyDone_checkedChanged(evt) {
            if (evt.checked == true) {
                if (!window.focus)
                    return true;
               
                var href;
                var w = 1200;
                var h = 600;
                var left = (screen.width / 2) - (w / 2);
                var top = (screen.height / 2) - (h / 2);
                var rowID = $("[id*= 'spnRowID']", evt.closest('tr'))[0].innerText;
                var rowIndex = evt.closest('tr').rowIndex - 1;
                href = "WorkOrderingPartlyDone.aspx?rowID=" + rowID + "&rowIndex=" + rowIndex;
                window.open(href, 'WorkOrderingPartlyDone', 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            }
                return false;
            

        }

        function cbDone_checkedChanged(evt) {
            if (evt.checked == true) {
               
                var row = $("[id*= 'spnRowID']", evt.parentElement.parentElement.parentElement)[0];
                href = "DeliveryConfirmationReport.aspx?rowID=" + row.innerText;
                window.location = href;
            }
            return false;


        }

        function popup(href, paramName, paramValue,w) {   
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open(href + paramName + "=" + paramValue, "", 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }

        function confirmRemove(checkbox)
        {
            if(checkbox.checked==true)
                checkbox.checked = confirm("האם אתה בטוח?");

                
        }

    </script>
        <style type="text/css" media="print">
  @page { size: landscape; }
</style>
    <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>      
            <div id="dvPrint" style="direction:rtl;text-align:center;">
            <h1 id="header" runat="server">
                סידור עבודה לנהג
            </h1>
            <h2 id="hDate" runat="server"></h2> 
             <h2 id="hShippingID" runat="server"></h2> 
            <h2 id="hDriverID" runat="server"></h2> 
            <asp:HiddenField ID="hfIndex" Value="" runat="server" />
    <asp:GridView ID="dgResultsR" runat="server" CssClass="grid"
        OnRowDataBound="dgResultsR_RowDataBound"
        EnableViewState="true" Width="1038px"
        style="right: -105px;position:relative"
        AutoGenerateColumns="false"
        EnablePersistedSelection="true"
        FooterStyle-Height="20px"
        DataKeyNames="NumInShipping"
        ShowFooter="true"
        >
        <HeaderStyle CssClass="header"/>
        <AlternatingRowStyle CssClass="alt" />
        <RowStyle CssClass="row" />
        <Columns>
                                    <asp:TemplateField HeaderText="Up" ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
        <ItemTemplate>
          <asp:Button ID="btnUp" Width="24px" ForeColor="White" Height="20px" Font-Bold="true"
                        ToolTip="Move Up" Font-Size="Medium" BorderStyle="None" OnClientClick="return up(this)"
                        BackColor="#507CD1" CommandName="Up" style="padding: 0px;" runat="server" Text="&uArr;" />
      </ItemTemplate>
      </asp:TemplateField>
      
      <asp:TemplateField HeaderText="Down" ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
        <ItemTemplate>
            <asp:Button ID="btnDown" Width="24px" ForeColor="White" Height="20px" Font-Bold="true"
                         ToolTip="Move Down" Font-Size="Medium" BorderStyle="None" OnClientClick="return down(this)" 
                        BackColor="#507CD1" CommandName="Down" style="padding: 0px;" runat="server" Text="&dArr;" />
        </ItemTemplate>
      </asp:TemplateField>

             <asp:TemplateField HeaderText="לקוח" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                     <a href="" onclick="return popup('CustomerScreen.aspx?','custID','<%#(Eval("CustomerID"))%>',1200)">
                        <span id="spnCustomerName" runat="server"><%#(Eval("CustomerName"))%> </span>
                    </a>
                    <span id="spnRowID" runat="server" style="display:none"><%#(Eval("rowID"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="מחלקה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span id="spnDepartmentName" runat="server"><%#(Eval("DepartmentName"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="עיר" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span ><%# (Eval("CityAddress"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="כתובת" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "FullAddress"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
                        <asp:TemplateField HeaderText="איש קשר" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "ContactMan"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
                        <asp:TemplateField HeaderText="טלפון 1" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "Telephone1"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
                                    <asp:TemplateField HeaderText="טלפון 2" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "Telephone2"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="הערות" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate> 
                     <asp:Image  runat="server" ImageUrl="resources/images/highlight.png"  style="height:14px;display:grid;" onclick="return highlightText(this)" alt="Alternate Text"/>
                                        
                    <asp:TextBox id="txtComment" runat="server" TextMode="MultiLine" onkeypress="return maxCharLength(event,64)" style="width:150px;height:auto" Text='<%#(Eval("Comment"))%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <b >סה"כ:</b>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ תיקים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumFiles"))%> </span>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label Font-Bold="true" id="lblSumFiles" runat="server" text="footer"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumBoxes"))%> </span>
                </ItemTemplate>
                 <FooterTemplate>
                    <asp:Label Font-Bold="true" id="lblSumBoxes" runat="server" text="footer"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים ריקים" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumNewBoxOrder"))%> </span>
                </ItemTemplate>
                 <FooterTemplate>
                    <asp:Label Font-Bold="true" id="lblSumNewBoxOrder" runat="server" text="footer"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים לפינוי" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumFullBoxes"))%> </span>
                </ItemTemplate>
                                 <FooterTemplate>
                    <asp:Label Font-Bold="true" id="lblSumFullBoxes" runat="server" text="footer"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים להחזרה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumRetriveBoxes"))%> </span>
                </ItemTemplate>
                                 <FooterTemplate>
                    <asp:Label Font-Bold="true" id="lblSumRetriveBoxes" runat="server" text="footer"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="סה''כ ארגזים לגריסה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <span><%#(Eval( "SumExterminateBoxes"))%> </span>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label Font-Bold="true" id="lblSumExterminate" runat="server" text="footer"/>
                </FooterTemplate>
            </asp:TemplateField>
                       <asp:TemplateField HeaderText="הסר" ItemStyle-CssClass="ltr">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" id="cbRemove" Width="80px" onclick="confirmRemove(this)" />
                     <span id="spnNumInShipping" runat="server" style="display:none"><%#(Eval("NumInShipping"))%> </span>
                </ItemTemplate>
            </asp:TemplateField>
                       <asp:TemplateField HeaderText="בוצע" ItemStyle-CssClass="ltr">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" id="cbDone" Width="80px"  onClick='cbDone_checkedChanged(this)' />
                </ItemTemplate>
            </asp:TemplateField>
                                   <asp:TemplateField HeaderText="בוצע חלקית" ItemStyle-CssClass="ltr">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" id="cbPartlyDone" Width="80px" onClick='cbPartlyDone_checkedChanged(this)' />
             
                       </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="העבר" ItemStyle-CssClass="ltr">
                <HeaderStyle CssClass="t-center" />
                <ItemStyle CssClass="t-natural va-middle" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" id="cbMove" Width="80px"  />
             
                       </ItemTemplate>
            </asp:TemplateField>
         </Columns>
        <FooterStyle />
    </asp:GridView>
   </div>
                <table style="float: left;width:100%">
                <tr>
                     <td style="width: 662px;text-align: right;">
                    <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />
                </td>
                  <td>                        
                      <asp:Button ID="btnUpdate" Text="עדכן" runat="server" OnClientClick="return btnUpdate_Click()" OnClick="btnUpdate_Click" Style="margin-bottom: 10px; width: 130px" />
</td> 
                </tr>
                <tr>
                    <td></td>
                    <td  style="float:left">
                    <asp:Button ID="btnPrint" Text="הדפס" runat="server" OnClientClick="return CallPrint()" Style="margin-bottom: 10px; width: 130px" />

                    </td>
                    <%--OnClientClick="return getSelectedFiles()"--%>
                </tr>
            </table>
    </ContentTemplate>
    </asp:UpdatePanel>
          <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
