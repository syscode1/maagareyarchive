﻿using MA_DAL.MA_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class Reports :BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                setBoxesCount();
            }
        }

        private void setBoxesCount()
        {
           var userDepartments = DepartmentController.GetUserDepartments(user);
            txtNomCustBoxes.Text = BoxesController.getBoxesCount(user.CustomerID, userDepartments.Select(d=>d.DepartmentID).ToList()).ToString();
            txtNomLendBoxes.Text = BoxesController.getLendBoxesCount(user.CustomerID, userDepartments.Select(d => d.DepartmentID).ToList()).ToString();

        }

        protected void btnExportDataToExcell_Click(object sender, EventArgs e)
        {
            Response.Redirect("ExportFilesReport.aspx");
        }

        protected void btnExterminate_Click(object sender, EventArgs e)
        {
           Response.Redirect("ExportExterminated.aspx");
        }

        protected void btnShipping_Click(object sender, EventArgs e)
        {
            Response.Redirect("ExportShipping.aspx");
        }

        protected void btnOrders_Click(object sender, EventArgs e)
        {
            Response.Redirect("ExportOrders.aspx");
        }

        protected void btnReceivedShippings_Click(object sender, EventArgs e)
        {
            Response.Redirect("ExportShippingsReceived.aspx");
        }

    }
}