﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class OrderConfirmation : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["orderID"]!=null)
            orderID.InnerText = Request.QueryString["orderID"].ToString();
        }
    }
}