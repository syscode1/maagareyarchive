﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class CustomerAgreement : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            clearMessages();
            if (!Page.IsPostBack)
            {
                fillAllDdl();
                string customerID = Request.QueryString["custID"];
                if (!string.IsNullOrEmpty(customerID))
                {
                    rbCustomerExist.Checked = true;
                    ddlCustomerID.SelectedValue = customerID;
                    ddlCustomerName.SelectedValue = customerID;
                    ddlCustomerName_SelectedIndexChanged(null, null);
                    btnBackCustomer.Style["display"] = "";
                    if (Request.QueryString["isDisable"] == "1")
                    {

                        dvCustAgree.Style["pointer-events"] = "none";
                        dvCustAgree.Style["opacity"] = "0.6";

                    }
                }
                else
                {
                    rbNewCustomer.Checked = true;
                    dvCustomerName.Style["display"] = "none";
                    dvCustomerID.Style["display"] = "none";
                    btnDepartments.Style["display"] = "none";
                    btnUsers.Style["display"] = "none";
                    clearFields();
                    cbDeliveryApproval.Checked = true;
                }
            }
        }
        private void fillAllDdl()
        {
              var listCache = new MyCache();
              ddlCities.DataSource = listCache.Cities;
              ddlCities.DataBind();
            fillddlCustomers();
            rblLinkTo.DataSource = PaymentController.getLinkToPaymentList();
            rblLinkTo.DataBind();

            clearFields();

            //fillDdl(PeriodOfPaymentController.getList(), ddlTemOfPayment);
        }
        private void fillDdl(DropDownList ddl,object data)
        {
            ddl.DataSource = data;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
        }
        private void fillddlCustomers()
        { 
                ddlCustomerName.DataSource = (new MyCache()).CustomersNames;
            ddlCustomerID.DataSource = (new MyCache()).CustomersIds;

            ddlCustomerName.DataTextField = "Value";
            ddlCustomerName.DataValueField = "Key";
            ddlCustomerName.DataBind();


            ddlCustomerID.DataTextField = "Value";
            ddlCustomerID.DataValueField = "Key";
            ddlCustomerID.DataBind();



            fillDdl(ddlCustomerStatus, CustomersController.getCustomersStatusList());
            ddlCustomerStatus.SelectedValue = ((int)Consts.enCustomerStatus.active).ToString();
            fillDdl(ddlPeriodOfPayment, PaymentController.getPeriodOfPaymentList());
            fillDdl(ddlPaidBy, (new MyCache()).CustomersNames);
            fillDdl(ddlMethodOfPayment, PaymentController.getMethodOfPaymentList());
            fillDdl(ddlCredit, PaymentController.getCreditTerms());

           
        }
        protected void rbCustomerExist_CheckedChanged(object sender, EventArgs e)
        {


            if (rbCustomerExist.Checked)
            {
                dvCustomerName.Style["display"] = "";
                dvCustomerID.Style["display"] = "";
                fillddlCustomers();
                btnDepartments.Style["display"] = "";
                btnUsers.Style["display"] = "";
            }
            else
            {
                dvCustomerName.Style["display"] = "none";
                dvCustomerID.Style["display"] = "none";
                btnDepartments.Style["display"] = "none";
                btnUsers.Style["display"] = "none";
                clearFields();
                cbDeliveryApproval.Checked = true;

            }
            //clearControls();
        }

        private void clearFields()
        {
            if (rblLinkTo.Items.FindByValue("0") != null)
                rblLinkTo.SelectedValue = "0";
            if (ddlPeriodOfPayment.Items.FindByValue("2") != null)
                ddlPeriodOfPayment.SelectedValue = "2";
            if (ddlMethodOfPayment.Items.FindByValue("1") != null)
                ddlMethodOfPayment.SelectedValue = "1";
            if (ddlCustomerStatus.Items.FindByValue(Consts.enCustomerStatus.active.ToString()) != null)
                ddlCustomerStatus.SelectedValue = Consts.enCustomerStatus.active.ToString();
            if (ddlPaidBy.Items.FindByValue("0") != null)
                ddlPaidBy.SelectedValue = "0";
            if (ddlCities.Items.FindByValue("0") != null)
                ddlCities.SelectedValue = "0";
           if (ddlCredit.Items.FindByValue("3") != null)
                ddlCredit.SelectedValue = "3";
           
            txtBoxPrice1.Text = string.Empty;
            txtBoxPrice2.Text = string.Empty;
            txtBoxPrice3.Text = string.Empty;
            txtBoxPrice4.Text = string.Empty;
            txtContactName.Value = string.Empty;
            //ddlCredit.SelectedValue,
            txtCrushing.Text = string.Empty;
            txtCrushingAndTransport.Text = string.Empty;
            txtCustomerName.Text = string.Empty;
            txtDateOfContract.Value = string.Empty;
            cbDeliveryApproval.Checked = false;
            txtDescription.Text = string.Empty;
            txtDescriptionAddress.Value = string.Empty;
            txtMail.Value = string.Empty;
            txtEmptyBox.Text = string.Empty;
            txtEmptyBoxMaxEmount.Text = string.Empty;
            txtFax.Value = string.Empty;
            txtFixedPayment.Text = string.Empty;
            txtAddress.Value = string.Empty;
            txtMaxRetrive.Text = string.Empty;
            txtmaxUrgentRetrive.Text = string.Empty;
            txtPO.Value = string.Empty;
            txtRegistration.Text = string.Empty;
            txtRetrive.Text = string.Empty;
            txtScan.Text = string.Empty;
            txtPhone1.Value = string.Empty;
            txtPhone2.Value = string.Empty;
            txtTransport.Text = string.Empty;
            txtTransportAndContract.Text = string.Empty;
            txtDateOfContract.Value = DateTime.Now.ToString();
            txtTransportAndIntake.Text = string.Empty;
            txtUrgentRetrive.Text = string.Empty;
            cbExtermYearReq.Checked = false;
            cbBoxDescReq.Checked = false;
            cbInsertData.Checked = false;

        }

        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerID.SelectedValue = ddlCustomerName.SelectedValue;
            fillControlsData(Helper.getInt(ddlCustomerName.SelectedValue).Value);

        }

        protected void ddlCustomerID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerName.SelectedValue = ddlCustomerID.SelectedValue;
            fillControlsData(Helper.getInt(ddlCustomerID.SelectedValue).Value);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Customers customer= fillCustomerFields();
            
            bool isSucceed =rbCustomerExist.Checked?CustomersController.update(customer): CustomersController.insert(customer);
            if (customer.CustomerStatusID == (int)Consts.enCustomerStatus.inactive)
                CustomersController.deactivate(customer.CustomerID, user.UserID);
            if(isSucceed)
            {
                (new MyCache()).ClearCustomersIdsList();
                (new MyCache()).ClearCustomersNamesList();
            }
            showIndicationMessage(isSucceed, isSucceed ? Consts.SAVED_SUCCEED : Consts.SAVED_FAIL);
        }

        private Customers fillCustomerFields()
        {

            Customers customer = new Customers()
            {
                BoxPrice1 = Helper.getDecimal(txtBoxPrice1.Text),
                BoxPrice2 = Helper.getDecimal(txtBoxPrice2.Text),
                BoxPrice3 = Helper.getDecimal(txtBoxPrice3.Text),
                BoxPrice4 = Helper.getDecimal(txtBoxPrice4.Text),
                CityAddress = ddlCities.SelectedValue,
                ContactMan = txtContactName.Value,
                Credit = Helper.getInt(ddlCredit.SelectedValue),
                Crushing = Helper.getDecimal(txtCrushing.Text),
                CrushingAndTransport = Helper.getDecimal(txtCrushingAndTransport.Text),
                CustomerStatusID = (int)Helper.getInt(ddlCustomerStatus.SelectedValue),
                CustomerName = txtCustomerName.Text,
                DateOfContract = Helper.getDate(txtDateOfContract.Value),
                DeliveryApproval = cbDeliveryApproval.Checked,
                Description = txtDescription.Text,
                DescriptionAddress = txtDescriptionAddress.Value,
                Email = txtMail.Value,
                EmptyBox = Helper.getDecimal(txtEmptyBox.Text),
                Fax = txtFax.Value,
                FixedPayment = Helper.getDecimal(txtFixedPayment.Text),
                FullAddress = txtAddress.Value,
                LinkTo = getLinkTo(),
                MaxRetrive = Helper.getInt(txtMaxRetrive.Text),
                maxUrgentRetrive = Helper.getInt(txtmaxUrgentRetrive.Text),
                EmptyBoxMaxEmount = Helper.getInt(txtEmptyBoxMaxEmount.Text),
                PaidBy = ddlPaidBy.SelectedItem != null && ddlPaidBy.SelectedValue != "0" ? Helper.getInt(ddlPaidBy.SelectedValue) : null,
                MethodOfPayment = (int)Helper.getInt(ddlMethodOfPayment.SelectedValue),
                PeriodOfPayment = (int)Helper.getInt(ddlPeriodOfPayment.SelectedValue),
                PO = txtPO.Value,
                Registration = Helper.getDecimal(txtRegistration.Text),
                Retrive = Helper.getDecimal(txtRetrive.Text),
                Scan = Helper.getDecimal(txtScan.Text),
                Telephon1 = txtPhone1.Value,
                Telephon2 = txtPhone2.Value,
                Transport = Helper.getDecimal(txtTransport.Text),
                TransportAndContract = Helper.getDecimal(txtTransportAndContract.Text),

                TransportAndIntake = Helper.getDecimal(txtTransportAndIntake.Text),
                UrgentRetrive = Helper.getDecimal(txtUrgentRetrive.Text),

                IsBoxDescRequiered = cbBoxDescReq.Checked,
                IsExtermYearRequiered=cbExtermYearReq.Checked,
                InsertData=cbInsertData.Checked,
                

            };
            if (rbCustomerExist.Checked)
            {
                int? customerId = Helper.getInt(ddlCustomerID.SelectedValue);
                if (customerId == null || customerId == 0)
                    return null;
                customer.CustomerID = (int)customerId;
            }
            else
            {
                customer.LastBox = 0;
            }

            return customer;
        }

        private void fillControlsData( int customerID)
        {

            Customers customer = CustomersController.getCustomerByID(customerID);
            if (customer != null)
            {
                rblLinkTo.SelectedValue = rblLinkTo.Items.FindByValue(customer.LinkTo.ToString()) != null ? customer.LinkTo.ToString() : rblLinkTo.SelectedValue = "0";
                ddlPeriodOfPayment.SelectedValue = ddlPeriodOfPayment.Items.FindByValue(customer.PeriodOfPayment.ToString()) != null ? customer.PeriodOfPayment.ToString() : ddlPeriodOfPayment.SelectedValue = "2";
                ddlMethodOfPayment.SelectedValue = ddlMethodOfPayment.Items.FindByValue(customer.MethodOfPayment.ToString()) != null ? customer.MethodOfPayment.ToString() : ddlMethodOfPayment.SelectedValue = "1";
                ddlCustomerStatus.SelectedValue = ddlCustomerStatus.Items.FindByValue(customer.CustomerStatusID.ToString()) != null ? customer.CustomerStatusID.ToString() : ddlCustomerStatus.SelectedValue = Consts.enCustomerStatus.active.ToString();
                ddlPaidBy.SelectedValue = ddlPaidBy.Items.FindByValue(customer.PaidBy.ToString()) != null ? customer.PaidBy.ToString() : ddlPaidBy.SelectedValue = "0";
                ddlCredit.SelectedValue = ddlCredit.Items.FindByValue(customer.Credit.ToString()) != null ? customer.Credit.ToString() : ddlCredit.SelectedValue = "3";
                ddlCities.SelectedValue = !string.IsNullOrEmpty(customer.CityAddress) && ddlCities.Items.FindByText(customer.CityAddress.TrimStart().TrimEnd()) !=null ? customer.CityAddress.TrimStart().TrimEnd() : null;

                txtBoxPrice1.Text = customer.BoxPrice1 != null ? customer.BoxPrice1.Value.ToString("N2") : string.Empty;
                txtBoxPrice2.Text = customer.BoxPrice2 != null ? customer.BoxPrice2.Value.ToString("N2") : string.Empty;
                txtBoxPrice3.Text = customer.BoxPrice3 != null ? customer.BoxPrice3.Value.ToString("N2") : string.Empty;
                txtBoxPrice4.Text = customer.BoxPrice4 != null ? customer.BoxPrice4.Value.ToString("N2") : string.Empty;
                txtContactName.Value = customer.ContactMan != null ? customer.ContactMan.ToString().TrimEnd() : string.Empty;
                //ddlCredit.SelectedValue,
                txtCrushing.Text = customer.Crushing != null ? customer.Crushing.Value.ToString("N2") : string.Empty;
                txtCrushingAndTransport.Text = customer.CrushingAndTransport != null ? customer.CrushingAndTransport.Value.ToString("N2") : string.Empty;
                txtCustomerName.Text = customer.CustomerName != null ? customer.CustomerName.ToString().TrimEnd() : string.Empty;
                txtDateOfContract.Value = customer.DateOfContract != null ? customer.DateOfContract.ToString() : string.Empty;
                cbDeliveryApproval.Checked = customer.DeliveryApproval != null ? (bool)customer.DeliveryApproval : false;
                txtDescription.Text = customer.Description != null ? customer.Description.ToString().TrimEnd() : string.Empty;
                txtDescriptionAddress.Value = customer.DescriptionAddress != null ? customer.DescriptionAddress.ToString().TrimEnd() : string.Empty;
                txtMail.Value = customer.Email != null ? customer.Email.ToString().TrimEnd() : string.Empty;
                txtEmptyBox.Text = customer.EmptyBox != null ? customer.EmptyBox.Value.ToString("N2") : string.Empty;
                txtFax.Value = customer.Fax != null ? customer.Fax.ToString().TrimEnd() : string.Empty;
                txtFixedPayment.Text = customer.FixedPayment != null ? customer.FixedPayment.Value.ToString("N2") : string.Empty;
                txtAddress.Value = customer.FullAddress != null ? customer.FullAddress.ToString().TrimEnd() : string.Empty;
                txtMaxRetrive.Text = customer.MaxRetrive != null ? customer.MaxRetrive.ToString() : string.Empty;
                txtmaxUrgentRetrive.Text = customer.maxUrgentRetrive != null ? customer.maxUrgentRetrive.ToString() : string.Empty;
                txtEmptyBoxMaxEmount.Text = customer.EmptyBoxMaxEmount != null ? customer.EmptyBoxMaxEmount.ToString() : string.Empty;
                txtPO.Value = customer.PO != null ? customer.PO.ToString().TrimEnd() : string.Empty;
                txtRegistration.Text = customer.Registration != null ? customer.Registration.Value.ToString("N2") : string.Empty;
                txtRetrive.Text = customer.Retrive != null ? customer.Retrive.Value.ToString("N2") : string.Empty;
                txtScan.Text = customer.Scan != null ? customer.Scan.Value.ToString("N2") : string.Empty;
                txtPhone1.Value = customer.Telephon1 != null ? customer.Telephon1.ToString().TrimEnd() : string.Empty;
                txtPhone2.Value = customer.Telephon2 != null ? customer.Telephon2.ToString().TrimEnd() : string.Empty;
                txtTransport.Text = customer.Transport != null ? customer.Transport.Value.ToString("N2") : string.Empty;
                txtTransportAndContract.Text = customer.TransportAndContract != null ? customer.TransportAndContract.Value.ToString("N2") : string.Empty;
                txtDateOfContract.Value = customer.DateOfContract != null ? customer.DateOfContract.ToString() : DateTime.Now.ToString();
                txtTransportAndIntake.Text = customer.TransportAndIntake != null ? customer.TransportAndIntake.Value.ToString("N2") : string.Empty;
                txtUrgentRetrive.Text = customer.UrgentRetrive != null ? customer.UrgentRetrive.Value.ToString("N2") : string.Empty;
                cbExtermYearReq.Checked = customer.IsExtermYearRequiered;
                cbBoxDescReq.Checked = customer.IsBoxDescRequiered;
                cbInsertData.Checked = customer.InsertData;
            }
        }
        private int getLinkTo()
        {
            int? value = rblLinkTo.SelectedItem != null? Helper.getInt(rblLinkTo.SelectedItem.Value):0;
            
            return value==null || value<0? 0 : (int)value;
        }
        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }
        private void clearMessages()
        {
            dvSucceedMessage.InnerText = "";
            dvSucceedMessage.Style["display"] = "none";
            dvErrorMessage.InnerText = "";
            dvErrorMessage.Style["display"] = "none";
        }

        protected void btnDepartments_Click(object sender, EventArgs e)
        {if(ddlCustomerID.SelectedValue!=null && ddlCustomerID.SelectedValue.Trim()!="0")
            Response.Redirect("CustomerDepartments.aspx?custID="+ ddlCustomerID.SelectedValue);
        else
                Response.Redirect("CustomerDepartments.aspx");
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("UsersManagerList.aspx?custID=" + ddlCustomerID.SelectedValue);

        }
    }
}