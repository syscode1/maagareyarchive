﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class WorkOrdering : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        public OrderedDictionary workDrivers
        {
            get
            {
                if (Session["WorkOrdering_Drivers"] != null)
                    return Session["WorkOrdering_Drivers"] as OrderedDictionary;
                return null;
            }
            set
            {
                Session["WorkOrdering_Drivers"] = value;
            }
        }
        public List<WorkOrderingUI> workOrders
        {
            get
            {
                if (Session["WorkOrdering_Orders"] != null)
                    return Session["WorkOrdering_Orders"] as List<WorkOrderingUI>;
                return null;
            }
            set
            {
                Session["WorkOrdering_Orders"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindData();
            }

        }

        private void bindData()
        {
            workOrders = OrdersController.getWorkOrders();
            dgResults.DataSource = workOrders;
            dgResults.DataBind();

                OrderedDictionary data = UserController.getUsersByWorkers(Consts.enJobs.Driver);
                if (data != null)
                {
                    data.Insert(0, 0, Consts.SELECT_DRIVER);
                }
                fillDdlOrderDict(data, ddlDrivers);
                ddlDrivers.SelectedValue = "0";
            

        }

        protected void btnOrder_Click(object sender, EventArgs e)
        {
            int numInShipping = 1;
            List<Shippings> shippings = new List<Shippings>();
            List<OrderIDs> orderIDs = new List<OrderIDs>();
            string shippingID = Helper.getShippingID(user.UserID);
            string[] items = hfIndex.Value.Split(',');
            string driver =!string.IsNullOrEmpty(ddlDrivers.SelectedValue)? ddlDrivers.SelectedValue:"";
            foreach (string item in items)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    int ind = Helper.getInt(item.Split(';')[0]).Value;

               //     bool isMark = item.Split(';')[1] == "1" ? true : false;
                    WorkOrderingUI workOrder = workOrders.Where(o => o.index == ind).FirstOrDefault();
                    shippings.Add(new Shippings()
                    {
                        ShippingID = shippingID,
                        CityAddress = workOrder.CityAddress,
                        ContactMan = workOrder.ContactMan,
                        CustomerID = workOrder.CustomerID,
                        DepartmentID = workOrder.DepartmentID,
                        FullAddress = workOrder.FullAddress,
                        Driver = driver,
                        NumInShipping = numInShipping,
                        OrderDate = DateTime.Now,
                        ShippingDate = DateTime.Now,
                        SumBoxes = workOrder.SumBoxes,
                        SumExterminateBoxes = workOrder.SumExterminateBoxes,
                        SumFiles = workOrder.SumFiles,
                        SumFullBoxes = workOrder.SumFullBoxes,
                        SumNewBoxOrder = workOrder.SumNewBoxOrder,
                        SumRetriveBoxes = workOrder.SumRetriveBoxes,
                        Urgent = workOrder.Urgent,
                        Telephone1=workOrder.Telephon1,
                        Telephone2=workOrder.Telephon2,
                        DoneStatus =(int)Consts.enDoneStatus.NotCommited,
               //         Color = isMark , 
                        LoginID = user.UserID,
                        Comment = workOrder.Urgent == true ? workOrder.Comment + Consts.URGENT : workOrder.Comment,
                    });
                    foreach (var id in workOrder.OrderIDs)
                    {
                        orderIDs.Add(new OrderIDs()
                        {
                            NumInOrder = id.NumInOrder,
                            OrderID = id.OrderID,
                            NumInShiping = numInShipping,

                        });
                    }

                }
                numInShipping++;
            }
            if (shippings.Count > 0)
            {
                bool isSucceed = ShippingController.insert(shippings);
                if (isSucceed)
                {
                    OrdersController.updateShippingRowID(orderIDs, shippings);
                    List<int> rowIds = shippings.Select(s => s.rowID).ToList();
                    List<string> files = OrdersController.getOrderByShipping(rowIds);
                    FilesController.updateLocation(files,true);
                    FileStatusController.Update(files, Consts.enFileStatus.sentToCustomer);
                    FileStatusArchiveController.Insert(files, Consts.enFileStatus.sentToCustomer,Helper.getInt( driver) );
                    Response.Redirect("WorkOrderingForDriver.aspx?id="+ shippingID+"&driver="+ddlDrivers.SelectedItem.Text+ "&isHistory=0");
                }
            }
        }

        protected void dgResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow )
            {
                WorkOrderingUI dataItem = e.Row.DataItem as WorkOrderingUI;
                if (dataItem.Urgent == true)
                    e.Row.BackColor = Color.Pink;
                if (dataItem.ShippingRowID != null)
                {
                    e.Row.Enabled = false;
                    e.Row.BackColor =dataItem.Urgent==true?Color.Pink:Color.FromArgb(1,151,195,108);
                }
            }
        }
    }
}








