﻿function isNumberKey(evt, maxChar) {
    
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46)
        return false;
    if (maxChar != "" && maxChar != undefined && evt.currentTarget.value.length>=maxChar)
        return false;
    return true;
}

function maxCharLength(evt, maxChar)
{
    
    if (maxChar != "" && maxChar != undefined && evt.currentTarget.value.length >= maxChar)
        return false;
    return true;
}

function exit() {
    window.location = "MainPage.aspx";
}
function exitArchive() {
    window.location = "MainPageArchive.aspx";
}


$(document).ready(function () {
    disableFrozen();
    var select = $("select")
    for (i = 0; i < select.length; i++)
        select[i].setAttribute("onload", "disableFrozen()");
});

function disableFrozen() {
    var options = $("option");
    for (i = 0; i < options.length; i++)
        if (options[i].text.indexOf("(מוקפא)") >= 0) {// ! if you change this value you must change it also in MA_DAL.MA_HELPER.Consts!
            options[i].style.color = "Grey";
            options[i].disabled = "true";
        }
}

function isCompleteField(textbox) {

    if (textbox.value.trim() == "") {
        drowField(true,textbox)
        return false;
    }

    drowField(false, textbox)
    return true;
}

function drowField(isDrow,field) {
    if(isDrow)
        //field.style.backgroundColor = "rgba(255, 0, 0, 0.27)";
        field.style.backgroundColor = "rgb(255, 182, 177)";
    else
        field.style.backgroundColor = "";
}


function exitCustomerScreen()
{
    window.location = "CustomerScreen.aspx?custID="+getParameterByName('custID');
}

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(function () {
    $("[id*=dgResultsR] td").hover(function () {
        $("td", $(this).closest("tr")).addClass("hover_row");
    }, function () {
        $("td", $(this).closest("tr")).removeClass("hover_row");
    });
    $("[id*=dgResultsR] td").click(function () {
        $("td", $(this).closest("tr")).addClass("hover_row");
    }, function () {
        $("td", $(this).closest("tr")).removeClass("hover_row");
    });
});