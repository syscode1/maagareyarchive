﻿
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MaagareyArchive
{
    public class MyCache
    {
        private List<string> _Cities = null;
        private Dictionary<int, string> _CustomersIds = null;
        private Dictionary<int, string> _CustomersNames = null;
     public List<string> Cities
        {
            get
            {
                if (_Cities == null)
                {
                    _Cities = (HttpContext.Current.Cache["Cities"] as List<string>);
                    if (_Cities == null)
                    {
                        ArchiveEntities contex = new ArchiveEntities();

                        _Cities = contex.Cities.OrderBy(x=>x.CityName).Select(c => c.CityName.TrimStart().TrimEnd()).ToList();
                        HttpContext.Current.Cache.Insert("Cities", _Cities);
                    }
                }
                return _Cities;
            }
            set
            {
                HttpContext.Current.Cache.Insert("Cities", _Cities);
            }
        }
public Dictionary<int,string> CustomersNames
        {
            get
            {
                if (_CustomersNames == null)
                {
                    _CustomersNames = (HttpContext.Current.Cache["CustomersNames"] as Dictionary<int, string>);
                    if (_CustomersNames == null)
                    {
                        ArchiveEntities contex = new ArchiveEntities();

                        OrderedDictionary customers = CustomersController.getAllActiveCustomersOrdered();
                        customers.Insert(0, 0, Consts.SELECT);
                        _CustomersNames = customers.Cast<DictionaryEntry>()
                       .OrderBy(r => r.Key.ToString() == "0" ? r.Key.ToString() : r.Value)
                       .ToDictionary(c => Helper.getIntNotNull(c.Key.ToString()), d =>d.Key.ToString()=="0"?d.Value.ToString() : string.Format("{0} - {1}",  d.Value, d.Key));
                        HttpContext.Current.Cache.Insert("CustomersNames", _CustomersNames);
                    }
                }
                return _CustomersNames;
            }
            set
            {
                HttpContext.Current.Cache.Insert("CustomersNames", _CustomersNames);
            }
        }
        public Dictionary<int,string> CustomersIds
        {
            get
            {
                if (_CustomersIds == null)
                {
                    _CustomersIds = (HttpContext.Current.Cache["CustomersIds"] as Dictionary<int, string>);
                    if (_CustomersIds == null)
                    {
                        ArchiveEntities contex = new ArchiveEntities();

                        OrderedDictionary customers = CustomersController.getAllActiveCustomersOrdered();
                        customers.Insert(0, 0, Consts.SELECT);
                        _CustomersIds = customers.Cast<DictionaryEntry>()
                       .OrderBy(r => r.Key)
                       .ToDictionary(c => Helper.getIntNotNull(c.Key.ToString()), d =>d.Key.ToString()=="0"?d.Value.ToString() : string.Format("{0} - {1}", d.Key, d.Value));
                        HttpContext.Current.Cache.Insert("CustomersIds", _CustomersIds);
                    }
                }
                return _CustomersIds;
            }
            set
            {
                HttpContext.Current.Cache.Insert("CustomersIds", _CustomersIds);
            }
        }
        public void ClearCitiesList()
        {
            HttpContext.Current.Cache.Remove("Cities");
        }
        public void ClearCustomersIdsList()
        {
            HttpContext.Current.Cache.Remove("CustomersIds");
        }
        public void ClearCustomersNamesList()
        {
            HttpContext.Current.Cache.Remove("CustomersNames");
        }
    }
}
