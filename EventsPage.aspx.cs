﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class EventsPage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rptResult.DataSource = EventController.getAllEvents(user.UserID,user.Job);
                rptResult.DataBind();
            }

        }

        protected void rptResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                (e.Item.FindControl("tdRowID") as HtmlTableCell).Style["font-weight"] = (e.Item.DataItem as EventsUI).IsRead ? "unset" : "bolder";

                (e.Item.FindControl("tdEventTypeName") as HtmlTableCell).Style["font-weight"] = (e.Item.DataItem as EventsUI).IsRead ? "unset" : "bolder";
            }
        }
        [System.Web.Services.WebMethod]
        public static string updateReadEvent(string rowID)
        {
            if (!string.IsNullOrEmpty(rowID))
            {
                return EventController.updateIsRead(Helper.getIntNotNull(rowID)) ? "1" : "0";
            }
            return "0";
        }
        
    }
}