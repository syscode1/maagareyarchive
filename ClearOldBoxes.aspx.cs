﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ClearOldBoxes : BasePage
    {

        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }


        string[] warehouseList = new string[] { "099", "100", "101", "102", "103" };
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                ddlFromWarehouse.DataSource = warehouseList;
                ddlFromWarehouse.DataBind();
                ddlToWarehouse.DataSource = warehouseList;
                ddlToWarehouse.DataBind();
            }
        }

        protected void txtViewReport_Click(object sender, EventArgs e)
        {

            exportViewData(true);
        }


        private void exportViewData (bool isView)

        {
            int fromWarehouse = 0;
            int toWarehouse = 0;
            int fromRow = 0;
            int toRow = 0;

            if (!string.IsNullOrEmpty(ddlFromWarehouse.SelectedValue))
                fromWarehouse = Helper.getInt(ddlFromWarehouse.SelectedValue).Value;
            else
                fromWarehouse = Helper.getIntNotNull(ddlFromWarehouse.Items[0].Value);

            if (!string.IsNullOrEmpty(ddlToWarehouse.SelectedValue))

                toWarehouse = Helper.getIntNotNull(ddlToWarehouse.SelectedValue);
            else
                toWarehouse = Helper.getIntNotNull(ddlToWarehouse.Items[ddlToWarehouse.Items.Count - 1].Value);

            if (!string.IsNullOrEmpty(txtFromRow.Text))
                fromRow = Helper.getInt(txtFromRow.Text).Value;

            if (!string.IsNullOrEmpty(txtToRow.Text))
                toRow = Helper.getInt(txtToRow.Text).Value;


            List<Cells> cellsList = MignazaController.getCellsList();
            List<ShelfBoxCalc> mignaza = new List<ShelfBoxCalc>();

            switch (rblReport.SelectedValue)
            {
                case "populated":
                    {
                        mignaza = MignazaController.getMignazaPopulated(fromWarehouse, toWarehouse, fromRow, toRow);
                        break;
                    }
                case "max":
                    {
                        mignaza= MignazaController.getMignazaMax(fromWarehouse, toWarehouse, fromRow, toRow);
                        break;
                    }
                case "empty":
                    {
                        mignaza = MignazaController.getMignazaEmpty(fromWarehouse, toWarehouse, fromRow, toRow);
                        break;
                    }
                default:
                    break;
            }
            if (mignaza != null)
            {
                DataTable dt = buildGrig(mignaza, cellsList);
                if (isView)
                {
                    dgResults.DataSource = dt;
                    dgResults.AutoGenerateColumns = true;
                    dgResults.DataBind();
                }
                else
                {
                    exportToExcell(dt, string.Format("archiv_structure_report_{0}", DateTime.Now.ToString("dd_MM_yyyy")));
                }
                // export(mignaza, cellsList, "mignaza");
            }
        }
        //public static bool export(List<Mignaza> data,List<Cells> cells, string fileName, Dictionary<string, string> searchParams = null, string[] sumBy = null)
        //{
        //    HttpResponse Response = System.Web.HttpContext.Current.Response;
        //    OfficeOpenXml.ExcelPackage excel = new ExcelPackage();
        //    Dictionary<string, int> sumByDict = new Dictionary<string, int>();
        //    if (sumBy != null && sumBy.Count() > 0)
        //    {
        //        foreach (string item in sumBy)
        //        {
        //            sumByDict.Add(item, 0);
        //        }
        //    }
        //    try
        //    {
        //        var GridView1 = new GridView();
        //        GridView1.DataSource = data;
        //        GridView1.DataBind();
        //        var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
        //        var totalCols =3+ cells.Count ;
        //        var totalRows = GridView1.Rows.Count;
        //        var headerRow = GridView1.HeaderRow;
        //        int numOfParam = 0;
        //        workSheet.Cells["A:A"].AutoFitColumns();
        //        if (data.Count == 0)
        //        {
        //            workSheet.Cells[numOfParam + 2, 1].Value = MA_DAL.MA_HELPER.Consts.NO_DATA_TO_DISPLAY;
        //        }
        //        else
        //        {
        //            workSheet.Cells[1,1].Value = "מחסן";
        //            workSheet.Cells[1,1].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //            workSheet.Cells[1,1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
        //            workSheet.Cells[1,1].Style.Font.Bold = true;
        //            workSheet.Cells[1,2].Value = "שורה";
        //            workSheet.Cells[1,2].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //            workSheet.Cells[1,2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
        //            workSheet.Cells[1,2].Style.Font.Bold = true;
        //            workSheet.Cells[1,3].Value = "מדף";
        //            workSheet.Cells[1,3].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //            workSheet.Cells[1,3].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
        //            workSheet.Cells[1,3].Style.Font.Bold = true;
        //            for (var i = 4; i <= totalCols; i++)
        //            {
        //                workSheet.Cells[1, i].Value = cells[i - 4].cellName;
        //                workSheet.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                workSheet.Cells[1, i].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
        //                workSheet.Cells[1, i].Style.Font.Bold = true;
        //            }
        //            int rowIndex = 2;
        //            for (var j = 1; j <= totalRows; j++)
        //            {
        //               List< ExcelRange> warehouse = (from cell in workSheet.Cells["a:a"]       
        //                           where cell.Value.ToString().Equals(data[j - 1].ShelfID.Substring(0, 3))  
        //                           select workSheet.Cells[cell.Start.Row, 2]).ToList();
        //                List<ExcelRange> row = (from cell in warehouse
        //                                              where cell.Value.ToString().Equals(data[j - 1].ShelfID.Substring(3, 2))
        //                                              select workSheet.Cells[cell.Start.Row, 3]).ToList();
        //                ExcelRange shelf = (from cell in row
        //                                        where cell.Value.ToString().Equals(data[j - 1].ShelfID.Substring(7, 2))
        //                                        select workSheet.Cells[cell.Start.Row, 3]).FirstOrDefault();

                        
        //                int currentRowIndex;
        //                if (shelf == null)
        //                {
        //                    currentRowIndex = rowIndex++;
        //                    workSheet.Cells[currentRowIndex, 1].Value = data[j - 1].ShelfID.Substring(0, 3);
        //                    workSheet.Cells[currentRowIndex, 2].Value = data[j - 1].ShelfID.Substring(3, 2);
        //                    workSheet.Cells[currentRowIndex, 3].Value = data[j - 1].ShelfID.Substring(7, 2);
        //                }
        //                else
        //                    currentRowIndex = shelf.Start.Row;

        //                for (var i = 4; i <= totalCols; i++)
        //                {
        //                    ExcelRangeBase cell = workSheet.Cells.Where(x => x.Value.ToString() == data[j - 1].ShelfID.Substring(5, 2)).FirstOrDefault();   
        //                    workSheet.Cells[currentRowIndex, cell.Start.Column].Value =data[j-1].NumBoxCapacity ;
        //                }
        //            }
        //        }
        //        using (var memoryStream = new MemoryStream())
        //        {

        //            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //            Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
        //            excel.SaveAs(memoryStream);
        //            memoryStream.WriteTo(Response.OutputStream);
        //            Response.Flush();
        //        }

        //    }

        //    catch (Exception ex)
        //    {
        //        logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
        //        return false;
        //    }
        //    finally
        //    {

        //        excel.Dispose();
        //        Response.End();
        //    }
        //    return true;
        //}

        private DataTable buildGrig(List<ShelfBoxCalc> data, List<Cells> cells)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("מחסן");
            dt.Columns.Add("שורה");
            dt.Columns.Add("מדף");
            var totalCols = 3 + cells.Count;

            for (var i = 4; i <= totalCols; i++)
            {

                dt.Columns.Add(cells[i - 4].cellName);
            }
            int rowIndex = 0;
            for (var j = 1; j <= data.Count(); j++)
            {
                DataRow[] dr = dt.Select("מחסן =" + data[j - 1].ShelfID.Substring(0, 3) +
                       "And שורה =" + data[j - 1].ShelfID.Substring(3, 2) +
                       "And מדף =" + data[j - 1].ShelfID.Substring(7, 2));

                int currentRowIndex;
                if (dr == null || dr.Count() == 0)
                {

                    dt.Rows.Add();
                    currentRowIndex = rowIndex++;
                    dt.Rows[currentRowIndex][0] = data[j - 1].ShelfID.Substring(0, 3);
                    dt.Rows[currentRowIndex][1] = data[j - 1].ShelfID.Substring(3, 2);
                    dt.Rows[currentRowIndex][2] = data[j - 1].ShelfID.Substring(7, 2);

                }
                else
                    currentRowIndex = dt.Rows.IndexOf(dr[0]);



               // for (var i = 3; i <= totalCols; i++)
                {
                    
                    dt.Rows[currentRowIndex][data[j - 1].ShelfID.Substring(5, 2)] = data[j - 1].Value;
                }
            }
            return dt;

        }

        protected void btnExportReport_Click(object sender, EventArgs e)
        {
            exportViewData(false);
        }

        public static bool exportToExcell(DataTable data, string fileName, Dictionary<string, string> searchParams = null, string[] sumBy = null)
        {
            HttpResponse Response = System.Web.HttpContext.Current.Response;
            ExcelPackage excel = new ExcelPackage();
            Dictionary<string, int> sumByDict = new Dictionary<string, int>();
            if (sumBy != null && sumBy.Count() > 0)
            {
                foreach (string item in sumBy)
                {
                    sumByDict.Add(item, 0);
                }
            }

            try
            {

                var GridView1 = new GridView();
                GridView1.DataSource = data;
                GridView1.DataBind();
                var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                var totalCols = data.Columns.Count > 0 ? GridView1.Rows[0].Cells.Count : 0;
                var totalRows = GridView1.Rows.Count;
                var headerRow = GridView1.HeaderRow;


                workSheet.Cells["A:A"].AutoFitColumns();
                if (data.Columns.Count == 0)
                {
                    workSheet.Cells[ 2, 1].Value = Consts.NO_DATA_TO_DISPLAY;
                }
                else
                {
                    for (var i = 0; i < totalCols; i++)
                    {
                        string headerText = ExcelHeaders.ResourceManager.GetString(headerRow.Cells[i ].Text);
                        workSheet.Cells[1, i + 1].Value = !string.IsNullOrEmpty(headerText) ? headerText : headerRow.Cells[i].Text;
                        workSheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                        workSheet.Cells[1, i + 1].Style.Font.Bold = true;
                    }
                    int rowIndex = 1;
                    for (var j = 0; j < totalRows; j++)
                    {
                        rowIndex++;
                        int currentRowIndex = rowIndex;

                        for (var i = 0; i < totalCols; i++)
                        {
                            workSheet.Cells[currentRowIndex, i+1].Value = data.Rows[j][i];
                        }
                    
                    }
                }
                using (var memoryStream = new MemoryStream())
                {

                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                }

            }

            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
            finally
            {

                excel.Dispose();
                Response.End();
            }
            return true;
        }

    }

}