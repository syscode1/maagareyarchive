﻿using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ArchiveMaintenance :BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCustomerAgreement_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerAgreement.aspx");
        }

        protected void btnCustomerBoxesSale_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerBoxesSale.aspx");
        }

        protected void btnDeactivateCustomer_Click(object sender, EventArgs e)
        {
            Response.Redirect("DeactivateCustomer.aspx");
        }


        protected void btnCustomerSubject_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerSubjects.aspx");
        }

        protected void btnArchiveStructure_Click(object sender, EventArgs e)
        {
            Response.Redirect("ArchiveStructure.aspx");

        }

        protected void btnUsersManager_Click(object sender, EventArgs e)
        {
            Response.Redirect("UsersManager.aspx");

        }

        protected void btnCustomerDepartment_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerDepartments.aspx");

        }

        protected void btnDaylyReport_Click(object sender, EventArgs e)
        {
            Response.Redirect("DaylyReport.aspx");
        }

        protected void btnInvoices_Click(object sender, EventArgs e)
        {
            Response.Redirect("Invoices.aspx");
        }

        protected void btnDailyReportDetailed_Click(object sender, EventArgs e)
        {
            Response.Redirect("DailyReportDetailed.aspx");
        }

        protected void btnClearOldBoxes_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClearOldBoxes.aspx");
        }
    }
}