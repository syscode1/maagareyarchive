﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ExportFilesReport : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                OrderedDictionary data;
                if (user.ArchiveWorker == true)
                {

                    trCustomers.Visible = true;
                    fillCustomerDdl();
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedItem.Value))
                    {
                        data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomer.SelectedItem.Value));
                        data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
                        fillDdlOrderDict(data, ddlDepartment);
                    }
                }
                else {
                    trCustomers.Visible = false;
                    data = DepartmentController.GetUserDepartmentsDictionary(user);
                    data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
                    fillDdlOrderDict(data, ddlDepartment);

                }

            }
        }
        private void fillCustomerDdl()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
        }


        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbBoxes.Checked = true;
            OrderedDictionary data;
            data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomer.SelectedItem.Value));
            data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
            fillDdlOrderDict(data, ddlDepartment);
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "showHideByRb()", true);
        }

        #region export

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (rbBoxes.Checked)
            {
                exportBoxes();
            }
            else {
                exportFiles();
            }
        }

        private bool exportBoxes()
        {
            try
            {

            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
            int? department = string.IsNullOrEmpty(ddlDepartment.SelectedValue)|| ddlDepartment.SelectedValue=="0"  ? null : (int?)Convert.ToInt16(ddlDepartment.SelectedValue);
            DateTime? fromInsertDate = Helper.getDate(txtFromInsertDate.Value);
            DateTime? toInsertDate = Helper.getDate(txtToInsertDate.Value);
            List<Boxes> boxes = BoxesController.exportBoxes(customerID, department,fromInsertDate, toInsertDate);
            Dictionary <string, string> param= new Dictionary<string, string>();
            param.Add(ExcelHeaders.ResourceManager.GetString("Customer"), ddlCustomer.SelectedItem!=null? ddlCustomer.SelectedItem.Text:user.CustomerID.ToString());
            param.Add(ExcelHeaders.ResourceManager.GetString("Department"), ddlDepartment.SelectedItem!=null? ddlDepartment.SelectedItem.Text:"");
            param.Add(ExcelHeaders.ResourceManager.GetString("FromInsertDate"), txtFromInsertDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("ToInsertDate"), txtToInsertDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("NumOfBoxes"), boxes.Count.ToString());

            return exportToExcell(boxes, "export_files_data", param);


            }

            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
            
        }

        private bool exportFiles()
        {
            try
            {
                int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
                int? department = string.IsNullOrEmpty(ddlDepartment.SelectedValue) || ddlDepartment.SelectedValue == "0" ? null : (int?)Convert.ToInt16(ddlDepartment.SelectedValue);
                DateTime? fromStartDate = Helper.getDate(txtStartDate.Value);
                DateTime? toStartDate = Helper.getDate(txtEndDate.Value);
                DateTime? fromInsertDate = Helper.getDate(txtFromInsertDate.Value);
                DateTime? toInsertDate = Helper.getDate(txtToInsertDate.Value);
                int? fromYear = Helper.getInt(txtFromYear.Text);
                int? toYear = Helper.getInt(txtToYear.Text);
                List<Files> files = FilesController.exportFiles(customerID, department, fromStartDate, toStartDate,
                      fromInsertDate, toInsertDate, fromYear, toYear, Helper.getInt( txtSenderNum.Text));

                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add(ExcelHeaders.ResourceManager.GetString("Customer"), ddlCustomer.SelectedItem!=null? ddlCustomer.SelectedItem.Text:"");
                param.Add(ExcelHeaders.ResourceManager.GetString("Department"), ddlDepartment.SelectedItem.Text);

                param.Add(ExcelHeaders.ResourceManager.GetString("FromInsertDate"), txtFromInsertDate.Value);
                param.Add(ExcelHeaders.ResourceManager.GetString("ToInsertDate"), txtToInsertDate.Value);
                param.Add(ExcelHeaders.ResourceManager.GetString("FromStartDate"), txtStartDate.Value);
                param.Add(ExcelHeaders.ResourceManager.GetString("ToEndDate"), txtEndDate.Value);
                param.Add(ExcelHeaders.ResourceManager.GetString("FromYear"), txtFromYear.Text);
                param.Add(ExcelHeaders.ResourceManager.GetString("ToYear"), txtToYear.Text);
                param.Add(ExcelHeaders.ResourceManager.GetString("NumOfFiles"), files.Count.ToString());
                return exportToExcell(files, "export_files_data", param);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }

}

        #endregion

    }
}