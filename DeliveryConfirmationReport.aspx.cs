﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class DeliveryConfirmationReport : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }


        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !string.IsNullOrEmpty(Request.QueryString["rowID"]))
            {
                if (Request.UrlReferrer != null)
                    Session["DeliveryConfirmationReport_historyPage"] = Request.UrlReferrer.PathAndQuery.Substring(Request.UrlReferrer.PathAndQuery.LastIndexOf("/")+1);
                bindData();
            }
        }

        private void bindData()
        {
            int rowID = Helper.getIntNotNull(Request.QueryString["rowID"]);
            List<Orders> orders=OrdersController.getOrderByShipping(rowID);
            List<string> fileIDs = orders.Select(o => o.FileID).ToList();
            List<string> BoxIDs = orders.Where(o=>string.IsNullOrEmpty(o.FileID) && !string.IsNullOrEmpty(o.BoxID)).Select(o => o.BoxID).ToList();
            rptResults.DataSource = FilesController.getFilesByID(fileIDs,BoxIDs);
            rptResults.DataBind();

            ShippingsUI ship = ShippingController.getSipping(rowID);
            lblAddress.Text = ship.FullAddress;
            lblCity.Text = ship.CityAddress;
            lblCust.Text = ship.CustomerName;
            lblDep.Text = ship.DepartmentName;
            if (ship.Driver != null)
                spnDriver.InnerText = ship.Driver.Trim();
            sumExterminateBoxes.Text = ship.SumExterminateBoxes.ToString();
            sumFullBoxes.Text = ship.SumFullBoxes.ToString();
            sumNewBoxe.Text = ship.SumNewBoxOrder.ToString();
            sumRetriveBoxes.Text = ship.SumRetriveBoxes.ToString();
            if (!string.IsNullOrEmpty(ship.DeliveryID))
            {
                enableFields(true, btnPrint);
                enableFields(false, btnUpdate);
                spnReceivedBy.Style["display"] = "";
                ddlReceivedBy.Style["display"] = "none";
                spnReceivedBy.InnerText = UserController.getUserByUserID(!string.IsNullOrEmpty( ship.ReceivedBy)?Helper.getIntNotNull(ship.ReceivedBy) :0).FullName;
                lblReceivedDate.InnerText = ship.ReceivedDate.ToString();
                header.InnerText += ship.DeliveryID;
            }
            else
            {
                enableFields(false, btnPrint);
                enableFields(true, btnUpdate);
                spnReceivedBy.Style["display"] = "none";
                ddlReceivedBy.Style["display"] = "";
                lblReceivedDate.InnerText = DateTime.Now.ToString();
                OrderedDictionary data = new OrderedDictionary();
            data = UserController.getUsersByCustomer(ship.CustomerID);
                if (!string.IsNullOrEmpty(ship.Driver))
                {
                    Users driver = UserController.getUserByUserID(Helper.getIntNotNull(ship.Driver));
                    if(driver!=null)
                        data.Insert(0, driver.UserID, driver.FullName);
                }
                
            if (data.Count > 0)
                fillDdlOrderDict(data, ddlReceivedBy);
            }

        
    }

        private void enableFields(bool enable, WebControl control)
        {
            if (enable)
            {
                control.Style.Clear();
            }
            else
            {
                control.Style["pointer-events"] = "none";
                control.Style["opacity"] = "0.4";
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
            ArchiveEntities contex = new ArchiveEntities();
            if (!string.IsNullOrEmpty(Request.QueryString["rowID"]))
            {
                bool isSucceed;
                int rowID = Helper.getIntNotNull(Request.QueryString["rowID"]);
                Shippings ship = contex.Shippings.Where(s => s.rowID == rowID).FirstOrDefault();
                ship.ReceivedBy = ddlReceivedBy.SelectedValue;
                ship.ReceivedDate = DateTime.Now;
                ship.DoneStatus = (int)Consts.enDoneStatus.Done;
                ship.DeliveryID = Helper.getDeliveryID(user.UserID);
                contex.SaveChanges();
                isSucceed= OrdersController.updateOrderStatusDone(ship.rowID,user.UserID);
                    if (isSucceed)
                    {
                        if(Session["DeliveryConfirmationReport_historyPage"]!=null)
                        Response.Redirect(Session["DeliveryConfirmationReport_historyPage"].ToString());
                        //bindData();
                        //showIndicationMessage(true, Consts.SAVED_SUCCEED);
                    }
                    else
                        showIndicationMessage(false, Consts.SAVED_FAIL); ;
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                showIndicationMessage(false, Consts.SAVED_FAIL);
            }
        }

        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }

        [System.Web.Services.WebMethod]
        public static string checkPassword(string userID, string password)
        {
            if (!string.IsNullOrEmpty(userID))
                return UserController.getUserByUserID(Helper.getIntNotNull(userID)).Password.TrimEnd() == password.TrimEnd() ? "1" : "0";
            return "0";
        }
    }
}