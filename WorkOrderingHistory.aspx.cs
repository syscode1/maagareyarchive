﻿using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class WorkOrderingHistory : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindData();
            }

        }

        private void bindData()
        {
            DateTime fromDate, toDate;
            if( DateTime.TryParse(Request.QueryString["fromDate"],out fromDate))
                header2.InnerText += "  מתאריך : XXX \n".Replace("XXX", fromDate.ToShortDateString());

            if (!DateTime.TryParse(Request.QueryString["toDate"], out toDate))
                toDate = DateTime.Now;
            else
                header2.InnerText += " עד תאריך : XXX \n".Replace("XXX", toDate.ToShortDateString());
            string shippingID = Request.QueryString["shippingID"];

            if(!string.IsNullOrEmpty(shippingID))
                header2.InnerText += " סידור עבודה : XXX \n".Replace("XXX", shippingID);

            int customerID = !string.IsNullOrEmpty(Request.QueryString["custID"])
                ?Helper.getInt(Request.QueryString["custID"]).Value :0;
            if(customerID>0)
                btnBackCustomer.Style["display"] = "";
            //workOrders = OrdersController.getWorkOrders();
            dgResultsR.DataSource = ShippingController.getGroupShippings(fromDate,toDate,shippingID,customerID);
            dgResultsR.DataBind();
        }

        protected void dgResultsR_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ShippingsUI dataItem = e.Row.DataItem as ShippingsUI;
                if (dataItem.DeliveryID != null && !string.IsNullOrEmpty(dataItem.DeliveryID))
                {
                    e.Row.BackColor = Color.Gray;
                }
            }
        }
    }
}