﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="DaylyReportDataPopup.aspx.cs" Inherits="MaagareyArchive.DaylyReportDataPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

                     <asp:DataGrid ID="dgResults" runat="server"
                        AutoGenerateColumns="false" CssClass="grid"
                         Width="300px">
                        <%--DataKeyField="FileID"--%>

                        <HeaderStyle CssClass="header" />

                        <AlternatingItemStyle CssClass="alt" />
                        <ItemStyle CssClass="row" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="לקוח" ItemStyle-CssClass="ltr" >
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                          <span id="spnCustomerID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "CustomerID"))%></span></a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="כמות" ItemStyle-CssClass="ltr" >
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                          <span id="spnCustomerID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "Sum"))%></span></a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                </Columns>
                         </asp:DataGrid>

</asp:Content>
