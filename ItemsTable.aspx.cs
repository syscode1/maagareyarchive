﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ItemsTable : BasePage
    {
        public int rownum = 1;

        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                bindGridData();
        }

        private void bindGridData()
        {
            dgResults.DataSource = ItemsController.getAllItems();
            dgResults.DataBind();
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            ItemsController.insertNewItem(new ItemsAll()
            {
                ItemName = txtItemName.Text,
                IsActive=true,
                UpdatedDate=DateTime.Now,
            });
            txtItemName.Text = "";
            bindGridData();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (DataGridItem item in dgResults.Items)
            {

                if ((item.FindControl("cbRemove") as CheckBox).Checked)
                {
                    string itemId = (item.FindControl("itemID") as HtmlGenericControl).InnerText;
                    if (!string.IsNullOrEmpty(itemId))
                        SuppliersItemsController.RemoveSupplierItemsByItemId((int)Helper.getInt(itemId));
                        ItemsController.removeItemByID((int)Helper.getInt(itemId));
                }
            }
            bindGridData();
        }
    }
}