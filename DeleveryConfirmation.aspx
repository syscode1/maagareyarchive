﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DeleveryConfirmation.aspx.cs" Inherits="MaagareyArchive.DeleveryConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
       

        function cbDone_checkedChanged(evt) {
            if (evt.checked == true) {
                var rowID = $("[id*= 'spnRowID']", evt.parentElement.parentElement)[0].innerText;
                href = "DeliveryConfirmationReport.aspx?rowID=" + rowID;
                window.location = href;
            }
            return false;


        }
    </script>
    <h1>הפקת דו"ח מסירה</h1>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlCustomerName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDepartment" runat="server" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
             <%--           <asp:DropDownList ID="ddlUser" runat="server" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>--%>
                    </td>
                </tr>
            </table>

            <asp:GridView ID="dgResultsR" runat="server" CssClass="grid"
                EnableViewState="true" Width="100%"
                AutoGenerateColumns="false"
                SelectedRowStyle-BackColor="Red"
                EnablePersistedSelection="true"
                DataKeyNames="rowID">
                <HeaderStyle CssClass="header" />
                <AlternatingRowStyle CssClass="alt" />
                <RowStyle CssClass="row" />
                <Columns>
                    <asp:TemplateField HeaderText="מס' אישור" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="spnShippingID" runat="server"><%#(Eval("ShippingID"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="לקוח" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="spnCustomerName" runat="server"><%#(Eval("CustomerName"))%> </span>
                            <span id="spnRowID" runat="server" style="display:none"><%#(Eval("rowID"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="מחלקה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span id="spnDepartmentName" runat="server"><%#(Eval("DepartmentName"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="עיר" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span><%# (Eval("CityAddress"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="כתובת" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span><%#(Eval( "FullAddress"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="סה''כ תיקים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span><%#(Eval( "SumFiles"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="סה''כ ארגזים למסירה" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <span><%#(Eval( "SumBoxes"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="תאריך חתימה" ItemStyle-CssClass="ltr">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                          <span><%#(DateTime?)(Eval( "ReceivedDate"))==DateTime.MaxValue?"":(Eval( "ReceivedDate"))%> </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="בחר" ItemStyle-CssClass="ltr">
                        <HeaderStyle CssClass="t-center" />
                        <ItemStyle CssClass="t-natural va-middle" />
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="cbCheck" onClick='cbDone_checkedChanged(this)' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div id="dvMessage" runat="server" style="display: none">לא נמצאו נתונים</div>

                            <table style="float: left;width:100%;padding-top:30px">
                <tr>
                     <td style="width: 662px;text-align: right;">
                    <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />
                </td>
                  <td>                        
                      </td> 
                </tr>
                                <tr>
                                                        <td  style="text-align: right;">
                        <input id="btnBackCustomer" type="button" value="חזרה למסך לקוח" onclick="exitCustomerScreen()" runat="server" style="display:none" />
                     </td>

                                </tr>
                                </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
