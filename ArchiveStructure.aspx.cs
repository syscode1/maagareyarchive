﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ArchiveStructure : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }


        string[] cellsList = new string[] { "A", "B", "C", "D", "E","F", "G", "H", "I", "J",
                                            "K", "L", "M", "N", "O","P", "Q", "R", "S", "T",
                                            "U", "V", "W", "X", "Y","Z",};
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                ddlFromWarehouse.DataSource=Consts.warehouseList;
                ddlFromWarehouse.DataBind();
                ddlToWarehouse.DataSource = Consts.warehouseList;
                ddlToWarehouse.DataBind();
                Dictionary<int,string> list = new Dictionary<int, string>();
                for (int i = 0; i < cellsList.Length; i++)
                {
                    list.Add(i, cellsList[i]);
                }
                ddlFromCell.DataSource= list;
                ddlFromCell.DataTextField = "Value";
                ddlFromCell.DataValueField = "Key";
                ddlFromCell.DataBind();
                ddlToCell.DataSource = list;
                ddlToCell.DataTextField = "Value";
                ddlToCell.DataValueField = "Key";
                ddlToCell.DataBind();
            }
        }

        protected void txtInsert_Click(object sender, EventArgs e)
        {
            bool isValid = true;
            int fromWarehouse=0;
            int toWarehouse = 0;
            int fromRow = 0;
            int toRow = 0;
            int fromCell_index = 0;
            int toCell_index =0;
            int fromCell = 0;
            int toCell =0;
            int fromShelf = 0;
            int toShelf = 0;
           int amount = 0;
            bool isNumericFrom=true;
            bool isNumericTo=true;
            if (!string.IsNullOrEmpty(ddlFromWarehouse.SelectedValue))
                fromWarehouse = Helper.getInt(ddlFromWarehouse.SelectedValue).Value;
            else
                isValid = false;

            if (!string.IsNullOrEmpty(ddlToWarehouse.SelectedValue))

                 toWarehouse = Helper.getInt(ddlToWarehouse.SelectedValue).Value;
            else
                isValid = false;


            if (!string.IsNullOrEmpty(txtFromRow.Text))
             fromRow = Helper.getInt(txtFromRow.Text).Value;
            else
                isValid = false;

            if (!string.IsNullOrEmpty(txtToRow.Text))
             toRow = Helper.getInt(txtToRow.Text).Value;
            else
                isValid = false;

            if (!string.IsNullOrEmpty(txtFromCell.Text))
                isNumericFrom = int.TryParse(txtFromCell.Text, out fromCell);
            else
                isValid = false;

            if (!string.IsNullOrEmpty(txtToCell.Text))
                isNumericTo = int.TryParse(txtToCell.Text, out toCell);
            else
                isValid = false;

            if(isNumericFrom!=isNumericTo )//|| (isNumericFrom==true && isNumericFrom>isNumericTo )
                isValid = false;

            fromCell_index = Helper.getInt(ddlFromCell.SelectedValue).Value;
            toCell_index = Helper.getInt(ddlFromCell.SelectedValue).Value;

            if (!string.IsNullOrEmpty(txtFromShelf.Text))
            fromShelf = Helper.getInt(txtFromShelf.Text).Value;
            else
                isValid = false;

            if (!string.IsNullOrEmpty(txtToShelf.Text))
            toShelf = Helper.getInt(txtToShelf.Text).Value;
            else
                isValid = false;

            if (!string.IsNullOrEmpty(txtAmount.Text))
            amount = Helper.getInt(txtAmount.Text).Value;
            else
                isValid = false;

            if (!isValid)
            {
                //error message
                return;
            }
           
            for (int w = fromWarehouse; w <= toWarehouse; w++)
            {
                for (int r = fromRow; r <= toRow; r++)
                {
                    for (int c = fromCell_index; c <= toCell_index; c++)
                    {
                        int from = isNumericFrom ? fromCell :cellsList.ToList().IndexOf(txtFromCell.Text.ToUpper());
                        int length =  MignazaController.getCellLength(cellsList[c] + (isNumericTo ?"" : txtToCell.Text.ToUpper()));
                        int minlength = Math.Min(length, isNumericTo ? (fromCell-toCell)*-1 : (from- cellsList.ToList().IndexOf(txtToCell.Text.ToUpper())) * -1);
                        for (int ci =from; ci <=  from+minlength; ci++)
                        {
                            for (int s = fromShelf; s <= toShelf; s++)
                            {
                                string shelfID = string.Format("{0}{1}{2}{3}", w < 100 ? "0" + w.ToString() : w.ToString(),
                                                                               r < 10 ? "0" + r.ToString() : r.ToString(),
                                                                               cellsList[c]+ (isNumericFrom?ci.ToString():cellsList[ci]),
                                                                               s < 10 ? "0" + s.ToString() : s.ToString());
                                Mignaza mignaza = new Mignaza()
                                {
                                    ShelfID = shelfID,
                                    NumBoxCapacity = amount,
                                };
                                MignazaController.insertUpdate(mignaza);
                            }
                        }
                    }
                }
            }

        }
    }
}