﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="OrderConfirmation.aspx.cs" Inherits="MaagareyArchive.OrderConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
         function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }
        function printPage() {
            window.print();
            return false;
        }
    </script>
      <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <h1 >הזמנה</h1>
          <h2> הזמנתך נקלטה במערכת ומספרה:</h2>
            <h3> <span id="orderID" runat="server" style="font-size: 25px;"> </span> </h3>  
            </ContentTemplate>
         </asp:UpdatePanel>

            <table style="margin-top:24px;width:100%">
            <tr>
                <td style="width: 462px;text-align: right;">
                    <input type="button" value="חזרה לתפריט הראשי" onclick="exit()" runat="server" />
                </td>
                <td>
                   <asp:Button ID="btnPrint" Text="הדפסה" OnClientClick="return printPage()" runat="server"/>
                </td>
               
            </tr>
        </table>
</asp:Content>
