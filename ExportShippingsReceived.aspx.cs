﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ExportShippingsReceived : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (user.ArchiveWorker == true)
                {
                    fillCustomerDdl();
                    trCustomers.Visible = true;
                    
                }
                else
                {
                    trCustomers.Visible = false;
                }
            }

        }
        private void fillCustomerDdl()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
            int? department = null;// user.ArchiveWorker == true ? null : (int?)user.DepartmentID;
            DateTime? fromReceivedDate = Helper.getDate(txtFromDate.Value);
            DateTime? toReceivedDate = Helper.getDate(txtToDate.Value);
            string shippingID = txtShippingNumber.Text;
            dgResults.DataSource = ShippingController.exportGroupedShippings(customerID, department, fromReceivedDate, toReceivedDate, shippingID);
            dgResults.DataBind();
            btnPrint.Visible = true;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {

            List<Shippings> orders = getData();
            Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add(ExcelHeaders.ResourceManager.GetString("Customer"), ddlCustomer.SelectedItem != null ? ddlCustomer.SelectedItem.Text : user.CustomerID.ToString());
                param.Add(ExcelHeaders.ResourceManager.GetString("FromReceivedDate"), txtFromDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("ToReceivedDate"), txtToDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("ShippingNum"), txtShippingNumber.Text);
            exportToExcell(orders, "export_received shippings_data", param);

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }



        private List<Shippings> getData()
        {
            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
            int? department = null;// user.ArchiveWorker == true ? null : (int?)user.DepartmentID;
            DateTime? fromReceivedDate = Helper.getDate(txtFromDate.Value);
            DateTime? toReceivedDate = Helper.getDate(txtToDate.Value);
            string shippingID = txtShippingNumber.Text;
            return ShippingController.exportShippingReceived(customerID, department, fromReceivedDate, toReceivedDate, shippingID);

        }
    }
}