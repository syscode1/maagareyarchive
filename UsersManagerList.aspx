﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="UsersManagerList.aspx.cs" Inherits="MaagareyArchive.UsersManagerList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="resources/js/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script>

        $(document).ready(function () {
            setSortedTable();
        });
        function setSortedTable() {
            $("#<%=dgResults.ClientID%>").tablesorter({ dateFormat: 'dd/mm/yyyy' });
            SetDefaultSortOrder();
        }
        function Sort(cell, sortOrder) {
            debugger
              var sorting = [[cell.cellIndex, sortOrder]];
              $("#<%=dgResults.ClientID%>").trigger("sorton", [sorting]);
        if (sortOrder == 0) {
            sortOrder = 1;
            cell.className = "sortDesc";
        }
        else {
            sortOrder = 0;
            cell.className = "sortAsc";
        }
        cell.setAttribute("onclick", "Sort(this, " + sortOrder + ")");
        cell.onclick = function () { Sort(this, sortOrder); };
    }

        function SetDefaultSortOrder() {
            
        var gvHeader = document.getElementById("<%=dgResults.ClientID%>");
            var dgResults = document.getElementById("<%=dgResults.ClientID%>");

            var headers = gvHeader.getElementsByTagName("TH");
            for (var i = 0; i < headers.length; i++) {
                headers[i].setAttribute("onclick", "Sort(this, 1)");
                headers[i].onclick = function () { Sort(this, 1); };
                headers[i].className = "sortDesc";
                //dgResults.getElementsByTagName("TH")[i].style.width = headers[i].style.width;
            }
       }
      
        function newUser() {
            window.location = "UsersManager.aspx?custID="+ document.getElementById("<%=ddlCustomerName.ClientID%>").value+"&isNew=1" ;
        }
    </script> 
       <style type="text/css">


        .grid {
            font-family: Arial;
            font-size: 10pt;
            width: 600px;
        }

            .grid THEAD {
                background-color: Green;
                color: White;
            }

        th {
            font-weight: bold;
            border-color: lightgray;
            color: rgb(6, 8, 6) !important;
            height: 30px;
            background-color: rgba(217, 251, 218, 0.23);
        }
    </style>
     <h1>רשימת משתמשים</h1>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlCustomerName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    <td>
             <%--           <asp:DropDownList ID="ddlUser" runat="server" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>--%>
                    </td>
                </tr>
            </table>

                    <asp:GridView ID="dgResults" runat="server"
                        AutoGenerateColumns="false" Style="direction: rtl; width: 100%">
                        <Columns>

                            <asp:TemplateField ItemStyle-Width="30px" HeaderText="מס'" ItemStyle-CssClass="ltr">
                                <ItemTemplate><%# rownum++  %></ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="שם" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <ItemTemplate> <a href="UsersManager.aspx?userID=<%#DataBinder.Eval(Container.DataItem,"UserID")%>&custID=<%#DataBinder.Eval(Container.DataItem,"CustomerID")%>">
                                        <span id="boxID" runat="server"><%#DataBinder.Eval(Container.DataItem,"FullName")%> </span></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="פלאפון" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "Phone")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="1 טלפון" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemTemplate>
                                  <%#DataBinder.Eval(Container.DataItem, "Telephone1")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="2 טלפון" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemTemplate>
                                   <%#DataBinder.Eval(Container.DataItem, "Telephone2")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="2 טלפון" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemTemplate>
                                   <%#DataBinder.Eval(Container.DataItem, "Telephone2")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="מייל" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemTemplate>
                                   <%#DataBinder.Eval(Container.DataItem, "Email")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="כתובת" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemTemplate>
                                   <%#DataBinder.Eval(Container.DataItem, "FullAddress")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="תאור כתובת" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemTemplate>
                                   <%#DataBinder.Eval(Container.DataItem, "DescriptionAddress")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="עיר" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <ItemStyle/>
                            <ItemTemplate>
                                   <%#DataBinder.Eval(Container.DataItem, "CityAddress")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                                           <asp:ImageButton ID="btnAddRow" Style="vertical-align: bottom;float:right" OnClientClick="newUser()" ImageUrl="resources/images/add.png" Height="31px" runat="server" />


                </div>             <div id="dvMessage" runat="server" style="display: none">לא נמצאו נתונים</div>

                            <table style="float: left;width:100%;padding-top:30px">
                <tr>
                     <td style="width: 662px;text-align: right;">
                    <input type="button" value="יציאה" onclick="exitArchive()" runat="server" />
                </td>
                </tr>
                                </table>
</ContentTemplate>
        </asp:UpdatePanel>
          <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
