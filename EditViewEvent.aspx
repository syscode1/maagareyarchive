﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="EditViewEvent.aspx.cs" Inherits="MaagareyArchive.EditViewEvent" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
        <div style="display: none">
        <asp:HyperLink ID="lnk1" runat="server" NavigateUrl="http://www.itorian.com">F7</asp:HyperLink>
        <br />
        <asp:HyperLink ID="lnk2" runat="server" NavigateUrl="http://www.itorian.com/articles/aspdotnet/article.aspx">F2</asp:HyperLink>
        <br />
        <asp:HyperLink ID="lnk3" runat="server" NavigateUrl="http://www.itorian.com/about">F3</asp:HyperLink>
    </div>

    <%--    <link href="https://fonts.googleapis.com/css?family=Alef" rel="stylesheet">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="resources/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="resources/js/jquery-ui.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="resources/css/master.css" />
    <link href="resources/css/jquery-ui.css" rel="stylesheet" />
    <script src="resources/js/main.js" type="text/javascript"></script>

     <script>

            function popup(href, windowname, fileID)
        {
            if (!window.focus)
                return true;
            var href;
            var w = 500;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open(href + "?fileID=" + fileID , windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }

        function exit() {
            if (<%=user.ArchiveWorker %> == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }
            function validate() {
                isValid = true;
                isValid = isValid & isCompleteDdl(document.getElementById('<%= ddlEventType.ClientID%>'));
                isValid = isValid & isCompleteDdl(document.getElementById('<%= ddlTo.ClientID%>'));


                if (isValid == false) {
                    document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "";
                    document.getElementById('<%= dvErrorMessage.ClientID%>').innerText = "יש למלאות את כל שדות החובה";
                }
                else
                    document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "none";
                
                return isValid == 0 ? false : true;
            }
            function isCompleteField(textbox) {

                if (textbox.value.trim() == "") {
                    drowField(true, textbox)
                    return false;
                }

                drowField(true, textbox)
                return true;
            }
            function isCompleteDdl(textbox) {

                if (textbox.value.trim() == "0") {
                    drowField(true, textbox)
                    return false;
                }

                drowField(true, textbox)
                return true;
            }
            function drowField(isDrow, field) {
                if (isDrow)
                    //field.style.backgroundColor = "rgba(255, 0, 0, 0.27)";
                    field.style.backgroundColor = "rgb(255, 182, 177);";
                else
                    field.style.backgroundColor = "";
            }

        var map = {}; // You could also use an array
        onkeydown = onkeyup = function (e) {
            e = e || event; // to deal with IE
            map[e.keyCode] = e.type == 'keydown';
            /* insert conditional here */
            if (map[17] && map[76]) { // CTRL+L
                alert('Control L , ' + window.getSelection().toString());
                map = {};
            }
            else if (map[121]) { // F10
                alert('F10, ' + window.getSelection().toString());
                map = {};
            }
            else if (map[118]) { // F7
                alert('F7, ' + window.getSelection().toString());
                map = {};
            }
            else if (map[13]) {
                debugger
                var index = $(':input').index(e.target) + 1;
                $(':input').eq(index).focus();

                e.preventDefault();
                map = {};
            }
        }
                        function PrintWindow() {
                   window.frames["print_frame"].document.body.innerHTML = document.getElementById("<%=form2.ClientID%>").outerHTML;
                       window.frames["print_frame"].window.focus();
                       window.frames["print_frame"].window.print();
               }

    </script>
    <form id="form2" style="direction:rtl" runat="server">
        <asp:ScriptManager runat="server" />
    <h1 id="header" runat="server">
        משימה
    </h1>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
        <span id="dvErrorMessage" runat="server" class="dvErrorMessage"> </span>
     <div  runat="server" id="dvSucceedMessage" class="dvSucceedMessage">
               
            </div> 

    <div id="dvEvent" runat="server">
    <table style="text-align: right;" runat="server">
         <tr id="trFrom" style="display:none">
            <td>
                <span>מאת:</span>
            </td> 
            <td>
                <asp:TextBox ID="txtFrom" runat="server" />
            
            </td>
        </tr>
        <tr>
            <td style="width:110px"> 
                <span>אל:</span>
            </td> 
            <td>
                <asp:DropDownList ID="ddlTo" runat="server"></asp:DropDownList>
               
            </td>
        </tr>
        <tr>
            <td>
                <span>סוג משימה:</span>
            </td> 
            <td>
                <asp:DropDownList ID="ddlEventType" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td>
                <span>לקוח:</span>
            </td> 
            <td>
                <asp:DropDownList ID="ddlCustomer" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
             <td>
                <span>פרטי תיק:</span>
            </td> 
            <td>
                <asp:TextBox ID="txtFile" onkeypress="return isNumberKey(event,13)"  runat="server" />
            </td>
        </tr>
        <tr>
           <td style="vertical-align:top">
                <span >תוכן המשימה:</span>
            </td> 
            <td>
                <asp:TextBox ID="txtDescription"  TextMode="MultiLine" onkeypress="return maxCharLength(event,4080)" runat="server" />
          <span id="spnDescription" runat="server"> </span>
                  </td>
        </tr>
 <%--       <tr>
             <td>
                <span>צרף קובץ:</span>
            </td> 
            <td>
               <asp:FileUpload runat="server" style="float:right" CssClass="file-upload"  ID="AttachedFile" />
            </td>
        </tr>--%>
    </table>
        </div>
     <table style="width:100%;">
         <tr>
             <td>
                 <asp:Button Text="הדפס" runat="server" OnClientClick="PrintWindow()" style="margin-bottom:10px;width:130px;float:right !important"/>
             </td>
                             <td>
                   <asp:Button ID="btnSend" Text="שלח" runat="server"  OnClientClick="return validate();" OnClick="btnSend_Click"  style="margin-bottom:10px;width:130px;float:left " />
                </td><%--OnClientClick="return getSelectedFiles()"--%>

            </tr>
        </table>
                            <iframe name="print_frame" style="float:right;direction:rtl;text-align:right" width="0" height="0" frameborder="0" src="about:blank"></iframe>

            </ContentTemplate>
    </asp:UpdatePanel>
         <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>

