﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MaagareyArchive.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div runat="server">
     <script src="resources/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="resources/js/jquery-ui.js" type="text/javascript"></script>
    <link href="resources/css/jquery-ui.css" rel="stylesheet" />
        <script>
            function inputFocus(i) {   
                $("#wrongUserDetailesPassword")[0].style.visibility = "hidden";
                $("#spnPasswordExist")[0].style.visibility = "hidden";
                $("#spnPasswordExist")[0].style.display = "none";
            }
            
            function validateLogin() {
                var isValid = false;
                if ($("#<%= txtUserName.ClientID %>")[0].value == '') {
                $("#UserNameRequired")[0].style.visibility = "visible";
                isValid = false
            }
            else {
                isValid = true;
                $("#UserNameRequired")[0].style.visibility = "hidden";
            }
            if ($("#<%= txtPassword.ClientID %>")[0].value == '') {
                isValid = false;
                $("#passwordRequired")[0].style.visibility = "visible";
            }
            else {
                $("#passwordRequired")[0].style.visibility = "hidden";
            }

            return isValid;
            }
            function showPasswordDialog() {
                $("#dialog")[0].style.visibility = "visible";
                $("#dialog")[0].style.display = "";
                $("#dialog").dialog({
                    position: {
                        my: "left-300 bottom+100",
                        at: "left bottom",
                        of: "#iconHelpNewPass"
                    }, height:100
                });
                  boxNum = document.getElementById('<%= btnChangePassword.ClientID%>').focus();
            }

            $(document).ready(function () {
                boxNum = document.getElementById('<%= btnLogin.ClientID%>').focus();
            });


            var map = {}; // You could also use an array
            onkeydown = onkeyup = function (e) {
                e = e || event; // to deal with IE
                map[e.keyCode] = e.type == 'keydown';
                /* insert conditional here */
                 if (map[13]) {
                     debugger
                     if (e.target == $(<%=txtPassword.ClientID%>)[0]) {
                         $(<%=btnLogin.ClientID%>).click();
                     }
                     else {
                         var index = $(':input').index(e.target) + 1;
                         $(':input').eq(index).focus();

                         e.preventDefault();
                         map = {};
                     }
                }
            }


        </script>
    </div>
    <link rel="stylesheet" type="text/css" href="resources/css/login.css" />
        <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css" />
    <link href="https://fonts.googleapis.com/css?family=Alef" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="resources/js/jquery-3.1.1.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-ui.js" type="text/javascript"></script>

    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" >
          
        </asp:ScriptManager>
        
        <table>
            <tr>
                <td>
                    <div class="dvRight">
                        <h1>מאגרי ארכיב וגניזה בע"מ
                        </h1>
                        <h2>שרותי ארכיון מקצועיים
                        </h2>
                        <div class="dvLoginBody"  ></div>
                         <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                        <div id="dvLogin" class="dvLogin" runat="server">
                            <h1>כניסה
                            </h1>
                            <h2>למערכת הניהול 
                            </h2>
                            <div class="form-group">
                                <div class="input-icon">
                                    <input type="text" id="txtUserName" runat="server"  placeholder="שם משתמש" onkeypress="inputFocus(this)" class="form-control" />
                                    <span id="UserNameRequired" style="color: Red; visibility: hidden;">אנא הזנ/י שם משתמש.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-icon">
                                    <input type="password" id="txtPassword"  runat="server" placeholder="סיסמא" onkeypress="inputFocus(this)" class="form-control" />
                                    <span id="passwordRequired" style="color: Red; visibility: hidden;">אנא הזנ/י סיסמא .</span>
                                </div>
                                <div>
                                    <asp:UpdatePanel id="upLogin" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnLogin" class="btnLogin" Text="התחבר" runat="server" OnClientClick="if ( ! validateLogin()) return false;" OnClick="btnLogin_Click" />
                                            <span id="wrongUserDetailesPassword" style="color: Red;visibility:hidden " runat="server">שם משתמש או סיסמא שגויים</span>

                                      </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>
                          </ContentTemplate>
                                    </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                           <div id="dvChangePassword" class="dvLogin" style="visibility:hidden;display:none" runat="server">
                            <h1>כניסה
                            </h1>
                            <span style="font-size: 1.17em;font-weight:bold;-webkit-margin-after: 0.67em;" >תוקף סיסמתך פג, הנך נדרש להזין סיסמא חוקית חדשה  
                           <i id="iconHelpNewPass" class="fa fa-question-circle" style="font-size:24px" title="חוקיות הסיסמא" onclick="showPasswordDialog()"></i>
                               </span>
                               <div style="height:10px"></div>
                           <div id="dialog" title="חוקיות הסיסמא" style="visibility:hidden;display:none">
  <p>סיסמא חייבת להכיל 8 תווים ולשלב אותיות ומיספרים</p>
</div>
                                <div class="form-group">
                                <div class="input-icon">
                                    <input type="password" id="txtNewPass" runat="server"  placeholder="סיסמא חדשה" onfocus="inputFocus(this)" class="form-control" />
                                    <span id="newPasswordRequired" style="color: Red; visibility: hidden;">אנא הזנ/י סיסמא חדשה.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-icon">
                                    <input type="password" id="txtNewPass2"  runat="server" placeholder="סיסמא חדשה בשנית" onfocus="inputFocus(this)" class="form-control" />
                                    <span id="confirmPasswordRequired" style="color: Red; visibility: hidden;">אנא הזנ/י סיסמא חדשה שנית .</span>
                                </div>
                                <div>
                                    <asp:UpdatePanel ID="upChangePassword" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnChangePassword" class="btnLogin" Text="שנה סיסמא" runat="server"  OnClick="btnChangePassword_Click" />
                                            <span id="spnPasswordExist" style="color: Red; visibility: hidden;display:none" runat="server">סיסמא זו היתה כבר בשימוש , אנא בחר סיסמא חדשה</span>
                                         <asp:RegularExpressionValidator runat="server" ControlToValidate="txtNewPass" ForeColor="Red"  ErrorMessage="הסיסמא אינה חוקית" ValidationExpression="^.*(?=.{8,})((?=.*[a-zA-Zא-ת]))(?=.*\d).*$"></asp:RegularExpressionValidator>
                                        <p> <asp:CompareValidator runat="server" ControlToValidate="txtNewPass" ControlToCompare="txtNewPass2" ForeColor="Red" ErrorMessage="הסיסמאות אינן תואמות " ></asp:CompareValidator></p>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                               
                </td>
                <td>
                    <div class="dvLeft">
                        <div class="dvContent">
                            <h3>
                                <p>ברוכים הבאים למערכת איתור והזמנות.</p>
                                <p>בעזרת המערכת, ניתן לבצע הזמנת תיקים או תיבות, הזמנת פינוי או אספקת תיבות ריקות, כל זה ביעילות ובזריזות ללא התערבות יד אדם.</p>
                                <p>לאחר ביצוע ההזמנה, תיקלט הבקשה במערכת הפנימית של הארכיב ותמתין לאישור המשרד ולביצוע המחסנאים.</p>
                                <p>בתום הטיפול, תתעדכן סטטוס ההזמנה .</p>
                                <p></p>
                                <p>אנחנו מזמינים את לקוחותינו להיכנס לכרטיסיית "חוות דעת" וספר לנו את דעתכם על המערכת והצעות שיפור וייעול כלליות .</p>
                           
                                 </h3>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <div>
            <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="upLogin"
        runat="server">
            <ProgressTemplate>
                 <div class="divWaiting">            
	<asp:Label ID="lblWait" runat="server" 

	Text=" Please wait... " style="font-size: 16px; font-weight: bolder;vertical-align:bottom" />
	<asp:Image ID="imgWait" runat="server" 

	ImageAlign="Middle" style="vertical-align:sub" ImageUrl="resources/images/16.gif" />           
       
               
            </ProgressTemplate>
        </asp:UpdateProgress>
            <asp:UpdateProgress ID="UpdateProgress1"
        AssociatedUpdatePanelID="upChangePassword"
        runat="server">
            <ProgressTemplate>
                 <div class="divWaiting">            
	<asp:Label ID="lblWait2" runat="server" 

	Text=" Please wait... " style="font-size: 16px; font-weight: bolder;vertical-align:bottom" />
	<asp:Image ID="imgWait2" runat="server" 

	ImageAlign="Middle" style="vertical-align:sub" ImageUrl="resources/images/16.gif" />           
       
               
            </ProgressTemplate>
        </asp:UpdateProgress>

  
        </div>
    </form>
</body>
</html>
