﻿using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class WorkOrderingMain : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnworkingReport_Click(object sender, EventArgs e)
        {
            Response.Redirect("WorkOrdering.aspx");
        }

        protected void btnworkingReportArchiv_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("WorkOrderingHistory.aspx?fromDate={0}&toDate={1}&shippingID={2}", txtFromDate.Value, txtToDate.Value, txtShippingID.Value));
        }

    }
}