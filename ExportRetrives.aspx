﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true" CodeBehind="ExportRetrives.aspx.cs" Inherits="MaagareyArchive.ExportRetrives" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <h1>דוח שליפות
    </h1>
    <script>
        function print() {
            window.frames["print_frame"].document.body.innerHTML =  document.getElementById("dvGridResults").innerHTML;
            window.frames["print_frame"].window.focus();
            window.frames["print_frame"].window.print();
            
        }
                function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }
    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table style="border-spacing: 10px; display: inline">
                <tr id="trCustomers" runat="server">
                    <td>
                        <span>לקוח</span>
                        <br />

                        <asp:DropDownList ID="ddlCustomer" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <span>מתאריך </span>
                        <br />
                        <input type="date" runat="server" id="txtFromDate" />
                    </td>
                    <td>
                        <span>עד תאריך</span>
                        <br />
                        <input type="date" runat="server" id="txtToDate" />
                    </td>

                </tr>

            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="width: 100%; float: left;margin-top:40px">
        <asp:Button Text="יצא" ID="btnExport" OnClick="btnExport_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <table style="margin: 0px; border-spacing: 0px; float: left;">
                    <tr>
                        <td style="width: 620px; text-align: right;">
                            <input type="button" value="יציאה" onclick="exit()" runat="server" />

                        </td>
                        <td>
                            <asp:Button Text="הצג" ID="btnShow" Style="float: left; margin: 10px" Width="85px" OnClick="btnShow_Click" runat="server" />

                        </td>
                    </tr>
                </table>

                            <div id="dvGridResults" style="overflow:auto;width: 1132px;padding: 20px;">
                                <asp:DataGrid ID="dgResults" AutoGenerateColumns="true" style="width: auto;"  runat="server" CssClass="gridPopup" OnItemDataBound="dgResults_ItemDataBound">
                                          <HeaderStyle CssClass="header"/>
                                      </asp:DataGrid>
                            </div>
                            <asp:Button ID="btnPrint" style="left:-98px;width: 85px; margin-top: 20px;" Text="הדפס" Visible="false" runat="server" OnClientClick="print()" />



                        <iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>




            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
