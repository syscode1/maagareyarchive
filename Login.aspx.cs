﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MA_CORE.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MA_DAL.MA_BL;
using log4net;
using System.Reflection;

namespace MaagareyArchive
{
    public partial class Login : System.Web.UI.Page
    {
        private static readonly ILog logger = LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                
                Session["allowedDepartmentsEntity"] = null;
                Session["UserEntity"] = null;
                Session["userDepartment"] = null;
                Session.Clear();
                ViewState.Clear();
            }

        }
        protected void Page_Unload(object sender, EventArgs e)
        {
            if (Session["UserEntity"] != null)
                Session.Timeout = (Session["UserEntity"] as Users).ArchiveWorker == true ? 120 :30;

        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
           
            Users user=null;
            try
            {
                user = UserController.Login(Helper.getInt( txtUserName.Value), txtPassword.Value);
                
            }
            catch (Exception ex)
            {

                logger.Error(string.Format("method: {0};  message: {1}",Helper.GetCurrentMethod(), ex.Message));
            }
           
            if  (user== null)
            {
                wrongUserDetailesPassword.Style["visibility"] = "visible";
                logger.Info(string.Format("method: {0};  message: {1}", Helper.GetCurrentMethod(),string.Format("try login fail , userName:{0} , password{1}",txtUserName.Value,txtPassword.Value)));
                return;
            }
            else
            {
                logger.Info(string.Format("method: {0};  message: {1}", Helper.GetCurrentMethod(), string.Format("login succeed! ,user:{0}  loginId:{1} , password{2}",user.FullName,user.UserID,user.Password)));

                int? status = CustomersController.getCustomerStatus(user.CustomerID);
                if(status==(int)Consts.enCustomerStatus.inactive)
                {
                    wrongUserDetailesPassword.Style["visibility"] = "visible";
                    logger.Info(string.Format("method: {0};  message: {1}", Helper.GetCurrentMethod(), string.Format("try login fail , userName:{0} , password{1}", txtUserName.Value, txtPassword.Value)));
                    return;

                }
                if (status==(int)Consts.enCustomerStatus.frozen)
                {
                    wrongUserDetailesPassword.Style["visibility"] = "visible";
                    wrongUserDetailesPassword.InnerText = Consts.CUSTOMER_STATUS_FROZEN;
                    logger.Info(string.Format("method: {0};  message: {1}", Helper.GetCurrentMethod(), string.Format("try login fail , userName:{0} , password{1}", txtUserName.Value, txtPassword.Value)));
                    return;

                }
                Session["UserEntity"] = user;
                Session["UserName"] = user.FullName;
                Session["UserID"] = user.UserID;
                Session["UserPassword"] = user.Password.Trim();
                wrongUserDetailesPassword.Style["visibility"] = "hidden";
                string paramDays = ParametersController.GetParameterValue(Consts.Parameters.PARAM_KEY_PASSWORD_EXPIRY_DAYS);
                int days = 0;
                int.TryParse(paramDays,out days);
                if (((DateTime)user.PasswordLastDate).AddDays(days)<=DateTime.Today)
                {
                    logger.Info(string.Format("method: {0} ;  message: {1}", Helper.GetCurrentMethod(), string.Format("password expired ,user:{0}  last password date:{1} , password{2}", user.FullName, user.PasswordLastDate, user.Password)));
                    dvLogin.Style["visibility"] = "hidden";
                    dvLogin.Style["display"] = "none";
                    dvChangePassword.Style["display"] = "";
                    dvChangePassword.Style["visibility"] = "visible";


                }
                else
                {
                    loginUser(user);
                }
            }
        }

        private void loginUser(Users user)
        {
            if (user.ArchiveWorker == true)
            {
                Response.Redirect("MainPageArchive.aspx", true);
                Session.Timeout = 120;
            }
            else
            {
                Response.Redirect("MainPage.aspx", true);
                Session.Timeout =30;
            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
             string newPass = txtNewPass.Value.Trim();
            if (newPass == Session["UserPassword"].ToString() || PasswordHistoryController.isPasswordExist(Helper.getIntNotNull( Session["UserID"].ToString()),newPass))
            {
                spnPasswordExist.Style["visibility"] = "visible";
                spnPasswordExist.Style["display"] = "";
            }
            else
            {
                UserController.ChangePassword(Helper.getIntNotNull( Session["UserID"].ToString()), newPass);
                loginUser(Session["UserEntity"] as Users);
            }

        }
    }
}