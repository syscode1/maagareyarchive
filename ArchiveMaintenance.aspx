﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ArchiveMaintenance.aspx.cs" Inherits="MaagareyArchive.ArchiveMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <table style="border-spacing: 20px">
                    <tr>
                        <td>
                              <asp:Button Text="גריעת לקוח" ID="btnDeactivateCustomer" OnClick="btnDeactivateCustomer_Click" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" /></td>

                    </tr>
                    <tr>
                        <td>
                            <asp:Button Text="הפקת חשבוניות" ID="btnInvoices" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" OnClick="btnInvoices_Click" /></td>
                    </tr>
                    <tr> 
                        <td> 
                            <asp:Button Text="ניהול משתמשים" ID="btnUsersManager" OnClick="btnUsersManager_Click"  class="button1" runat="server" Style="margin-top: 40px; width: 185px;" />

                        </td>

                     </tr>
                    <tr>
                        <td>                            
                          
                            <asp:Button Text="הסכם לקוח" ID="btnCustomerAgreement" OnClick="btnCustomerAgreement_Click" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" />

                        </td>
                      
                    </tr>
                    <tr>
                        <td>                            
                          
                            <asp:Button Text="מבנה מגנזה" ID="btnArchiveStructure" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" OnClick="btnArchiveStructure_Click" />

                        </td>
                      
                    </tr>
                </table>
            </td>
            <td>
                <table style="border-spacing: 20px">
                    <tr>
                        <td>
                            <asp:Button Text="דוח יומי" ID="btnDaylyReport" class="button1" runat="server" OnClick="btnDaylyReport_Click" Style="margin-top: 40px; width: 185px;" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button Text="דוח יומי מפורט" ID="btnDailyReportDetailed" OnClick="btnDailyReportDetailed_Click" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button Text="מכירת ארגזים ללקוח" ID="btnCustomerBoxesSale"  OnClick="btnCustomerBoxesSale_Click" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" />

                        </td>
                    </tr>  
                     <tr>
                        <td>
                            <asp:Button Text="תחזוקת נושאים ללקוח" ID="btnCustomerSubject"  OnClick="btnCustomerSubject_Click" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" />

                        </td>
                    </tr> 
                    <tr>
                        <td>
                                                        <asp:Button Text="בנית מחלקות ללקוח" ID="btnCustomerDepartment"  OnClick="btnCustomerDepartment_Click" class="button1" runat="server" Style="margin-top: 40px; width: 185px;" />

</td>
                    </tr>
                </table>
            </td>

                        <td>
                <table style="border-spacing: 20px">
                    <tr>
                        <td>
                            <asp:Button Text="ניקוי ארגזים ישנים" ID="btnClearOldBoxes" class="button1" runat="server" OnClick="btnClearOldBoxes_Click" Style="margin-top: 40px; width: 185px;" /></td>
                    </tr>
                    <tr>
                        <td>
                     </tr>
                    <tr>
                        <td>
  
                        </td>
                    </tr>  
                     <tr>
                        <td>
                        </td>
                    </tr> 
                    <tr>
                        <td>

</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
