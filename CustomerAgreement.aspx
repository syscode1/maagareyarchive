﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerAgreement.aspx.cs" Inherits="MaagareyArchive.CustomerAgreement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function validate() {
            isValid = true;
            isValid=isValid & isCompleteField(document.getElementById('<%= txtCustomerName.ClientID%>'));
<%--            isValid=isValid & isCompleteField(document.getElementById('<%= txtBoxPrice1.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtBoxPrice2.ClientID%>'));

            isValid=isValid & isCompleteField(document.getElementById('<%= txtBoxPrice3.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtBoxPrice4.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtTransport.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtTransportAndIntake.ClientID%>'));

            isValid=isValid & isCompleteField(document.getElementById('<%= txtScan.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtTransportAndContract.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtRetrive.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtMaxRetrive.ClientID%>'));

            isValid=isValid & isCompleteField(document.getElementById('<%= txtUrgentRetrive.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtmaxUrgentRetrive.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtCrushing.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtCrushingAndTransport.ClientID%>'));

            isValid=isValid & isCompleteField(document.getElementById('<%= txtFixedPayment.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%=txtDescriptionAddress.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtEmptyBox.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtEmptyBoxMaxEmount.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtFax.ClientID%>'));

            isValid=isValid & isCompleteField(document.getElementById('<%= txtMail.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtPhone1.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtPhone2.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtPO.ClientID%>'));

            isValid=isValid & isCompleteField(document.getElementById('<%= txtAddress.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtContactName.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtDescription.ClientID%>'));
            isValid=isValid & isCompleteField(document.getElementById('<%= txtPhone1.ClientID%>'));--%>

            document.getElementById('<%= dvErrorMessage.ClientID%>').innerText = "";

            if (isValid == false) {
                document.getElementById('<%= dvSucceedMessage.ClientID%>').style.display = "none";
                document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "";
                document.getElementById('<%= dvErrorMessage.ClientID%>').innerText = "יש למלאות את כל שדות החובה";
            }
            if (document.getElementById('<%= rbCustomerExist.ClientID%>').checked && document.getElementById('<%= ddlCustomerName.ClientID%>').value.trim() == "0") {
                document.getElementById('<%= dvSucceedMessage.ClientID%>').style.display = "none";
                document.getElementById('<%= dvErrorMessage.ClientID%>').style.display = "";
                document.getElementById('<%= dvErrorMessage.ClientID%>').innerText += "\nיש לבחור לקוח";
                isValid = false;
            }
            return isValid==0?false:true;

        }

        function isCompleteField(textbox) {

            if (textbox.value.trim() == "") {
                drowField(true, textbox)
                return false;
            }

            drowField(true, textbox)
            return true;
        }

        function drowField(isDrow, field) {
            debugger
            if (isDrow)
                //field.style.backgroundColor = "rgba(255, 0, 0, 0.27)";
                $(field)[0].style.backgroundColor = "#ffb6b1";
            else
                $(field)[0].style.backgroundColor = "";
        }
 
    </script>
    <h1>הסכם לקוח</h1>
   
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
             <div id="dvCustAgree" runat="server">
            <table style="width: 100%">
                <tr>
                    <td>
                        <div style="clear: both">
                            <div style="float: right; margin-top: 20px;">
                                <asp:RadioButton Text="לקוח חדש" Font-Bold="true" ID="rbNewCustomer" GroupName="customerType" AutoPostBack="true" OnCheckedChanged="rbCustomerExist_CheckedChanged" runat="server" />
                                <asp:RadioButton Text="לקוח קיים" Font-Bold="true" ID="rbCustomerExist" AutoPostBack="true"  OnCheckedChanged="rbCustomerExist_CheckedChanged" GroupName="customerType" runat="server" />
                            </div>

                            <div id="dvCustomerName" runat="server" style="float: right; margin-top: 3px; margin-right: 20px;">
                                <label>בחר לקוח</label>

                                <asp:DropDownList ID="ddlCustomerName" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                            <div id="dvCustomerID" style="float: right; margin-top: 3px; margin-right: 20px;" runat="server">
                                <label>מספר לקוח</label>
                                <asp:DropDownList ID="ddlCustomerID" on AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerID_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </div>
                            <div id="dvNewCustomerName" runat="server" style="float: right; margin-top: 3px; margin-right: 20px;">
                                <label>שם לקוח</label>
                                <asp:TextBox ID="txtCustomerName"  onkeypress="return maxCharLength(event,30)" runat="server"  />
                            </div>
                            <div  style="float: right; margin-top: 19px; margin-right: 30px;" runat="server">
                                <asp:Button ID="btnDepartments" Text="ניהול מחלקות" Width="115px"  style="margin-right:15px" OnClick="btnDepartments_Click" runat="server" />
                                <asp:Button ID="btnUsers" Text="ניהול משתמשים"  Width="124px"  OnClick="btnUsers_Click" runat="server" />

                            </div>
                            <div id="dvCustomerStatus" style="float: left; margin-top: 3px; margin-right: 6px;" runat="server">
                                <label>מצב לקוח</label>
                                <asp:DropDownList ID="ddlCustomerStatus" AutoPostBack="true" runat="server"></asp:DropDownList>
                            </div>

                        </div>

                    </td>
                </tr>
            </table>
                        <div class="h1_sub"></div>

            <table style="width: 100%; text-align: center;border-spacing: 21px;">
                <tr>
                    <td>
                        <label>אחסון שנתי לתיבה:</label>

                    </td>
                    <td>
                        <label>רגילה </label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)"  ID="txtBoxPrice1" runat="server" />

                    </td>

                    <td>
                        <label>מיוחדת 1</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtBoxPrice2" runat="server" />

                    </td>

                    <td>
                        <label>מיוחדת 2</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtBoxPrice3" runat="server" /></td>

                    <td>
                        <label>מיוחדת 3</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtBoxPrice4" runat="server" /></td>

                    <td>
                        </td>


                </tr>
                <tr>
                    <td>
                        <label>הובלה וקליטה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtTransportAndIntake" runat="server" />
                    </td>
                    <td>
                        <label>הובלה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtTransport" runat="server" />

                    </td>

                    <td>
                        <label>רישום תוכן</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtRegistration" runat="server" /></td>



                    <td>
                        <label>תיבה ריקה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtEmptyBox" runat="server" /></td>

                    <td>
                        <label>סריקה ומיפתוח</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtScan" runat="server" /></td>

                    <td>
                        <label>הובלה בסיום התקשרות</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtTransportAndContract" runat="server" /></td>


                </tr>
                <tr>
                    <td>
                        <label>שליפה רגילה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtRetrive" runat="server" />
                    </td>
                    <td>
                        <label>מכמות שליפות בשנה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtMaxRetrive" runat="server" />

                    </td>

                    <td>
                        <label>שליפה דחופה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtUrgentRetrive" runat="server" /></td>

                    </td>

                    <td>
                        <label>מכמות שליפות בשנה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtmaxUrgentRetrive" runat="server" /></td>

                    <td>
                        <label>גריסה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtCrushing" runat="server" /></td>

                    <td>
                        <label>הובלה וגריסה</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtCrushingAndTransport" runat="server" /></td>


                </tr>
                <tr>
                    <td>
                   
                        <label>מקסימום תיבות ריקות</label>
                        <br />
                        <asp:TextBox CssClass="customerAgreement" Font-Bold="true"  onkeypress="return isNumberKey(event,10)" ID="txtEmptyBoxMaxEmount" runat="server" /></td>

                    </td>
                </tr>
            </table>
            <div class="h1_sub"></div>
            <table style="width: 100%; text-align: center">
                <tr>



                    <td style="width: 200px;">
                        <label>חיוב קבוע</label>
                        <br />
                        <asp:TextBox  style="width:200px" Font-Bold="true"  onkeypress="return isNumberKey(event,15)" ID="txtFixedPayment" runat="server" />

                    </td>

                    <td>
                        <label>תנאי אשראי</label>
                        <br />
                        <asp:DropDownList ID="ddlCredit" Font-Bold="true"  runat="server" runat="server">
                        </asp:DropDownList></td>

                    <td>
                        <label>התחשבנות</label>
                        <br />
                        <asp:DropDownList  ID="ddlPeriodOfPayment" Font-Bold="true"  runat="server" runat="server">
                        </asp:DropDownList></td>

                    <td>
                        <label>גורם משלם</label>
                        <br />
                         <asp:DropDownList  ID="ddlPaidBy" Font-Bold="true"  runat="server" runat="server">
                        </asp:DropDownList></td>



                </tr>
                <tr>



                    <td>
                        <label>תשלום</label>
                        <asp:DropDownList  ID="ddlMethodOfPayment" Font-Bold="true"  runat="server" runat="server">
                        </asp:DropDownList></td>

                    <td>
                        <label>אישורי מסירה</label>
                        <br />
                        <asp:CheckBox ID="cbDeliveryApproval" runat="server" />

                        <td style="width: 173px;">
                            <label>הצמדה: </label> 
                             <asp:RadioButtonList runat="server" id="rblLinkTo" RepeatDirection="Horizontal" Width="100%" style="width: 173px;" DataTextField="LinkToName" DataValueField="LinkToID" >

                             </asp:RadioButtonList>
                        

                        </td>

                        <td></td>
                </tr>
            </table>
            <div class="h1_sub"></div>
                 <div style=" width: 100%;text-align: right;padding-top: 11px;padding-bottom: 11px;">
                 <asp:CheckBox Text="שנת בעור בארגז שדה חובה" ID="cbExtermYearReq" runat="server" />
                     <br /><br />
                 <asp:CheckBox Text="טקסט לארגז שדה חובה" ID="cbBoxDescReq"  runat="server" />
        <br /><br />
                 <asp:CheckBox Text="הקלדה" ID="cbInsertData"  runat="server" />

</div>
            <div class="h1_sub"></div>
            <table style="line-height: 20px;width:100%;border-spacing: 25px;">
                <tr>
                    <td>
                        <asp:Label runat="server" Text="עיר" />
                        <br />
                        <asp:DropDownList   ID="ddlCities" runat="server" />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="כתובת" />
                        <br />
                        <input id="txtAddress" runat="server"  onkeypress="return maxCharLength(event,30)"  />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="תיאור" />
                        <br />
                        <input id="txtDescriptionAddress"  onkeypress="return maxCharLength(event,30)" runat="server"  />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="איש קשר" />
                        <br />
                        <input id="txtContactName" runat="server"  onkeypress="return maxCharLength(event,20)"  />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="טלפון 1" />
                        <br />
                        <input id="txtPhone1" runat="server" onkeypress="return isNumberKey(event,10)"   />
                    </td>
                    <td>
                        <asp:Label runat="server"  Text="טלפון 2" />
                        <br />
                        <input id="txtPhone2" runat="server" onkeypress="return isNumberKey(event,10)"   />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="פקס" />
                        <br />
                        <input id="txtFax" runat="server" onkeypress="return isNumberKey(event,10)"   />
                    </td>
                    <td>
                        <asp:Label runat="server" Text="מייל" />
                        <br />
                        <input id="txtMail" runat="server"  />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="תאריך חתימת ההסכם" />
                        <br />
                        <input id="txtDateOfContract" disabled="disabled" runat="server"  />
                    </td>
                    <td colspan="2">
                        <asp:Label runat="server" Text="PO" />
                        <br />
                        <input id="txtPO" runat="server"  onkeypress="return maxCharLength(event,25)"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label runat="server" Text="הערות" />
                        <br />
                        <asp:TextBox ID="txtDescription" runat="server" onkeypress="return maxCharLength(event,256)" />

                    </td>
                </tr>
            </table>
            <table style="display: inline;">
                <tr>
                    <td>
                       <span id="dvErrorMessage" runat="server" class="dvErrorMessage" style="display:none"></span>
                       <span id="dvSucceedMessage" runat="server" class="dvSucceedMessage" style="display:none"></span>
                        </br/>

                    </td>
                </tr>
            </table>
                 </div>
             <table style="margin-top: 24px; width: 875px">
                <tr>
                    <td style="width: 462px; text-align: right;">
                        <input type="button" value="חזרה לתפריט הראשי" onclick="exitArchive()" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnSave" Text="שמור" OnClientClick="return validate();"  OnClick="btnSave_Click" runat="server" />
                    </td>

                </tr>
                 <tr>
                     <td  style="text-align: right;">
                        <input id="btnBackCustomer" type="button" value="חזרה למסך לקוח" onclick="exitCustomerScreen()" runat="server" style="display:none" />
                     </td>
                 </tr>
                 </table>

        </ContentTemplate>
    </asp:UpdatePanel>
      <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
