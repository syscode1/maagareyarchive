﻿using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class CustomerBoxesSale : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                fillAllDdl();
                string customerID = Request.QueryString["custID"];
                if (!string.IsNullOrEmpty(customerID))
                {
                    ddlCustomers.SelectedValue = customerID;
                }
            }
        }



        private void fillAllDdl()
        {
            ddlCustomers.DataSource = (new MyCache()).CustomersNames;
            ddlCustomers.DataTextField = "Value";
            ddlCustomers.DataValueField = "Key";
            ddlCustomers.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue) && Convert.ToInt16(ddlCustomers.SelectedValue) > 0)
            {
                int customerID = Convert.ToInt16(ddlCustomers.SelectedValue);
                Customers customer = CustomersController.getCustomerByID(customerID);
                int calcNumOfPaiedBoxes = BoxesController.calcNumOfPaiedBoxes(customer);

                if (calcNumOfPaiedBoxes-Convert.ToInt16(txtAmount.Text)<0)
                {
                    showIndicationMessage(false, Consts.MISSING_PAIED_BOXES.Replace("XXX", calcNumOfPaiedBoxes.ToString()));
                }
                else
                {
                    string orderID = Helper.getOrderID(user.UserID);
                    OrdersController.Insert(new List<Orders>() { new Orders()
                    {
                        LoginID=user.UserID,
                        DepartmentID = 0,///
                        CustomerID =customer.CustomerID,
                        NumNewBoxOrder=Convert.ToInt16(txtAmount.Text),
                        Urgent = false,
                        Date = DateTime.Now,
                        OrderID = orderID,
                        NumInOrder = 1,
                        CityAddress =customer.CityAddress,
                        FullAddress = customer.FullAddress,
                        DescriptionAddress = customer.DescriptionAddress,
                        ContactMan = customer.ContactMan,
                        Telephone1 = customer.Telephon1,
                        Telephone2 = customer.Telephon2,
                        Email = customer.Email,
                        
                    } });
                }

            }
        }
        private void showIndicationMessage(bool isSucceed, string message)
        {
            HtmlGenericControl control = isSucceed ? dvSucceedMessage : dvErrorMessage;
            HtmlGenericControl hideControl = isSucceed ? dvErrorMessage : dvSucceedMessage;
            control.Style["display"] = "";
            control.InnerText = message;
            hideControl.Style["display"] = "none";
        }


        private bool validateBoxesCount()
        {
            return true;        }
    }
}