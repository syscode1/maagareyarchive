﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class DaylyReportDataPopup : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime fromDate = Helper.getDate(Request.QueryString["from"]).Value;
            DateTime toDate = Helper.getDate(Request.QueryString["to"]).Value;
            switch (Request.QueryString["dataType"])
            {
                case "notExistExtractions":
                    {
                        dgResults.DataSource = FileStatusController.sumNotFoundFilesGroupByCust(fromDate,toDate);
                        dgResults.DataBind();
                        break;
                    }
                case "fullNewBoxes":
                    {
                        dgResults.DataSource = BoxesController.getSumFullNewBoxesByCust(fromDate,toDate);
                        dgResults.DataBind();
                        break;

                    }
                case "emptyNewBoxes":
                    {
                        dgResults.DataSource = BoxesController.getSumEmptyNewBoxesByCust(fromDate,toDate);
                        dgResults.DataBind();
                        break;

                    }
                case "newPlacedBoxes":
                    {
                        dgResults.DataSource = BoxesController.getSumPlacedBoxesByCustomer(fromDate,toDate);
                        dgResults.DataBind();
                        break;

                    }
                case "notPlacedBoxes":
                    {
                        dgResults.DataSource = BoxesController.getSumNotPlacedBoxesByCustomer(fromDate,toDate);
                        dgResults.DataBind();
                        break;

                    }
                case "newBoxes":
                    {
                        dgResults.DataSource = BoxesController.getSumNewBoxes(fromDate,toDate);
                        dgResults.DataBind();
                        break;

                    }
                case "boxesForExterminate":
                    {
                        dgResults.DataSource = BoxesController.sumBoxesForExterminateByCust();
                        dgResults.DataBind();
                        break;

                    }
                default:
                    break;
            }
        }
    }
}