﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class SuppliersOrderTracing_popup : BasePage
    {
        public int rownum = 1;

        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string suppliersOrderID = Request.QueryString["suppliersOrderID"];
                string supplierID = Request.QueryString["supplierID"];
                lblOrderNum.InnerText = suppliersOrderID;
                lblSupplier.InnerText = supplierID;
                dgResults.DataSource = SuppliersOrderController.getByOrderID(suppliersOrderID);
                dgResults.DataBind();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            List<SuppliersOrder> data = new List<SuppliersOrder>();
            foreach (GridViewRow item in dgResults.Rows)
            {
                
                int? numInOrder =Helper.getInt((item.FindControl("spnNumInOrder") as HtmlGenericControl).InnerText);
                string supplierOrderId = Request.QueryString["suppliersOrderID"];
                int? amountProvided =Helper.getInt( (item.FindControl("txtAmountProvided") as TextBox).Text);

                if (amountProvided != null && !string.IsNullOrEmpty(supplierOrderId))
                    data.Add(new SuppliersOrder()
                    {
                        SuppliersOrderID = supplierOrderId,
                        numInOrder = (int)numInOrder,
                        AmountProvided = (int)amountProvided
                    });

            }
           bool isSucceed= SuppliersOrderController.updateAmountProvided(data);

        }

        protected void btnSaveToFile_Click(object sender, EventArgs e)
        {
            try
            {
                List<SuppliersOrderUI> list = SuppliersOrderController.getByOrderID(lblOrderNum.InnerText);
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add(ExcelHeaders.ResourceManager.GetString("Supplier"), lblSupplier.InnerText);
                param.Add(ExcelHeaders.ResourceManager.GetString("OrderNum"), lblOrderNum.InnerText);

                exportToExcell(list, "export_supplier_order", param);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                int? supplierId = Helper.getInt(Request.QueryString["supplierID"]);
                if (supplierId == null || supplierId <= 0)
                {
                    showHideSucceedMessage(false, Consts.MISSING_SUPPLIER_NAME);
                    return;
                }
                string to = SuppliersController.getSupplierById((int)supplierId).Email;
                if (string.IsNullOrEmpty(to.Trim()))
                {
                    showHideSucceedMessage(false, Consts.CONST_MISSING_MAIL_ADDRESS);
                    return;
                }
                string from = ParametersController.GetParameterValue(Consts.Parameters.PARAM_EMAIL_MESSAGE_FROM_ADDRESS);
                string subject = Consts.SUPPLIER_MAIL_SUBJECT.Replace("XXX", Request.QueryString["suppliersOrderID"]);
                string gridHtml = string.Empty;
                string body = gridToHTMLReport(out gridHtml);
                EventController.insert(new Events()
                {
                    CustomerID = user.CustomerID,
                    Date = DateTime.Now,
                    DepartmentID = 0,//user.DepartmentID,
                    EmailSubject = subject,
                    EmailBody = gridHtml,
                    EventType = (int)Consts.enEventTypes.orderFromSupplier,
                    UserID = user.UserID,
                    UserName = user.FullName,
                    EmailTo = to,
                    SendTo=Consts.OFFICE_VALUE,
                });
                if (Helper.sendEmail(from,
                                     to,
                                     subject,
                                     body,
                                     ConfigurationManager.AppSettings["SmtpServerUserName"],
                                     ConfigurationManager.AppSettings["SmtpServerPassword"],
                                     "",
                                    Server.MapPath("~/resources/images/Logo.png")
                                     ))


                    showHideSucceedMessage(true, Consts.CONST_SUCCEED_MESSAGE);




            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }


        }
        private void showHideSucceedMessage(bool toShow, string text = "")
        {
            if (toShow)
            {
                dvErrorMessage.InnerText = text;
                dvErrorMessage.Style["display"] = "block";
                dvErrorMessage.Style["visibility"] = "visible";
                dvErrorMessage.Style["background-color"] = "rgba(182, 255, 0, 0.27)";
            }
            else
            {
                dvErrorMessage.Style["display"] = "none";
                dvErrorMessage.Style["visibility"] = "hidden";
                dvErrorMessage.Style["background-color"] = "rgba(255, 255, 255, 0.16)";
            }

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {

            StringWriter swLogo = new StringWriter();
            HtmlTextWriter hwLogo = new HtmlTextWriter(swLogo);
            imgLogo.RenderControl(hwLogo);
            string logoHTML = swLogo.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            string gridHtml = string.Empty; 

            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            string style = "<style type = 'text/css'>thead {display:table-header-group;} tfoot{display:table-footer-group;}</style>";
            sb.Append(style + gridToHTMLReport(out gridHtml).Replace("logo", logoHTML));
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();");
            sb.Append("};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            //List<SuppliersOrderUI> list = SuppliersOrderController.getByOrderID(orderId);
            //dgResults.DataSource = list;
            //dgResults.DataBind();

        }


        private string gridToHTMLReport(out string gridHtml)
        {

            dgResults.UseAccessibleHeader = true;
            if (dgResults.HeaderRow != null)
                dgResults.HeaderRow.TableSection = TableRowSection.TableHeader;
            if (dgResults.FooterRow != null)
                dgResults.FooterRow.TableSection = TableRowSection.TableFooter;
            dgResults.Attributes["style"] = "border-collapse:separate";
            foreach (GridViewRow row in dgResults.Rows)
            {
                if (row.RowIndex % 10 == 0 && row.RowIndex != 0)
                {
                    row.Attributes["style"] = "page-break-after:always;";
                }
            }
            if (dgResults.Rows.Count > 23)
                dvFooterButtom.Style["position"] = "inherit";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            dgResults.RenderControl(hw);
            gridHtml = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");

            StringWriter swHeader = new StringWriter();
            HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
            dvHeader.RenderControl(hwHeader);

            SuppliersOrderUI order = SuppliersOrderController.getOrder(!string.IsNullOrEmpty( Request.QueryString["suppliersOrderID"])? Request.QueryString["suppliersOrderID"] :"");

            string headerHTML = swHeader.ToString().Replace("\"", "'")
            .Replace("spnOrderNum", order==null?"": order.SuppliersOrderID)
            .Replace("spnOrderDate", order == null ? "" : order.Date.ToString())
            .Replace("spnSupplier", order == null ? "" : order.SupplierName)
            .Replace("spnPhone", order == null ? "" : order.Telephon1)
            .Replace("spnFax", order == null ? "" : order.Fax)
            .Replace(System.Environment.NewLine, "");
            StringWriter swFooter = new StringWriter();
            HtmlTextWriter hwFooter = new HtmlTextWriter(swFooter);
            dvFooter.RenderControl(hwFooter);
            string footerHTML = swFooter.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
            return "<div style='height:100%;width:840px;border-style:solid'>" + headerHTML + "<div style='direction: rtl;padding-right:30px' >" + gridHtml + "</div>" + footerHTML + "</div";
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /*Verifies that the control is rendered */
        }

    }
}