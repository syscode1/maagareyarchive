﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class EditViewEvent:Page
    {

        public Users user
        {
            get
            {
                if (Session["UserEntity"] == null)
                    Response.Redirect("Login.aspx");
                return Session["UserEntity"] as Users;
            }
        }
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillddlCustomers();
                fillddlEventTypes();
                fillddlUsers();


                if (Request.QueryString["row_id"] != null)
                {
                    int rowID = Helper.getInt(Request.QueryString["row_id"]).Value;
                    header.InnerText += (" " + rowID.ToString());
                    EventsUI evnt = EventController.getEventByID(rowID);
                    txtFile.Text = evnt.FileID;
                    //txtDescription.Text = evnt.EventDescription ;
                    txtDescription.Height =new Unit(50);
                    spnDescription.InnerHtml = evnt.EventDescription + evnt.EmailBody;
                    ddlCustomer.SelectedValue = evnt.CustomerID.ToString();
                    ddlEventType.SelectedValue = evnt.EventType.ToString();
                    if (evnt.SendTo != null)
                        ddlTo.SelectedValue = evnt.SendTo.ToString();
                    txtFrom.Text = evnt.FullName;
                    trFrom.Style["display"] = "";
                    //btnSend.Style["display"] = "none";
                    txtFile.Font.Bold = true;
                    txtFile.Style["cursor"] = "pointer";
                    txtFile.Font.Underline = true;
                    txtFile.Attributes["onclick"] = "return popup('FileStatusHistory.aspx', 'notes'," + evnt.FileID + ")";
                    disableFields();
                }
            }
        }

        private void disableFields()
        {
            //txtDescription.Style["background-color"] = "#eaeaea";
            //txtDescription.Enabled = false;
            txtFile.Style["background-color"] = "#eaeaea";
            txtFrom.Enabled = false;
            txtFrom.Style["background-color"] = "#eaeaea";
            ddlCustomer.Enabled = false;
            ddlCustomer.Style["background-color"] = "#eaeaea";
            ddlEventType.Enabled = false;
            ddlEventType.Style["background-color"] = "#eaeaea";
        }

        private void fillddlUsers()
        {
            OrderedDictionary data=new OrderedDictionary();
            if (user.ArchiveWorker == true)
            {
                data = UserController.getAllArchiveUsers();
            }
            data.Insert(0, Consts.OFFICE_VALUE, Consts.OFFICE);
            fillDdl(ddlTo, data);
        }

        private void fillddlCustomers()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
            if (ddlCustomer.Items.FindByValue("0") != null)
                ddlCustomer.Items.Remove(ddlCustomer.Items.FindByValue("0"));
        }
        private void fillddlEventTypes()
        {
            ddlEventType.DataSource = EventController.getEventTypes();
            ddlEventType.DataTextField = "EventTypeName";
            ddlEventType.DataValueField = "EventTypeID";
            ddlEventType.DataBind();
        }

        private void fillDdl(DropDownList ddl, object data)
        {
            ddl.DataSource = data;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                bool isSuccedd;
                string to = ParametersController.GetParameterValue(Consts.Parameters.PARAM_EMAIL_MESSAGE_TO_ADDRESS);
                string from = ParametersController.GetParameterValue(Consts.Parameters.PARAM_EMAIL_MESSAGE_FROM_ADDRESS);
                if(Request.QueryString["row_id"] != null)
                {
                    isSuccedd= EventController.updateTo(Helper.getIntNotNull(Request.QueryString["row_id"]), Helper.getIntNotNull(ddlTo.SelectedValue), txtDescription.Text);
                }
                else
                     isSuccedd = EventController.insert(new Events()
                {
                    CustomerID = Helper.getIntNotNull(ddlCustomer.SelectedValue),
                    Date = DateTime.Now,
                    EventDescription = txtDescription.Text,
                    EventType = Helper.getInt(ddlEventType.SelectedValue).Value,
                    FileID = txtFile.Text,
                    UserID = user.UserID,
                    UserName = user.FullName,
                    SendTo =Helper.getIntNotNull( ddlTo.SelectedValue),
                });

                showHideSucceedMessage(isSuccedd,true, isSuccedd? Consts.CONST_EVENT_SUCCEED_MESSAGE: Consts.SAVED_FAIL);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "CallMyFunction", "window.opener.location.reload();window.close()", true);

            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

        }
        private void showHideSucceedMessage(bool isSuccedd,bool toShow, string text = "")
        {
            if (toShow)
            {
                if (isSuccedd)
                {
                    dvSucceedMessage.InnerText = text;
                    dvSucceedMessage.Style["display"] = "block";
                    dvSucceedMessage.Style["visibility"] = "visible";
                    dvSucceedMessage.Style["background-color"] = "rgba(182, 255, 0, 0.27)";
                }
                else
                {
                    dvErrorMessage.InnerText = text;
                    dvErrorMessage.Style["display"] = "block";
                    dvErrorMessage.Style["visibility"] = "visible";
                    //dvErrorMessage.Style["background-color"] = "rgba(182, 255, 0, 0.27)";

                }
            }
            else
            {
                dvSucceedMessage.Style["display"] = "none";
                dvSucceedMessage.Style["visibility"] = "hidden";
                dvSucceedMessage.Style["background-color"] = "rgba(255, 255, 255, 0.16)";
                dvErrorMessage.Style["display"] = "none";
                dvErrorMessage.Style["visibility"] = "hidden";
               // dvErrorMessage.Style["background-color"] = "rgba(255, 255, 255, 0.16)";
            }
        }

    }
}