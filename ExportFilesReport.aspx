﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ExportFilesReport.aspx.cs" Inherits="MaagareyArchive.ExportFilesReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>יצוא דוחו"ת אקסל
    </h1>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            enableDatepicker();
        });

        $(function () {
            enableDatepicker();
        });


        function enableDatepicker() {
            $('#<%=txtStartDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
            $('#<%=txtEndDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
            $('#<%=txtFromInsertDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
            $('#<%=txtToInsertDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
        }

        function rbChanged(radioButton) {

            if (radioButton.id.indexOf("rbBoxes") > -1) {
                if (radioButton.checked == true) {
                    showHideFields(false)
                }
                else {
                    showHideFields(true)
                }
            }
            if (radioButton.id.indexOf("rbFiles") > -1) {
                if (radioButton.checked == true) {
                    showHideFields(true)
                }
                else {
                    showHideFields(false)
                }
            }
            return true;

        }

        function showHideFields(toShow) {
            if (toShow == true) {
                document.getElementById('trYear').style["visibility"] = "visible";
                document.getElementById('trYear').style["display"] = "";

                document.getElementById('trDate').style["visibility"] = "visible";
                document.getElementById('trDate').style["display"] = "";

                document.getElementById('trSendUser').style["visibility"] = "visible";
                document.getElementById('trSendUser').style["display"] = "";


            }
            else {
                document.getElementById('trYear').style["visibility"] = "hidden";
                document.getElementById('trYear').style["display"] = "none";

                document.getElementById('trDate').style["visibility"] = "hidden";
                document.getElementById('trDate').style["display"] = "none";

                document.getElementById('trSendUser').style["visibility"] = "hidden";
                document.getElementById('trSendUser').style["display"] = "none";
            }

        }
        $(document).ready(function () {
            showHideByRb();
        });

        function showHideByRb() {
            showHideFields(document.getElementById('<%= rbFiles.ClientID%>').checked);
        }

        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                        window.location = "MainPageArchive.aspx";
                    else
                        window.location = "MainPage.aspx";
                }
    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <div id="blinkDiv" name="blinkDiv">
                            <asp:Image ImageUrl="resources/images/light.png" Style="width: 25px" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
            <table style="border-spacing: 10px; display: inline">
                <tr id="trCustomers" runat="server">
                    <td>
                        <span>לקוח</span>
                        <br />

                        <asp:DropDownList ID="ddlCustomer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td>
                        <span>מחלקה</span>
                        <br />

                        <asp:DropDownList ID="ddlDepartment" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <span>מתאריך קליטה בארכיון</span>
                        <br />
                        <input type="text" runat="server" id="txtFromInsertDate" />
                    </td>
                    <td>
                        <span>עד תאריך קליטה בארכיון</span>
                        <br />
                        <input type="text" runat="server" id="txtToInsertDate" />
                    </td>

                </tr>
                <tr>
                    <td colspan="3">
                        <asp:RadioButton ID="rbBoxes" Text="ארגזים" Checked="true" GroupName="urgent" runat="server" onclick="rbChanged(this)" />
                        <asp:RadioButton ID="rbFiles" Text="תיקים" GroupName="urgent" runat="server" onclick="rbChanged(this)" />

                    </td>
                </tr>
                <tr id="trDate" style="display: none; visibility: hidden">
                    <td style="text-align: right"></td>

                    <td>
                        <span>מתאריך</span>
                        <br />
                        <input type="text" runat="server" id="txtStartDate" />
                    </td>
                    <td>
                        <span>עד תאריך</span>
                        <br />
                        <input type="text" runat="server" id="txtEndDate" />
                    </td>

                </tr>
                <tr id="trYear" style="display: none; visibility: hidden">
                    <td></td>
                    <td>
                        <span>משנה</span>
                        <br />
                        <asp:TextBox ID="txtFromYear" runat="server" />
                    </td>
                    <td>
                        <span>עד שנה</span>
                        <br />
                        <asp:TextBox ID="txtToYear" runat="server" />
                    </td>

                </tr>

                <tr id="trSendUser" style="display: none; visibility: hidden">
                    <td></td>
                    <td>
                        <span>מס' משתמש שולח</span>
                        <br />
                        <asp:TextBox ID="txtSenderNum" runat="server" />
                    </td>
                    <td></td>
                </tr>

            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

        <table style="margin-top: 10px; border-spacing: 0px; float: left; width: 100%">
        <tr>
            <td style="width: 240px; text-align: right;">
                <input type="button" value="יציאה" onclick="exit()" runat="server" />
            </td>
            <td>
       <asp:Button Text="יצא" ID="btnExport" Style="float: left" Width="85px" OnClick="btnExport_Click" runat="server" />

            </td>
        </tr>
    </table>

</asp:Content>
