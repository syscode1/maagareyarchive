﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ExportShippingsReceived.aspx.cs" Inherits="MaagareyArchive.ExportShippingsReceived" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>דוח אישורי מסירה
    </h1>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            enableDatepicker();
        });

        $(function () {
            enableDatepicker();
        });


        function enableDatepicker() {
            $('#<%=txtToDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
            $('#<%=txtFromDate.ClientID%>').datepicker({ dateFormat: "dd-mm-yy" });
        }

        function print() {
            window.frames["print_frame"].document.body.innerHTML = document.getElementById("dvGridResults").innerHTML;
            window.frames["print_frame"].window.focus();
            window.frames["print_frame"].window.print();

        }

        function popup(mylink, windowname, fileID) {
            if (!window.focus)
                return true;
            var href;
            var w = 1200;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            if (typeof (mylink) == 'string')
                href = mylink;
            else href = mylink.href;
            window.open(href + "?ShippingID=" + fileID, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }

        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                        window.location = "MainPageArchive.aspx";
                    else
                        window.location = "MainPage.aspx";
                }

    </script>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table style="border-spacing: 10px; display: inline">
                <tr id="trCustomers" runat="server">
                    <td>
                        <span>לקוח</span>
                        <br />

                        <asp:DropDownList ID="ddlCustomer" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td></td>
                </tr>

                <tr>

                    <td>
                        <span>מתאריך מסירה  </span>
                        <br />
                        <input type="text" runat="server" id="txtFromDate" />
                    </td>
                    <td>
                        <span>עד תאריך מסירה</span>
                        <br />
                        <input type="text" runat="server" id="txtToDate" />
                    </td>

                </tr>

                <tr>
                    <td>
                        <span>מספר מסירה</span>
                        <br />
                        <asp:TextBox ID="txtShippingNumber" onkeypress="return isNumberKey(event)" runat="server" />
                    </td>
                    <td></td>
                </tr>

            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="width: 100%; float: left; margin-top: 40px">
        <asp:Button Text="יצא" ID="btnExport" OnClick="btnExport_Click" Style="float: left; margin: 10px" Width="85px" runat="server" />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <table style="margin: 0px; border-spacing: 0px; float: left;">
                    <tr>
                        <td style="width: 620px; text-align: right;">
                            <input type="button" value="יציאה" onclick="exit()" runat="server" />

                        </td>
                        <td>
                            <asp:Button Text="הצג" ID="btnShow" Style="float: left; margin: 10px" Width="85px" OnClick="btnShow_Click" runat="server" />

                        </td>
                    </tr>
                </table>

                <div id="dvGridResults">
                    <asp:DataGrid ID="dgResults" runat="server"
                        AutoGenerateColumns="false" CssClass="grid">
                        <%--DataKeyField="FileID"--%>

                        <HeaderStyle CssClass="header" />

                        <AlternatingItemStyle CssClass="alt" />
                        <ItemStyle CssClass="row" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="מספר מסירה" ItemStyle-CssClass="ltr" SortExpression="FileID">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <a href="ShippingReceivedReport.aspx" onclick="return popup(this, 'notes','<%#(DataBinder.Eval(Container.DataItem, "ShippingID"))%>')">
                                        <span id="fileID" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ShippingID"))==null?"":(DataBinder.Eval(Container.DataItem, "ShippingID")).ToString()%></span></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="תאריך מסירה" ItemStyle-CssClass="ltr" SortExpression="Date">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <span id="date" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ReceivedDate"))%> </span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="כמות תיקים/ הזמנות" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <span id="numOfFiles" runat="server"><%#(DataBinder.Eval(Container.DataItem, "count")).ToString()%> </span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="נמסר ל:" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                                <HeaderStyle CssClass="t-center" />
                                <ItemStyle CssClass="t-natural va-middle" />
                                <ItemTemplate>
                                    <span id="fullName" runat="server"><%#(DataBinder.Eval(Container.DataItem, "ReceivedBy"))%> </span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <asp:Button ID="btnPrint" Style="left: -98px; width: 85px; margin-top: 20px;" Text="הדפס" Visible="false" runat="server" OnClientClick="print()" />



                <iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>




            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
