﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ViewOrder : BasePage
    {
        public int rownum = 1;
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int customerId=loadData();
                setAddressFields();
                if (user.ArchiveWorker == true)
                    fillPlacesDdl(customerId);
            }

        }



        private void fillPlacesDdl(int customerId)
        {
            List<ListItem> data = new List<ListItem>();
            List<Users> users = UserController.getUsersListCustomers(Convert.ToInt32(customerId));

            foreach (var item in users)
            {
                string text = String.Format("{0} {1} {2}", item.CityAddress, item.FullAddress, item.DescriptionAddress);
                if(!string.IsNullOrEmpty(text.Trim()))
                   data.Add(new ListItem { Text = text, Value = item.UserID.ToString() });
            };
            Customers cust = CustomersController.getCustomerByID(customerId);
            if(cust!=null)
                data.Add(new ListItem { Text = String.Format("{0} {1} {2}", cust.CityAddress, cust.FullAddress, cust.DescriptionAddress), Value = "-1" });
            ddlPlace.DataSource = data;
            ddlPlace.DataTextField = "Text";
            ddlPlace.DataValueField = "Value";
            ddlPlace.DataBind();
            ddlPlace.Items.Add(new ListItem(Consts.CONST_ELSE_COMBO_ITEM, "0"));
            ddlPlace.SelectedValue = "-1";
           // tblAddressText.Style["display"] = "none";
            tblAddressDdl.Style["display"] = "";
        }

        private void setAddressFields()
        {
            //lblContactrName.InnerText = department.ContactMan + " ";
            //lblCityAddress.InnerText = department.CityAddress + " ";
            //lblFullAddress.InnerText = department.FullAddress + " ";
            //lblDescriptionAddress.InnerText = department.DescriptionAddress + " ";
           // lblCollectionPhone.InnerText = department.CollectionPhone;

            var listCache = new MyCache();
            ddlCities.DataSource = listCache.Cities;
            ddlCities.DataBind();
        }

        private int loadData()
        {
            List<OrderBufferUI> ob= OrderBufferController.GetOrderBuffer(user.UserID);
            dgResults.DataSource = ob;
            dgResults.DataBind();
            return ob.Count > 0 ? ob[0].CustomerID : 0;
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                List<OrderBufferUI> orderBuffer = new List<OrderBufferUI>();
                foreach (DataGridItem item in dgResults.Items)
                {

                    if ((item.FindControl("cbRemove") as CheckBox).Checked)
                    {
                        removeOrder((item.FindControl("fileID") as HtmlGenericControl).InnerText.Trim(), (item.FindControl("boxID") as HtmlGenericControl).InnerText.Trim());
                    }
                }

                loadData();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }

        private void removeOrder(string fileID, string boxID)
        {
            try
            {

                OrderBufferController.Remove(user.UserID, fileID, boxID);
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }

        }

        protected void btnOrder_Click(object sender, EventArgs e)
        {
            try
            {
                List<OrderBufferUI> orderBuffer = new List<OrderBufferUI>();
                foreach (DataGridItem item in dgResults.Items)
                {

                    if ((item.FindControl("cbRemove") as CheckBox).Checked)
                    {
                        removeOrder((item.FindControl("fileID") as HtmlGenericControl).InnerText.Trim(), (item.FindControl("boxID") as HtmlGenericControl).InnerText.Trim());
                    }
                    else {
                        OrderBufferController.UpdateUrgent(user.UserID,(item.FindControl("fileID") as HtmlGenericControl).InnerText.Trim(), (item.FindControl("boxID") as HtmlGenericControl).InnerText.Trim(), (item.FindControl("cbUrgent") as CheckBox).Checked);
                       
                    }
                }
                orderBuffer = OrderBufferController.GetOrderBuffer(user.UserID);
                if (orderBuffer.Count > 0)
                {
                    List<Orders> orders = new List<Orders>();
                    string orderID = Helper.getOrderID(user.UserID);
                    Users orderUser= user.ArchiveWorker == true && rbDepartmentAddress.Checked == true && !string.IsNullOrEmpty(ddlPlace.SelectedValue) ? UserController.getUserByUserID( Helper.getInt(ddlPlace.SelectedValue).Value) : user;
                    Customers cust = CustomersController.getCustomerByID(orderBuffer[0].CustomerID);
                    foreach (OrderBufferUI item in orderBuffer)
                    {
                        orders.Add(new Orders()
                        {
                            CustomerID = item.CustomerID,
                            DepartmentID = item.DepartmentID,
                            BoxID = item.BoxID,
                            FileID = item.FileID,
                            Date = DateTime.Now,
                            Urgent = item.Urgent,
                            OrderEmail = item.Email,
                            OrderFax = item.Fax,
                            CityAddress = rbDepartmentAddress.Checked ? (ddlPlace.SelectedValue=="-1"?cust.CityAddress: orderUser.CityAddress) : (rbOffice.Checked || rbFiling.Checked ? "" : ddlCities.SelectedItem.Text),
                            FullAddress = rbDepartmentAddress.Checked ? (ddlPlace.SelectedValue == "-1" ? cust.FullAddress : orderUser.FullAddress) : (rbOffice.Checked || rbFiling.Checked ? "" : txtAddress.Value),
                            DescriptionAddress = rbDepartmentAddress.Checked ? (ddlPlace.SelectedValue == "-1" ? cust.DescriptionAddress : orderUser.DescriptionAddress) : (rbOffice.Checked || rbFiling.Checked ? "" : txtDescription.Value),
                            Telephone1 = rbDepartmentAddress.Checked ? (ddlPlace.SelectedValue == "-1" ? cust.Telephon1 : orderUser.Telephone1) : (rbOffice.Checked || rbFiling.Checked ? "" : txtPhone.Value),
                            Telephone2 = (ddlPlace.SelectedValue == "-1" ? cust.Telephon2 : orderUser.Telephone2),
                            Fax = (ddlPlace.SelectedValue == "-1" ? cust.Fax : orderUser.Fax),
                            Email = (ddlPlace.SelectedValue == "-1" ? cust.Email : orderUser.Email),
                            LoginID = user.UserID,
                            NumInOrder = item.NumInOrder,
                            ContactMan = rbDepartmentAddress.Checked ? (ddlPlace.SelectedValue == "-1" ? cust.ContactMan : orderUser.FullName): (rbOffice.Checked || rbFiling.Checked ? "" : txtContactName.Value),
                            OrderID = orderID,
                            Office = rbOffice.Checked ? true : false,
                            Filing = rbFiling.Checked ? true : false,
                            Done = false,
                            Comment= txtComment.Value,
                            FaxMailComment= item.Comment + (rbOffice.Checked==true? " משרד ":(rbFiling.Checked==true?" תיוקים ":"")),
                        });

                    }
                    bool isSucceed = OrdersController.Insert(orders);
                    if (isSucceed)
                    {
                        foreach (OrderBufferUI item in orderBuffer)
                        {
                            OrderBufferController.Remove(item.LoginID, item.FileID, item.BoxID);
                        }

                        Response.Redirect("OrderConfirmation.aspx?orderID=" + orderID, false);
                    }
                    else
                    {
                       
                       
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }
    }
}
