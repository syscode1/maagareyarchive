﻿using MA_DAL.MA_HELPER;
using System;


namespace MaagareyArchive
{
    public partial class CustomerScreen : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("MainPageArchive.aspx"+(Request.QueryString!=null? ("?"+Request.QueryString.ToString()):""));
        }
    }
}