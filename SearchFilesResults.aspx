﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" CodeBehind="SearchFilesResults.aspx.cs" Inherits="MaagareyArchive.SearchFilesResults" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="resources/js/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        function urgentOrderCheck(button) {
            if (button.checked) {
                var userUrgentPermission = '<%= user.PermForUrgentDelivery%>';
                if (userUrgentPermission == 'False') {
                    alert("ההרשאה להזמנת חומר דחוף חסומה.")
                    return false;
                }
                else {
                    var isConfirm = confirm("משלוח דחוף מחייב בתשלום נוסף")
                    if (!isConfirm)
                        button.checked = false;
                }
            }

        }

        //function getSelectedFiles() {
        //    var frm = document.forms[0];
        //     for(i=0;i< frm.length;i++)                                 
        //     {                                                                 
        //            e=frm.elements[i];                                   
        //            if(e.type=='checkbox' && e.name.indexOf('cbCheck') != -1)
        //               e.checked= true ;                        
        //     }                                                               
        //    }                                                                


        function popup(mylink, windowname, paramName, ID, w, h) {
            if (!window.focus)
                return true;
            var href;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            if (typeof (mylink) == 'string')
                href = mylink;
            else href = mylink.href;
            window.open(href + "?" + paramName + "=" + ID, windowname, 'width=' + w + ',height=' + h + ',scrollbars=no, top=' + top + ', left=' + left);
            return false;
        }

        function exit() {
            if ('<%= user.ArchiveWorker %>' == 'True')
                window.location = "MainPageArchive.aspx";
            else
                window.location = "MainPage.aspx";
        }



        $(document).ready(function () {
            $("#<%=dgResults.ClientID%>").tablesorter({ dateFormat: "uk" });
            SetDefaultSortOrder();

            $('#<%=dgResults.ClientID%> input').click(function () {

                  var order = this.checked ? '1' : '0';
                  $(this).prev().html(order);
                  $(this).parents("table").trigger("update");
              })
              $('[id*=cbFax] ,[id*=cbEmail]').click(function () {
                  if (this.checked == true) {
                      var hfComment = $('[id*=hfComment]', this.closest('tr'))[0];
                      $('[id*=cbCheck]', this.closest('tr'))[0].checked = true;
                      $('#txtComment').get(0).value = hfComment.value;
                      var x = $("#dialog").dialog(
                          {
                              buttons: {
                                  'OK': function () {
                                      hfComment.value = $('#txtComment').get(0).value;
                                      $(this).dialog("destroy");
                                  },
                                  'Cancel': function () {
                                      $(this).dialog("destroy");
                                      // I'm sorry, I changed my mind                 
                                  }
                              }
                          });

                  }
                  else if ($('[id*=cbFax]', this.closest('tr'))[0].checked == false &&
                           $('[id*=cbEmail]', this.closest('tr'))[0].checked == false &&
                           $('[id*=cbBox]', this.closest('tr'))[0].checked == false)
                      $('[id*=cbCheck]', this.closest('tr'))[0].checked = false;

              })
        });

          function Sort(cell, sortOrder) {
              var sorting = [[cell.cellIndex, sortOrder]];
              $("#<%=dgResults.ClientID%>").trigger("sorton", [sorting]);
              if (sortOrder == 0) {
                  sortOrder = 1;
                  cell.className = "sortDesc";
              }
              else {
                  sortOrder = 0;
                  cell.className = "sortAsc";
              }
              cell.setAttribute("onclick", "Sort(this, " + sortOrder + ")");
              cell.onclick = function () { Sort(this, sortOrder); };
              document.getElementById("container").scrollTop = 0;
          }

          function SetDefaultSortOrder() {
              var gvHeader = document.getElementById("<%=dummyHeader.ClientID%>");
        var dgResults = document.getElementById("<%=dgResults.ClientID%>");
        var dvGrid = document.getElementById("dvGrid");

        var width = 571;
        var headers = gvHeader.getElementsByTagName("TH");
        for (var i = 0; i < headers.length; i++) {
            if (i > 7)
                width = width + 103;
            headers[i].setAttribute("onclick", "Sort(this, 1)");
            headers[i].onclick = function () { Sort(this, 1); };
            headers[i].className = "sortDesc";
            //dgResults.getElementsByTagName("TH")[i].style.width = headers[i].style.width;
        }
        dvGrid.style.width = (width + 17 + (width * 0.005)).toString() + "px";
        gvHeader.style.width = (width + (width * 0.005)).toString() + "px";
    }
    window.onload = function () {
    };
    function getShelfHistoryTable(boxID) {
        jQuery.ajax({
            url: 'OrderDetailes.aspx/getShelfHistory',
            type: "POST",
            data: "{boxID : '" + boxID + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (data) {
                debugger
                if (data.d != null) {
                    buildShelfHistoryTable(data.d);

                }
            },
            failure: function (msg) { }
        });
    }
    function buildShelfHistoryTable(data) {

        var obj = JSON.parse(data);
        var globalCounter = 0;
        tbody = document.getElementById('tbodyShelfsHistory');
        tbody.innerHTML = "";
        for (var i = 0; i < obj.length; i++) {
            var tr = "<tr>";
            /* Must not forget the $ sign */
            tr += "<td>" + new Date(obj[i].Date).format('MM/dd/yyyy') + "</td><td>" + obj[i].ShelfID + "</td>" + "</tr>";

            /* We add the table row to the table body */
            tbody.innerHTML += tr;
        }

    }
    function updateShelf(a, boxID) {

        var newShelf;
        $('#txtNewShelf')[0].value = "";
        $('#dvShelfMessage').get(0).innerText = "";
        $('#txtNewShelf')[0].focus();
        getShelfHistoryTable(boxID);
        var text = $("#dialogShelf").dialog(
            {
                buttons: {

                    'OK': function () {
                        debugger
                        var x = confirm('האם אתה בטוח?');
                        if (x == true) {
                            $('#dvShelfMessage').get(0).innerText = '';
                            newShelf = $('#txtNewShelf').get(0).value;
                            $('#txtNewShelf').focus();
                            jQuery.ajax({
                                url: 'OrderDetailes.aspx/updateShelfID',
                                type: "POST",
                                data: "{boxID : '" + boxID + "',newShelfID: '" + newShelf + "'}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                beforeSend: function () {
                                },
                                success: function (data) {
                                    debugger
                                    if (data.d == "0") {
                                        $('#dvShelfMessage').get(0).innerText = 'שגיאה בשמירת נתונים';
                                    }
                                    else {
                                        $('#spnShelfID', a)[0].innerText = newShelf;
                                        $("#dialogShelf").dialog("destroy");

                                    }
                                },
                                failure: function (msg) { }
                            });
                        }


                    },
                    'Cancel': function () {
                        $(this).dialog("destroy");
                        // I'm sorry, I changed my mind                 
                    }
                }

            });
        $('#txtNewShelf').focus();
    }
    </script>
    <style type="text/css">
        .grid {
            font-family: Arial;
            font-size: 10pt;
            width: 600px;
        }

            .grid THEAD {
                background-color: Green;
                color: White;
            }

        th {
            font-weight: bold;
            border-color: lightgray;
            color: rgb(6, 8, 6) !important;
            height: 30px;
            background-color: rgba(217, 251, 218, 0.23);
        }
    </style>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>

            <div id="dialog" title="הוסף הערה" style="display: none">
                <input id="txtComment" type="text" size="64" />
            </div>
            <div id="dialogShelf" title="הכנס מיקום" style="display: none">
                <input id="txtNewShelf" size="25" />
                <div id="dvShelfMessage" style="color: red"></div>
                <div id="dvShelfsHistory">
                    <div class="h1_sub" style="width: 216px; font-size: 16px; padding-top: 5px; margin-bottom: 8px;">הסטורית מיקומים</div>
                    <table style="width: 239px;" border="1" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>תאריך</th>
                                <th>מדף</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyShelfsHistory"></tbody>
                    </table>
                </div>
            </div>

            <h1>תוצאות חיפוש</h1>
            <div style="min-height: 300px;">
                <table style="width: 500px">
                    <tr>
                        <td>
                            <span><b>שם לקוח:</b></span>
                            <label id="lblCustName" runat="server"></label>
                        </td>
                        <td>
                            <span><b>מס' לקוח:</b></span>
                            <label id="lblCustNum" runat="server"></label>
                        </td>
                        <td>
                            <span><b>מחלקה:</b></span>
                            <label id="lblDep" runat="server"></label>
                        </td>
                    </tr>
                </table>
                <div id="dvMessage" runat="server" style="font-size: 20px; color: rgb(6, 8, 6);"></div>

                <table cellspacing="0" rules="all" border="1" id="dummyHeader" runat="server" style="border-collapse: collapse; word-wrap: break-word; width: 5401px; position: absolute; top: 239px; right: 3.16%;">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 30px">מס'</th>
                            <th scope="col" style="width: 40px">בחר</th>
                            <th scope="col" style="width: 61px">ארגז מלא</th>
                            <th scope="col" style="width: 40px">פקס</th>
                            <th scope="col" style="width: 40px">מייל</th>
                            <th scope="col" style="width: 40px">דחוף</th>

                            <th scope="col" style="width: 120px">ארגז </th>
                            <th scope="col" style="width: 120px">תיק</th>
                            <th scope="col" style="width: 120px">מדף</th>
                            <th scope="col" style="width: 120px">מס' ארגז לקוח</th>
                            <th scope="col" style="width: 120px">נושא</th>
                            <th scope="col" style="width: 120px">מס' נושא</th>

                        </tr>
                    </thead>
                </table>
                <div id="dvGrid" style="height: 300px; overflow-y: scroll; overflow-x: hidden; width: 5418px; direction: ltr; position: absolute; top: 273px; right: 2%;">

                    <asp:GridView ID="dgResults" runat="server"
                        AutoGenerateColumns="false" Style="direction: rtl; width: 100%"
                        OnRowDataBound="dgResults_RowDataBound">
                        <Columns>

                            <asp:TemplateField ItemStyle-Width="30px" HeaderText="מס'" ItemStyle-CssClass="ltr">
                                <ItemTemplate><%# rownum++  %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="בחר" ItemStyle-CssClass="ltr">
                                <HeaderStyle />
                                <ItemStyle Width="40px" />
                                <ItemTemplate>
                                    <span style="display: none">0</span>
                                    <asp:CheckBox ID="cbCheck" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ארגז מלא" ItemStyle-CssClass="ltr">
                                <ItemStyle Width="60px" />
                                <ItemTemplate>
                                    <span style="display: none">0</span>
                                    <asp:CheckBox ID="cbBox" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="פקס" ItemStyle-CssClass="ltr">
                                <ItemStyle Width="40px" />
                                <ItemTemplate>
                                    <span style="display: none">0</span>
                                    <asp:CheckBox ID="cbFax" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="מייל" ItemStyle-CssClass="ltr">
                                <ItemStyle Width="40px" />
                                <ItemTemplate>
                                    <span style="display: none">0</span>
                                    <asp:CheckBox ID="cbEmail" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="דחוף" ItemStyle-CssClass="ltr">
                                <ItemStyle Width="40px" />
                                <ItemTemplate>
                                    <span style="display: none">0</span>
                                    <asp:CheckBox ID="cbUrgent" OnClick="return urgentOrderCheck(this)" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ארגז" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                                <ItemStyle Width="120px" />
                                <ItemTemplate>
                                    <a href="InputData.aspx" onclick="return popup(this, 'notes','boxId','<%#(DataBinder.Eval(Container.DataItem, "bBoxID"))%>',1000,700)">
                                        <span id="boxID" runat="server"><%#(Eval("bBoxID"))%> </span></a>
                                    <span id="departmentID" runat="server" style="visibility: hidden; display: none"><%#(DataBinder.Eval(Container.DataItem, "BoxDepartmentID")).ToString()%> </span>
                                    <span id="customerID" runat="server" style="visibility: hidden; display: none"><%#(DataBinder.Eval(Container.DataItem, "BoxCustomerID")).ToString()%> </span>
                                    <asp:HiddenField ID="hfComment" runat="server" />
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="תיק" ItemStyle-CssClass="ltr" SortExpression="FileID">
                                <ItemStyle Width="120px" />
                                <ItemTemplate>
                                    <a href="FileStatusHistory.aspx" onclick="return popup(this, 'notes','fileID','<%#(DataBinder.Eval(Container.DataItem, "FileID"))%>',500,600)">
                                        <span id="fileID" runat="server"><%#(Eval("FileID"))==null?"":(DataBinder.Eval(Container.DataItem, "FileID")).ToString()%></span></a>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="מדף" ItemStyle-CssClass="ltr" SortExpression="FileID">
                                <ItemStyle Width="120px" />
                                <ItemTemplate>
                                    <a id="shelfID" href="#" onclick="return '<%=user.ArchiveWorker %>'=='True'? updateShelf(this,'<%#(DataBinder.Eval(Container.DataItem, "bBoxID"))%>'):false">
                                        <span id="spnShelfID"><%#DataBinder.Eval(Container.DataItem, "ShelfID")%></span></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="מס' ארגז לקוח" ItemStyle-CssClass="ltr" SortExpression="FileID">
                                <ItemStyle Width="120px" />
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "CustomerBoxNum")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="נושא" ItemStyle-CssClass="ltr" SortExpression="FileID">
                                <ItemStyle Width="120px" />
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "SubjectName")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="מס' נושא" ItemStyle-CssClass="ltr" SortExpression="FileID">
                                <ItemStyle Width="120px" />
                                <ItemTemplate>
                                    <span><%#DataBinder.Eval(Container.DataItem, "SubjectID")!=null && DataBinder.Eval(Container.DataItem, "SubjectID").ToString()!="0"?DataBinder.Eval(Container.DataItem, "SubjectID").ToString():""%></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>


                <%--        <div style="width:100%;direction:ltr;margin-right: -160px;height:400px; overflow:auto">
                <asp:GridView ID="dgResults1" runat="server" 
                    AutoGenerateColumns="false" CssClass="grid" style="direction:rtl"
                    EnableViewState="true" OnRowDataBound="dgResults_RowDataBound" AllowSorting="true" OnSortCommand="dgResults_SortCommand">


                    <HeaderStyle CssClass="header" Width="100px"  />

                    <AlternatingRowStyle CssClass="alt" />
                    <RowStyle CssClass="row" />
                    <Columns>

                        <asp:TemplateField HeaderText="מס'" ItemStyle-CssClass="ltr">
                            <HeaderStyle  />
                            <ItemStyle  />
                            <ItemTemplate><%# rownum++  %></ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ארגז" ItemStyle-CssClass="ltr" SortExpression="BoxID">
                            <HeaderStyle  />
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <span id="boxID" runat="server"><%#(Eval("bBoxID"))%> </span>
                                <span id="departmentID" runat="server" style="visibility: hidden; display: none"><%#(DataBinder.Eval(Container.DataItem, "BoxDepartmentID")).ToString()%> </span>
                                <span id="customerID" runat="server" style="visibility: hidden; display: none"><%#(DataBinder.Eval(Container.DataItem, "BoxCustomerID")).ToString()%> </span>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="תיק" ItemStyle-CssClass="ltr" SortExpression="FileID">
                            <HeaderStyle  />
                            <ItemStyle  Width="100px"/>
                            <ItemTemplate>
                                <a href="FileStatusHistory.aspx" onclick="return popup(this, 'notes','<%#(DataBinder.Eval(Container.DataItem, "FileID"))%>')">
                                    <span id="fileID" runat="server"><%#(Eval("FileID"))==null?"":(DataBinder.Eval(Container.DataItem, "FileID")).ToString()%></span></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--                  <asp:TemplateColumn HeaderText="תאור" ItemStyle-CssClass="ltr"  SortExpression="Description1">
                    <HeaderStyle  />
                    <ItemStyle  />
                    <ItemTemplate> <%#DataBinder.Eval(Container.DataItem, "FileID")==null?DataBinder.Eval(Container.DataItem, "BoxDescription"): DataBinder.Eval(Container.DataItem, "Description1")%></ItemTemplate>
                </asp:TemplateColumn>  
                  <asp:TemplateColumn HeaderText="משנה" ItemStyle-CssClass="ltr"  SortExpression="StartYear">
                    <HeaderStyle  />
                    <ItemStyle  />
                    <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "StartYear")%></ItemTemplate>
                </asp:TemplateColumn>  
                  <asp:TemplateColumn HeaderText="עד שנה" ItemStyle-CssClass="ltr"  SortExpression="EndYear">
                    <HeaderStyle  />
                    <ItemStyle  />
                    <ItemTemplate><%# DataBinder.Eval(Container.DataItem, "EndYear")%></ItemTemplate>
                </asp:TemplateColumn>   
                        <asp:TemplateField HeaderText="בחר" ItemStyle-CssClass="ltr">
                            <HeaderStyle  />
                            <ItemStyle  Width="100px"/>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbCheck" runat="server" /></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ארגז" ItemStyle-CssClass="ltr">
                            <HeaderStyle  />
                            <ItemStyle  Width="100px"/>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbBox" runat="server" /></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="פקס" ItemStyle-CssClass="ltr">
                            <HeaderStyle  />
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="cbFax" runat="server" /></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="מייל" ItemStyle-CssClass="ltr">
                            <HeaderStyle  />
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="cbEmail" runat="server" /></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="דחוף" ItemStyle-CssClass="ltr">
                            <HeaderStyle  />
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="cbUrgent" OnClick="return urgentOrderCheck(this)" runat="server" /></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                    </div>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div>
        <table style="margin-top: 65px">
            <tr>
                <td style="width: 462px; text-align: right;">
                    <input type="button" value="יציאה" onclick="exit()" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnAddAndBack" Text="הוספה וחזרה לחיפוש" runat="server" OnClick="btnAddAndBack_Click" />
                </td>
                <td>
                    <asp:Button ID="btnAdd" Text="הוסף להזמנה" runat="server" OnClick="btnAdd_Click" />
                </td>
                <%--OnClientClick="return getSelectedFiles()"--%>
            </tr>
        </table>

    </div>
</asp:Content>
