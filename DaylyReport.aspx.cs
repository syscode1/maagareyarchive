﻿using MA_DAL.MA_BL;
using MA_DAL.MA_HELPER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class DaylyReport : BasePage
    {
        protected override string[] AllowedPermissions { get { return new string[] { Permissions.PermissionKeys.archivWorker }; } }

        private List<MignazaUI> calcWarehouse
        {
            get
            {
                if (Session["DaylyReport_calcWarehouse"] != null)
                    return Session["DaylyReport_calcWarehouse"] as List<MignazaUI>;
                return new List<MignazaUI>();
            }
            set
            {
               Session["DaylyReport_calcWarehouse"]  = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            sumBoxes.Text = Consts.SUM_OF_BOXES;
            if(!Page.IsPostBack)
            bindData();
        }

        private void bindData()
        {
            sumBoxes.Text += " " + BoxesController.getBoxesCount(0, new List<int>()).ToString();
            DateTime? fromDate = Helper.getDate(txtFromDate.Value);
            fromDate = fromDate == null ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 0, 0, 0) : fromDate;
            DateTime? toDate = Helper.getDate(txtToDate.Value);
            toDate = toDate == null ? new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59) :
                new DateTime(toDate.Value.Year, toDate.Value.Month, toDate.Value.Day, 23, 59, 59);

            int sumFull = BoxesController.getSumFullNewBoxes(fromDate.Value, toDate.Value);
            spnFullNewBoxes.InnerText = sumFull.ToString();
            int sumEmptyNewBoxes = BoxesController.getSumEmptyNewBoxes(fromDate.Value, toDate.Value, sumFull);
            spnEmptyNewBoxes.InnerText = sumEmptyNewBoxes.ToString();
            int sumPlacedBoxes = BoxesController.getSumPlacedBoxes(fromDate.Value, toDate.Value);
            spnNewPlacedBoxes.InnerText = sumPlacedBoxes.ToString();
            spnNewNotPlacedBoxes.InnerText = (sumFull + sumEmptyNewBoxes - sumPlacedBoxes).ToString();
            spnNewBoxes.InnerText = (sumFull + sumEmptyNewBoxes).ToString();
            int sumBoxesForExterminate = BoxesController.sumBoxesForExterminate();
            spnBoxesForExterminate.InnerText = sumBoxesForExterminate.ToString();
            spnNotExistExtractions.InnerText = FileStatusController.sumNotFoundFiles(fromDate.Value, toDate.Value).ToString();

            calcWarehouse=MignazaController.calcSumByWarehous();
            dgResults.DataSource = calcWarehouse;
            dgResults.DataBind();
        }

        protected void showReport_Click(object sender, EventArgs e)
        {
            bindData();
        }

        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                (e.Item.FindControl("lblSum") as Label).Text = calcWarehouse.Select(x => x.NumBoxCapacity - x.NumBoxesOut).Sum().ToString();
            }
        }
    }
}