﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageSuppliers.aspx.cs" Inherits="MaagareyArchive.ManageSuppliers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <table style="padding: 20px; margin: 20px; border-spacing: 40px">
                    <tr>
                        <td>
                            <asp:Button Text="הזמנה מספק" ID="OrderFromSupplier" class="button1" runat="server" OnClick="OrderFromSupplier_Click" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button Text="ניהול ומעקב הזמנות" ID="ManageOrders" class="button1" runat="server" OnClick="ManageOrders_Click"  /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button Text="פתח ספק חדש" ID="NewSupplier" class="button1" OnClick="NewSupplier_Click" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button Text="טבלת פריטים" ID="Items" class="button1" OnClick="Items_Click" runat="server"  /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>
