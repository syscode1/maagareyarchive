﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ClearOldBoxes.aspx.cs" Inherits="MaagareyArchive.ClearOldBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>    

    </script>
    <h1>ניקוי ארגזים ישנים</h1>  
   
        <table cellpadding="10" cellspacing="10" style="display:inline">
        <tr>
            <td>
                <span>ממחסן</span>
                <asp:DropDownList ID="ddlFromWarehouse" runat="server">
                </asp:DropDownList>

            </td>
            <td>
                <span>עד מחסן</span>
                <asp:DropDownList ID="ddlToWarehouse" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <span>משורה</span>
                <asp:TextBox ID="txtFromRow" onkeypress="return isNumberKey(event,2)"   runat="server">
                </asp:TextBox>
            </td>
            <td>
                <span>עד שורה</span>
                <asp:TextBox ID="txtToRow" onkeypress="return isNumberKey(event,2)" runat="server">
                </asp:TextBox>
            </td>
        </tr>
            <tr>
                <td colspan="2" style="text-align:right">
               
                <asp:RadioButtonList ID="rblReport" runat="server" CellSpacing="10" >
                    <asp:ListItem Value="populated" Selected="True" Text="כמות מאוכלס" />
                    <asp:ListItem Value="max" Text="כמות מקסימלית" />
                    <asp:ListItem Value="empty" Text="כמות פנוי" />
                </asp:RadioButtonList>

                    

               </td>
            </tr>
                 
</table>


        <table style="width: 100%;">
        <tr>
            <td >
                <input type="button" value="חזרה לתפריט הראשי" onclick="window.location='MainPageArchive.aspx'; return false;" runat="server" />
            </td>
            <td style="float:left">
                                <asp:Button Text="יצא לאקסל" id="btnExportReport" OnClick="btnExportReport_Click" runat="server" />
</td>
        </tr>

    </table>

            <asp:UpdatePanel runat="server" ID="up">
        <ContentTemplate>  

                   <table style="width: 100%;">
        <tr>
            <td >
             </td>
            <td style="float:left">
                <asp:Button ID="txtViewReport" Text="הצג דוח" OnClick="txtViewReport_Click"  runat="server"  />
            </td>
        </tr>
                       </table>            
    <div>
                <asp:DataGrid  ID="dgResults" runat="server"
             AutoGenerateColumns="false"  CssClass="gridPopup"
             EnableViewState="true" style="width: auto;">
            <HeaderStyle CssClass="header"/>
            <AlternatingItemStyle CssClass="alt" />
            <ItemStyle CssClass="row" />
            <Columns>
            </Columns>
        </asp:DataGrid>

    </div>

        </ContentTemplate>
    </asp:UpdatePanel>
              <asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="up"
        runat="server">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Label ID="lblWait" runat="server"
                    Text=" Please wait... " Style="font-size: 16px; font-weight: bolder; vertical-align: bottom" />
                <asp:Image ID="imgWait" runat="server"
                    ImageAlign="Middle" Style="vertical-align: sub" ImageUrl="resources/images/16.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
