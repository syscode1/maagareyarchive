﻿using log4net;
using MA_CORE.MA_BL;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class ExportShipping : BasePage
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                OrderedDictionary data;
                if (user.ArchiveWorker == true)
                {

                    trCustomers.Visible = true;
                    fillCustomerDdl();
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedItem.Value))
                    {
                        data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomer.SelectedItem.Value));
                        data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
                        fillDdlOrderDict(data, ddlDepartment);

                        string customerID = Request.QueryString["custID"];
                        if (!string.IsNullOrEmpty(customerID))
                        {
                            ddlCustomer.SelectedValue = customerID;
                            if (Request.QueryString["from"] != null)
                                txtFromDate.Value = Request.QueryString["from"];
                            if (Request.QueryString["to"] != null)
                                txtToDate.Value = Request.QueryString["to"];
                            btnShow_Click(null, null);
                        }

                    }
                }
                else {
                    trCustomers.Visible = false;
                    data = DepartmentController.GetUserDepartmentsDictionary(user);
                    data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
                    fillDdlOrderDict(data, ddlDepartment);

                }
                fillDdlOrderDict(getUsers(), ddlLoginID);

            }
        }


        private void fillCustomerDdl()
        {
            ddlCustomer.DataSource = (new MyCache()).CustomersNames;
            ddlCustomer.DataTextField = "Value";
            ddlCustomer.DataValueField = "Key";
            ddlCustomer.DataBind();
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderedDictionary data;
            data = DepartmentController.GetCustomerDepartmentsDictionary(Convert.ToInt32(ddlCustomer.SelectedItem.Value));
            data.Insert(0, 0, Consts.ALL_DEPARTMENTS);
            fillDdlOrderDict(data, ddlDepartment);
            fillDdlOrderDict(getUsers(), ddlLoginID);
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillDdlOrderDict(getUsers(), ddlLoginID);
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            dgResults.DataSource = getData();//.Select(a => new { a.BoxID, a.BoxType,a.BoxDescription,a.CustomerBoxNum,a.CustomerID,a.DepartmentID,a.Exterminate,a.ExterminateDate,a.ExterminateYear,a.InsertDate,a.LablePrint,a.Location,a.LoginID,a.Mignaza });
            dgResults.AutoGenerateColumns = true;
            dgResults.DataBind();
            btnPrint.Visible = true;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
            List<Shippings> boxes = getData();
            Dictionary<string, string> param = new Dictionary<string, string>();
        
            param.Add(ExcelHeaders.ResourceManager.GetString("Customer"), ddlCustomer.SelectedItem != null ? ddlCustomer.SelectedItem.Text : user.CustomerID.ToString());
            param.Add(ExcelHeaders.ResourceManager.GetString("Department"), ddlDepartment.SelectedItem != null ? ddlDepartment.SelectedItem.Text : "");
            param.Add(ExcelHeaders.ResourceManager.GetString("FromShippingDate"), txtFromDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("ToShippingDate"), txtToDate.Value);
            param.Add(ExcelHeaders.ResourceManager.GetString("User"), string.IsNullOrEmpty(ddlLoginID.SelectedValue) || ddlLoginID.SelectedValue == "0" ? "" : ddlLoginID.SelectedValue.ToString());
            param.Add(ExcelHeaders.ResourceManager.GetString("NumOfBoxes"), boxes.Count.ToString());
                Shippings s = new Shippings();
            exportToExcell(boxes, "export_shipping_data", param, new string[] { nameof(s.LoginID), nameof(s.DepartmentID) });
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
            }
        }




        private List<Shippings> getData()
        {
            int customerID = user.ArchiveWorker == true ? Convert.ToInt16(ddlCustomer.SelectedValue) : user.CustomerID;
            int? department = string.IsNullOrEmpty(ddlDepartment.SelectedValue) || ddlDepartment.SelectedValue == "0" ? null : (int?)Convert.ToInt16(ddlDepartment.SelectedValue);
            DateTime? fromShippingDate = Helper.getDate(txtFromDate.Value);
            DateTime? toShgippingDate = Helper.getDate(txtToDate.Value);
            int? loginID = string.IsNullOrEmpty(ddlLoginID.SelectedValue) || ddlLoginID.SelectedValue == "0" ?null :(int?)Helper.getIntNotNull( ddlLoginID.SelectedValue.ToString());
            return ShippingController.exportShipping(customerID, department, fromShippingDate, toShgippingDate, loginID);

        }

        private OrderedDictionary getUsers()
        {
            OrderedDictionary data;
            data = UserController.getUsersByCustomer(user.ArchiveWorker==true?  Convert.ToInt32(ddlCustomer.SelectedValue):user.CustomerID);
            data.Insert(0, 0, Consts.ALL_USERS);
            return data;
        }

        protected void dgResults_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                foreach (TableCell item in e.Item.Cells)
                {
                    string header=ExcelHeaders.ResourceManager.GetString(item.Text);
                    item.Text = string.IsNullOrEmpty(header) ? item.Text : header;
                }
        }
    }
}