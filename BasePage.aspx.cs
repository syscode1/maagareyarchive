﻿using log4net;
using MA_DAL.MA_BL;
using MA_DAL.MA_DAL;
using MA_DAL.MA_HELPER;
using MaagareyArchive.resources.resources;
using NPOI.SS.Formula.Functions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MaagareyArchive
{
    public partial class BasePage : System.Web.UI.Page
    {
        private static readonly ILog logger = LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //internal Department department
        //{
        //    get
        //    {
        //        if (Session["userDepartment"] == null)
        //            Session["userDepartment"] = DepartmentController.GetDepartment(user.CustomerID, user.DepartmentID);
        //        return Session["userDepartment"] as Department;
        //    }
        //}
        public Users user
        {
            get
            {
                if (Session["UserEntity"] == null)
                    Response.Redirect("Login.aspx");
                return Session["UserEntity"] as Users;
            }
        }

        internal List<Department> allowedDepartments
        {
            get
            {
                if (Session["allowedDepartmentsEntity"] == null)
                    Session["allowedDepartmentsEntity"] = DepartmentController.GetUserDepartments(user);
                return Session["allowedDepartmentsEntity"] as List<Department>;
            }

        }
 
        private void Page_PreInit(object sender, System.EventArgs e)
        {
            if ((user == null))
            {
                Response.Redirect("login.aspx");
            }
            VerifyAccessToThisPage();
        }

        public static bool exportToExcell<T>(List<T> data,string fileName,Dictionary<string,string> searchParams=null,string[] sumBy=null)
        {
            HttpResponse Response = System.Web.HttpContext.Current.Response;
            ExcelPackage excel = new ExcelPackage();
            Dictionary<string,int> sumByDict= new Dictionary<string, int>();
            if(sumBy!=null && sumBy.Count()>0)
            {
                foreach (string item in sumBy)
                {
                    sumByDict.Add(item, 0);
                }
            }

            try
            {
               
                var GridView1 = new GridView();
                GridView1.DataSource = data;
                GridView1.DataBind();
                var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                var totalCols =data.Count>0? GridView1.Rows[0].Cells.Count:0;
                var totalRows = GridView1.Rows.Count;
                var headerRow = GridView1.HeaderRow;
                int numOfParam = searchParams == null ? 0 : searchParams.Count+1;
                
                for (int d = 1; d < numOfParam; d++)
                {
                    workSheet.Cells[d, 1].Value = searchParams.ElementAt(d-1).Key;
                    workSheet.Cells[d, 1].Style.Font.Bold = true;
                    workSheet.Cells[d, 2].Value = searchParams.ElementAt(d-1).Value;
                }
                workSheet.Cells["A:A"].AutoFitColumns();
                if (data.Count == 0)
                {
                    workSheet.Cells[numOfParam + 2, 1].Value = Consts.NO_DATA_TO_DISPLAY;
                }
                else
                {
                    numOfParam += 1;
                    for (var i = 1; i <= totalCols; i++)
                    {
                        string headerText = ExcelHeaders.ResourceManager.GetString(headerRow.Cells[i - 1].Text);
                        workSheet.Cells[numOfParam, i].Value =!string.IsNullOrEmpty(headerText)?headerText: headerRow.Cells[i - 1].Text;
                        workSheet.Cells[numOfParam, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Cells[numOfParam, i].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                        workSheet.Cells[numOfParam, i].Style.Font.Bold = true;
                    }
                    int rowIndex = numOfParam;
                    for (var j = 1; j <= totalRows; j++)
                    {
                        rowIndex++;
                        int currentRowIndex= rowIndex;

                        for (var i = 1; i <= totalCols; i++)
                        {
                            var item = data.ElementAt(j - 1);
                            PropertyInfo property = (item.GetType().GetProperty(headerRow.Cells[i - 1].Text));
                            if(sumBy!=null && sumBy.Contains(property.Name))
                            {
                                sumByDict[property.Name] += 1;
                                if (j == totalRows || property.GetValue(item, null).ToString() != property.GetValue(data.ElementAt(j), null).ToString())
                                {
                                    rowIndex++;
                                    workSheet.Cells[rowIndex, i].Value = Consts.SUM_ITEM.Replace("XXX",sumByDict[property.Name].ToString());
                                    sumByDict[property.Name] = 0;
                                }
                            }
                            workSheet.Cells[currentRowIndex, i].Value = property.GetValue(item, null);
                            if(property.PropertyType.ToString().Contains(typeof(DateTime).FullName))
                                workSheet.Cells[currentRowIndex, i].Style.Numberformat.Format = "yyyy-mm-dd HH:MM";
                        }
                    }
                }
                using (var memoryStream = new MemoryStream())
                {
                   
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                }

            }

            catch (Exception ex)
            {
                logger.Error(string.Format("method: {0} ; message: {1}", Helper.GetCurrentMethod(), ex.Message));
                return false;
            }
            finally
            {

                excel.Dispose();
                Response.End();
            }
            return true;
        }

        public void fillDdlOrderDict(OrderedDictionary data, DropDownList ddl)
        {
            
            ddl.DataSource = data;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            ddl.SelectedIndex = 0;


        }
        public void fillDdl(Dictionary<int,string> data, DropDownList ddl)
        {
            
            ddl.DataSource = data;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            ddl.SelectedIndex = 0;


        }

        protected virtual string[] AllowedPermissions { get { return null; } }
        protected virtual void VerifyAccessToThisPage()
        {
            MasterPage master = Master as MasterPage;
            if(master!=null)

            master.LimitToPermissions(user, AllowedPermissions);
            else
            {
                PopupMaster popupmaster = new PopupMaster();
                popupmaster.LimitToPermissions(user, AllowedPermissions);

            }
        }
    }
}